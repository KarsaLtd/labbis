function [foundPeaks, msg] = krs_identify_peaks( eventFile, peakList, varargin )
%KRS_IDENTIFY_PEAKS Try to identify found peaks for an event, from a given peaklist.
%   input:  filename
%           peaklist, cell or a .db
%           varargin, parameters for the identification
%   output: foundPeaks, cell with columns: composition, [peak id], [isotope ids]
%% Default parameters
args.minIsoAbu = 0.15; % Isotopes with greater abundance used for matching
args.mzTolPpm = 10; % m/z error tolerance (ppm)
args.isoRatioTol = 0.20; % Accepted relative error in isotope ratios
args.signalThreshold = 0.05; % Signal is thresholded when isotope ratios are compared
args.write = true;
%% User parameters
if nargin>2
    if iscell(varargin{:})
        varargin = varargin{:};
    end
    for i=1:2:length(varargin)
        field = varargin{i};
        if isvarname(field)
            args.(field) = varargin{i+1};
        end
    end
end
%%
minIsoAbu = args.minIsoAbu;
mzTolPpm = args.mzTolPpm;
isoRatioTol = args.isoRatioTol;
signalThreshold = args.signalThreshold;
write = args.write;
%% Input parameter check
if iscell(eventFile)
    eventFile = eventFile{:}; 
end

%% Load peak list <-------------- NOTE: If there is even one invalid peak it will not continue
% Peaklist given as .db file
if ~iscell(peakList) && exist(peakList,'file')~=0
    try
        pL = krs_tof_read_peakList(peakList);
        peakComps = {pL.molComp};
        peakMasses = [pL.mass];
    catch
        peakComps = {};
        peakMasses = [];
    end
% Peaklist given as a cell of compound names or masses
elseif iscell(peakList)
    peakComps = cell(length(peakList));
    peakMasses = zeros(length(peakList),1);
    for p=1:length(peakList)
        peak = peakList{p}; 
        peakComps(p) = krs_get_composition(peak);
        peakMasses(p) = krs_tof_exact_mass(krs_get_composition(peak));
        if isnan(peakMasses(p)) || peakMasses(p)==0
            disp('Error: Identification failed! Invalid peak list.')
            return
        end
    end
end

%% Preallocate output
pexist = nan(length(peakMasses),1);
pIds = cell(length(peakMasses),1);
isoIds = cell(length(peakMasses),1);
msg = cell(length(peakMasses),1);
%% Main loop
for p=1:length(peakMasses)
    pComp = peakComps(p);
    pMass = peakMasses(p);
    % Load peak data
    [fPeaks, fPeakData] = krs_get_peaks(eventFile,'mz',pMass,'mz_tol',mzTolPpm);
    % If peak was not found within set tolerance, move to next iteration
    if isempty(fPeaks)
        pexist(p) = 0;
        msg{p} = 'No peak found within set m/z tolerance.';
        continue
    % If multiple peaks were found, output their number and move toe next iteration
    elseif size(fPeaks,1)>1
        pexist(p) = 0;
        msg{p} = 'Found multiple peaks within set m/z tolerance.';
        continue
    end
    % Get the isotopes with minimum 'minIsoAbu' abundance
    pIsotopes = krs_tof_exact_isotope_masses(pComp, minIsoAbu);
    pIsotopes = sortrows(pIsotopes,1);
    if size(pIsotopes,1)==1
        % m/z was a match and no relevant isotopes exist
        pexist(p) = 1;
        msg{p} = 'Matching peak found, no isotopes.';
        pIdp = fPeaks(1,2);
        pIds{p} = pIdp;
        isoIds{p} = [];
        continue
    end
    isoMasses = pIsotopes(2:end,1);
    [isoPeaks, isoPeakData] = krs_get_peaks(eventFile,'mz',isoMasses,'mz_tol',mzTolPpm);
    % If not all isotope peaks were not found within set tolerance,
    % move to next iteration
    if size(isoPeaks,1)~=length(isoMasses)
        pexist(p) = 0;
        msg{p} = 'No isotope peak found within set m/z tolerance.';
        continue
    end
    %% Check isotope ratios
    % Main peak data
    mpHei = fPeakData{1}(:,4);
    pisoIds = zeros(length(isoPeakData),1);
    for i=1:length(isoPeakData)
        isoData = isoPeakData{i};
        isoHei = isoData(:,4);
        pisoIds(i) = isoPeaks(i,2);

        isoRs = corrcoef(mpHei, isoHei,'Rows','complete');
        if size(isoRs, 2) > 1
            isoR = isoRs(1,2);
        else
            isoR = isoRs;
        end
        if isoR < isoRatioTol
            pexist(p) = 0;
            msg{p} = 'Isotope signal correlation not within set tolerance.';
            break
        end
    end
    if isnan(pexist(p))
        % m/z was a match and isotope ratios matched so it is a detection
        pexist(p) = 1;
        msg{p} = 'Matching peak found.';
        pIdp = fPeaks(1,2);
        pIds{p} = pIdp;
        isoIds{p} = pisoIds;
    end
    
end

foundp = logical(pexist);
foundComps = peakComps(foundp);
foundPeaks = [foundComps pIds(foundp) isoIds(foundp)];

if write
    [~ ,Ids] = krs_tof_exist_in_file(eventFile,'/Peaks/Compositions');
    if Ids
        %old_compositions = h5read(eventFile,'/Peaks/Compositions')';
        krs_unlinkHDFDataset(eventFile,'/Peaks','Compositions');
        % Remove empty columns
        %while ~isempty(old_compositions) && all(old_compositions(:,end)==0)
        %    old_compositions = old_compositions(:,1:end-1);
        %end
        %longest = size(old_compositions,2);
    end
    if any(foundp)
        longest = 0;
        for p=1:size(foundComps,1)
            if size(foundComps{p},2)>longest
                longest = size(foundComps{p},2);
            end
        end
        
        allpeaks = krs_get_peaks(eventFile);
        compositions = zeros(max(allpeaks(:,2)),longest,'uint8');
        h5create(eventFile,'/Peaks/Compositions',size(compositions'),'Datatype','uint8');
        
        for p=1:size(foundPeaks,1)
            foundPeak = foundPeaks(p,:);
            pComp = uint8(foundPeak{1});
            pId = double(foundPeak{2});
            isoIds = double(foundPeak{3});
            compositions(pId,1:length(pComp)) = pComp;
            for i=1:length(isoIds)
                compositions(isoIds(i),1:length(pComp)) = pComp;
            end
        end
        h5write(eventFile,'/Peaks/Compositions',compositions');
        % Write parameters
        attr = {'m/z error tolerance (ppm)',mzTolPpm, ...
                    'isotopic ratio tolerance',isoRatioTol};
        krs_tof_write_attributes(eventFile,attr,'/Peaks/Compositions','dataset')
    end
end
%disp(eventFile)
%for p=1:length(peakList)
%    disp([peakList{p} msg{p}])
%end
end

