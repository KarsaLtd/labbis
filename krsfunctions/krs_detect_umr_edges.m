function [ timestamps, notes, signal ] = krs_detect_umr_edges( rawFiles, amu, slope )
%KRS_DETECT_UMR_EDGES Detect edges in unit mass signal to use as event triggers.
%   Detailed explanation goes here

timestamps = [];
notes = {};
signal = {};

if nargin==2
    slope = 'rising';
end

for f=1:length(rawFiles)
    % Get amu signal
    amudata = h5read(rawFiles{f},'/PeakData/PeakData',[amu,1,1,1],[1,Inf,Inf,Inf]);
    amudata = squeeze(amudata);
    amudata = reshape(amudata,[],1);
    amudata = amudata/max(amudata);
    signal{f} = amudata;
    % Find edges
    [dData,edges,~] = AnalyzeEdges(amudata,[1, 4, 16], [0.0, 0.1, 0.15]);
    % Get time
    time = h5read(rawFiles{f},'/TimingData/BufTimes');
    time = reshape(time,[],1);
    tsf = datenum(0,0,0,0,0,time(edges==1))+krs_tof_get_file_start_time(rawFiles{f});
    % Choose requested slopes
    pos = tsf( dData(logical(edges),end) >0 );
    neg = tsf( dData(logical(edges),end) <0 );
    if strcmp(slope,'rising')
        timestamps = [timestamps; pos];
    elseif strcmp(slope,'falling')
        timestamps = [timestamps; neg];
    end
end

notes = cell(length(timestamps), 1);
for i=1:length(timestamps)
    notes{i} = ['Event ' num2str(i)];
end

end

