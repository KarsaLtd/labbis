function [ R, msg ] = krs_check_updates()
%KRS_CHECK_UPDATES Summary of this function goes here
%   Detailed explanation goes here

msg = '';
R = 0;

% Read local revision number
fID = fopen('ver.h','r');
if fID == -1
    apppath = fileparts( fileparts( mfilename('fullpath' )) );
    fID = fopen(fullfile(apppath, 'ver.h'),'r');
end
if fID == -1
    msg = 'Checking local version number failed';
    return
end
local_r = fscanf(fID,'%u');
fclose(fID);
% Read remote revision number
try
    if isdeployed()
       remote_r = str2double( webread('https://bitbucket.org/KarsaLtd/labbisexe/raw/master/ver.h') );
    else
        remote_r = str2double( webread('https://bitbucket.org/KarsaLtd/labbis/raw/master/ver.h') );
    end
catch
    msg = 'Checking remote version number failed.';
    return
end
if remote_r > local_r
    R = remote_r;
    msg = 'Update available for download.';
else
    msg = 'Your version of KARSA Labbis is up to date.';
end

end