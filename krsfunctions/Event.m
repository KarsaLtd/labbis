classdef Event
    %EVENT Class holding the properties and methods relevant for the analysis of an event
    %   Each event has its own HDF file, and an Event instance refers to this file.
    
    properties
        Name
        FileName
        StartTime
        EndTime
        Length
        RawFiles
        
        DeconvolvedAmus
        FoundPeaks
        IdentifiedPeaks
        Rfun
        PeakShape
    end
    
    properties (Dependent)
        SumSpectrum
    end
    
    properties (Access = private)
        MzCalib
    end
    
    methods
        function obj = Event()
        end
        
        function obj = Create(obj, datapath, t_range, varargin)
            [obj.Name, obj.FileName, obj.RawFiles] = krs_make_event(datapath, t_range, varargin{:});
            obj.StartTime = t_range(1);
            obj.EndTime = t_range(2);
            % Calculate event length
            mask = h5read(obj.FileName, '/tofData/mask')';
            time_res = h5readatt(obj.FileName, '/tofData', 'Data time resolution [s]');
            obj.Length = sum(mask(:,2)) * time_res;
        end
        
        function obj = Load(obj, filename)
            obj.FileName = filename;
            [~,obj.Name,~] = fileparts(filename);
            try
                obj.RawFiles = h5readatt(filename,'/tofData','Data file');
            catch
            end
            obj.StartTime = h5readatt(filename,'/tofData','Start Time');
            obj.EndTime = h5readatt(filename,'/tofData','Stop Time');
            mask = h5read(obj.FileName, '/tofData/mask')';
            time_res = h5readatt(obj.FileName, '/tofData', 'Data time resolution [s]');
            obj.Length = sum(mask(:,2)) * time_res;
            obj.FoundPeaks = krs_get_peaks(filename);
            if ~isempty(obj.FoundPeaks)
                obj.DeconvolvedAmus = unique(round(obj.FoundPeaks(:,1)));
            else
                obj.DeconvolvedAmus = [];
            end
            [~ ,Ids] = krs_tof_exist_in_file(filename,'/Peaks/Compositions');
            if Ids
                compositions = h5read(filename,'/Peaks/Compositions')';
                species = cellstr(unique(deblank(char(compositions)),'rows'));
                species = species(~cellfun('isempty',species));
                pids = cell(length(species),1);
                isoids = cell(length(species),1);
                for p=1:size(compositions,1)
                    compp = deblank(char(compositions(p,:)));
                    if ~isempty(compp)
                        compind = find(strcmp(species,compp));
                        if isempty(pids{compind})
                            pids{compind} = p;
                        else
                            isoids{compind} = [isoids{compind}, p];
                        end
                    end
                end
                obj.IdentifiedPeaks = [species, pids, isoids];
            end
            try
                rfun = h5readatt(obj.FileName,'/Peaks/Rvalues','Rfnc');
                obj.Rfun = str2func(rfun{:});
            catch
                %disp(['Resolution function not defined in file ' obj.FileName])
            end
            try
                obj.PeakShape = h5read(obj.FileName,'/Peaks/PeakShape')';
            catch
                %disp(['Peak shape not defined in file ' obj.FileName])
            end
        end
        
        function obj = set.FoundPeaks(obj, found_peaks)
            obj.FoundPeaks = found_peaks;
        end
        
        function obj = set.IdentifiedPeaks(obj, id_peaks)
            obj.IdentifiedPeaks = id_peaks;
        end
        
        function SumSpectrum = get.SumSpectrum(obj)
            SumSpectrum = h5read(obj.FileName,'/tofData/SumSpectrum');
        end
        
        function obj = Deconvolve(obj, masses, limit, threshold, redoall, force, fit_all, labapp)
            if nargin<=4
                redoall = 0;
            end
            if nargin <=5 || ~force
                pL = [];
            elseif force
                pL = masses;
            end
            if nargin==8
                filenos = regexp(labapp.progbar.Message,'\d*','Match');
                filei = str2double(filenos{1});
                fileno = str2double(filenos{2});
            end
            if ~fit_all
                amus = unique(round(masses));
            else
                nbr_peaks = h5readatt(obj.RawFiles{1, :}, '/', 'NbrPeaks');
                amus = 20:(double(nbr_peaks)-1);
            end
            alrFittedPeaks = [];
            if ~redoall
                [~,Ids] = krs_tof_exist_in_file(obj.FileName,'/Peaks/PeakDataGauss');
                if Ids
                    alrFittedPeaks = h5read(obj.FileName, '/Peaks/PeakDataGauss')';
                    alrFittedAmus = unique(round(alrFittedPeaks(:,2)));
                    amus = setdiff(amus, alrFittedAmus);
                end
            end
            fittedPeaks = zeros(length(amus)*limit,8);
            cancel = false;
            for u=1:length(amus)
                init = [];
                limit = max(length(init), limit);
                uPeaks = krs_fit_u(obj.FileName, amus(u), 'limit', limit, ...
                                                          'threshold', threshold, ...
                                                          'init', init, ...
                                                          'peaklist', pL);
                if u==1
                    j=0;
                end
                for i=1:size(uPeaks,1)
                    fittedPeaks(u+i+j-1,:) = uPeaks(i,:);
                end
                j = j+i-1;
                progBar(labapp, 'value', ((filei-1)/fileno)+(u/length(amus)*(0.5/fileno)));
                if labapp.progbar.CancelRequested
                    cancel = true;
                    break
                end
            end
            if ~cancel
                % Remove zeros left from preallocation
                fittedPeaks = fittedPeaks(fittedPeaks(:,2)~=0,:);
                % Write and refit
                krs_write_and_refit_peaks(obj.FileName, fittedPeaks, alrFittedPeaks, labapp)
                obj.FoundPeaks = fittedPeaks;
                obj.DeconvolvedAmus = amus;
            end
            obj.IdentifiedPeaks = [];
        end
        
        function obj = IdentifyPeaks(obj, peak_list, varargin)
            % Identify and write to file
            [identified_peaks, msg] = krs_identify_peaks( obj.FileName, peak_list, varargin );
            % Store to object
            obj.IdentifiedPeaks = identified_peaks;
            if nargin>2
                % Print message to LabApp
                labapp = [];
                for i=1:2:length(varargin)
                    field = varargin{i};
                    if isvarname(field)
                        if strcmp(field, 'labapp')
                        	labapp = varargin{i+1};
                            break
                        end
                    end
                end
                if ~isempty(labapp)
                    for p=1:length(peak_list)
                        labapp.printMessage([ obj.Name ': ' peak_list{p} ': ' msg{p} ]);
                    end
                end
            end
        end
        
        function [ has, peakId, species ] = HasPeak(obj, peak, tol)
            has = false; peakId = []; species = {};
            if isempty(obj.FoundPeaks)
                return
            end
            while iscell(peak)
                peak = peak{:};
            end
            % Looking for composition
            if ischar(peak) || isstring(peak)
                species = peak;
                if ~isempty(obj.IdentifiedPeaks)
                    ind = find(strcmp(obj.IdentifiedPeaks(:,1), peak));
                    if ~isempty(ind)
                        has = true;
                        peakId = [obj.IdentifiedPeaks{ind,2}, obj.IdentifiedPeaks{ind,3}];
                    end
                end
            % Looking for mass    
            else
                if nargin==2
                    tol = 0.5e-4;
                end
                peakId = obj.FoundPeaks(abs(obj.FoundPeaks(:,1)-peak)<tol,2);
                if ~isempty(peakId)
                    has = true;
                    for i=1:size(obj.IdentifiedPeaks,1)
                        if obj.IdentifiedPeaks{i,2}==peakId
                            species = obj.IdentifiedPeaks{i,1};
                        elseif ~isempty(obj.IdentifiedPeaks{i,3}) && ...
                                                any(obj.IdentifiedPeaks{i,3}==peakId)
                            species = obj.IdentifiedPeaks{i,1};
                        end
                    end
                end
            end
        end
        
        function [um_data, time] = GetUM(obj, varargin)
            [um_data, time] = krs_get_um(obj.FileName, varargin);
        end
        
        function [peaks, peak_data, fitsum, mz] = GetPeaks(obj, varargin)
            switch nargout
                case 1
                    peaks = krs_get_peaks(obj.FileName, varargin);
                case 2
                    [peaks, peak_data] = krs_get_peaks(obj.FileName, varargin);
                case {3,4}
                    [peaks, peak_data] = krs_get_peaks(obj.FileName, varargin);
                    % Get mass axis
                    [pars,eq] = krs_tof_get_mz_calib_pars(obj.FileName);
                    funcArgs = eq();
                    sNo = krs_tof_get_dataset_size(obj.FileName,'/tofData/SumSpectrum');
                    sNo = sNo(2);
                    minsNo = Inf;
                    maxsNo = 0;
                    mz = krs_tof_mass_axis(sNo, pars, eq);
                    % Get peak shape
                    yi = obj.PeakShape(:,2)';
                    rxi = obj.PeakShape(:,1)';
                    yi = krs_H_scale(yi);
                    pp = spline(rxi,yi);
                    
                    fitsum = zeros(1,length(mz));
                    for p=1:length(peak_data)
                        pdata = peak_data{p};
                        %while any(isnan(pdata(end,:)))
                        %    pdata = pdata(1:end-1,:);
                        %end
                        ppos = pdata(1,2);
                        phei = pdata(:,4);
                        psig = krs_tof_fwhm_to_sig(ppos/obj.Rfun(ppos));
                        rxi2 = psig*rxi+ppos;
                        ind1 = floor(funcArgs.inverseEQ(rxi2(1),pars));
                        if ind1<minsNo
                            minsNo = ind1;
                        end
                        ind2 = ceil(funcArgs.inverseEQ(rxi2(end),pars));
                        if ind2 > length(mz)
                            ind2 = length(mz);
                        end
                        if ind2>maxsNo
                            maxsNo = ind2;
                        end
                        partmz = mz(ind1:ind2);
                        fit = ppval(pp, (partmz - ppos) / psig);
                        fit = phei*fit;
                        fitsum(ind1:ind2) = fitsum(ind1:ind2) + krs_tof_nanmean(fit,1);
                    end
                    if ~isinf(minsNo) && maxsNo~=0
                        fitsum = fitsum(minsNo:maxsNo);
                        mz = mz(minsNo:maxsNo);
                    end
            end
        end
        
        function [peaks, sum_data, peak_data] = GetIdentifiedPeaks(obj, varargin)% species, t_range)
            peaks = {}; sum_data = []; peak_data = [];
            % Interpret varargin
            args.species = {}; args.t_range = []; args.t_res = 0;
            if nargin>1
                for i=1:2:length(varargin)
                    field = varargin{i};
                    if isvarname(field)
                        args.(field) = varargin{i+1};
                    end
                end
            end
            species = {args.species}; t_range = args.t_range; t_res = args.t_res;
            if isempty(obj.IdentifiedPeaks)
                return
            else
                speciesInd = find(strcmp(obj.IdentifiedPeaks(:,1),species));
                if isempty(speciesInd)
                    return
                end
            end
            peakInd = [obj.IdentifiedPeaks{speciesInd,2}, obj.IdentifiedPeaks{speciesInd,3}];
            mz = obj.FoundPeaks(any(obj.FoundPeaks(:,2)==peakInd,2),1);
            args = {'mz',mz,'mz_tol',1e-5,'t_range',t_range,'t_res',t_res};
            switch nargout
                case 1
                    peaks = krs_get_peaks(obj.FileName, args);
                case {2, 3}
                    [peaks, peak_data] = krs_get_peaks(obj.FileName, args);
                    % Calculate sum of species peaks
                    if ~isempty(peaks)
                        sum_data = zeros(size(peak_data{1}));
                        for u=1:size(peaks,1)
                            p_data = peak_data{u};
                            sum_data(:,1) = p_data(:,1);
                            nanind = isnan(p_data(:,4));
                            p_data(nanind,3) = NaN;
                            sum_data(:,3) = sum_data(:,3)+p_data(:,3);
                            sum_data(:,4) = sum_data(:,4)+p_data(:,4);
                        end
                        sum_data(:,2) = zeros(size(sum_data,1),1);
                        sum_data = {sum_data};
                    end
            end
        end
        
        function [sumsignal, signal, mzaxis] = GetSignal(obj, varargin)
            % Interpret varargin
            args.mz = []; args.ttrim = [0 Inf]; args.loadsignal = 1;
            if nargin>1
                for i=1:2:length(varargin)
                    field = varargin{i};
                    if isvarname(field)
                        args.(field) = varargin{i+1};
                    end
                end
            end
            mz = args.mz; ttrim = args.ttrim;
            [pars, eq] = krs_tof_get_mz_calib_pars(obj.FileName);
            funcArgs = eq();
            if ~isempty(mz)
                amu = round(mz);
                s0 = round(double(funcArgs.inverseEQ(amu-0.5, pars)));
                s1 = round(double(funcArgs.inverseEQ(amu+0.5, pars)));
            else
                s0 = 0;
                s1 = Inf;
            end
            % Load sum spectrum
            if isempty(ttrim) || (ttrim(1)==0 && isinf(ttrim(2)))
                sumspec = obj.SumSpectrum * obj.Length;
                if s0==0 && isinf(s1)
                    sumsignal = sumspec;
                    if args.loadsignal
                        signal = krs_get_signal(obj.FileName);
                    else
                        signal = [];
                    end
                else
                    sumsignal = sumspec(s0:s1);
                    if args.loadsignal
                        signal = krs_get_signal(obj.FileName, ttrim, [s0, s1]);
                    else
                        signal = [];
                    end
                end
            % Load time range
            else
                signal = krs_get_signal(obj.FileName, ttrim, [s0, s1]);
                cc = krs_tof_get_convcoef(obj.FileName);
                t_range = [0, 0];
                t_range(1) = obj.StartTime+datenum(0,0,0,0,0,ttrim(1));
                if isinf(ttrim(2))
                    t_range(2) = obj.EndTime;
                else
                    t_range(2) = t_range(1)+datenum(0,0,0,0,0,ttrim(2));
                end
                len = (t_range(2)-t_range(1))*60*60*24;
                sumsignal = krs_tof_nanmean(signal,2)*cc(1)*len;
            end
            % Get mass axis
            if s0==0 && isinf(s1)
                mzaxis = funcArgs.usedEQ(1:length(sumsignal), pars);
            else
                mzaxis = funcArgs.usedEQ(s0:s1, pars);
            end
        end
    end
    
end

