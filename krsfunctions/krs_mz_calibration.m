function [ msg ] = krs_mz_calibration( eventFiles, peakList, param )
%KRS_MZ_CALIBRATION Function for doing mass calibration to averaged file
%
% A peaklist with calibration peak candidates needs to be given as input.
% The function tries to select single peaks suitable for mass calibration from the 
% given list, performs the calibration and evaluates the resulted calibration.
%
% Input:
%       eventFiles = {};                            % cell of files to calibrate
%       peakList = {};                              % candidates for calibration peaks (cell or .db)
%       param = struct( noPeaks,5, ...              % (optional) number of peaks to use
%                       eq,@tof_fit_tof_mz_3p, ...  % (optional) equation to use (eg. @tof_fit_tof_mz)
%                       verbose,0 );                % (optional) display diagnostics
%% Default parameter values
noPeaks = 0;
eq = @krs_tof_fit_tof_mz_3p;
verbose = 0;
%%
msg = '';

if ~iscell(eventFiles)
    eventFiles = {eventFiles};
end
filesDone = cell(length(eventFiles),1);

if nargin==3
    if isfield(param,'noPeaks')
        noPeaks = param.noPeaks;
    end
    if isfield(param,'eq')
        eq = param.eq;
    end
    if isfield(param,'verbose')
        verbose = param.verbose;
    end
end

%%
mzCalibPars = struct( ...
        'names',[], ...
        'masses',[], ...
        'equation',eq, ...
        'mxNrMasses',noPeaks, ...
        'timeRes',0, ... %[hour]
        'polarity',[], ...
        'doPlot',0, ...
        'mode','moving', ...
        'peakShape',[], ...
        'sparse',0, ...
        'useLoggedParameters',1);

for f=1:length(eventFiles)
    eventFile = eventFiles{f};
    calib_comp = peakList';
    calib_ms = krs_tof_exact_mass(calib_comp);
    if noPeaks==0 || noPeaks>length(calib_ms)
        noPeaks = length(calib_ms);            
    end
    mzCalibPars.mxNrMasses = noPeaks;
    mzCalibPars.names = calib_comp;
    mzCalibPars.masses = calib_ms;
    pol = deblank(h5readatt(eventFile, '/tofData', 'Polarity'));
    mzCalibPars.polarity = pol{:};
    try
        pSpath = h5readatt(eventFile,'/Peaks/PeakShape','Peak shape path');
        pSpath = deblank(pSpath{:});
        mzCalibPars.peakShape = pSpath;
    catch
        try
            pS = h5read(eventFile,'/Peaks/PeakShape');
            mzCalibPars.peakShape.dat = pS';
            mzCalibPars.peakShape.id = h5readatt(eventFile,'/Peaks/PeakShape','id');
            mzCalibPars.peakShape.par = [0,0];
        catch
            mzCalibPars.peakShape = [];
        end
    end
    krs_tof_mz_calib_mm(eventFile, mzCalibPars);
    
    % Evaluate calibration
    %[pars, eq] = krs_tof_get_mz_calib_pars(eventFile);
    %eq = eq();
    %pars = pars(:,1);
    usedMasses = h5read(eventFile,'/MassCalib/masses',[1 1],[Inf 1]);
    %usedSn = h5read(eventFile,'/MassCalib/sn',[1 1],[Inf 1]);
    %usedNames = h5readatt(eventFile,'/MassCalib/','names');
    ppms = zeros(length(usedMasses),1);
    for m=1:length(usedMasses)
        mass = double(usedMasses(m));
        u = round(mass);
        mz_fit = [];
        free_fit = [];
        % Force fit to exact mass
        mz_fit = krs_fit_u(eventFile,u,'peaklist',mass,'limit',1);
        mz_ppos = mz_fit(:,2);
        mz_phei = mz_fit(:,3);
        mz_psig = mz_fit(:,6);
        mz_fit = [mz_ppos, mz_phei, mz_psig];
        % Free fit the same peak
        free_fit = krs_fit_u(eventFile,u,'init',mass,'limit',1);
        if ~isempty(free_fit)
            ppos = free_fit(:,2);
            phei = free_fit(:,3);
            psig = free_fit(:,6);
            free_fit = [ppos, phei, psig];
        else
        end
        % Peak mass error
        ppms(m) = ((mz_ppos-ppos)/mz_ppos)*1e6;
    end
    ppm_mean = mean(abs(ppms));
    krs_tof_write_HDF(eventFile, '/MassCalib/ppms', ppms);
    %unlinkHDFDataset(eventFile, '/MassCalib/','ppm');
    krs_tof_write_HDF(eventFile, '/MassCalib/ppm', ppm_mean);
    filesDone{f} = {eventFile, ppm_mean};
end
end

