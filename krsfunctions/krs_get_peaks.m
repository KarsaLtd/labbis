function [ peaks, peak_data ] = krs_get_peaks( eventFile, varargin)
%KRS_GET_PEAKS Get peak data for peaks found within 'mz_tol' of 'mz' given as input.
%
%               peaks:      peak masses
%               peak_data:  cell containing a matrix with the columns
%                           time, mass, area, height and mass error in ppm


%% Default parameters
args.mz = [];       % Masses of peaks whose data to return
args.mz_tol = 0.5;  % Peaks within mz +/- mz_tol will be returned
args.t_range = [];  % Time window. If empty, whole event will be returned
args.t_res = 0;     % Time resolution. If zero, no averaging will be done

%% User parameters
if nargin>1
    if length(varargin)==1
        varargin = varargin{:};
    end
    for i=1:2:length(varargin)
        field = varargin{i};
        if isvarname(field)
            args.(field) = varargin{i+1};
        end
    end
end
mz = args.mz;
mz_tol = args.mz_tol;
t_range = args.t_range;
t_res = args.t_res;
if t_res~=0
    avTm = h5readatt(eventFile,'/tofData','Data time resolution [s]');
    stepsize = round(t_res/avTm);
end
%%
peaks = [];
peak_data = {};

[~ ,Ids] = krs_tof_exist_in_file(eventFile,'/Peaks/PeakDataGauss');
if ~Ids
    return
end

allpeakdata = h5read(eventFile,'/Peaks/PeakDataGauss')';
[allpeaks, Ia, ~] = unique(allpeakdata(:,2));
peakIds = double(allpeakdata(Ia,end));
allpeaks = [allpeaks, peakIds];

% If mz was not defined, return all peaks in the event
if isempty(mz)
    peaks = allpeaks;
    if nargout==2
        for p=1:length(peaks)
            p_data = krs_tof_get_HR_sticks_timeseries(eventFile, peaks(p), ...
                                            'accuracy', 1e-5);
            if ~isempty(t_range)
                p_data = p_data{:};
                p_data = p_data(p_data(:,1)>=t_range(1) & p_data(:,1)<t_range(2),:);
                p_data = {p_data};
            end
            if t_res~=0
                average_pdata;
            end
            peak_data{end+1} = p_data{:};
        end
    end
    return
end

for m=1:length(mz)
    amu = mz(m);
    % mz_tol was given in ppm
    if mz_tol>1
        mz_tol_amu = mz_tol*amu*1e-6; % Convert from ppm to Th
    else
        mz_tol_amu = mz_tol;
    end
    pInd = abs(allpeaks(:,1)-amu)<mz_tol_amu;
    peaksm = allpeaks(pInd,:);
    peaks = [peaks; peaksm];
    if nargout==2
        for p=1:size(peaksm,1)
            p_data = krs_tof_get_HR_sticks_timeseries(eventFile, peaksm(p), ...
                                            'accuracy', 1e-5);
            if ~isempty(t_range)
                p_data = p_data{:};
                p_data = p_data(p_data(:,1)>=t_range(1) & p_data(:,1)<t_range(2),:);
                p_data = {p_data};
            end
            if t_res~=0
                average_pdata;
            end
            if ~isempty(p_data{:})
                peak_data{end+1} = p_data{:};
            end
        end
    end
end
if ~isempty(t_range) && nargout==2
    for p=1:length(peak_data)
        p_data = peak_data{p};
        peak_data{p} = p_data(p_data(:,1)>=t_range(1) & p_data(:,1)<=t_range(2),:);
    end
end

    function average_pdata
        p_data = p_data{:};
        p_data_avr = zeros(ceil(size(p_data,1)/stepsize),size(p_data,2));
        j=1;
        for t=1:stepsize:size(p_data,1)
            endind = min(t+stepsize-1, size(p_data,1));
            p_data_avr(j,1) = krs_tof_nanmean(p_data(t:endind,1));
            p_data_avr(j,[2,5]) = p_data(t,[2,5]);
            p_data_avr(j,[3,4]) = krs_tof_nanmean(p_data(t:endind,[3,4]),1);
            j = j+1;
        end
        
        p_data = {p_data_avr};
    end

end

