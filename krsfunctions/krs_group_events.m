function event_group = krs_group_events(event_files, group_title)
%KRS_GROUP_EVENTS Combine multiple event files into one file

    event_group = [group_title '.h5'];
    
    domask = 1;
    domz = 1;
    for i=1:length(event_files)
        ef = event_files{i};
        [Ig, Ids] = krs_tof_exist_in_file(ef, '/tofData/mask');
        if ~Ids
            domask = 0;
        end
        [Ig, Ids] = krs_tof_exist_in_file(ef, '/MassCalib/masses');
        if ~Ids
            domz = 0;
        end
    end
    
    %/tofData
    SI = [];
    SumSpectrum = [];
    coadds = [];
    mask = [];
    signal = [];
    time = [];
    %/MassCalib
    a_log = [];
    b_log = [];
    masses = [];
    pars = [];
    ppm = [];
    ppms = [];
    ps_log = [];
    sn = [];
    
    for i=1:length(event_files)
        ef = event_files{i};
        
        SIi = h5read(ef, '/tofData/SI');
        SumSpectrumi = h5read(ef, '/tofData/SumSpectrum');
        coaddsi = h5read(ef, '/tofData/coadds');
        signali = h5read(ef, '/tofData/signal');
        timei = h5read(ef, '/tofData/time');
        
        SI = [SI, SIi];
        if i==1
            t0 = h5readatt(ef, '/tofData', 'Start Time');
            SumSpectrum = SumSpectrumi;
        else
            SumSpectrum = (sum(coadds)*SumSpectrum + sum(coaddsi)*SumSpectrumi) / (sum(coadds)+sum(coaddsi));
        end
        coadds = [coadds, coaddsi];
        signal = [signal, signali];
        time = [time, timei];
        
        if domask
            maski = h5read(ef, '/tofData/mask');
            mask = [mask, maski];
        end
        
        a_logi = h5read(ef, '/MassCalib/a_log');
        b_logi = h5read(ef, '/MassCalib/b_log');
        ps_logi = h5read(ef, '/MassCalib/ps_log');
        a_log = [a_log, a_logi];
        b_log = [b_log, b_logi];
        ps_log = [ps_log, ps_logi];
        
        if domz
            massesi = h5read(ef, '/MassCalib/masses');
            parsi = h5read(ef, '/MassCalib/pars');
            ppmi = h5read(ef, '/MassCalib/ppm');
            ppmsi = h5read(ef, '/MassCalib/ppms');
            sni = h5read(ef, '/MassCalib/sn');
            masses = [masses, massesi];
            pars = [pars, parsi];
            ppm = [ppm, ppmi];
            ppms = [ppms, ppmsi];
            sn = [sn, sni];
        end
        
    end
    t1 = h5readatt(ef, '/tofData', 'Stop Time');
    
    [d, f, e] = fileparts(ef);
    event_group = fullfile(d, 'groups', event_group);
    if ~isfolder(fullfile(d, 'groups'))
        mkdir(fullfile(d, 'groups'));
    end
    krs_tof_write_HDF(event_group, '/tofData/SI', SI');
    krs_tof_write_HDF(event_group, '/tofData/SumSpectrum', SumSpectrum');
    krs_tof_write_HDF(event_group, '/tofData/coadds', coadds');
    krs_tof_write_HDF(event_group, '/tofData/signal', signal');
    krs_tof_write_HDF(event_group, '/tofData/time', time');
    
    if domask
        krs_tof_write_HDF(event_group, '/tofData/mask', mask');
    end
    
    krs_tof_write_HDF(event_group, '/MassCalib/a_log', a_log');
    krs_tof_write_HDF(event_group, '/MassCalib/b_log', b_log');
    krs_tof_write_HDF(event_group, '/MassCalib/ps_log', ps_log');
    
    if domz
        krs_tof_write_HDF(event_group, '/MassCalib/masses', masses');
        krs_tof_write_HDF(event_group, '/MassCalib/pars', pars');
        krs_tof_write_HDF(event_group, '/MassCalib/ppm', ppm');
        krs_tof_write_HDF(event_group, '/MassCalib/ppms', ppm');
        krs_tof_write_HDF(event_group, '/MassCalib/sn', sn');
        % Read attributes
        eq = h5readatt(ef, '/MassCalib', 'equation');
        mzs = h5readatt(ef, '/MassCalib', 'masses');
        mode = h5readatt(ef, '/MassCalib', 'mode');
        ps = h5readatt(ef, '/MassCalib', 'peakShape');
        psid = h5readatt(ef, '/MassCalib', 'peakShapeId');
        pol = h5readatt(ef, '/MassCalib', 'polarity');
        % Write attributes
        mz_attr = {'equation', eq, 'masses', mzs, 'mode', mode, 'peakShape', ps, 'peakShapeId', psid, 'polarity', pol};
        krs_tof_write_attributes(event_group, mz_attr, '/MassCalib','group');
    end
    
    % Read attributes
    avg_step = h5readatt(ef, '/tofData', 'Averaging time step [min]');
    data_res = h5readatt(ef, '/tofData', 'Data time resolution [s]');
    pol = h5readatt(ef, '/tofData', 'Polarity');
    smpl_int = h5readatt(ef, '/tofData', 'Sampling Interval (s)');
    sig_unit = h5readatt(ef, '/tofData', 'Signal Unit');
    tof_freq = h5readatt(ef, '/tofData', 'Tof Frequency');
    proc = h5readatt(ef, '/tofData', 'UsedProcessingType');
    
    %eq = h5readatt(ef, '/MassCalib', 'equation');
    %polarity = h5readatt(ef, '/MassCalib', 'polarity');
    % Write attributes
    td_attr = {'Averaging time step [min]', avg_step, 'Data time resolution [s]', data_res, ...
               'Polarity', pol, 'Sampling Interval (s)', smpl_int, 'Signal Unit', sig_unit, ...
               'Start Time', t0, 'Stop Time', t1, 'Tof Frequency', tof_freq, 'UsedProcessingType', proc, ...
               'baselineCorrection', 0.0, 'diffSpec', 0.0, 'hasIMS', 0.0};
    krs_tof_write_attributes(event_group, td_attr, '/tofData','group');
    
    %mc_attr = {'equation', eq}
end

