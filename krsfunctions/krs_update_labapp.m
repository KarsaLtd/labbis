function [ success, msg ] = krs_update_labapp()
%KRS_UPDATE_LABAPP Summary of this function goes here
%   NOTE: Compiled Matlab apps do not support modifications in the file
%   system, so this function never gets called from Labbis if isdeployed()

success = 0;
msg = '';

new_labapp = 'labapp';

try
    if isdeployed()
        websave(new_labapp, 'https://bitbucket.org/KarsaLtd/labbisexe/get/HEAD.zip');
        first_file = 1;
        [status, result] = system('set PATH');
        apppath = char(regexpi(result, 'Path=(.*?);', 'tokens', 'once'));
        cd(apppath)
    else
        websave(new_labapp, 'https://bitbucket.org/KarsaLtd/labbis/get/HEAD.zip');
        first_file = 3;
        apppath = fileparts( fileparts( mfilename('fullpath' )) );
        cd(apppath)
    end
catch
    msg = 'Download failed.';
    return
end

% Make a backup of current version
%mkdir backup

try
    % Install new version
    new_labapp_zip = [new_labapp '.zip'];
    new_files = unzip(new_labapp_zip);
    % new_files{1} = repo folder
    install_dir = apppath;
    for i=first_file:length(new_files)
        if contains(new_files{i}, 'parameters')
            % Do not update parameter files
            continue
        end
        [status, message] = movefile(new_files{i}, install_dir);
    end
    
    % For backwards compatibility
    param_dir = fullfile(install_dir, 'parameters');
    if ~isdeployed() && ~isfolder(param_dir)
        mkdir(param_dir)
        % Move config files to parameters directory
        try
            movefile(fullfile(install_dir, '*.cfg'), param_dir);
            % Move peakshape files to parameters directory
            psdir = fullfile(install_dir, 'peakShapes');
            if isfolder(psdir)
               movefile(psdir, param_dir);
            end
            % Move peaklist files to parameters directory
            pldir = fullfile(install_dir, 'peakLists');
            if isfolder(pldir)
                movefile(pldir, param_dir);
            end
            docdir = fullfile(install_dir, 'doc');
            if isfolder(docdir)
                rmdir(docdir, 's');
            end
        catch
            disp('Backwards compatibility attempt failed.')
            for i=first_file:length(new_files)
                if contains(new_files{i}, 'parameters')
                	% Copy parameter files from installation package
                    [status, message] = movefile(new_files{i}, install_dir);
                end
            end
        end
    end
    % End backwards compatibility
    
    delete(new_labapp_zip);
    rmdir(new_files{1}, 's');
    
    addpath(genpath(install_dir))
    savepath
    
    success = 1;
    msg = 'KARSA Labbis succesfully updated.';
catch ME
    % Revert to backup
    msg = ME.message;
end

end

