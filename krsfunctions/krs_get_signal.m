function [ signal ] = krs_get_signal( eventFile, ttrim, mztrim, forceRaw )
%KRS_GET_SIGNAL Utility function to load signal (mV/ext) from raw data file in the
%               case where it is not copied to the event file.
%
%   Optional input: ttrim           Two element array, first element is the
%                                   offset from beginning of the event (s);
%                                   second element is the desired length (s)
%                   mztrim          Two element array with sample numbers
%                                   to include
%                   forceRaw        Return raw data even if event contains
%                                   averaged data
if nargin==1
    ttrim = [0 Inf];
    mztrim = [0 Inf];
end
if isempty(ttrim)
    ttrim = [0 Inf];
end
if isempty(mztrim)
    mztrim = [0 Inf];
end
if nargin<4
    forceRaw = 0;
end
% Data time resolution
datRes = h5readatt(eventFile,'/tofData','Data time resolution [s]');
avTm = h5readatt(eventFile,'/tofData','Averaging time step [min]')*60;
if avTm==0
    avTm = datRes;
end
% Check if data is contained within the file or load from raw file
[~ ,Ids] = krs_tof_exist_in_file(eventFile,'/tofData/signal');

if forceRaw && avTm~=0
    Ids = 0;
end

if Ids
    ttrim = round(ttrim/avTm);
    dim = krs_tof_get_dataset_size(eventFile,'/tofData/signal');
    if sum(ttrim)>=dim(1) || isnan(ttrim(2))
        ttrim(2) = dim(1)-ttrim(1)-1;
    end
    dat = h5read(eventFile,'/tofData/signal',[1+mztrim(1), 1+ttrim(1)], ...
                                                [mztrim(2)-mztrim(1)+1, ttrim(2)+1]);
else
    % Load from raw file, handle windowing and data dimensions
    % Get raw data file path
    rawFileNames = deblank(h5readatt(eventFile,'/tofData','Data file'));
    rawFileName = rawFileNames{1};
    % Timing
    nbrBlocks = h5readatt(rawFileName,'/','NbrBlocks');
    nbrMemories = h5readatt(rawFileName,'/','NbrMemories');
    nbrWaveforms = h5readatt(rawFileName,'/','NbrWaveforms');
    extractions = double(nbrBlocks)*double(nbrMemories)*double(nbrWaveforms);
    % Start and stop time of the event
    t0 = h5readatt(eventFile,'/tofData','Start Time');
    t0 = t0+datenum(0,0,0,0,0,ttrim(1));
    t1e = h5readatt(eventFile,'/tofData','Stop Time');
    t1 = t0+datenum(0,0,0,0,0,ttrim(2));
    if isnan(t1) || t1>t1e
        t1 = t1e;
    end
    % Number of data points
    writes = round((t1-t0)*(24*60*60)/datRes);
    % Raw data file start and stop time
    ft = krs_tof_get_time(rawFileName);
    ft0 = ft(1);
    % Event start time offset from raw data file start time
    offset = round((t0-ft0)*24*60*60/datRes);
    % Get data dimensions and figure out a suitable slab to load
    datdim = krs_tof_get_dataset_size(rawFileName,'/FullSpectra/TofData');
    row_offset = floor(offset/datdim(2));
    no_rows = ceil((writes+offset-row_offset*datdim(2))/datdim(2));
    no_extra_rows = 0;
    if no_rows+row_offset > datdim(1)
        no_extra_rows = (no_rows+row_offset)-datdim(1);
        no_rows = no_rows-no_extra_rows;
    end
    dat = h5read(rawFileName,'/FullSpectra/TofData',[1+mztrim(1) 1 1 row_offset+1],...
                                            [mztrim(2)-mztrim(1)+1 Inf Inf no_rows]);
    % Reorganize data dimensions
    dat = squeeze(dat);
    dat = reshape(dat,size(dat,1),1,[]);
    dat = squeeze(dat);
    if no_extra_rows>0
        ft1 = ft(end);
        while isnan(ft1)
            ft = ft(1:end-1);
            ft1 = ft(end);
        end
        f2t = krs_tof_get_time(rawFileNames{2});
        f2t0 = f2t(1);
        missingpoints = round((f2t0-ft1)*60*60*24/avTm);
        writes = writes-missingpoints;
        dat2 = h5read(rawFileNames{2},'/FullSpectra/TofData',[1+mztrim(1) 1 1 1],...
                                            [mztrim(2)-mztrim(1)+1 Inf Inf no_extra_rows]);
        dat2 = squeeze(dat2);
        dat2 = reshape(dat2,size(dat2,1),1,[]);
        dat2 = squeeze(dat2);
        dat = [dat, dat2];
    end
    
    % Remove extra data points if any
    ind0 = (1+(offset-row_offset*datdim(2)));
    ind1 = ind0+writes-1;
    dat = dat(:,ind0:ind1);
    % Average per extraction
    dat = dat/extractions;
end
signal = dat;
end

