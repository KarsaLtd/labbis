function [peakInd] = krs_peakFind(spec,peakMin)
% Searches peaks from processed TOF spectras
% spec is a matrix of column vectors, one per spectra
% peakMin is the minimum signal intensity that gets analyzed
debug = 0;
    
spec2 = spec;        % Temp spec
spec2(spec < peakMin) = nan;    % Remove noise / small signal
spec2 = diff(spec2);            % Derivative
ind = 1;                        % Initiate first peak
peakInd = [];

if debug == 1
    figure(5)
    clf
    plot(spec,'b-')
    hold on
    plot(spec2,'r--')
    plot(spec*0+peakMin,'k--')

end

for ip = 1:size(spec2)-1
    if spec2(ip) > 0 && spec2(ip+1) < 0 % Find derivative zeros
        peakInd(end+1) = ip + 1; % Store peak location

        if debug == 1
            plot(ip+1,0,'ro')
            xlim([-100 100]+ip+1)
        end
        ind = ind + 1;                  % Next peak
    end
end
        
end

