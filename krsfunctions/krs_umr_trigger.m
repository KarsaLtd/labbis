function [ timestamps, notes, signal, times ] = krs_umr_trigger( rawFiles, src, amu, thresh, stepsize, hyst, sign )
%KRS_UMR_TRIGGER Summary of this function goes here
%   Detailed explanation goes here
timestamps = [];
notes = {};
signal = cell(length(rawFiles), 1);
times = cell(length(rawFiles), 1);


sigmax = 0;
for f=1:length(rawFiles)
    switch src
        case 'signal'
            % Get amu signal
            um_data = h5read(rawFiles{f},'/PeakData/PeakData',[amu,1,1,1],[1,Inf,Inf,Inf]);
            um_data = squeeze(um_data);
            um_data = reshape(um_data,[],1);
        case 'tps'
            um_data = h5read(rawFiles{f}, '/TPS2/TwData', [amu, 1, 1], [1, Inf, Inf]);
            um_data = squeeze(um_data);
            um_data = reshape(um_data,[],1);
            um_data = squeeze(um_data);
    end
    sigmax = max(sigmax, max(um_data));
    % Get time
    time = h5read(rawFiles{f},'/TimingData/BufTimes');
    time = reshape(time,[],1);
    i = length(time);
    while time(i)<1e-5
        i = i-1;
    end
    time = time(1:i);
    um_data = um_data(1:i);
    % Average if requested
    if stepsize ~= 0
        average_umdata
    end
    times{f} = datenum(0,0,0,0,0,time) + krs_tof_get_file_start_time(rawFiles{f});
    signal{f} = um_data;
end

% Do thresholding with hysteresis
for f=1:length(rawFiles)
    signal{f} = signal{f}/sigmax;
    ge = signal{f} >= (thresh+hyst);
    le = signal{f} <= (thresh-hyst);
    known_val = ge | le;
    known_ind = find(known_val);
    acc = cumsum(known_val);
    % Find zeros in the beginning
    i = 0;
    while acc(i+1)==0
        i = i+1;
        if i==length(acc)
            break
        end
    end
    switch sign
        case 'le'
            ind = [ ones(i, 1); le(known_ind(acc(i+1:end))) ];
        case 'ge'
            ind = [ ones(i, 1); ge(known_ind(acc(i+1:end))) ];
        case 'rng'
            ind = ~known_val;
            acc = 1;
    end
    if ~acc(1)
        ind(1:i) = 0;
    end
    
    gate = 0;
    t0 = 0;
    t1 = 0;
    for i=1:length(ind)
        if ind(i)
            if gate
                % Move end point
                t1 = times{f}(i);
            else
                % Begin event
                gate = 1;
                t0 = times{f}(i);
                t1 = times{f}(i);
            end
        else
            if gate
                % Add event timestamp
                gate = 0;
                if t1 > t0
                    timestamps = [ timestamps; [t0, t1] ];
                end
            end
        end
    end
    if gate
        % Add event timestamp
        gate = 0;
        if t1 > t0
            timestamps = [ timestamps; [t0, t1] ];
        end
    end
end

notes = cell(length(timestamps), 1);
for i=1:length(timestamps)
    notes{i} = ['Event ' num2str(i)];
end

    function average_umdata
        time_avr = zeros(ceil(length(time)/stepsize), 1);
        um_data_avr = zeros(ceil(size(um_data,1)/stepsize), 1);
        j=1;
        for t=1:stepsize:size(um_data,1)
            endind = min(t+stepsize-1, size(um_data,1));
            time_avr(j) = krs_tof_nanmean(time(t:endind));
            um_data_avr(j) = krs_tof_nanmean(um_data(t:endind));
            j = j+1;
        end
        time = time_avr;
        um_data = um_data_avr;
    end

end

