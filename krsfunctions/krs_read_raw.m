function [ eventObjs ] = krs_read_raw( dataPath, timestamps, param, labapp )
%KRS_READ_RAW Function to read raw data file and make event files for further processing.
%
% 
%
%   input: dataPath: Path to raw data
%          timestamps: Array of event timestamps. Either a column vector of start 
%                      times or a two-column array with end times in the second
%                      column. If none are given, use start times of found
%                      data files.
%          param: Struct with (optional) fields
%               'save_path'   (default: 'dataPath\processed\')
%               'dataPrType'  (default: ADC)
%               't_offset'    (default: 0)
%               't_after'     (default: until the start time of next event)
%               'average'     (default: 0)
%               'copyData'    (default: 1)
%               'merge'       (default: 1)
%               'event_title' (default: 'event')
%               'eventNo'     (default: 1)
%   output: filenames of the created h5 files, message
%
%% Check input parameters
% Check windowing parameters
t_offset = 0;
t_after = 0;
savePath = fullfile(dataPath,'processed');
dataPrType = 'ADC';
average = 0;
copyData = 1;
merge = 1;
eventNo = 1;
event_title = '';

if nargin>=3 && ~isempty(param)
    if isfield(param,'t_offset') && ~isempty(param.t_offset)
        t_offset = param.t_offset;
    end
    if isfield(param,'t_after') && ~isempty(param.t_after)
        t_after = param.t_after;
    end
    if isfield(param,'event_title') && ~isempty(param.event_title)
        event_title = param.event_title;
    end
    if isfield(param,'eventNo') && ~isempty(param.eventNo)
        eventNo = param.eventNo;
    end
    if isfield(param,'average') && ~isempty(param.average)
        average = param.average;
    end
    if isfield(param,'copyData') && ~isempty(param.copyData)
        copyData = param.copyData;
    end
    if isfield(param,'merge') && ~isempty(param.merge)
        merge = param.merge;
    end
    if isfield(param,'dataPrType') && ...
            (strcmp(param.dataPrType,'ADC') || strcmp(param.dataPrType,'TDC'))
        dataPrType = param.dataPrType;
    end
    if isfield(param,'save_path') && ~isempty(param.save_path)
        savePath = param.save_path;
    end
end

% No timestamps were given, use start and end times of raw data files
if nargin==1
    [ ~, timestamps ] = krs_scan_raw_data( dataPath );
% Timestamps were given
else
    % End times were not given
    if size(timestamps,2)==1
        % Windows were not defined, use file start and end times
        if t_offset==0 && t_after==0
            % Add end times of events (until next trigger)
            timestamps = [timestamps, [timestamps(2:end); 0]];
            % From last trigger until the end of file in question
            [ ~, filetimes ] = krs_scan_raw_data( dataPath );
            tend = max(filetimes(:,2));
            timestamps(end,2) = tend;
        % Use given windows
        else
            timestamps = timestamps + datenum(0,0,0,0,0,t_offset);
            timestamps = [timestamps, timestamps+datenum(0,0,0,0,0,t_after)];
        end
    end
end

% Set tofTools path
krs_tof_set_path(dataPath, savePath);

%% Make event files
noEvents = size(timestamps,1);
eventObjs = cell(ceil(noEvents/merge),1);
args = {'save_path', savePath, 'dataPrType', dataPrType, 'average', average, ...
        'copyData', copyData, 'event_title', event_title, 'eventNo', eventNo, ...
        'noEvents', noEvents, 'labapp', labapp};
[~, sInd] = sort(timestamps(:,1));
timestamps = timestamps(sInd,:);

e = 1;
for i=1:merge:noEvents
    % Update progress bar
    progBar(labapp, 'message', ['Creating event ' num2str(e) '/' num2str(length(eventObjs))]);
    if labapp.progbar.CancelRequested
        eventObjs = eventObjs(~cellfun('isempty', eventObjs));
        break
    end
    % Set time window
    i0 = i;
    i1 = min(i+merge-1, noEvents);
    t_range = [timestamps(i0:i1,1), timestamps(i0:i1,2)];
    eventObj = Event();
    eventObj = eventObj.Create( dataPath, t_range, args{:} );
    eventObjs{e} = eventObj;
    args{end-4} = args{end-4}+1;
    e = e+1;
end

end