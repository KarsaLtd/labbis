function krs_cp_mz_calib( from_fNam, to_fNam )
%KRS_CP_MZ_CALIB Copy mass calibration parameters from one spectrum to another
%

%% Read mass calibration parameters from calibrated file
try
    masses = h5read(from_fNam,'/MassCalib/masses')';
    pars = h5read(from_fNam,'/MassCalib/pars')';
    ppm = h5read(from_fNam,'/MassCalib/ppm')';
    sn = h5read(from_fNam,'/MassCalib/sn')';
catch
    disp('Error: Mass calibration copying failed! Invalid source file.')
    return
end

%% Read attributes from calibrated file
try
    doPlot = h5readatt(from_fNam,'/MassCalib','doPlot');
    equation = h5readatt(from_fNam,'/MassCalib','equation');
    massesAtt = h5readatt(from_fNam,'/MassCalib','masses');
    mode = h5readatt(from_fNam,'/MassCalib','mode');
    mxNrMasses = h5readatt(from_fNam,'/MassCalib','mxNrMasses');
    names = h5readatt(from_fNam,'/MassCalib','names');
    peakShape = h5readatt(from_fNam,'/MassCalib','peakShape');
    peakShapeId = h5readatt(from_fNam,'/MassCalib','peakShapeId');
    polarity = h5readatt(from_fNam,'/MassCalib','polarity');
    sparse = h5readatt(from_fNam,'/MassCalib','sparse');
    timeRes = h5readatt(from_fNam,'/MassCalib','timeRes');
    useLoggedParameters = h5readatt(from_fNam,'/MassCalib','useLoggedParameters');
catch
    disp('Error: Mass calibration copying failed! Invalid source file.')
    return
end
    
%% Remove previous calibration from file to be calibrated
% Get logged calibration parameters
as_log = hdf5read(to_fNam, '/MassCalib/a_log')';
bs_log = hdf5read(to_fNam, '/MassCalib/b_log')';
ps_log = hdf5read(to_fNam, '/MassCalib/ps_log')';
% Remove previous calibration results from 'MassCalib' group in the file
krs_unlinkHDFDataset(to_fNam, 'MassCalib', [], true);
% Write back logged parameters
krs_tof_write_HDF(to_fNam,'/MassCalib/a_log',as_log,'chunk',size(as_log));
krs_tof_write_HDF(to_fNam,'/MassCalib/b_log',bs_log,'chunk',size(bs_log));
krs_tof_write_HDF(to_fNam,'/MassCalib/ps_log',ps_log,'chunk',size(ps_log));

%% Make sure the parameters to be copied are of correct size even if copying from sum file to time series
len = length(h5read(to_fNam, '/tofData/time'));
masses = repmat(masses(1,:), len, 1);
pars = repmat(pars(1,:), len, 1);

ppm = repmat(ppm(1,:), len, 1);
sn = repmat(sn(1,:), len, 1);

%% Write the parameters
krs_tof_write_HDF(to_fNam,'/MassCalib/masses',masses,'memType','H5T_NATIVE_FLOAT','chunk',size(masses));
krs_tof_write_HDF(to_fNam,'/MassCalib/pars',pars,'memType','H5T_NATIVE_FLOAT','chunk',size(pars));
krs_tof_write_HDF(to_fNam,'/MassCalib/ppm',ppm,'memType','H5T_NATIVE_FLOAT','chunk',size(ppm));
krs_tof_write_HDF(to_fNam,'/MassCalib/sn',sn,'memType','H5T_NATIVE_FLOAT','chunk',size(sn));

%% Write the attriutes
krs_tof_write_attributes(to_fNam,{'doPlot',doPlot},'/MassCalib','group')
krs_tof_write_attributes(to_fNam,{'equation',equation},'/MassCalib','group')
krs_tof_write_attributes(to_fNam,{'masses',massesAtt},'/MassCalib','group')
krs_tof_write_attributes(to_fNam,{'mode',mode},'/MassCalib','group')
krs_tof_write_attributes(to_fNam,{'mxNrMasses',mxNrMasses},'/MassCalib','group')
krs_tof_write_attributes(to_fNam,{'names',names},'/MassCalib','group')
krs_tof_write_attributes(to_fNam,{'peakShape',peakShape},'/MassCalib','group')
krs_tof_write_attributes(to_fNam,{'peakShapeId',peakShapeId},'/MassCalib','group')
krs_tof_write_attributes(to_fNam,{'polarity',polarity},'/MassCalib','group')
krs_tof_write_attributes(to_fNam,{'sparse',sparse},'/MassCalib','group')
krs_tof_write_attributes(to_fNam,{'timeRes',timeRes},'/MassCalib','group')
krs_tof_write_attributes(to_fNam,{'useLoggedParameters',useLoggedParameters},'/MassCalib','group')

%%
% Evaluate calibration
usedMasses = h5read(to_fNam,'/MassCalib/masses',[1 1],[Inf 1]);
ppms = zeros(length(usedMasses),1);
for m=1:length(usedMasses)
    mass = double(usedMasses(m));
    u = round(mass);
    mz_fit = [];
    free_fit = [];
    % Force fit to exact mass
    mz_fit = krs_fit_u(to_fNam,u,'peaklist',mass,'limit',1);
    mz_ppos = mz_fit(:,2);
    mz_phei = mz_fit(:,3);
    mz_psig = mz_fit(:,6);
    mz_fit = [mz_ppos, mz_phei, mz_psig];
    % Free fit the same peak
    free_fit = krs_fit_u(to_fNam,u,'limit',1);
    if ~isempty(free_fit)
        ppos = free_fit(:,2);
        phei = free_fit(:,3);
        psig = free_fit(:,6);
        free_fit = [ppos, phei, psig];
    else
    end
    % Peak mass error
    ppms(m) = ((mz_ppos-ppos)/mz_ppos)*1e6;
end
ppm_mean = mean(abs(ppms));
krs_tof_write_HDF(to_fNam, '/MassCalib/ppms', ppms);
unlinkHDFDataset(to_fNam, '/MassCalib/','ppm');
krs_tof_write_HDF(to_fNam, '/MassCalib/ppm', ppm_mean);
end

