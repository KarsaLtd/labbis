function [ filenames, times ] = krs_scan_raw_data( data_path, scan_subfolders )
%KRS_SCAN_RAW_DATA Scan raw data folder to find available data
%   Assumes raw file names are of the form 'Data_*s.h5'. 
%   Outputs file names and start and end times of raw data files (datenum)

if nargin==1
    scan_subfolders = 0;
end

fls = dir(fullfile(data_path,'*_*h*m*s.h5'));
filenames = cell(length(fls),1);
times = [];

if scan_subfolders
    subf = dir(data_path);
    subf = subf([subf.isdir]);
    subf = subf(3:end); % Remove . and ..
    for i=1:length(subf)
        sfls = dir(fullfile(data_path, subf(i).name, '*_*h*m*s.h5'));
        fls = [fls; sfls];
    end
end

if isempty(fls)
    return
end
t0s = zeros(length(fls),1);
t1s = zeros(length(fls),1);

for f=1:length(fls)
    fl = fls(f);
    fNam = fullfile(fl.folder, fl.name);
    filenames{f} = fNam(length(data_path):end);
    flInfo = krs_tof_get_file_info(fl);
    t0s(f) = flInfo.startTime;
    t1s(f) = flInfo.stopTime;
end

times = [t0s t1s];

end

