function [ um_data, time ] = krs_get_um( eventFile, varargin )
%KRS_GET_UM Function to retrieve UM stick data for an event
%   input:  filename of the event
%           varargin: mz        % Masses of peaks whose data to return
%                     t_range   % Time window. If empty, whole event will be returned
%                     t_res     % Time resolution. If zero, no averaging will be done
%!!!! NOTE: Does not properly handle events extending over two raw files yet !!!!

%% Default parameters
args.mz = [];       % Masses of peaks whose data to return
args.t_range = [];  % Time window. If empty, whole event will be returned
args.t_res = 0;     % Time resolution. If zero, no averaging will be done

%% User parameters
if nargin>1
    if length(varargin)==1
        varargin = varargin{:};
    end
    for i=1:2:length(varargin)
        field = varargin{i};
        if isvarname(field)
            args.(field) = varargin{i+1};
        end
    end
end
mz = args.mz;
t_range = args.t_range;
t_res = args.t_res;

% Raw data files
rawFileNames = deblank(h5readatt(eventFile,'/tofData','Data file'));
rawFileName = rawFileNames{1};
%
time = krs_tof_get_time(rawFileName);
while isnan(time(end))
    time = time(1:end-1);
end
% Start and stop time of the event
t0 = h5readatt(eventFile,'/tofData','Start Time');
t1 = h5readatt(eventFile,'/tofData','Stop Time');
time = time(time>=t0 & time<t1);
%time = h5read(eventFile,'/tofData/time');
%%
um_data = [];
[~ ,Ids] = krs_tof_exist_in_file(eventFile,'/Sticks/signal');
if false %Ids
    allum = h5read(eventFile,'/Sticks/amus');
    allumdata = h5read(eventFile,'/Sticks/signal');
else
    allumdata = krs_get_um_signal(eventFile);
    allum = 1:size(allumdata,1);
end

if ~isempty(mz)
    um_data = allumdata(round(allum)==mz,:);
else
    um_data = allumdata;
end

while length(time)>size(um_data,2) % <---------- Stupid fix to solve tofTools bug
    time = time(1:end-1);
end
while size(um_data,2)>length(time)
    um_data = um_data(:,1:end-1);
end

if ~isempty(t_range)
    timeInd = time>=t_range(1) & time<t_range(2);
    time = time(timeInd);
    um_data = um_data(:,timeInd);
end

avTm = h5readatt(eventFile,'/tofData','Averaging time step [min]')*60;
if t_res==0
    t_res = avTm;
end
if t_res~=0
    datRes = h5readatt(eventFile,'/tofData','Data time resolution [s]');
    stepsize = min( round(t_res/datRes),  size(um_data,2));
    average_umdata
end

    function average_umdata
        time_avr = zeros(1, ceil(length(time)/stepsize));
        um_data_avr = zeros(size(um_data,1),ceil(size(um_data,2)/stepsize));
        j=1;
        for t=1:stepsize:size(um_data,2)
            endind = min(t+stepsize-1, size(um_data,2));
            time_avr(j) = krs_tof_nanmean(time(t:endind));
            um_data_avr(:,j) = krs_tof_nanmean(um_data(:,t:endind),2);
            j = j+1;
        end
        time = time_avr;
        um_data = um_data_avr;
    end

end

