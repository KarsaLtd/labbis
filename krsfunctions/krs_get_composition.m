function mol_comp = krs_get_composition( compound, names_file )
%KRS_GET_COMPOSITION Find elemental composition of compounds given by name.
%   List of compound names given in 'names.txt' located in application root directory.

if ~iscell(compound)
    compound = {compound};
end
if nargin < 2
    names_file = '';
end

mol_comp = cell(size(compound));
for i=1:length(compound)
    if isnumeric(compound{i})
        mol_comp{i} = num2str(compound{i});
    elseif isfile(names_file)
        fileID = fopen(names_file);
        if fileID ~= -1
            comps = textscan(fileID,'%s %s');
            fclose(fileID);
            for c=1:length(comps{1})
                if contains(compound{i}, comps{1}{c})
                    mol_comp{i} = strrep(compound{i}, comps{1}{c}, comps{2}{c});
                    break
                end
            end
        end
        if isempty(mol_comp{i})
            mol_comp{i} = compound{i};
        end
    else
        mol_comp{i} = compound{i};
    end
end

end
