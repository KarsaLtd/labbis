function compound = krs_name_compound( mol_comp, names_file )
%KRS_NAME_COMPOUND Find a name for compound given as elemental composition.
%   List of compound names given in 'names.txt' located in application root directory.

if ~iscell(mol_comp)
    mol_comp = {mol_comp};
end
compound = cell(size(mol_comp));
if nargin < 2
    names_file = '';
end

for i=1:length(mol_comp)
    if isnumeric(mol_comp{i})
        compound{i} = num2str(mol_comp{i});
    elseif isfile(names_file)
        fileID = fopen(names_file);
        if fileID ~= -1
            comps = textscan(fileID,'%s %s');
            fclose(fileID);
            for c=1:length(comps{2})
                if contains(mol_comp{i}, comps{2}{c})
                    compound{i} = strrep(mol_comp{i}, comps{2}{c}, comps{1}{c});
                    break
                end
            end
        end
        if isempty(compound{i})
            compound{i} = mol_comp{i};
        end
    else
        compound{i} = mol_comp{i};
    end
end

end
