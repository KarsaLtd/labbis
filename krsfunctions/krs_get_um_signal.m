function allumdata  = krs_get_um_signal( eventFile )
%KRS_GET_UM_SIGNAL Function to read stick data from raw file for the time period of 
%                  a given event
%   input:  filename of the event file
%   output: UM stick data [ions/s]


avTm = h5readatt(eventFile,'/tofData','Data time resolution [s]');

% Raw data files
rawFileNames = deblank(h5readatt(eventFile,'/tofData','Data file'));
rawFileName = rawFileNames{1};

% Start and stop time of the event
t0 = h5readatt(eventFile,'/tofData','Start Time');
t1 = h5readatt(eventFile,'/tofData','Stop Time');
% Number of data points
writes = round((t1-t0)*(24*60*60)/avTm);
% Raw data file start and stop time
ft = krs_tof_get_time(rawFileName);
i = 0;
while isnan(ft(length(ft)-i))
    i = i+1;
end
ft0 = ft(1);
ft1 = ft(length(ft)-i);
% Event start time offset from raw data file start time
offset = round((t0-ft0)*24*60*60/avTm);
% Get data dimensions and figure out a suitable slab to load
datdim = krs_tof_get_dataset_size(rawFileName,'/FullSpectra/TofData');
row_offset = floor(offset/datdim(2));
no_rows = ceil((writes+offset-row_offset*datdim(2))/datdim(2));
no_extra_rows = 0;
if no_rows+row_offset > datdim(1) && length(rawFileNames)>1
    f2t = krs_tof_get_time(rawFileNames{2});
    f2t0 = f2t(1);
    missingpoints = round((f2t0-ft1)*60*60*24/avTm);
    writes = writes-missingpoints;
    no_extra_rows = (no_rows+row_offset)-datdim(1);
    no_rows = no_rows-no_extra_rows;
else
    no_rows = datdim(1)-row_offset;
end

rawumdata = h5read(rawFileName,'/PeakData/PeakData',[1 1 1 row_offset+1],...
                                                            [Inf Inf Inf no_rows]);
% Reorganize data dimensions
rawumdata = squeeze(rawumdata);
rawumdata = reshape(rawumdata,size(rawumdata,1),1,[]);
rawumdata = squeeze(rawumdata);
% If event overlaps two raw data files
if no_extra_rows>0
    rawumdata2 = h5read(rawFileNames{2},'/PeakData/PeakData',[1 1 1 1],...
                                                        [Inf Inf Inf no_extra_rows]);
    rawumdata2 = squeeze(rawumdata2);
    rawumdata2 = reshape(rawumdata2,size(rawumdata2,1),1,[]);
    rawumdata2 = squeeze(rawumdata2);
    rawumdata = [rawumdata, rawumdata2];
end
% Remove extra data points if any
ind0 = (1+(offset-row_offset*datdim(2)));
ind1 = ind0+writes-1;
ind1 = min(ind1, size(rawumdata, 2));
rawumdata = rawumdata(:,ind0:ind1); % ions/ext
% Unit conversion
tp = h5readatt(rawFileName,'/TimingData','TofPeriod');
rawumdata = rawumdata*(1e9/double(tp)); % ions/s

allumdata = rawumdata;

end

