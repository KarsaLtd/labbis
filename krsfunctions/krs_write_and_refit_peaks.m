function krs_write_and_refit_peaks( eventFile, peaks, oldpeaks, labapp )
%KRS_WRITE_AND_REFIT_PEAKS Fit found peaks to the time resolution of the
%event data and write to file. Calls 'krs_tof_refit_peaks_mm'.
%   input:  filename of the event
%           peaks as given by krs_fit_u
%           oldpeaks, already fitted peaks not to be refitted

if nargin==2
    oldpeaks = [];
end
% Check if /Peaks/PeakData already exists and delete if it does
file = H5F.open(eventFile,'H5F_ACC_RDWR','H5P_DEFAULT');
try
    group = H5G.open(file,'/Peaks');
    H5G.unlink(group,'PeakData');
    H5G.unlink(group,'PeakDataGauss');
    H5G.close(group);
catch
    try
        H5G.close(group);
    end
end
H5F.close(file);

% Expand the peaklist in time dimension
if ~isempty(peaks)
    time = h5read(eventFile, '/tofData/time');
    specNo = length(time);
    peakNo = size(peaks,1);
    peaks = repmat(peaks,specNo,1);
    for i=1:specNo
        idx0 = (i-1)*peakNo+1;
        idx1 = idx0+peakNo-1;
        peaks(idx0:idx1,1) = i;
    end
end
if ~isempty(oldpeaks)
    peaks = [peaks; oldpeaks];
    [~,sInd] = sort(peaks(:,1));
    peaks = peaks(sInd,:);
end
% Refit peaks
if ~isempty(peaks)
    krs_tof_write_HDF(eventFile,'/Peaks/PeakData',peaks,'memType','H5T_NATIVE_FLOAT','keepopen',false);
    
    [~, Ids] = krs_tof_exist_in_file(eventFile, '/tofData/signal');
    if Ids
        % Refit
        peakShape = h5readatt(eventFile,'/Peaks/PeakShape','Peak shape path');
        fit_par.peakShape = deblank(peakShape{:});
        Rfnc = h5readatt(eventFile,'/Peaks/Rvalues','Rfnc');
        fit_par.R = str2func(Rfnc{:});
        fit_par.peakList = [];
        krs_tof_refit_peaks_mm(eventFile, fit_par, labapp);
    end
end
krs_unlinkHDFDataset(eventFile, '/Peaks/', 'Compositions');
end

