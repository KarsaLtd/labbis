function [timestamps, notes] = krs_read_ext_log( logfile )
%KRS_READ_LOG Read acquisition log from external file. Parse the log such
%that th
%   Input: log: cell containing path to the log file
%   Output: timestamps: column vector of triggers or two-column vector with start and
%                       end times of events(datenum)
%           notes: (optional) cell of strings with information about the events
timestamps = [];
notes = {};


% !!! Write here the code to parse your log file and set the outputs !!!


%fID = fopen(logfile{:}); % Open log file

%log = fscanf(fID, %s); % help fscanf

%fclose(fID); % Close log file


% !!! Your code ends here !!!


% If notes were not set, fill them up with 'Event n'
if isempty(notes)
    notes = cell(length(timestamps), 1);
    for i=1:length(timestamps)
        notes{i} = ['Event ' num2str(i)];
    end
end

end
