function [ ] = krs_write_instrument_functions( eventFiles, pSpath, Rfun )
%KRS_WRITE_INSTRUMENT_FUNCTIONS Write the peakshape and resolution function
%                               to event file
%%
if ~iscell(eventFiles)
    eventFiles = {eventFiles};
end
for f=1:length(eventFiles)
    eventFile = eventFiles{f};
    % Peak shape
    if ~isempty(pSpath)
        load(pSpath)
        krs_tof_ps_write(eventFile, peakShape)
        krs_tof_write_attributes(eventFile,{'Peak shape path', pSpath},'/Peaks/PeakShape','dataset')
    end
    % Resolution function
    if ~isempty(Rfun)
        RfunStr = func2str(Rfun);
        krs_tof_write_HDF(eventFile,'/Peaks/Rvalues',[1 NaN],'chunk',[1 2]);
        krs_tof_write_attributes(eventFile,{'Rfnc', RfunStr},'/Peaks/Rvalues','dataset')
    end
end
end

