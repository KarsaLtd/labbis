function [ msg ] = krs_copy_signal_raw_to_event( eventFile )
%KRS_COPY_SIGNAL_RAW_TO_EVENT Utility function to copy the signal from raw file to event file.
%   This function is used to copy the signal to an event file, for example
%   to examine it in tofTools GUI, if it was not done at the moment of
%   creation of the event.

msg = [];
[~, Ids] = krs_tof_exist_in_file(eventFile, '/tofData/signal');
if Ids
    msg = 'Error: ''/tofData/signal'' dataset already exists';
else
    signal = krs_get_signal(eventFile)';
    dim_sig = size(signal, 1);
    dim_tim = krs_tof_get_dataset_size(eventFile, '/tofData/time');
    if dim_sig~=dim_tim(1)
        msg = 'Error: Size mismatch of signal and ''/tofData/time'' dataset';
        return
    end
    krs_tof_write_HDF(eventFile,'/tofData/signal',signal);
    msg = 'Signal copied successfully.';
end

end

