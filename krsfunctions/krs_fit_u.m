function [ peakData, msg ] = krs_fit_u( fNam, u, varargin )
%KRS_FIT_U Fit peaks(s) to single unit mass window
%   Detailed explanation goes here

peakData = [];

% Set default parameter values
args.range = [u u];

args.init = [];
args.peaklist = [];
args.threshold = 0.9;
args.limit = 10;
args.tolerance = 0.25;
args.write = false;
args.verbose = false;
args.spec = 0;

if nargin>2
    for i=1:2:(nargin-2)
        field = lower(varargin{i});
        if isvarname(field)
            args.(field) = varargin{i+1};
        end
    end
end

if ~isfield(args,'peakshape')
    % Try to load peak shape from file
    [~, PSexist] = krs_tof_exist_in_file(fNam, '/Peaks/PeakShape');
    if PSexist
        peakshape.dat = double(hdf5read(fNam, '/Peaks/PeakShape'))';
        peakshape.id = krs_tof_ps_generate_id(peakshape.dat(:, 2));
        args.peakshape = peakshape;
    else
        msg = {'Error: Peak fitting failed! Peak shape not found from file.'};
        return
    end
else
    % Use gaussian peak shape
    rxi = (-30:0.1:30)';
    args.peakshape.dat = [rxi krs_tof_normpdf(rxi,0,1)];
    args.peakshape.id = '0';
end

if ~isfield(args,'rfun')
    % Try to load resolution function from file
    try
        RfunStr = h5readatt(fNam,'/Peaks/Rvalues','Rfnc');
        args.rfun = str2func(RfunStr{:});
    catch
        msg = {'Error: Peak fitting failed! Resolution not defined.'};
        return
    end
end

if args.spec==0
    % Get sum spectrum
    spec = h5read(fNam,'/tofData/SumSpectrum')';
else
    
    spec = h5read(fNam,'/tofData/signal',[1, args.spec(1)], ...
                                            [Inf, args.spec(end)-args.spec(1)+1]);
    spec = mean(spec,2)';
end

% Get mass axis
nSample = length(spec);
[pars, eq] = krs_tof_get_mz_calib_pars(fNam);
funcArgs = eq();
mz = krs_tof_mass_axis(nSample, pars(:,1), eq);

% Get unit mass window spectrum
fInd = floor(funcArgs.inverseEQ(u-0.5, pars(:,1)));
lInd = ceil(funcArgs.inverseEQ(u+0.5, pars(:, 1)));
lInd = min(lInd, nSample);
pInd = fInd:lInd;
partMz = mz(pInd);
partSpec = spec(pInd);

% Fit
pData = krs_tof_fit_n_peaks(partMz, partSpec, args);
if ~isempty(pData)
    peakData = pData;
else
    msg = {'Error: Peak fitting failed! Dunno why though.'};
end

end

