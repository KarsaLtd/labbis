function [ event_name, event_file, raw_files ] = krs_make_event( datapath, t_range, varargin )
%KRS_MAKE_EVENT Function to create an event out of raw data.
%
% 
%   input: datapath:   Path to raw data
%          t_range:    Array of event timestamps. Either a start 
%                      time or a two-element vector with end time
%
%   Optional parameters:
%               'save_path'   (default: 'dataPath\processed\')
%               'dataPrType'  (default: ADC)
%               'average'     (default: 0)
%               'copyData'    (default: 0)
%               'event_title' (default: 'event')
%               'eventNo'     (default: 1)
%   output: filenames of the created h5 file, message
%
%% Default parameters
args.save_path = fullfile(datapath,'\processed\');
args.dataPrType = 'ADC';
args.average = 0;
args.copyData = false;
args.event_title = '';
args.eventNo = 1;
args.progbar = [];

%% User parameters
if nargin>2
    for i=1:2:(nargin-2)
        field = varargin{i};
        if isvarname(field)
            args.(field) = varargin{i+1};
        end
    end
end
if args.average~=0
    args.copyData = true;
end
%% Make event files
avr_par = struct('data_timeRes', args.average/60, ...
    'noise_timeRes', 0, ...
    'baselineCorrection', 0, ...
    'diff', 0, ...
    'mask', [], ...
    'mask_time', [], ...
    'data_processing_type', args.dataPrType, ...
    'eventNo', args.eventNo, ...
    'noEvents', args.noEvents, ...
    'labapp', args.labapp);

fileId = sprintf('%03d', args.eventNo);

if isempty(args.event_title)
    % Fetch event title from acquisition log
    log = krs_tof_read_log([t_range(1,1), t_range(end,2)+datenum(0,0,0,0,10,0)]);
    for i=1:size(log, 1)
        if ( ~strcmp(log{i, 3}, 'Acquisition started') && ...
             ~contains(log{i, 3}, 'Acquisition aborted') && ...
             ~strcmp(log{i, 3}, 'End of acquisition') )
            et = log{i, 3};
            % Remove illegal characters
            et = strrep(et, ' ', '_'); et = strrep(et, '\', '_');
            et = strrep(et, '/', '_'); et = strrep(et, '.', ',');
            et = strrep(et, ':', '_'); et = strrep(et, '*', '_');
            et = strrep(et, '"', '_'); et = strrep(et, '?', '_');
            et = strrep(et, '<', '_'); et = strrep(et, '>', '_');
            et = strrep(et, '|', '_');
            args.event_title = et;
            break
        end
    end
    if isempty(args.event_title)
        % No log note found
        args.event_title = 'untitled_event';
    end
end

event_name = [num2str(fileId) '_' args.event_title];
saveTo = fullfile(args.save_path, [event_name '.h5']);

% Generate mask
[m, mt] = krs_tof_generate_mask(t_range);
avr_par.mask = m;
avr_par.mask_time = mt;

% Make the file
if args.copyData
    [savedFile, fls] = krs_tof_load_time_range2HDF([t_range(1,1), t_range(end,2)], avr_par, saveTo);
    event_file = savedFile;
    % Reduce dimensions of m/z calibration parameters etc.
    %{
    as_log = h5read(event_file, '/MassCalib/a_log');
    bs_log = h5read(event_file, '/MassCalib/b_log');
    ps_log = h5read(event_file, '/MassCalib/ps_log');
    krs_unlinkHDFDataset(event_file,'/MassCalib',{'a_log','b_log','ps_log'});
    krs_tof_write_HDF(event_file,'/MassCalib/a_log',as_log(1));
    krs_tof_write_HDF(event_file,'/MassCalib/b_log',bs_log(1));
    krs_tof_write_HDF(event_file,'/MassCalib/ps_log',ps_log(1));
    %}
else
    % Use 'tof_load_time_range2HDF' to get the sum spectrum and later
    % move it to different dataset
    event_length = etime(datevec(t_range(2)),datevec(t_range(1)))/60; %min
    avr_par.data_timeRes = event_length;
    [savedFile, fls] = krs_tof_load_time_range2HDF(t_range, avr_par, saveTo);
    event_file = savedFile;
end

% Calculate and write sum spectrum over the set time window
sumSpec = krs_tof_get_average_spectrum(event_file, [t_range(1,1), t_range(end,2)]); % ions/s
krs_tof_write_HDF(event_file,'/tofData/SumSpectrum',sumSpec');
attr = {'Signal unit', 'ions/sec'};
krs_tof_write_attributes(event_file,attr,'/tofData/SumSpectrum','dataset');

% Calculate data time resolution and write it as an attribute
raw_files = cell(length(fls),1);
for f=1:length(fls)
    raw_files{f} = fullfile(fls(f).folder,fls(f).name); % <----------------NOTE: Assumes no parameter changes between raw files
end
tp = h5readatt(raw_files{1},'/TimingData','TofPeriod');
nbrwf = h5readatt(raw_files{1},'/','NbrWaveforms');
res = double(tp)*double(nbrwf)*1e-9; %sec
attr = {'Data time resolution [s]',res, 'Data file',raw_files};
krs_tof_write_attributes(event_file,attr,'/tofData','group');

% Get polarity and write it to file since 'tof_load_timerange2HDF' gets it wrong
ionMode = h5readatt(raw_files{1},'/','IonMode');
krs_tof_write_attributes(event_file,{'Polarity',ionMode},'/tofData','group');

% Clean up
if ~args.copyData
    si = h5read(event_file,'/tofData/SI');
    krs_unlinkHDFDataset(event_file,'/tofData',{'coadds','signal','time','SI'});
    attr = {'Averaging time step [min]', args.average/60};
    krs_tof_write_attributes(event_file,attr,'/tofData','group');
    % Get and write time vector
    [tim,~,~,~] = krs_tof_get_time(raw_files{1});
    if length(raw_files)>1
        [tim2,~,~,~] = krs_tof_get_time(raw_files{2});
        tim = [tim; tim2];
    end
    [~,t0ind] = min(abs(tim-t_range(1)));
    [~,t1ind] = min(abs(tim-t_range(2)));
    tim = tim(t0ind:t1ind-1);
    krs_tof_write_HDF(event_file,'tofData/time',tim);
    % Increase dimensions to be compatible with tofTools GUI
    coadds = repmat(double(nbrwf),length(tim),1);
    krs_tof_write_HDF(event_file,'/tofData/coadds',coadds);
    SI = repmat(si,length(tim),1);
    krs_tof_write_HDF(event_file,'/tofData/SI',SI);
    as_log = h5read(event_file, '/MassCalib/a_log');
    as_log = repmat(as_log,length(tim),1);
    bs_log = h5read(event_file, '/MassCalib/b_log');
    bs_log = repmat(bs_log,length(tim),1);
    ps_log = h5read(event_file, '/MassCalib/ps_log');
    ps_log = repmat(ps_log,length(tim),1);
    krs_unlinkHDFDataset(event_file,'/MassCalib',{'a_log','b_log','ps_log'});
    krs_tof_write_HDF(event_file, '/MassCalib/a_log', as_log);
    krs_tof_write_HDF(event_file, '/MassCalib/b_log', bs_log);
    krs_tof_write_HDF(event_file, '/MassCalib/ps_log', ps_log);
end

end

