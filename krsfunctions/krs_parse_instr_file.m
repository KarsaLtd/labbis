function paramout = krs_parse_instr_file( filepath )
%KRS_PARSE_INSTR_FILE Function to parse configuration file. Returns set of
%parameters.

paramout = struct;

fileID = fopen(filepath);
C = textscan(fileID,'%s %s');
fclose(fileID);

for p=1:length(C{1})
   par = C{1}(p);
   val = C{2}(p);
   paramout.(par{:}) = val{:};
end
end

