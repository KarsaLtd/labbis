function [ indices ] = krs_find_index( time_vector, time_points, tol )
%KRS_FIND_INDEX A utility function to find indices of given time points
%(within tolerance) in a time vector.
%
%   Input: time_vector: The time vector being indexed (serial date number)
%          time_points: Moments in time who's indices are to be returned
%          tol: Tolerance in seconds. Default value 1 second.
%   Output: Array of indices, NaN if index within tolerance was not found

% Check input arguments
    if nargin < 2
        error('Too few input arguments')
    elseif nargin == 2
        % Tolerance not given, set to 1 second
        tol = 1;
    elseif nargin > 3
        error('Too many input arguments')
    end
    
    indices = [];
    for i=1:length(time_points)
        t_diff = abs( time_vector - time_points(i) );
        t_closest = find( t_diff == min(t_diff) );
        % Check whether the found index is within one second to the logged time
        if abs( time_vector(t_closest) - time_points(i) ) > tol/(24*60*60)
            indices(i) = NaN;
        else
            indices(i) = t_closest;
        end
    end

end

