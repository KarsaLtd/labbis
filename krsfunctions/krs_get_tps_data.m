function [TPSdata, TPSinfo, TPStime] = krs_get_tps_data( eventFile, v_ind )
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

TPSinfo = [];
TPSdata = [];
TPStime = [];

% Raw data files
rawFileNames = deblank(h5readatt(eventFile,'/tofData','Data file'));
rawFileName = rawFileNames{1};
%
time = krs_tof_get_time(rawFileName);
while isnan(time(end))
    time = time(1:end-1);
end
% Start and stop time of the event
t0 = h5readatt(eventFile,'/tofData','Start Time');
t1 = h5readatt(eventFile,'/tofData','Stop Time');
tind = time>=t0 & time<t1;
TPStime = time(tind);

%%
[~ ,Ids] = krs_tof_exist_in_file(rawFileName,'/TPS2/TwData');
if Ids
    data = h5read(rawFileName, '/TPS2/TwData', [v_ind, 1, 1], [1, Inf, Inf]);
    data = squeeze(data);
    data = reshape(data,[],1);
    data = squeeze(data);
    TPSdata = data(tind);
    info = deblank( h5read(rawFileName, '/TPS2/TwInfo', [v_ind], [1]) );
    TPSinfo = info{:};
else
    return
end

end

