function [ timestamps, notes ] = krs_scan_events( data_path, selFiles, mode, extLog )
%KRS_SCAN_EVENTS Scan event triggers from three possible sources: 1) TofDaq
%acquisition log, 2) Separate text file, 3) Autosampler dataset in datafile
%   External log file will be parsed by 'krs_read_ext_log'. Modify to suit your
%   needs.

fNams = fullfile(data_path, selFiles);
timestamps = cell(length(fNams), 1);
notes = cell(length(fNams), 1);

switch mode
    case 'internal'
        for f=1:length(fNams)
            fNam = fNams{f};
            log = krs_tof_read_log(fNam);
            tsf = zeros(size(log, 1)-2, 1);
            ntf = cell(size(log, 1)-2, 1);
            for i=1:size(log, 1)-2
                try
                    tsf(i) = datenum(log{i+1, 2}, 'mm/dd/yyyy HH:MM:SS AM');
                catch
                    tsf(i) = datenum(log{i+1, 2}, 'yyyy-mm-ddTHH:MM:SS');
                end
            ntf(i) = log(i+1, 3);
            end
            timestamps{f} = tsf;
            notes{f} = ntf;
        end
        timestamps = cell2mat(timestamps);
        notes = vertcat( notes{~cellfun('isempty', (notes))} );
    case 'external'
        [timestamps, notes] = krs_read_ext_log(extLog);
        if isempty(notes)
            notes = cell(length(timestamps), 1);
            for i=1:length(timestamps)
                notes{i} = cellstr(['Trigger ' num2str(i)]);
            end
        end
    case 'autosampler'
        timestamps = [];
        for f=1:length(fNams)
            fNam = fNams{f};
            % Use the internal log in data file
            [Ig, Ids]= krs_tof_exist_in_file(fNam, '/HT3000A_G/TwData');
            if Ig && Ids
                trig = h5read(fNam,'/HT3000A_G/TwData');
                trig = trig(1,:,:);
                trig = squeeze(trig);
                trig = reshape(trig,[],1);
                trig = find(trig==1);
                for i=length(trig):-1:2
                    if trig(i)-trig(i-1)<4 % <------- NOTE: hard-coded blind time for debouncing
                        trig(i) = 0;
                    end
                end
                trig = trig(trig~=0);
                % Extract the time vector from raw data file
                file_time = krs_tof_get_time(fNam);
                while isnan(file_time(end))
                    file_time = file_time(1:end-1);
                end
                timestamps = [timestamps; file_time(trig)];
            end
        end
        notes = cell(length(timestamps), 1);
        for i=1:length(timestamps)
            notes{i} = ['Injection ' num2str(i)];
        end
    case 'mion'
        for f=1:length(fNams)
            fNam = fNams{f};
            [Ig, Ids]= krs_tof_exist_in_file(fNam, '/TPS2/TwInfo');
            if Ig && Ids
                % Find MION mode ID index
                TwInfo = h5read(fNam,'/TPS2/TwInfo');
                mionind = 0;
                for i=1:length(TwInfo)
                    iinfo = deblank(TwInfo{i});
                    if strcmp(iinfo, 'MION Active mode ID target []')
                        mionind = i;
                        break
                    elseif i == length(TwInfo)
                        break
                    end
                end
            end
            if mionind ~= 0
                TwData = h5read(fNam, '/TPS2/TwData', [mionind, 1, 1], [1, Inf, Inf]);
                TwData = squeeze(TwData);
                TwData = reshape(TwData,[],1);
                mionmode = squeeze(TwData);
            else
                continue
            end
            % Extract the time vector from raw data file
            file_time = krs_tof_get_time(fNam);
            skip = 0;
            while isnan(file_time(end))
                file_time = file_time(1:end-1);
                skip = skip+1;
            end
            tsf = [];
            indf = [];
            for t=1:length(mionmode)-skip
                if t==1 || (mionmode(t) ~= modeprev)
                    tsf = [tsf; file_time(t)];
                    indf = [indf; t];
                end
                modeprev = mionmode(t);
            end
            notesf = cell(length(tsf), 1);
            for i=1:length(tsf)
               switch round(mionmode(indf(i)))
                    case 0
                        note = 'Ambient';
                    case 1
                        note = 'Ion Source 1';
                    case 2
                        note = 'Ion Source 2';
                    case 3
                        note = 'IS1+Amb';
                    case 4
                        note = 'IS2+Amb';
                    case 5
                        note = 'Zero';
                    otherwise
                        note = 'Idle';
                end
                notesf{i} = note; 
            end
        timestamps{f} = tsf;
        notes{f} = notesf;
        end
        timestamps = cell2mat(timestamps);
        notes = vertcat( notes{~cellfun('isempty', (notes))} );
        keep = ones(length(notes), 1);
        for i=2:length(notes)
            if strcmp(notes{i-1}, notes{i}) && (timestamps(i)-timestamps(i-1))<datenum(0,0,0,1,0,0)
                keep(i) = 0;
            end
        end
        keep = logical(keep);
        timestamps = timestamps(keep);
        notes = notes(keep);
end

end

