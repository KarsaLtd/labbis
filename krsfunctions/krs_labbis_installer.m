function krs_labbis_installer()
%KRS_LABBIS_INSTALLER Install KARSA Labbis

disp('----- Welcome to KARSA Labbis installer! -----')

% Check Matlab version
if verLessThan('matlab','9.4')
    disp('KARSA Labbis is only compatible with Matlab version 9.4 (2018a) or higher. Please update your Matlab before installing.')
    return
end

% Read Labbis remote revision number
try
    remote_r = str2double( webread('https://bitbucket.org/KarsaLtd/labbis/raw/master/ver.h') );
catch
    disp('Installation failed. Could not connect to remote repository.')
    return
end

disp('Please select installation destination.')

install_root = uigetdir('C:\', 'Select destination folder');
if install_root==0
    disp('Installation aborted.')
    return
end
install_sub1 = fullfile(install_root, 'Karsa');
if ~isfolder(install_sub1)
    mkdir(install_sub1);
end
install_sub2 = fullfile(install_sub1, 'labbis');
if isfolder(install_sub2)
    disp('Installation aborted. Found an existing installation of Labbis.')
    return
else
    mkdir(install_sub2);
end

install_path = install_sub2;

disp(['Installing to: ' install_path])

cd(install_path)

new_labapp = ['labapp' num2str(remote_r)];
try
    disp('Downloading installation package from remote server...')
    websave(new_labapp, 'https://bitbucket.org/KarsaLtd/labbis/get/HEAD.zip');
catch
    disp('Installation failed. Unable to download from remote repository.');
    return
end

disp('Download finished.')

% Install
new_labapp_zip = [new_labapp '.zip'];
try
    disp('Extracting package...')
    new_files = unzip(new_labapp_zip);
catch
    disp(['Installation failed. Failed to unzip ' fullfile(install_path, new_labapp_zip)])
    cleanup(1);
    return
end
disp('Extracted.')

try
    disp('Copying files...')
    % new_files{1} = zip root directory
    % new_files{2} = .gitignore
    for i=3:length(new_files)
        disp(new_files{i});
        [status, message] = movefile(new_files{i}, install_path);
    end
catch
    disp(['Installation failed. Failed to move ' fullfile(install_path, new_files{i})])
    cleanup(1);
    return
end
disp('Copied everything.')
disp('Finishing installation...')

cleanup()

addpath(genpath(install_path))
savepath

try
    asvr = actxserver('WScript.Shell');
    lnk = asvr.CreateShortcut(fullfile(install_path, ['KarsaLabbis','.lnk']));
    lnk.TargetPath = fullfile(install_path, 'krsfunctions', 'krs_labbis.mlapp');
    lnk.Icon = fullfile(install_path, 'assets', 'k.ico');
    lnk.Save();
    delete(asvr)
catch
    
end

disp('KARSA Labbis succesfully installed. Congratulations!')

    function cleanup(all)
        if nargin==0
            all = 0;
        end
        try
            delete(new_labapp_zip);
            rmdir(new_files{1}, 's');
        catch
            disp('Failed to clean up temporary files.')
        end
        if all
            rmdir(install_path, 's')
        end
    end

end

