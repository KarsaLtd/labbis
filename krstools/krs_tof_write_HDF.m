function ids = krs_tof_write_HDF(fNam, location, data, varargin)

% ids = tof_write_HDF(fNam, location, data, varargin)
% Writes data to a HDF-file. If the file doesn't exist, it will be created.
% Otherwise, data will be applied to the file. Only the three first
% arguments are obligatory.
%
% in:
%     fNam     - name of the HDF-file
%     location - path inside the file (e.g. '/group/dataset')
%     data     - data to be written
%     varargin - in the form of property, value
%       'compress' - data compression level, 0-9
%       'chunk'    - chunk size, MATLAB dimensions
%       'memtype'  - data type, has to match the chosen dataset, options:
%                    H5T_NATIVE_DOUBLE, H5T_NATIVE_FLOAT, H5T_NATIVE_CHAR
%       'mode'     - 'append', 'edit' or 'create'
%       'offset'   - (when 'mode' is 'edit') location in the dataset to
%                    write to
%       'keepopen' - if set to true, leaves the handles of the file and
%                    dataset open (to be used with caution, the handles
%                    have to be closed manually)
%       'ids'      - structure of handles outputted by this script
%
% out:
%     ids      - to be used with 'keepopen', outputs handles of the file
%                and dataset
%
% usage:
%     tof_write_HDF('c:\data.h5', '/myGroup/myDataSet', dat,'memtype', 'H5T_NATIVE_FLOAT');
%     ids = tof_write_HDF('c:\data.h5', '/myData', dat, 'keepopen', true, 'mode', 'edit', 'offset', [10 1]);

% Gustaf L?nn, after Heikki Junninen
% June 2012

%% initialize variables, interpret and check inputs
persistent HDFFilterAvailable HDFFilterInfo
create = false;
idsGiven = false;
keepopen = false;
chunk = [];
groupName = [];
ids.fId = [];
ids.dId = [];
ids.dims_old = [];
memType = [];
offset = [];
DSexist = 1;
Gexist = 1;

compress = 9;
mode = 'append';

if nargin >= 4
    i = 1;
    numVarArgs = length(varargin);
    while i <= numVarArgs
        switch lower(varargin{i})
            case 'compress'
                compress = varargin{i + 1}; i = i + 1;
            case 'chunk'
                chunk = varargin{i + 1}; i = i + 1;
            case 'memtype'
                memType = varargin{i + 1}; i = i + 1;
            case 'mode'
                mode = varargin{i + 1}; i = i + 1;
            case 'offset'
                offset = varargin{i + 1}; i = i + 1;
            case 'keepopen'
                keepopen = varargin{i + 1}; i = i + 1;
            case 'ids'
                if ~isempty(varargin{i + 1})
                    idsGiven = true;
                    ids = varargin{i + 1};
                    if ~isfield(ids, 'dId'); % the existence of dId is assumed later
                        ids.dId = [];
                    end
                end
                i = i + 1;
            otherwise
                disp(['tof_write_HDF: Unknown argument ''' varargin{i} '''. ']); i = i + 1;
        end
        i = i + 1;
    end
end

if strcmp(mode, 'edit') && isempty(offset)
    disp('tof_write_HDF: Offset argument must be given when mode is ''edit''.');
    return;
end

if nargin < 3
    disp('tof_write_HDF: Too few input arguments.');
    return;
end

% assume that the file exists (don't create) if idsGiven true
if (~idsGiven && ~exist(fNam, 'file')) || strcmpi(mode, 'create')
    create = true;
end

if location(1) ~= '/'
    location = ['/', location];
end
if location(end) == '/'
    location(end) = [];
end
parts = regexp(location, '/', 'split');
if length(parts) >= 3
    groupName = strjoin(parts(1:end-1),'/');
end

dims = size(data);
if any(dims == 0)
    disp('tof_write_HDF: Data is empty.');
    return;
end

ndims = length(dims);
if any(dims(2:end) > 1)
    data = permute(data, [ndims:-1:1]); % switch the dimensions of the data if has more than one dimension (MATLAB ~= HDF)
end


if isempty(chunk)
    chunk = [ones(1, ndims - 1) dims(end)];
end

if isempty(memType) % identify the needed memory type
    switch class(data)
        case 'double'
            memType = 'H5T_NATIVE_DOUBLE';
        case 'single'
            memType = 'H5T_NATIVE_FLOAT';
        case 'logical'
            memType = 'H5T_NATIVE_CHAR';
            data = uint8(data);
        case 'uint8'
            memType = 'H5T_NATIVE_CHAR';
        case 'char'
            memType = 'H5T_NATIVE_CHAR';
            data = uint8(data);
        case 'struct'
            doubleType=H5T.copy('H5T_NATIVE_DOUBLE');
            sz(1)     =H5T.get_size(doubleType);
            strType   = H5T.copy ('H5T_C_S1');
            H5T.set_size (strType, 'H5T_VARIABLE');
            sz(2)     =H5T.get_size(strType);
            memType = 'H5T_COMPOUND';

        otherwise
            disp('tof_write_HDF: Unsupported data type.');
            return;
    end
else
    switch(memType)
        case {'H5T_NATIVE_DOUBLE', 'H5T_IEEE_F64LE'}
            if ~isa(data, 'double')
                data = double(data);
            end
        case {'H5T_NATIVE_FLOAT', 'H5T_IEEE_F32LE'}
            if ~isa(data, 'single')
                data = single(data);
            end
        case {'H5T_NATIVE_CHAR', 'H5T_STD_I8LE'};
            if ~isa(data, 'uint8')
                data = uint8(data);
            end
    end
end

if compress ~= 0 && isempty(HDFFilterAvailable) % check compression availability
    HDFFilterAvailable = H5Z.filter_avail('H5Z_FILTER_DEFLATE');
    HDFFilterInfo = H5Z.get_filter_info('H5Z_FILTER_DEFLATE');
    if ~HDFFilterAvailable || HDFFilterInfo ~= 3
        disp('tof_write_HDF: Compression not available.');
        compress = 0;
    end
end

%% create file or check existence of dataset/group

if create
    ids.fId = H5F.create(fNam, 'H5F_ACC_TRUNC', 'H5P_DEFAULT', 'H5P_DEFAULT');
    Gexist = isempty(groupName); % if empty, ignore the group by assuming that it exists
    DSexist = 0;
elseif isempty(ids.fId)
    ids.fId = H5F.open(fNam, 'H5F_ACC_RDWR', 'H5P_DEFAULT');
end

if ~create && isempty(ids.dId) % check if the group and dataset exists
    if ~isempty(groupName)
        Gexist = H5L.exists(ids.fId, groupName, 'H5P_DEFAULT');
    else
        Gexist = 1;
    end
    if Gexist
        DSexist = H5L.exists(ids.fId, location, 'H5P_DEFAULT');
    else
        DSexist = 0;
    end
end

%% create dataset/group if needed

createDG = ~DSexist && Gexist || ~Gexist;
if createDG % create group and/or dataset
    H5S_UNLIMITED = H5ML.get_constant_value('H5S_UNLIMITED');
    maxdims = ones(1, ndims) .* H5S_UNLIMITED;
    if ~Gexist
        group = H5G.create(ids.fId, groupName, 1);
    end
    
%     if strcmp(memType,'H5T_COMPOUND')
%         %
%         % Computer the offsets to each field. The first offset is always zero.
%         %
%         offs(1)=0;
% %         offset(2:3)=cumsum(sz(1:2));
%         offs(2)=sz(1);
%         
%         %
%         % Create the compound datatype for memory.
%         %
%         memtype = H5T.create ('H5T_COMPOUND', sum(sz));
%         H5T.insert (memtype,...
%             'Time',offs(1),doubleType);
%         H5T.insert (memtype,...
%             'Message',offs(2), strType);
%       
%         memType=memtype;
%         data.msg
%     end
    sId = H5S.create_simple(ndims, dims, maxdims);
    pId = H5P.create('H5P_DATASET_CREATE');
    H5P.set_chunk(pId, chunk);
    if compress ~= 0
        H5P.set_deflate(pId, compress);
    end
    ids.dId = H5D.create(ids.fId, location, memType, sId, pId);
    ids.dims_old = [0 dims(2:end)]; % dataset is still empty
    
end

%% write data

if create
    H5D.write(ids.dId, memType, 'H5S_ALL', 'H5S_ALL', 'H5P_DEFAULT', data);
    ids.dims_old(1) = ids.dims_old(1) + dims(1); % correct current dimensions
else
    if isempty(ids.dId) % dataset has to be opened only if we don't have the handle
        ids.dId = H5D.open(ids.fId, location);
        sId = H5D.get_space(ids.dId);
        [~, ids.dims_old] = H5S.get_simple_extent_dims(sId);
    end    
    if strcmp(mode, 'append')
        dims_extended = [ids.dims_old(1) + dims(1) ids.dims_old(2:end)]; % extend first dimension
        offset = [ids.dims_old(1) zeros(1, ndims - 1)];
    elseif strcmp(mode, 'edit')
        offset = offset - 1; % HDF has 0-based indexing
        dims_extended = offset + dims;
    end
    if any(dims_extended > ids.dims_old)
        H5D.extend(ids.dId, dims_extended);
        ids.dims_old = dims_extended;
    end
    sId = H5D.get_space(ids.dId);
    H5S.select_hyperslab(sId, 'H5S_SELECT_SET', offset, [], dims, []);
    mId = H5S.create_simple(ndims, dims, dims);
    H5D.write(ids.dId, memType, mId, sId, 'H5P_DEFAULT', data);
end

%% close file

if ~keepopen % closes the handles
    H5D.close(ids.dId);
    H5F.close(ids.fId);
    ids = 1;
end