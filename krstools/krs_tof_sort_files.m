function [fls,flsTim,nrFls]=krs_tof_sort_files(fls)
%
% sort files in structure to time order.
% time is taken from file name
%
% 

%Heikki Junninen, Mikael Ehn
% Jun, 2009

if isempty(fls)
    flsTim = [];
    nrFls = 0;
    return;
end

[nrFls n] = size(fls);

for i = 1:nrFls
    FileName = fls(i).name;
    %     ind = regexp(FileName,'[0-9]+.[0-9]+.[0-9]+-[0-9]+h[0-9]+m[0-9]+s');
    %     if isempty(ind)
    %         ind = strfind(FileName,'2010');
    %     end
    %     if isempty(ind)
    %         ind = strfind(FileName,'2011');
    %     end
    c=regexp(FileName,'[0-9]+.[0-9]+.[0-9]+[-,_][0-9]+h[0-9]+m[0-9]+s','match');  % 2009.04.27-22h40m34
    if ~isempty(c)
        DateString0(i,:) = c{1};
    else
        DateString0(i,:) = '0000.00.00_00h00m00s';
    end
end
if 0
    %matlab 7->
    DateString = DateString0(:,[1:4 6:7 9:10 12:13 15:16 18:19]); % 20090427224034
    flsTi = datenum(DateString,'yyyymmddHHMMSS');
else
    %for matlab 6.5
    ys = str2num(DateString0(:,[1:4])); % 20090427224034
    ms = str2num(DateString0(:,[6:7])); % 20090427224034
    ds = str2num(DateString0(:,[9:10])); % 20090427224034
    Hs = str2num(DateString0(:,[12:13])); % 20090427224034
    Ms = str2num(DateString0(:,[15:16])); % 20090427224034
    Ss = str2num(DateString0(:,[18:19])); % 20090427224034
    flsTi = datenum(ys,ms,ds,Hs,Ms,Ss);
end


[flsTim,Is]=sort(flsTi);
fls=fls(Is);
