function ids = krs_tof_write_HDF_logs(fNam, location, data, varargin)

% DIM0           = 2;

dims = size(data.msg,1);
compress=1;

%% create file or check existence of dataset/group

if location(1) ~= '/'
    location = ['/', location];
end
if location(end) == '/'
    location(end) = [];
end
parts = regexp(location, '/', 'split');
if length(parts) >= 3
    groupName = ['/' parts{2}];
end

%open file
ids.fId = H5F.open(fNam, 'H5F_ACC_RDWR', 'H5P_DEFAULT');

% if ~create && isempty(ids.dId) % check if the group and dataset exists
if ~isempty(groupName)
    Gexist = H5L.exists(ids.fId, groupName, 'H5P_DEFAULT');
else
    Gexist = 1;
end
if Gexist
    DSexist = H5L.exists(ids.fId, location, 'H5P_DEFAULT');
else
    DSexist = 0;
end
% end


%%
%
%Create the required data types
%
doubleType=H5T.copy('H5T_NATIVE_DOUBLE');
sz(1)     =H5T.get_size(doubleType);
strType   = H5T.copy ('H5T_C_S1');
H5T.set_size (strType, 'H5T_VARIABLE');
sz(2)     =H5T.get_size(strType);
memType = 'H5T_COMPOUND';



% Computer the offsets to each field. The first offset is always zero.

offs(1)=0;
offs(2)=sz(1);

%
% Create the compound datatype for memory.
%
memtype = H5T.create ('H5T_COMPOUND', sum(sz));
H5T.insert (memtype,...
    'Time',offs(1),doubleType);
H5T.insert (memtype,...
    'Message',offs(2), strType);
%
% Create the compound datatype for the file.  Because the standard
% types we are using for the file may have different sizes than
% the corresponding native types, we must manually calculate the
% offset of each member.
%
filetype = H5T.create ('H5T_COMPOUND', sum(sz));
H5T.insert (filetype,...
    'Time',offs(1),doubleType);
H5T.insert (filetype,...
    'Message',offs(2), strType);

%
% Create dataspace.  Setting maximum size to [] sets the maximum
% size to be the current size.
%
% space = H5S.create_simple (1,fliplr( dims), []);

%
% Create the dataset and write the compound data to it.
%
sId = H5S.create_simple (1,fliplr( dims), []);
pId = H5P.create('H5P_DATASET_CREATE');
if compress ~= 0
    H5P.set_deflate(pId, compress);
end
% ids.dId = H5D.create(ids.fId, location, memype, sId, pId);

group = H5G.create(ids.fId, groupName, 1);
% 
% sId = H5D.get_space(ids.dId);
% mId = H5S.create_simple(ndims, dims, dims);

dset = H5D.create (ids.fId, location, memtype, sId, 'H5P_DEFAULT');
H5D.write (dset, filetype, 'H5S_ALL', 'H5S_ALL', 'H5P_DEFAULT', data);


%
% Close and release resources.
%
H5D.close (dset);
H5S.close (sId);
H5T.close (filetype);
% H5F.close (file);


