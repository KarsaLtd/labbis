function log=krs_tof_read_log(input)
%
% Read log entries from raw data files
%
% log=tof_read_log(dateRange)
% dateRange - 1x2 - datenum
%
% or
%
% log=tof_read_log(fNam)
% fNam - file name with path
%
% log - nx3 - cell,
%             1. column datenum in UTC
%             2. column date string
%             3. column log entry
%

% Heikki Junninen
% Oct 2011

log=[];

if ischar(input)
    fNam=input;
    if exist(fNam,'file')
        rawStr=hdf5read(fNam,'/AcquisitionLog/Log');
        
        L=length(rawStr);
        log=cell(L,3);
        for iL=1:L
            log{iL,3}=rawStr(iL).Data{3}.Data;
            %             log{iL,1}=(double(rawStr(iL).Data{1})*100/1e9)/(60*60*24)+datenum(1601,1,1); %UTC
            
            %             log{iL,1}=tof_winTime2matlabTime(double(rawStr(iL).Data{1}));
            winTim=double(rawStr(iL).Data{1});
            timStr=rawStr(iL).Data{2}.Data;
            log{iL,1}=krs_tof_winTime2matlabTimeLocal(winTim,timStr);
            
            log{iL,2}=timStr;
        end
        
    else
        disp(['tof_read_log: File ',fNam,' not found!'])
    end
else
    global tofPath
    dateRange=input;
    [fls]=krs_tof_scan_files(dateRange);
    
    nrF=length(fls);
    if nrF==0
        krs_tof_message(['No files found in: ',tofPath])
    else
        h=0;
        for iF=1:nrF
            rawStr=hdf5read([tofPath,fls(iF).name],'/AcquisitionLog/Log');
            
            L=length(rawStr);
            
            for iL=1:L
                h=h+1;
%                 log{h,3}=rawStr(iL).Data{3}.Data;
%                 %             log{h,1}=datenum((rawStr(iL).Data{2}.Data),'mm/dd/yyyy HH:MM:SS AM');
%                 %             log{h,1}=(double(rawStr(iL).Data{1})*100/1e9)/(60*60*24)+datenum(1601,1,1); %UTC
%                 log{h,1}=tof_winTime2matlabTime(double(rawStr(iL).Data{1}));
%                 log{h,2}=rawStr(iL).Data{2}.Data;
                
                
                log{h,3}=rawStr(iL).Data{3}.Data;
                winTim=double(rawStr(iL).Data{1});
                timStr=rawStr(iL).Data{2}.Data;
                log{h,1}=krs_tof_winTime2matlabTimeLocal(winTim,timStr);
                
                log{h,2}=timStr;
                
                
            end
        end
        logm=cell2mat(log(:,1));
        Iok=logm >= dateRange(1) & logm <= dateRange(2);
        
        log=log(Iok,:);
    end
end
