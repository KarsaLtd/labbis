function noise=krs_tof_calc_noise(spec)
%
% mode of non-zero values of first derivative
%
%

% Heikki Junninen
% Feb 2012

absDifs=abs(diff(spec));
%absDifs(absDifs==0)=[];

%  noise=nanmedian(absDifs);
%  noise=nanmean(absDifs);
noise=mode(absDifs(absDifs~=0));
% noise=nanmedian(absDifs(absDifs~=0));