function [peakList_out,R]=krs_tof_refit_gauss_peakShape(mz,spec,peakList,R,peakShape,peakComp,pIds,mzcalibpar,isoAbund)
%
% Refit gaussian to peak data coming from tof_locate_peaks or own peak list
%

if nargin < 8
    mzcalibpar = 0;
end

if nargin < 9
    isoAbund = [];
end

if (nargin == 9 && isempty(isoAbund))
    isoAbund = zeros(length(peakComp),10,2);
    %isoAbund = [];
end

% Include isotopes?
% This means that if an ion is at a mass where the isotope of a previously fit mass
% makes up more than isotope_limit of the peak height, then that isotope will be included
% and its area locked based on its relative isotopic abundance. The masses will be sorted
% in ascending order, so only isotopes heavier than their parent will be
% included. For elements like Fe that have isotopes also at lower unit
% masses, the fit will be made to the isotope with the lowest mass!
if isempty(peakComp) || isnumeric([peakComp{:}])
    isotopes_included = 0;
else
    isotopes_included = 1;
    isotope_limit = 0.002;
    [~, IX] = sort(peakList(:,2));
    peakList = peakList(IX,:);
    peakComp = peakComp(IX);
    pIds = pIds(IX);
    if isempty(isoAbund)
        [~,elNam,elNum]=krs_tof_exact_mass(peakComp);
        elemComp = cell(length(peakComp),1);
        for f=1:length(peakComp)
            for e=1:length(elNam{f})
                if elNum{f}(e)>0
                    elemComp{f}=[elemComp{f},elNam{f}{e},num2str(elNum{f}(e)),' '];
                end
            end
        end
    else
        isoAbund=isoAbund(IX,:,:);
    end
end

%[min_val min_ind] = min(isoAbund(:,:,1),[],2);

% peakList=unique(Pk_high97,'rows');
% spec=spec_corr_high97;
if ~isstruct(mzcalibpar)
    rmz=round(mz);
else
    funcArgs = mzcalibpar.eq();
end
rpks=round(peakList(:,2));
urpks=unique(rpks);
% G: unused
nrP=length(unique(rpks));


%            1. peak number
%            2. position [Th]
%            3. height [mV]
%            4. fwhm [Th] full width at half maximum
%            5. area [ions/sec]
%            6. sigma [Th]
%            7. fitError, norm of residuals

%estimate resolution and fix it for the whole spectrum

fnc = 0;
if isempty(R)
    Isel=peakList(:,3)>prctile(peakList(:,3),95);
    Rconst=nanmedian(peakList(Isel,2)./peakList(Isel,4));
    if isnan(Rconst)
        Rconst = 5500; %hard wired default value
    end
    R = @(m)(Rconst);
elseif strcmp(class(R),'function_handle')
    fnc = 1;
else
    Rconst = R;
    R = @(m)(Rconst);
end

numIso = 10;
peakList_out=NaN(length(rpks),8);
isotopeList=NaN(length(rpks)*(numIso-1),8);
isotopeList_incl_out = NaN(length(rpks)*(numIso-1),8);
hmi=0; hmi_iso=0; iso_incl_ind = 0;
py = peakShape.dat(:,2)';
rxi = peakShape.dat(:,1)';
if peakShape.par(1) ~= 0
    may = krs_tof_normpdf(rxi,peakShape.par(2),peakShape.par(1));
    b = py.*may;
    b(isnan(b)) = 0;
else
    b = py;
end
b = krs_H_scale(b);
%area = trapz(rxi,b);
uselsqlin = 0;
trapzThreshold = 50;
if exist('lsqlin','file') ~= 0
    uselsqlin = 1;
    opt = optimset('Disp','off','Algorithm','interior-point');
end
%stupid fix to make peak shape go to zero at the ends
step = round((rxi(2)-rxi(1))*100)/100;
rxpp = rxi(1)-10:step:rxi(end)+10;
bpp = [zeros(1,round(10/step)) b zeros(1,round(10/step))];
pp = spline(rxpp,bpp);
if isempty(isoAbund)
    loop = min(urpks)-1:max(urpks)+1;
else
    loop = min(urpks)-1:max(max(max(isoAbund(:,:,1)))+1,max(urpks)+1);
end
for mi=loop
    if any(mi==urpks)
        m=mi;
        if ~isstruct(mzcalibpar)
            Isel=rmz==m;
        else
            fInd = ceil(funcArgs.inverseEQ(m-0.5,mzcalibpar.pars));
            lInd = floor(funcArgs.inverseEQ(m+0.5,mzcalibpar.pars));
            nSample = length(spec);
            lInd = min(lInd, nSample);
            Isel = fInd:lInd;
        end
        
        Isel_p=rpks==m;
        nr_peaks=sum(Isel_p);
        
        xsel=mz(Isel);
        ysel=spec(Isel);
        lng=length(xsel);
        
        xsel=xsel(ones(1,nr_peaks),:);
        
        %h=peakList(Isel_p,5);
        fer=peakList(Isel_p,7);
        fwhm=m/feval(R,m(1));
        m=m(ones(1,nr_peaks),:);
        fwhm=fwhm(ones(1,nr_peaks),:);
        w=fwhm/(2*sqrt(2*log(2)));
        z=peakList(Isel_p,2);
        
        %h  = h(:,ones(lng,1));
        w  = w(:,ones(lng,1));
        z  = z(:,ones(lng,1));
        
        b=b(ones(1,nr_peaks),:);
        
        b2 = zeros(nr_peaks,length(xsel));
        for ijk = 1:nr_peaks
            %b2(ijk,:) = interp1(rxi*w(1,1)+z(ijk,1),b(1,:),xsel(1,:));
            b2(ijk,:) = ppval(pp,(xsel(1,:) - z(ijk,1))/w(1,1));
        end
        b2(isnan(b2)) = 0;
        
        %Isotopes at this mass?
        if isotopes_included == 1
            isotope_mz = round(isotopeList(1:hmi_iso,2));
            inds = find(isotope_mz==m(1));
            if isempty(inds)~=1
                y_iso = isotopeList(inds,3);
                inds2 = find(y_iso>max(ysel)*isotope_limit);
                isotopeList_incl = isotopeList(inds(inds2),:);
                for ii = 1:length(inds2)
                    %b_iso = interp1(rxi*w(1,1)+isotopeList_incl(ii,2),b(1,:),xsel(1,:));
                    b_iso = ppval(pp,(xsel(1,:)-isotopeList_incl(ii,2))/w(1,1));
                    b_iso(isnan(b_iso)) = 0;
                    ysel = ysel - b_iso*isotopeList_incl(ii,3);
                    ymx = max(b(1,:)*isotopeList_incl(ii,3));
                    iso_incl_ind = iso_incl_ind + 1;
                    isotopeList_incl_out(iso_incl_ind,1:8) = isotopeList_incl(ii,:);
                    isotopeList_incl_out(iso_incl_ind,3) = ymx;
                    isotopeList_incl_out(iso_incl_ind,4) = fwhm(1,1);
                    isotopeList_incl_out(iso_incl_ind,6) = w(1,1);
                end
            end
        end
        y2=ysel;
        indii = find(xsel(1,:)>min(z(:,1))-10*w(1,1) & xsel(1,:)<max(z(:,1))+10*w(1,1));
        b2 = b2(:,indii); y2 = y2(:,indii);
        
        %constrain to positive solution only, uses optimization toolbox
        %     h_out=lsqnonneg((double(b2')),double(y2'))';
        
        %another way of constraining to positive solution only, uses optimization toolbox
        % opt=optimset('Display','Off');
        % h_out = lsqlin(double(b2)',double(y2')',[],[],[],[],zeros(1,size(b2,1)),opt);
        
        % solve using pinv
        % h_out = y2*pinv(b2);
        
        %about 10x faster than using pinv
        h_out = (y2/b2)';
        if ~all(h_out>=0)
            if uselsqlin
                if ~any(isnan(y2))
                    h_out = lsqlin(double(b2'),double(y2'),[],[],[],[],zeros(1,size(b,1)),[],[],opt);
                else
                    h_out=NaN(size(b2,1),1);
                end
            else
                if size(b,1) > 40
                    disp(['Refitting over 40 peaks for mass ',num2str(m(1)),', fit might be slow.']);
                end
                h_out = lsqnonneg((double(b2')),double(y2'));
            end
        end
        
        %calculate y
        h2  = h_out(:,ones(length(rxi),1));
        y = h2.*b;
        ss = bsxfun(@times,b2,h_out)';
        if length(b2) < trapzThreshold
            ar = trapz(1:length(ss),ss);
        else
            ar = sum(ss,1);
        end
        %h_out = h_out .* w(:,1) * area;
        ymx=max(y');
        
        z  = z(:,1);
        
        for pi=1:nr_peaks
            hmi=hmi+1;
            try
                %peakList_out(hmi,:)=[hmi,z(pi,1),ymx(pi),fwhm(pi),ar(pi),w(pi,1),fer(pi)];
                peakList_out(hmi,1) = hmi;
                peakList_out(hmi,2) = z(pi,1);
                peakList_out(hmi,3) = ymx(pi);
                peakList_out(hmi,4) = fwhm(pi);
                peakList_out(hmi,5) = ar(pi);
                peakList_out(hmi,6) = w(pi,1);
                peakList_out(hmi,7) = fer(pi);
                PeakID_0 = pIds(Isel_p);
                peakList_out(hmi,8) = PeakID_0(pi);
                % Make another peakList for isotopes. These may be included
                % into the real peaklist later if they are significant.
                if ~isempty(isoAbund) || (isotopes_included==1 && isempty(elemComp{hmi})==0)
                    if ~isempty(isoAbund)
                        isodist = reshape(isoAbund(hmi,1:numIso,:),numIso,2);
                        isodist(isodist(:,1)==0,:) = [];
                    else
                        isodist0 = isoDalton_exact_mass(elemComp{hmi},numIso);
                        isodist=krs_tof_unique_isotopes(isodist0,100);
                    end
                    if ~isempty(isodist)
                        %isodist=sortrows(isodist,1);
                        isodist(:,2) = isodist(:,2)/isodist(1,2);
                        for isoind = 2:size(isodist,1)
                            hmi_iso = hmi_iso + 1;
                            %isotopeList(hmi_iso,:)=[hmi_iso+100,isodist(isoind,1),ymx(pi)*isodist(isoind,2),NaN,ar(pi)*isodist(isoind,2),NaN,NaN];
                            isotopeList(hmi_iso,1) = hmi_iso+100;
                            isotopeList(hmi_iso,2) = isodist(isoind,1);
                            isotopeList(hmi_iso,3) = ymx(pi)*isodist(isoind,2);
                            isotopeList(hmi_iso,5) = ar(pi)*isodist(isoind,2);
                            isotopeList(hmi_iso,8) = PeakID_0(pi);
                        end
                    end
                end
            catch exception
                disp(exception.message);
            end
        end
    else
        if isotopes_included == 1
            isotope_mz = round(isotopeList(1:hmi_iso,2));
            inds = find(isotope_mz==mi);
            if isempty(inds)~=1
                y_iso = isotopeList(inds,3);
                inds2 = find(y_iso>max(ysel)*isotope_limit);
                isotopeList_incl = isotopeList(inds(inds2),:);
                for ii = 1:length(inds2)
                    %b_iso = interp1(rxi*w(1,1)+isotopeList_incl(ii,2),b(1,:),xsel(1,:));
                    %b_iso = ppval(pp,(xsel(1,:)-isotopeList_incl(ii,2))/w(1,1));
                    %b_iso(isnan(b_iso)) = 0;
                    %ysel = ysel - b_iso*isotopeList_incl(ii,3);
                    %ymx = max(b(1,:)*isotopeList_incl(ii,3));
                    iso_incl_ind = iso_incl_ind + 1;
                    isotopeList_incl_out(iso_incl_ind,1:8) = isotopeList_incl(ii,:);
                    %isotopeList_incl_out(iso_incl_ind,3) = ymx;
                    isotopeList_incl_out(iso_incl_ind,4) = fwhm(1,1);
                    isotopeList_incl_out(iso_incl_ind,6) = w(1,1);
                end
            end
        end
    end
end

%reorder the peak list to original order
% G: remove doubles
%[~,Is] = unique(peakList_out(:,2),'rows');
%peakList_out=peakList_out(Is,:);

% Add the isotopes to be included:
isotopeList_incl_out(isnan(isotopeList_incl_out(:,1)),:) = [];
peakList_out = [peakList_out; zeros(1,8); isotopeList_incl_out];

% sets R to NaN if R is a function
if fnc
    R = NaN;
else
    R = feval(R,0);
end