function [par1,ppm,yhat]=krs_tof_fit_tof_mz_3p(mz,tof,init,opt)
%fit function for tof_vs_amu
%
% par1=tof_fit_tof_mz(mz,tof,init)
% mz = mass axis
% tof  = time of flight or sample number
% init = starting values
% amu=((tof-b)/a)^2
%

if nargin == 0
    par1.numOfInitValues = 3;
    par1.useOriginalParams = 1;
    par1.numOfPars = 3;
    %par1.usedEQ = @(x,p)((x-p(2))./p(1)).^p(3);
    par1.usedEQ = @(x,p)abs(exp(p(3).*log((x-p(2))./p(1))));
    %par1.inverseEQ = @(x,p)(p(1)*x.^(1/p(3))+p(2));
    par1.inverseEQ = @(x,p)(p(1).*exp((1/p(3)).*log(x))+p(2));
    par1.accurateInverseEQ = par1.inverseEQ;
    par1.reprEQ = 'm = [(t - p2) / p1]^p3';
    par1.texEQ = 'm = [(t - p_2) / p_1]^{p_3}';
    return;
end

xy=[tof(:),mz(:)];
% init=[600,-1300,0.49];

% df=abs(init*0.05);
% limLow=init-df;
% limHgh=init+df;
limLow=[0 -1e5 1.999];
limHgh=[1e5 1e5 2.001];


%optimize

%,'OutputFcn', @outfun

% [par1]=fminimax(@(par1) funfit(par1,xy),init,[],[],[],[],limLow,limHgh,[],opt);
% [par1]=fmincon(@(par1) funfit(par1,xy),init,[],[],[],[],limLow,limHgh,[],opt);

[par1]=fminsearch(@(par1) funfit(par1,xy),init,opt);
% par1 = lsqnonlin(@(par1) funfit(par1,xy),init,[],[],opt);

% bar(mzs_ok,predTOF_ok-tof_hat)

yhat=((xy(:,1)-par1(2))/par1(1)).^par1(3);
L=length(yhat);
f=sqrt(sum((xy(:,2)-yhat).^2)/2);

ppm=mean(abs(xy(:,2)-yhat)./xy(:,2))*1000000;

function f=funfit(par,xy)
%par(1)= slope
%par(2)= intercept
%par(3)= power
%yhat=((xy(:,1)-par(2))/par(1)).^par(3);
yhat = exp(par(3).*log((xy(:,1)-par(2))/par(1)));
f=norm(xy(:,2)-yhat)/sqrt(2);

function stop = outfun(x, optimValues, state)
stop = false;
figure(201)
hold on;
plot(optimValues.funccount,optimValues.fval,'.')
drawnow