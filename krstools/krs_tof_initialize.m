function krs_tof_initialize

% tof_initialize
% Performs tofTools initialization actions, like adding folders to the
% MATLAB path and pre-loading data. This script is automatically run by
% tof_set_path and tof_gui_eval_params. It will not run if it already has
% been executed during the current MATLAB session.

% Gustaf L?nn
% July 2012

if getappdata(0, 'tofToolsVersionChecked') % return if already run
    return
end

% don't update java path unless necessary, javaaddpath clears all variables
tofToolsPath = fileparts(mfilename('fullpath'));
curJavaDynPath = javaclasspath;
isAlreadyAdded = regexp(curJavaDynPath, 'misc_funs');
isAlreadyAdded = any(cellfun(@(x)~isempty(x), isAlreadyAdded));
if ~isAlreadyAdded
    javaaddpath([tofToolsPath filesep 'misc_funs']);
end

% add tofTools folder tree (excluding .svn-folders) to path
folders = regexp(genpath(tofToolsPath), pathsep, 'split');
folders(end) = [];
useFolder_svn = cellfun(@isempty, strfind(folders, '.svn')); % folders with no '.svn' in their path
useFolder_git = cellfun(@isempty, strfind(folders, '.git')); % folders with no '.git' in their path
useFolder=useFolder_git&useFolder_svn;

folders = folders(useFolder);
folders = cellfun(@(x)[x pathsep], folders, 'UniformOutput', false); % add pathsep to the end of every path
addpath([folders{:}]);

% load abundances and masses
abund = load('abundances.mat','elemNams','masses');
abund.masses(:, 3) = abund.masses(:, 3) / 100;
load('masstable.mat');

% preload isoDalton
isoDalton_get_isotope_info;

% disable warnings
warning('off', 'MATLAB:imagesci:H5:HDF5libDeprecated');
warning('off', 'MATLAB:imagesci:H5ML:HDF5libDeprecated');
warning('off', 'MATLAB:H5ML:HDF5lib:deprecated');
warning('off', 'MATLAB:chckxy:IgnoreNaN');
warning('off', 'MATLAB:rankDeficientMatrix'); 
warning('off', 'MATLAB:datenum:EmptyDate');
warning('off', 'MATLAB:illConditionedMatrix');
warning('off', 'MATLAB:singularMatrix');
warning('off', 'MATLAB:nearlySingularMatrix');

% check for updates
% set constants
const.lowlimR = 3000;
const.highlimR = 15000;
const.keeplog = 0;
setappdata(0, 'tofToolsConstants', const);
setappdata(0, 'tofToolsPath', tofToolsPath);


% load NIST database
global nistdb
nistdb = load('nist_db_2009Oct26.mat');

% set flag
setappdata(0, 'tofToolsVersionChecked', true);