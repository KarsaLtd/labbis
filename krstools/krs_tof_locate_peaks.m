function varargout=krs_tof_locate_peaks(mz,spec,param)
%
% Find peaks in spectrum, detecting change in derivative and amplitude
%
% [PeakList, parameters]=tof_locate_peaks(mz,spec,param)
%
% mz - mass axis in Th
% spec - signal in mV
% param - parameters for peak detection
%
% PeakList - [peak number, position, height, hwfm, area, sigma, fitError]
%            columns:
%            1. peak number
%            2. position [Th]
%            3. height [mV]
%            4. hwfm [Th] full width at half maximum
%            5. area [mV*Th]
%            6. sigma [Th]
%            7. fitError, norm of residuals
%
% In case of shoulders hwfm, area, sigma, fitError are NaN
%
% Parameters - parameters used for fitting, if input parameters had 'auto'
% settings, the evaluated values are in output parameters structure.
%
% param=struct(...
%     'amuRange',[10 800],... % range where to find peaks
%     'step',10,...%Th, peak and noise detection is done in parts that wide
%     'SignalThreshold','auto',... %signal threshold determined automatically in amu steps defined above. Threshold=noise*ScaleMedian
%     'SlopeThreshold','auto',... %slope threshold (=signal threshold of 1st derivative) determined automatically in amu steps defined above. Threshold=noise(signal 1st derivative)*ScaleMedian
%     'ScaleMedian',5,... %multi of noise as threshold for peaks in the step window
%     'R',4000,... %used to evaluate smoothing window - it's a smoothing parameter and should be about close the real resolution
%     'doPlot',0,... % plot peak find raw result,
%     'baselineCorrection',1,... %correct for baseline if not already done
%     'enhanceFactor1',3,... %factor for resolution enhancement that improves shoulder detection,
%     'enhanceFactor2',50); %factor for resolution enhancement
%
% Resolution enhancement follows logic:
% enhancedSignal=signal-1stDerivative*enhanceFactor1+4thDerivative*enhanceFactor2
% 4thDerivative - 4thDerivative value where 1stDerivative is positive
%
% This will enhance peaks and shoulders, but will give artfacts in
% baseline. A compromise have to be found manually. The enhanced signal is
% only used for peak detection, not for any peak fitting.


%parameters

%st=5e-7;
if nargin==2
    sw=4;
    amuRange=[ceil(min(mz)),floor(max(mz))];
    %at=3e-7;
    at='auto';
    st='auto';
    param.step = 0.1;
    doPlot=1;
    %select amu range
    param.amuRange=amuRange;
    scaleMedian=2;
    
elseif nargin==3
    amuRange=param.amuRange;
    %     mxRes_lim=param.mxRes;
    %     relRes_lim=param.relRes;
    at=param.SignalThreshold; %amplitude treshold
    %     sw=param.SmoothWindow;
    st=param.SlopeThreshold;
    step=param.step;
    doPlot=param.doPlot;
    scaleMedian=param.ScaleMedian;
    if isfield(param,'enhanceFactor1') %legacy check
        enhanceFactor1=param.enhanceFactor1;
        enhanceFactor2=param.enhanceFactor2;
    else
        enhanceFactor1=0;
        enhanceFactor2=0;
    end
end


if max(mz)<amuRange(1)
    disp('tof_locate_peaks: range exceeds the masses axes')
    if nargout==1
        varargout{1}=[];
    elseif nargout==2
        varargout{1}=[];
        varargout{2}=param;
    end
    return
end

% calculate a and b, used for smoothing parameter
%     mz=((tof-b)/a).^2;
%     tof1=a*sqrt(mz1)+b;
%     b=tof1-a*sqrt(mz1);
%     tof2=a*sqrt(mz2)+b;
%     tof2=a*sqrt(mz2)+tof1-a*sqrt(mz1);
%     a(sqrt(mz2)-sqrt(mz1))+tof1=tof2;

mz1=mz(1);
mz2=mz(end);
tof1=1;
tof2=length(mz);
a=(tof2-tof1)/(sqrt(mz2)-sqrt(mz1));
b=tof1-a*sqrt(mz1);

param.a=a;
param.b=b;

difLim=0.01; %if polynome fitted peak location is more than difLim awau from measured peak location  the peak is considerered as shoulder

mzr=round(mz);
Imz=mzr>=amuRange(1) & mzr<=amuRange(2);
mz=mz(Imz);
mzr=mzr(Imz);

% smooth signal for initial peak finding
y=spec(Imz);
% ya=movavr(y,sw);

[ya,swn]=krs_H_fastsmooth(mz,y,param);


% ya=y;
y_orig=y;
y=ya;

% yd1=[0,y(2:end)-y(1:end-1)];
% yd2=[0,yd1(1:end-2)-yd1(3:end),0];

% yd3(1,:)=[0,yd1(1:end-1)-yd1(2:end)];
% yd3(2,:)=[0,yd1(1:end-2)-yd1(3:end),0];
% yd3(3,:)=[0,0,yd1(1:end-3)-yd1(4:end),0];
% yd3(4,:)=[0,0,yd1(1:end-4)-yd1(5:end),0,0];
% yd3(5,:)=[0,0,0,yd1(1:end-5)-yd1(6:end),0,0];
%
% yd2=mean(yd3);

% enhanceFactor=0;
remTails=1;
[ya]=krs_tof_resolution_enhancement(y_orig,enhanceFactor1,enhanceFactor2);
[~,yd1,yd2]=krs_tof_resolution_enhancement(ya,0,0);
yd2=-yd2;

aSteps=amuRange(1):step:amuRange(end);
nrSteps=length(aSteps);

if ischar(at)
    %auto set the amplitude treshold to 99% precentile of baseline values
    %     at=prctile(H_rm_spikes(y((diff(mzr)==1))),99);
    %     at=nanmedian(abs(y-ya))*4;
    
    % auto set amplitude treshold: 5 times non-zero values median difference of first
    % derivative
    ats=NaN(nrSteps,1);
    for i=1:nrSteps-1
        Isel=mzr>aSteps(i) & mzr<=aSteps(i+1);
        absDifs=abs(diff(y_orig(Isel)));
        absDifs(absDifs==0)=[];
        ats(i)=krs_tof_nanmedian(absDifs)*scaleMedian;
    end
    try
        ats=MDrepl1(ats);
    catch
        ats=zeros(nrSteps,1);
    end
    ats(end)=ats(end-1);
    at=interp1(aSteps,ats,mz);
else
    ats=at(ones(nrSteps,1),:);
    at=interp1(aSteps,ats,mz);
end

if ischar(st)
    %auto set the slope treshold to 98% precentile of baseline values
    %     st=prctile(abs(yd2((diff(mzr)==1))),98);
    % auto set slope treshold: 5 times median difference of second
    % derivative
    sts=NaN(nrSteps,1);
    for i=1:nrSteps-1
        Isel=mzr>aSteps(i) & mzr<=aSteps(i+1);
        absDifs=abs(diff(yd2(Isel)));
        absDifs(absDifs==0)=[];
        sts(i)=krs_tof_nanmedian(absDifs)*scaleMedian;
    end
    try
        sts=MDrepl1(sts);
    catch
        sts=zeros(nrSteps,1);
    end
    sts(end)=sts(end-1);
    st=interp1(aSteps,sts,mz);
else
    sts=st(ones(nrSteps,1),:);
    st=interp1(aSteps,sts,mz);
end

%apply criterias
Iat=ya(1:end-1)>at(1:end-1);

Izc=(yd1(1:end-1)>=0 & yd1(2:end)<=0);

% use original data for slope detection
% yd1=[0,y(2:end)-y(1:end-1)];
% Ist=(yd1(1:end-1)-yd1(2:end))>st;
Ist=yd2(2:end)>st(2:end);

%
% Isel=find(Izc & Ist & Iat & (Ionslope & Iat ));
Isel=find(Izc & Ist & Iat);
Idel=y(Isel)<y(Isel-1) & y(Isel)<y(Isel+1);
Isel(Idel)=[];
nrP=length(Isel);

% Isel=find(Izc & Iat);

% Ionslope=(yd1(1:end-1)-yd1(2:end))>0 & yd1(1:end-1)<0;

% Ist=yd2(2:end)>st;



% Isel=find((yd2(1:end-2)<yd2(2:end-1) & yd2(2:end-1)>yd2(3:end))& yd2(2:end-1)>st);
%  & yd2(2:end-1)>st


if doPlot
    
    figure(220200032),
    clf
    ax(1)=subplot(3,1,1);
    plot(mz,y_orig,'.-'),hold all
    plot(mz,ya,'.-'),
    %     plot(mz([1 end]),[at at],'k')
    plot(mz,at,'k')
    plot(mz(Isel),y(Isel),'or')
    
    ax(2)=subplot(3,1,2);
    plot(mz(1:end),yd1,'.-')
    hold on
    plot(mz([1 end]),[0 0],'k')
    plot(mz(Isel),yd1(Isel),'or')
    
    ax(3)=subplot(3,1,3);
    plot(mz(1:end-1),yd2(2:end),'.-')
    hold on
    %     plot(mz([1 end]),[st st],'k')
    plot(mz,st,'k')
    yd2t=yd2(2:end-1);
    plot(mz(Isel),yd2t(Isel),'or')
    linkaxes(ax,'x')
    
    
    title(['Number of peaks', num2str(nrP)])
end
%disp(['Number of peaks: ', num2str(nrP)])

%% evaluate each peak
% yd1=[0,y(2:end)-y(1:end-1)];
% yd2=[0,yd1(1:end-1)-yd1(2:end)];

%shoulder detection
% Ishould=find((yd2(1:end-2)<yd2(2:end-1) & yd2(2:end-1)>yd2(3:end))& yd2(2:end-1)>st);

P=NaN(nrP,7);
% figure
% plot(mz,y)
% res=y;

for p=1:nrP
    
    m=mz(Isel(p));
    pmPoints=max(round(swn(round(m))/2),1); %plus minus points
    %take points around the peak
    Ip=Isel(p)-pmPoints:Isel(p)+pmPoints;
    %     Ipex=max(Isel(p)-(pmPoints+10),1):min(Isel(p)+(pmPoints+10),length(mz));
    %     xxex=mz(Ipex);
    %     xx=mz(Ip);
    yy=y_orig(Ip);
    %     yy=ya(Ip);
    [~,Imx]=max(yy);
    Imx=Imx+Ip(1)-1;
    Ip=Imx-pmPoints:Imx+pmPoints;
    yy=y_orig(Ip);
    
    xx=mz(Ip);
    
    xx=xx(:);
    yy=yy(:);
    
    [coef,S,MU]=polyfit(xx,log(abs(yy)),2);  % Fit parabola to log of sub-group with centering and scaling
    if coef(1)<0
        c1=coef(3);c2=coef(2);c3=coef(1);
        PeakX=-((MU(2).*c2/(2*c3))-MU(1));   % Compute peak position and height of fitted parabola
        PeakY=exp(c1-c3*(c2/(2*c3))^2);
        hwfm=norm(MU(2).*2.35703/(sqrt(2)*sqrt(-1*c3)));
        sigma=hwfm/(2*sqrt(2*log(2)));
        Area=PeakY*(sigma*sqrt(2*pi));
        P(p,:) = [p PeakX PeakY hwfm Area sigma S.normr];
        %         Cs(p,:)=coef;
        %         yhat=exp(polyval(Cs(p,:),xxex,[],MU));
        %         plot(xxex,yhat,'k.-')
        %         plot(xx,exp(polyval(Cs(p,:),xx,[],MU)),'r.')
        %         res(Ipex)=res(Ipex)-yhat;
        %         mxRes=max(abs(yyex-yhat)/range(yyex));
        %         mxRess(p)=mxRes;
        %         if mxRes<2 & mxRes>0.8
        %             plot([y(Ipex);yhat]')
        %             [yhatm,param]=H_2gauss_fit(xxex,(abs(yyex)));
        %         end
        if abs(mz(Isel(p))-PeakX)>difLim
            P(p,:) = [p mz(Isel(p)) y_orig(Isel(p)) NaN NaN NaN NaN];
        end
    else
        P(p,:) = [p mz(Isel(p)) y_orig(Isel(p)) NaN NaN NaN NaN];
    end
    
    
    %           if peakgroup<7,
    %              PeakY=max(yy);
    %              pindex=val2ind(yy,PeakY);
    %              PeakX=xx(pindex(1));
    %           end
    
end

%disp(['Estimated resolution: ',num2str(1/nanmedian(P(:,4)./P(:,2)))])

% G: added a few checks
Ibad=isnan(P(:,2))|isinf(P(:,3))|P(:,3)>1e5|P(:,2)<0|P(:,4)>=1|P(:,3)<0;
P(Ibad,:)=[];
muh = size(P);
i = 0;
while(i < muh(1)-1)
    i = i + 1;
    if P(i,2)>=P(i+1,2)
        P(i,:)=[];
        i = i - 1;
    end
    muh = size(P);
end

if doPlot
    hold all
    
    figure(220200032),
    plot(ax(1),[P(:,2),P(:,2)]',[zeros(size(P(:,3))),P(:,3)]','r');
    Is=isnan(P(:,4));
    plot(ax(1),[P(Is,2),P(Is,2)]',[zeros(size(P(Is,3))),P(Is,3)]','k');
    
    
end


%
% % subplot(3,1,1)
% plot(P(:,2),P(:,3),'*k')
% plot(P(:,2)-P(:,4)/2,P(:,3)/2,'r.')
% plot(P(:,2)+P(:,4)/2,P(:,3)/2,'r.')
% hold off

if nargout==1
    varargout{1}=P;
elseif nargout==2
    varargout{1}=P;
    %write out also the parameters structure
    param.amuRange=amuRange;
    %     In=find(~isnan(at),1);
    %     param.SignalThreshold=at(In);
    %     param.SlopeThreshold=st(In);
    param.SignalThreshold=at;
    param.SlopeThreshold=st;
    param.SmoothWindow=swn;
    param.doPlot=doPlot;
    
    varargout{2}=param;
end

