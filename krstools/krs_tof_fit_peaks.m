function [ytot, fval, y1, h_out, par] = krs_tof_fit_peaks(mz, spec, compList, param)

% [ytot, fval, y1, h_out, par] = tof_fit_peaks(mz, spec, compList, param)
% Fit peaks of predefined peakshape, and/or resolution.
%
% In:
%     mz       - mass axis
%     spec     - spectrum
%     compList - initial mass list (guesses)
%     param    - structure of parameters, the fields are:
%       R:         fixed resolution value or anonymous function (R(mz))
%                  if empty it will be fitted
%       peakShape: peak shape function
%       tol:       determines how much the peak positions may deviate from
%                  the given values, can be vector with different
%                  tolerances for different peaks, default is 0.25
%       peakList:  peaks defined by mass to be included, only their height
%                  is fitted
%
% Out:
%     ytot  - the complete fitted profile
%     fval  - the norm of the difference between the data and the fit
%     y1    - array of y-data of all the peaks fitted
%     h_out - areas of the fitted peaks (in mV * Th)
%     par   - the final parameters used for the fit, [R mz1 mz2 ...]

% NOT CURRENTLY USED:
% param.nrIsotopes - number of isotopes to calculate
% param.relAbundLimit - limit for how big isotopes to include to analysis.
%                       Relative fraction of isotope in a peak (height)
%                       If this is left empty all (nrIsotopes) isotopes
%                       will be used
%
% Heikki Junninen

%error(nargchk(3, 4, nargin, 'struct'));

if verLessThan('matlab','7.13')
    % old matlab pre 2011
    error(nargchk(3,4,nargin));
else
    narginchk(3,4);
end


mz = mz(:);
spec = spec(:);
compList = compList(:)';
param.fitR = 0; % fit resolution
tol = .25;
param.usePeakList = false;

% interpret arguments

if nargin == 4
    if isfield(param, 'peakList')
        param.peakList = param.peakList(:)';
        param.usePeakList = true;
    end
    if isfield(param, 'R')
        if isempty(param.R)
            param.fitR = 1;
        elseif isa(param.R, 'function_handle')
            if ~isempty(compList)
                param.R = param.R(mean(compList));
            else
                param.R = param.R(mean(param.peakList));
            end
        end
    else
        param.fitR = 1;
    end
    if isfield(param, 'tol')
        tol = param.tol(:)';
    end
    if isfield(param, 'peakShape') && ~isfield(param.peakShape, 'sp')
        pp = spline(param.peakShape.dat(:,1),param.peakShape.dat(:,2));
        param.peakShape.sp = pp;
    elseif ~isfield(param, 'peakShape')
        rxi = -30:0.1:30;
        param.peakShape.sp = spline(rxi, krs_tof_normpdf(rxi, 0, 1));
    end
else
    % if only 3 arguments given, use Gaussian shape and fit resolution
    rxi = -30:0.1:30;
    param.peakShape.sp = spline(rxi, krs_tof_normpdf(rxi, 0, 1));
    param.fitR = 1;
end

% set values and fit

param.calc = false;
const = getappdata(0, 'tofToolsConstants');
nrP = length(compList);
spec = spec(:, ones(max(nrP, 1), 1));

if nrP > 0 || param.fitR % only do if fitting positions and/or resolution
    if length(tol) > nrP
        tol = tol(1:nrP);
    end
    opt = optimset('display', 'off', 'TolFun', 1e-12);
end

if param.fitR
    init = [8500 compList];
    ll = [const.lowlimR (compList - tol)];
    ul = [const.highlimR (compList + tol)];
    par = fminsearchbnd(@(par) fitfun(par, mz, spec, param), init, ll, ul, opt);
else
    if nrP > 0 % only do if fitting positions
        init = compList;
        ll = compList - tol;
        ul = compList + tol;
        par = fminsearchbnd(@(par) fitfun(par, mz, spec, param), init, ll, ul, opt);
        par = [param.R, par];
    else
        par = param.R;
    end
end

% calculate the final parameters

param.calc = true;
param.fitR = true; % par has already been converted to the form used when fitting resolution
[fval, ytot, h_out, kernel] = fitfun(par, mz, spec, param); % use fitfun to calculate the final parameters
h_out = h_out(:, 1);
if param.usePeakList
    nrP = nrP + length(param.peakList);
end
for i = 1:nrP
    y1(:, i) = kernel(:, i) * h_out(i);
end

function [fval, yhat, h_out, kernel] = fitfun(par, x, y, param)

% NOTE:
% * when fitting resolution, param.fitR = 1 and par = [R mz1 mz2 ...]
% * otherwise, param.fitR = 0 and par = [mz1 mz2 ...]
% * fitfun behaves in different ways depending on the value of param.fitR
% * outputs the area in the right units only when param.calc = 1

if param.fitR
    nrP = length(par) - 1;
else
    nrP = length(par);
end
kernel = zeros(length(x), nrP);
if param.usePeakList
    par = [par param.peakList];
    nrP = nrP + length(param.peakList);
end
for i = 1:nrP
    % fast kernel generation without checks

    if param.fitR
        w = 0.4246609 * round(par(i + 1)) / par(1);
        xi = (x - par(i + 1)) / w;
    else
        w = 0.4246609 * round(par(i)) / param.R;
        xi = (x - par(i)) / w;
    end
    
    % custom pp-evaluation
    [~, index] = histc(xi, [-inf, param.peakShape.sp.breaks(2:param.peakShape.sp.pieces), inf]);
    xs = xi - param.peakShape.sp.breaks(index)';
    v = param.peakShape.sp.coefs(index, 1);
    for j = 2:param.peakShape.sp.order
        v = xs(:) .* v + param.peakShape.sp.coefs(index, j);
    end
    
    kernel(:, i) = max(v, 0);
    if param.calc
        kernel(:, i) = kernel(:, i) / trapz(x, kernel(:, i));
    end
end
h_out = kernel \ y;
if any(h_out(:, 1) < 0)
    t.Display = 'off';
    [h_out, ~, ~, ~, ~] = lsqnonneg(kernel, y(:, 1), t);
    h_out = h_out(:, ones(nrP, 1));
end
yhat = kernel * h_out(:, 1);
fval = norm(y(:, 1) - yhat);