function tim=krs_tof_get_file_start_time(fNam)
%
% Get file starting time in local time (not using time in file name)
%
% tim=tof_get_file_start_time(fNam)
%
% tim  - file starting time in local time (datenum)
% fNam - file name

% Heikki Junninen
% Feb 2016

tim=[];

if exist(fNam,'file')
    st=krs_tof_parse_time_from_file_name(fNam);
    if isempty(st)
        d=hdf5read(fNam,'/','HDF5 File Creation Time');
        st=krs_tof_parse_date_string(d.Data);
    end
    if isempty(st)
        d=hdf5read(fNam,'/','HDF5 File Creation Time');
        tof_message(['Date format not supported ',d.Data]);
    else
        tim=st;
    end
else
    disp(['tof_get_file_start_time: File ',fNam,' not found!'])
end
