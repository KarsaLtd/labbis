function [en_y,yd1,yd2,yd3,yd4]=krs_tof_resolution_enhancement(y,enhanceFactor,enhanceFactor2,remTails)
%
% Enhance resolution of spectrum using derivative method
%
% [enhancedSpec,yd1,yd2,yd3,yd4]=tof_resolution_enhancement(spec,enhanceFactor1,enhanceFactor2,remTails)
%
% spec - spectrum
% enhanceFactor1 - weighting factor for second derivative addition.
% enhanceFactor2 - weighting factor for second derivative addition.
% remTails       - remove peak tails
% Bigger value increases the effect. Compromise between increased noise and
% enhancement have to be found experimentally
% enhancedSpec - resolution enhanced spectum
% yd1 - first derivative of the spectrum
% yd2 - second derivative of the spectrum
% yd3 - third derivative of the spectrum
% yd4 - forth derivative of the spectrum, only where yd2 is positive
%
% Resolution enhancement follows logic:
% enhancedSignal=signal-2ndDerivative*enhanceFactor1+4thDerivative*enhanceFactor2
% 2thDerivative = 2thDerivative value where 2stDerivative is positive and
%                        signal value/enhanceFactor1 where negative (will remove peak tails)
% 4thDerivative = 4thDerivative value where 2stDerivative is positive
%


% based on ideas of T. C. O'Haver,2006
% Heikki Junninen
% Mar 2011

if nargin==3
    remTails=0;
end

% first derivative over few steps, to reduce noise
yd0(1,:)=[0,y(2:end)-y(1:end-1)];
yd0(2,:)=[0,y(3:end)-y(1:end-2),0];
% yd0(3,:)=[0,0,y(5:end)-y(1:end-4),0,0];
yd1=tsmooth(mean(yd0),3);

%second derivative, over few steps, to reduce noise
yd02(1,:)=[0,yd1(2:end)-yd1(1:end-1)];
yd02(2,:)=[0,yd1(3:end)-yd1(1:end-2),0];
% yd02(3,:)=[0,0,yd1(5:end)-yd1(1:end-4),0,0];

yd2=tsmooth(mean(yd02),3);

%second derivative,
yd03(1,:)=[0,yd2(2:end)-yd2(1:end-1)];
yd03(2,:)=[0,yd2(3:end)-yd2(1:end-2),0];
% yd02(3,:)=[0,0,yd1(5:end)-yd1(1:end-4),0,0];

yd3=tsmooth(mean(yd03),5);

%forth derivative,
yd04(1,:)=[0,yd3(2:end)-yd3(1:end-1)];
yd04(2,:)=[0,yd3(3:end)-yd3(1:end-2),0];
yd04(3,:)=[0,0,yd3(5:end)-yd3(1:end-4),0,0];
yd04(4,:)=[0,0,0,yd3(7:end)-yd3(1:end-6),0,0,0];

yd4=tsmooth(mean(yd04),5);

%chose only values where second derivative is positive
Ipos=yd2>0;
if remTails
    yd2(Ipos)=y(Ipos)/enhanceFactor;
end

yd4(Ipos)=0;

% yd2(~Ipos)=yd2(~Ipos)*10;
% yd4(~Ipos)=yd4(~Ipos)*10;

%enhance signal
en_y=y-yd2*enhanceFactor+yd4*enhanceFactor2;
