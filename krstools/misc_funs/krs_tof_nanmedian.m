function m = krs_tof_nanmedian(dat,dim)
% Calculate median value while ignoring NaNs.
% m = nanmedian(dat,dim)
%
% m - median
% dat - data, can be matrix or vector
% dim - takes the median along the dimension dim of dat

% Heikki Junninen
% Aug 2017

if nargin == 1
    m = krs_tof_prctile(dat, 50);
else
    m = krs_tof_prctile(dat, 50, dim);
end
