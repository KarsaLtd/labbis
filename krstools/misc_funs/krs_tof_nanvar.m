function y = krs_tof_nanvar(dat)
% Calculate variance while ignoring NaNs.
%
%   See syntacs: var

% Heikki Junninen
% Aug 2017

% y = var(varargin{:},'omitnan');

[n,m]=size(dat);

avr=krs_tof_nanmean(dat);
nOk=sum(~isnan(dat));
y=krs_tof_nansum((dat-repmat(avr,n,1)).^2)./max(nOk-1,1);