function y = krs_tof_normpdf(x,my,sig)
% Caluclate Normal probability density function (pdf).
%   y = normpdf(x,my,sig)
%

% Heikki Junninen
% Aug 2017

if nargin < 2
    my = 0;
end
if nargin < 3
    sig = 1;
end

y=exp(-0.5*((x-my)./sig).^2)./(sqrt(2*pi).*sig);

