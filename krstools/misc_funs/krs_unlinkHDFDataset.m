function krs_unlinkHDFDataset(fNam, group, datasets, unlinkGroup)

% unlinkHDFDataset(fNam, group, datasets, unlinkGroup)
% Unlink datasets from a HDF-file. The unlinking is done inside a
% try-block, so there is no checking that the datasets actually exist. If
% the given group does not exist, nothing is done.
%
% in:
%   fNam        - path of the HDF-file
%   group       - parent group of the datasets
%   datasets    - name(s) of the dataset(s) (in cell form if more than one)
%   unlinkGroup - if set to true, also unlinks the group
%
% Example usage:
%   unlinkHDFDataset('data.h5', 'myData', {'dataset1', 'dataset2'}, true);

% Gustaf L�nn
% July 2013

if nargin == 3
    unlinkGroup = false;
end
if ~iscell(datasets)
    datasets = {datasets};
end
if group(1) ~= '/'
    group = ['/' group];
end

Gexist = krs_tof_exist_in_file(fNam, [group '/']);
if Gexist
    fileID = H5F.open(fNam, 'H5F_ACC_RDWR', 'H5P_DEFAULT');
    groupID = H5G.open(fileID, group);
    for i = 1:length(datasets)
        try
            H5G.unlink(groupID, datasets{i});
        end
    end
    H5G.close(groupID);
    if unlinkGroup
        H5L.delete(fileID, group, 'H5P_DEFAULT');
    end
    H5F.close(fileID);
end