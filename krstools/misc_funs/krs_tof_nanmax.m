function [p, pi] = krs_tof_nanmax(dat,dim)
%NANMAX Maximum value, ignoring NaNs.


narginchk(1,2)
if nargin==1
    dim=1;
    dat=dat(:);
end


dims=[2 1];
L=size(dat,dims(dim));

if dim==1
    p=NaN(1,L);
else
    p=NaN(L,1);
end
%index
pi=p;

pr=1;


    for i=1:L
        
        if dim==1
            d=dat(:,i);
        else
            d=dat(i,:);
        end
        
        d=d(:);
        
        %remove NaNs
        In=isnan(d);
        if any(In)
            d(In)=[];
        end
        
        if dim==1
            [p(pr,i),pi(pr,i)]=max(d);
        else
            [p(i,pr),pi(i,pr)]=max(d);
        end
    end

