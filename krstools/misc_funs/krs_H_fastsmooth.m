function [ya,wsn]=krs_H_fastsmooth(mz,y,param)
%
% Smoothing signal using fastsmooth, but changes smoothing window for each
% mz according to resolution
%
% param.amuRange
% param.a
% param.b
% param.R

%old version
ya=y;
% rmz=round(mz);
stp=param.step;
rmz=round(mz/stp)*stp;
wsn=NaN(param.amuRange(2),1);
[~, Imn]=min(abs(rmz-param.amuRange(1)));
amu1=rmz(Imn);
if amu1==0
    amu1=round(mz(Imn));
end
[~, Imn]=min(abs(rmz-param.amuRange(2)));
amu2=rmz(Imn);

% % for m=param.amuRange(1):stp:param.amuRange(2)
% amusStps=[1,amu1:stp:amu2];
% for m=amu1:stp:amu2
%     %     Isel1=mz>m-1 & mz<m+1;
%     %     Isel2=rmz(Isel1)==m;
%     Isel=rmz==m;
%     
%     R=param.R;
%     ws=m/R; %estimated peak width
%     %mz=((([1:length(xD)]-b)/a).^2);
%     wsns1=sqrt(m-ws)*param.a+param.b;
%     wsns2=sqrt(m+ws)*param.a+param.b;
%     wsn(m)=max(round((wsns2-wsns1)/2),3);
%     
%     %works better if window is odd
%     if ~rem(wsn(m),2)
%         wsn(m)=wsn(m)+1;
%     end
%     
%     %     ya_tmp=fastsmooth(y(Isel1),wsn(m));
%     ya_tmp=fastsmooth(y(Isel),wsn(m));
%     
%     %     ya(Isel)=ya_tmp(Isel2);
%     ya(Isel)=ya_tmp;
% end
% if isnan(wsn(1)),wsn(1)=wsn(find(~isnan(wsn),1));end
% wsn=MDrepl1(wsn);

% new version
% make smooting in 3 different window and then calculate weighted average
% tic
cases=4;
stp=round((amu2-amu1)/(cases-1));

h=0;
ya_tmp=NaN(cases,length(y));
weight=ya_tmp;
for m=amu1:stp:amu2
    h=h+1;
    %     Isel1=mz>m-1 & mz<m+1;
    %     Isel2=rmz(Isel1)==m;
    [~, Isel]=min(abs(mz-m));
    
    R=param.R;
    ws=m/R; %estimated peak width
    %mz=((([1:length(xD)]-b)/a).^2);
    wsns1=sqrt(m-ws)*param.a+param.b;
    wsns2=sqrt(m+ws)*param.a+param.b;
    wsn(m)=max(round((wsns2-wsns1)/2),3);
    
    %works better if window is odd
    if ~rem(wsn(m),2)
        wsn(m)=wsn(m)+1;
    end
%     ya_tmp(h,:)=fastsmooth(y,wsn(m));
    ya_tmp(h,:)=tsmooth(y,wsn(m));
    weight(:,Isel)=0;
    weight(h,Isel)=1;
end
weight(:,1)=0;
weight(1,1)=1;

weight(:,end)=0;
weight(end,end)=1;

weight=MDrepl1(weight')';
if isnan(wsn(1)),wsn(1)=wsn(find(~isnan(wsn),1));end
wsn=MDrepl1(wsn);
ya=sum(ya_tmp.*weight)./sum(weight,1);
% toc

