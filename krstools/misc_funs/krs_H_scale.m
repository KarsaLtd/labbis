function [sx,mins,maxs]=krs_H_scale(x,mn,mx,prc)
%
% Scale x colums 0-1
%
%
%

if nargin<4
    prc=100;
end
[n,m]=size(x);

if nargin == 1 % scale to [0, 1]
   mn = 0;
   mx = 1;
end

% scaling
mins=krs_tof_nanmin(x);
xm=x-repmat(mins,n,1);

% maxs=nanmax(xm);

maxs=krs_tof_prctile(xm,prc);
% disp(['H_scale: using ',num2str(prc),'% precentile, not max'])

sx=(xm./repmat(maxs,n,1))*(mx-mn)+mn;
% check if all values are identical in any column, if: set the values equal
% to the scale maximum value
maxZero = maxs == 0; 
if any(maxZero)
    if n == 1
        sx(:) = mx;
    else
        sx(:,maxZero) = mx;
    end
end