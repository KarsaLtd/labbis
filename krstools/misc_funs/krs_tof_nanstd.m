function y = krs_tof_nanstd(varargin)
% Calculate Standard deviation while ignoring NaNs.
%
%see syntacs: var

%Heikki Junninen
%Aug 2017

y = sqrt(krs_tof_nanvar(varargin{:}));
