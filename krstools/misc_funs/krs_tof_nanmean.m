function [m,nrOk] = krs_tof_nanmean(dat,dim)
% Calculate mean while ignore NaNs
%
% [m]=nanmean(dat,[dim]);
%
% m - calculated mean
% dat - data matrix or vector
% dim - dimension to be used

% Heikki Junninen
% Aug 2017


if nargin==1
    [s, nrOK]=krs_tof_nansum(dat);
else
    [s, nrOK]=krs_tof_nansum(dat,dim);
end
m=s./nrOK;
