function p=krs_tof_prctile(dat,prcs,dim)
%
% Calculate percentile for data, if matrix look each column (dim=1) or row (dim=2) separately.
% percentile is given in percents
%
% p=H_prctile(dat,prcs,dim)
%
% p - calculated precentiles
% dat - data, can be vector matrix
% prcs - precentile, can be scalar or vector
% dim - dimention
%

% Heikki Junninen
% Aug 2017

narginchk(2,3)
if nargin==2
    %if vector work along the longest dimention
    [n,m]=size(dat);
    if n==1
        dim=2;
    elseif m==1
        dim=1;
    else
        
        dim=1;
    end
end

if any(prcs<0) || any(prcs>100)
    error('percentile should be between 0 and 100')
end

dims=[2 1];
L=size(dat,dims(dim));

if dim==1
    p=NaN(1,L);
else
    p=NaN(L,1);
end
for pr=1:length(prcs)
    prc=prcs(pr);
    for i=1:L
        
        if dim==1
            d=dat(:,i);
        else
            d=dat(i,:);
        end
        
        d=d(:);
        
        %remove NaNs
        In=isnan(d);
        if any(In)
            d(In)=[];
        end
        L2=length(d);
        a=(L2+0.99)*prc/100;
        indx1=max(floor(a),1);
        indx2=min(ceil(a),L2);
        
        d=sort(d);
        if dim==1
            p(pr,i)=mean(d([indx1,indx2]));
        else
            p(i,pr)=mean(d([indx1,indx2]));
        end
    end
end