function [S,y,mo,d,h,min,s] = H_datestr(D,dateform,pivotyear)
%DATESTR String representation of date.
%   DATESTR(D,DATEFORM) converts a serial data number D (as returned by
%   DATENUM) or a free format date string into a date string with a
%   format specified by format number or string DATEFORM (see table below).
%   By default, DATEFORM is 1, 16, or 0 depending on whether D contains
%   dates, times or both. Date strings with 2 character years are interpreted
%   to be within the 100 years centered around the current year.
%
%   DATESTR(D,DATEFORM,PIVOTYEAR) uses the specified pivot year as the
%   starting year of the 100-year range in which a two-character year
%   resides.  The default pivot year is the current year minus 50 years.
%   DATEFORM = -1 uses the default format.
%
%   DATEFORM number   DATEFORM string         Example
%      0             'dd-mmm-yyyy HH:MM:SS'   01-Mar-2000 15:45:17
%      1             'dd-mmm-yyyy'            01-Mar-2000
%      2             'mm/dd/yy'               03/01/00
%      3             'mmm'                    Mar
%      4             'm'                      M
%      5             'mm'                     03
%      6             'mm/dd'                  03/01
%      7             'dd'                     01
%      8             'ddd'                    Wed
%      9             'd'                      W
%     10             'yyyy'                   2000
%     11             'yy'                     00
%     12             'mmmyy'                  Mar00
%     13             'HH:MM:SS'               15:45:17
%     14             'HH:MM:SS PM'             3:45:17 PM
%     15             'HH:MM'                  15:45
%     16             'HH:MM PM'                3:45 PM
%     17             'QQ-YY'                  Q1-96
%     18             'QQ'                     Q1
%     19             'dd/mm'                  01/03
%     20             'dd/mm/yy'               01/03/00
%     21             'mmm.dd,yyyy HH:MM:SS'   Mar.01,2000 15:45:17
%     22             'mmm.dd,yyyy'            Mar.01,2000
%     23             'mm/dd/yyyy'             03/01/2000
%     24             'dd/mm/yyyy'             01/03/2000
%     25             'yy/mm/dd'               00/03/01
%     26             'yyyy/mm/dd'             2000/03/01
%     27             'QQ-YYYY'                Q1-1996
%     28             'mmmyyyy'                Mar2000
%     29 (ISO 8601)  'yyyy-mm-dd'             2000-03-01
%     30 (ISO 8601)  'yyyymmddTHHMMSS'        20000301T154517
%     31             'yyyy-mm-dd HH:MM:SS'    2000-03-01 15:45:17
%     32             'HH'                     15 (my addition)
%     33             'ddD'                    01D (my addition)
%     34             'yyyymmdd'               20000301 (my addition)
%     35             'yymmdd'                 200301 (my addition)
%     36             'yyyymmddHHMMSS'         20000301154517 (my addition)
%     37             'yyyymmdd'               2000Mar01 (my addition)
%     38             'dd.mmm'                 01.Mar (my addition)
%
% if more than one output:
% [S,y,mo,d,h,min,s] = H_datestr(D,dateform)
% S = dateString
% y = year
% mo= month
% d = day
% h = hour
% min = minute
% s = second
%
%   See also DATE, DATENUM, DATEVEC, DATETICK.

% Heikki Junninen 270804

% Convert strings and clock vectors to date numbers.
if isstr(D) | iscell(D) | (size(D,2)==6 & ...
        all(all(D(:,1:5)==fix(D(:,1:5)))) & all(abs(sum(D,2)-2000)<500))
    if nargin < 3
        D = datenum(D);
    else
        D = datenum(D,pivotyear);
    end
end

% Determine format if none specified.  If all the times are zero,
% display using date only.  If all dates are all zero display time only.
% Otherwise display both time and date.

D = D(:);
if (nargin < 2) | (dateform == -1)
    if all(floor(D)==D),
        dateform = 1;
    elseif all(floor(D)==0)
        dateform = 16;
    else
        dateform = 0;
    end
end

% Handle the empty case properly.  Return an empty which is the same
% length of the string that is normally returned for each dateform.
if isempty(D)
    try
        S= zeros(0,length(datestr(now,dateform)));
    catch
        error(lasterr);
    end
    return;
end

% Determine from string.

if isstr(dateform)
    switch dateform
        case 'dd-mmm-yyyy HH:MM:SS', dateform = 0;
        case 'dd-mmm-yyyy', dateform = 1;
        case 'mm/dd/yy', dateform = 2;
        case 'mmm', dateform = 3;
        case 'm', dateform = 4;
        case 'mm', dateform = 5;
        case 'mm/dd', dateform = 6;
        case 'dd', dateform = 7;
        case 'ddd', dateform = 8;
        case 'd', dateform = 9;
        case 'yyyy', dateform = 10;
        case 'yy', dateform = 11;
        case 'mmmyy', dateform = 12;
        case 'HH:MM:SS', dateform = 13;
        case 'HH:MM:SS PM', dateform = 14;
        case 'HH:MM', dateform = 15;
        case 'HH:MM PM', dateform = 16;
        case 'QQ-YY', dateform = 17;
        case 'QQ', dateform = 18;
        case 'dd/mm', dateform = 19;
        case 'dd/mm/yy', dateform = 20;
        case 'mmm.dd,yyyy HH:MM:SS', dateform = 21;
        case 'mmm.dd,yyyy', dateform = 22;
        case 'mm/dd/yyyy', dateform = 23;
        case 'dd/mm/yyyy', dateform = 24;
        case 'yy/mm/dd', dateform = 25;
        case 'yyyy/mm/dd', dateform = 26;
        case 'QQ-YYYY', dateform = 27;
        case 'mmmyyyy', dateform = 28;
        case 'yyyy-mm-dd', dateform = 29;
        case 'yyyymmddTHHMMSS', dateform = 30;
        case 'yyyy-mm-dd HH:MM:SS', dateform = 31;
        case 'HH', dateform = 32;
        case 'dd_HH', dateform = 33;
        case 'yyyymmdd', dateform = 34;
        case 'yyyymmmdd', dateform = 37;
        case 'dd.mmm', dateform = 38;
        otherwise
            error(['Unknown date format: ' dateform])
    end
end

mths = real(['Jan';'Feb';'Mar';'Apr';'May';'Jun';
    'Jul';'Aug';'Sep';'Oct';'Nov';'Dec']);

% Obtain components using mex file, rounding to integer number of seconds.

[y,mo,d,h,min,s] = datevecmx(D,1.);  mo(mo==0) = 1;

if dateform<31
    [S] = datestr(D,dateform);
else
    
    % Vectorization is done within sprintf.
    switch dateform
        
        case 32   % 'HH'
            f = '%.2d';
            S = sprintf(f,[h]');
        case 33   % 'dd_HH'
            f = '%.2d%c';
            
            Id=find(h<=20 & h>8);
            In=find(h>20 | h<=8);
            Inhomme=find(h>20);
            d(Inhomme)=d(Inhomme)+1;
            c=ones(size(h,1),1);
            c(Id)=double('D');
            c(In)=double('N');
            
            S = sprintf(f,[d,char(c)]');
            
        case 34   % 'yyyymmdd'
            f = '%c%.3d%.2d%.2d';
            c = '0'+fix(mod(y,10000)/1000); c(y<0) = '-';
            S = sprintf(f,[c,mod(abs(y),1000),mo,d]');
        case 35   % 'yymmdd'
            f = '%.2d%.2d%.2d';
            S = sprintf(f,[mod(abs(y),1000),mo,d]');
        case 36   % 'yyyymmddTHHMMSS'
            f = '%c%.3d%.2d%.2d%.2d%.2d%.2d';
            c = '0'+fix(mod(y,10000)/1000); c(y<0) = '-';
            S = sprintf(f,[c,mod(abs(y),1000),mo,d,h,min,round(s)]');
            %     case 37
            %         f = '%2.0d';
            %         [wn,c]=H_weeknumber(y,mo,d);
            %         S = sprintf(f,[wn]');
        case 37   % 'yyyymmmdd'
            f = '%c%.3d%c%c%c%.2d';
            c = '0'+fix(mod(y,10000)/1000); c(y<0) = '-';
            S = sprintf(f,[c,mod(abs(y),1000),mths(mo,:),d]');
        case 38   % 'dd.mmm'
            f = '%.2d.%c%c%c';
            c = '0'+fix(mod(y,10000)/1000); c(y<0) = '-';
            S = sprintf(f,[d,mths(mo,:)]')
        case 39   % 'yyyy.mm.dd'
            f = '%c%.3d.%.2d.%.2d';
            c = '0'+fix(mod(y,10000)/1000); c(y<0) = '-';
            S = sprintf(f,[c,mod(abs(y),1000),mo,d]');
        otherwise
            error(['Unknown date format number: ' int2str(dateform)])
    end
S = reshape(S',length(S)/length(D),length(D))';
end

