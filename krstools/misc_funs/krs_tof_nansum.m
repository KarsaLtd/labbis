function [s,nrOK]=krs_tof_nansum(dat,dim)
%
% Calculate sum while ignore NaNs
%
% [s, nrOK]=nansum(dat,[dim]);
%
% s - calculated sum
% nrOK - number of ok datapoints
% dat - data matrix or vector
% dim - dimension to be used
%
% See more: sum
%


% Heikki Junninen
% Aug 2017

%replace NaNs with zeros
In=isnan(dat);
dat(In)=0;

%calculate sum
if nargin==1
    s=sum(dat);
    nrOK=sum(In==0);
else
    s=sum(dat,dim);
    nrOK=sum(In==0,dim);
end
