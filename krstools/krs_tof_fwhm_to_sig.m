function sig=krs_tof_fwhm_to_sig(fwhm)
%
% Convert full-width-at-half-maximum (fwhm) to sigma parameter from gaussian fit
%
% sig=tof_fwhm_to_sig(fwhm)
%

%Heikki Junninen
%July 2011

sig=fwhm/(2*sqrt(2*log(2)));
