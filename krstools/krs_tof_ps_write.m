function krs_tof_ps_write(fNam, peakShape)

% tof_ps_write(fNam, peakShape)
% Helper function, writes a peak shape to a preprocessed file. If the file
% already contains a peak shape with the same ID as the peak shape given,
% the script doesn't do anything.
%
% In:
%     fNam      - path of the preprocessed file
%     peakShape - peak shape structure with at least the fields 'dat' and
%                 'par', 'id' is calculated if not present

% Gustaf L�nn
% September 2012

if ~isfield(peakShape, 'id')
    peakShape.id = krs_tof_ps_generate_id(peakShape.dat(:,2));
end

[~, PSexist] = krs_tof_exist_in_file(fNam, '/Peaks/PeakShape');
if PSexist
    try
        curId = hdf5read(fNam, '/Peaks/PeakShape', 'id');
        curId = curId.Data;
        idPresent = true;
    catch
        % no ID found, generate ID from data
        idPresent = false;
        ps = hdf5read(fNam, '/Peaks/PeakShape')';
        curId = krs_tof_ps_generate_id(ps(:,2));
    end
    if strcmp(peakShape.id, curId) % same peak shape
        if ~idPresent % write ID if not present
            krs_tof_write_attributes(fNam, {'id', peakShape.id}, '/Peaks/PeakShape');
        end
        return;
    end
    krs_tof_message('Overwriting the current peak shape.', 'warn');
    % delete old peak shape
    fId = H5F.open(fNam, 'H5F_ACC_RDWR', 'H5P_DEFAULT');
    H5L.delete(fId, 'Peaks/PeakShape', 'H5P_DEFAULT');
    H5F.close(fId);
end

% write new peak shape
krs_tof_write_HDF(fNam, '/Peaks/PeakShape', peakShape.dat, 'memType', 'H5T_NATIVE_FLOAT');
krs_tof_write_attributes(fNam, {'par1', 0, 'par2', 0, 'id', peakShape.id}, '/Peaks/PeakShape');