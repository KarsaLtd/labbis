function [outlist, peaklist] = krs_tof_get_HR_sticks_timeseries(fNam, mz, varargin)

% [outlist, peaklist] = tof_get_HR_sticks_timeseries(fNam, mz, varargin)
% Retrieves HR sticks time series for given compounds from a preprocessed
% file. The default behavior is to use PeakDataGauss, but if this dataset
% is not present, PeakData will be used.
%
% in:
%     fNam     - name of file containing preprocessed data
%     mz       - mass/composition of the compound(s) to get time series for
%     varargin - optional arguments in the form of property, value
%       'accuracy'  - determines the interval in which to search for peaks
%       'doplot'    - 1 plots the results, 0 doesn't
%       'peaklist'  - raw peak data from preprocessed file
%       'plotsave'  - saves all plotted graphs as figures
%       'simul'     - plot all masses to the same figure, default is 1
%       'normalize' - scales the signal of all time series to [0, 1]
%       'dataset'   - specifies which dataset to use, can be 'PeakData' or
%                     'PeakDataGauss'
%       'spec'      - only extracts data from the given spectra
%
% out:
%     outlist  - cell array, every cell contains a matrix with the columns
%                time, mass, area, height and mass error in ppm
%     peaklist - original raw peak data from preprocessed file
%
% usage:
%     ts = tof_get_HR_sticks_timeseries('data.hdf',{'HSO4-','NO3-'},'doplot',1);
%

% Gustaf L?nn
% March 2011

%% interpret arguments and read data

accuracy = 0.01;
doPlot = 0;
rdattmp = 0;
plotSave = 0;
normalize = 0;
timeRange = 0;
spec = [];
hol = 0;
outlist={[]};
peaklist=[];
dataset = '/Peaks/PeakDataGauss';
forceDataset = 1;

if ~exist(fNam, 'file')
    krs_tof_message(['Cannot open file ' fNam '.'], 'err');
    return;
end

if ischar(mz)
    simul = 0;
elseif length(mz) > 1
    simul = 1;
else
    simul = 0;
end

for i = 1:length(varargin)/2
    switch(lower(varargin{2*i-1}))
        case {'accuracy'}
            accuracy = varargin{2 * i};
        case {'doplot'}
            doPlot = varargin{2 * i};
        case {'peaklist'}
            rdattmp = 1;
            peaklist = varargin{2 * i};
        case {'plotsave'}
            plotSave = varargin{2 * i};
        case {'simul'}
            simul = varargin{2 * i};
        case {'normalize'}
            normalize = varargin{2 * i};
        case {'timerange'}
            timeRange = varargin{2 * i};
        case {'hold'}
            hol = varargin{2 * i};
        case {'dataset'}
            if strcmpi(varargin{2 * i}, 'PeakData')
                dataset = '/Peaks/PeakData';
            elseif strcmpi(varargin{2 * i}, 'PeakDataGauss')
                dataset = '/Peaks/PeakDataGauss';
            else
                dataset = varargin{2 * i};
            end
            forceDataset = 1;
        case {'spec'}
            spec = varargin{2 * i};
    end
end

convcoef = krs_tof_get_convcoef(fNam);
% convcoef = convcoef(Imn);

if iscell(mz)
    plotAmu = krs_tof_exact_mass(mz);
else
    if ischar(mz)
        plotAmu = krs_tof_exact_mass(mz);
    else
        plotAmu = mz;
    end
end

if ~rdattmp
    dataIso = [];
    try
        data = hdf5diskmap(fNam, dataset);
        if strcmp(dataset, '/Peaks/PeakDataGauss');
            try
                dataIso = hdf5read(fNam, '/Peaks/PeakDataGaussIso');
                dataIso = dataIso(1:7,:);
            end
        end
    catch
        if ~forceDataset
            try
                data = hdf5diskmap(fNam,'/Peaks/PeakData');
                disp('tof_get_HR_sticks_timeseries: Dataset PeakDataGauss not found, using PeakData instead.');
            catch ME
                ME.message
                return;
            end
        else
            disp(['tof_get_HR_sticks_timeseries: Dataset ' dataset ' not found.']);
            return
        end
    end
    tim = hdf5read(fNam,'/tofData/time');
    res = hdf5read(fNam,'/tofData','Averaging time step [min]');
end

if doPlot && ~plotSave && ~hol
    if strcmp(get(gcf,'Tag'),'tof_progress_bar')
        figures = findall(0,'FileName','');
        figures(figures==gcf) = [];
        if ~isempty(figures)
            set(0,'CurrentFigure',figures(1));
            clf;
        else
            figure;
        end
    else
        clf;
    end
end

if ~rdattmp
    %    peaklist = zeros(7,size(data,1));
    peaklist = data(1:7,1:size(data,1))';
    if ~isempty(dataIso)
        peaklist = [peaklist;dataIso']; %STUPID!
        disp('fix HR sticks isotopes')
        peaklist = sortrows(peaklist);
    end
    %{
    chunk = 50000;
    for j = 1:ceil((size(data,1)) / chunk);
        if (j-1)*chunk + chunk > size(data,1)
            peaklist(1:7,(j-1)*chunk+1:size(data,1)) = data(1:7,(j-1)*chunk+1:size(data,1));
        else
            peaklist(1:7,(j-1)*chunk+1:(j-1)*chunk+chunk) = data(1:7,(j-1)*chunk+1:(j-1)*chunk+chunk);
        end
    end
    %}
    if ~isempty(spec)
        peaklist = peaklist(ismember(peaklist(:, 1), spec), :);
        tim = tim(spec);
        outInds(spec) = 1:length(spec);
    end
end

if plotSave
    dirName = fNam(1:length(fNam)-3);
    [~,~,~] = mkdir(dirName);
    if ~simul
        progressbar(0,[0.1 0.9]);
    end
    if ~doPlot
        doPlot = 1;
        %disp('Automatically set doPlot to 1.');
    end
end

outlist = cell(length(plotAmu),1);

for u = 1:length(plotAmu)
    %% finds peaks within range
    %amus = peaklist(:,2);
    %Isel = find(amus > plotAmu(u) - accuracy & amus < plotAmu(u) + accuracy);
    %Isel = find(peaklist(:,2) > plotAmu(u) - accuracy & peaklist(:,2) < plotAmu(u) + accuracy);
    Isel = find(abs(peaklist(:,2) - plotAmu(u)) < accuracy);
    amus = peaklist(Isel,2);
    if length(Isel)>0
        specNr = peaklist(Isel,1);
        areas = peaklist(Isel,5); %ions / sec
        currConv = convcoef(peaklist(Isel,1));
        hghts=peaklist(Isel,3) .* currConv(:); %ions / sec
        errors = (amus - plotAmu(u)) / plotAmu(u) * 1e6;
        tab = [specNr amus areas hghts errors];
        
        %% #peaks per spectra > 1 => choose the best one
        [uniqueSpec uniqueInds] = unique(tab(:,1),'rows','last');
        if length(uniqueSpec) ~= size(tab,1);
            uniqueInds = [0;uniqueInds];
            uniqueIndsDiff = diff(uniqueInds);
            multiplePeaksInds = find(uniqueIndsDiff ~= 1)';
            for j = multiplePeaksInds
                curSpecInds = find(tab(:,1) == uniqueSpec(j));
                [~, minInd] = min(abs(tab(curSpecInds,2) - plotAmu(u)));
                tab(curSpecInds(1),:) = tab(curSpecInds(1) + minInd - 1,:);
            end
            [~, s] = unique(tab(:,1),'first');
            tab = tab(s,:);
        end
        
        %% construct outlist (NaN is used for spectras with no peaks within range)
        outlist{u} = NaN(size(tim,2),5);
        outlist{u}(:,1) = tim(1,:);
        if isempty(spec)
            outlist{u}(tab(:,1),2) = tab(:,2);
            outlist{u}(tab(:,1),3) = tab(:,3);
            outlist{u}(tab(:,1),4) = tab(:,4);
            outlist{u}(tab(:,1),5) = tab(:,5);
        else
            outlist{u}(outInds(tab(:,1)),2) = tab(:,2);
            outlist{u}(outInds(tab(:,1)),3) = tab(:,3);
            outlist{u}(outInds(tab(:,1)),4) = tab(:,4);
            outlist{u}(outInds(tab(:,1)),5) = tab(:,5);
        end
        
        if (plotSave && ~simul) || (plotSave && simul && u == 1)
            h = figure;
        end
    end
end

if plotSave && simul
    saveas(h,[dirName,filesep,'simulplot',datestr(now,'yyyymmddHHMMSS'),'.fig']);
    close(h);
end

end