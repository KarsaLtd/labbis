function out = krs_tof_exact_isotope_masses(compound,limit,helper)

% Calculates isotopic abundances for the given compound(s).
%
% in:
%   compound - string or cell of compound name(s)
%   limit    - if > 1: maximum number of rows to include in output
%              if <= 1: lowest abundance to include in output
%   helper   - internal usage only
%
% out:
%   out      - matrix or cell of matrices, first column is mass, second is
%              abundance
%
% usage:
%   abundList = tof_exact_isotope_masses({'HSO4-','NO3-'},1e-4);

% March 2012
% Gustaf L�nn

global isoAbundStruct

if isempty(isoAbundStruct)
    load('isoAbundStruct');
end
electron = 0.0005485799;

if nargin == 1
    limit = 0;
    helper = [];
elseif nargin == 2
    helper = [];
end

dist = [];

if ~iscell(compound)
    out = [];
    compound = {compound};
else
    out = cell(length(compound),1);
end

compound = krs_get_composition(compound);

if isempty(helper)
    [~,namEl,numEl] = krs_tof_exact_mass(compound);
else
    namEl = helper.elNam;
    numEl = helper.elNum;
end
for j = 1:length(namEl)
    isNumerical = isnumeric(compound{j}) || ~isnan(str2double(compound{j}));
    if ~isNumerical
        isoPresent = ~isempty(regexp(compound{j},'[','once'));
        if isoPresent
            %separate isotopes from compound and calculate isotopic pattern
            %only for non-isotopically labeled elements
            isoElem = find(~cellfun(@isempty, regexp(namEl{j}, '[')));
            isoPart = [];
            for i = 1:length(isoElem)
                isoPart = [isoPart namEl{j}{isoElem(i)} sprintf('%d', numEl{j}(isoElem(i)))];
            end
            isoMass = krs_tof_exact_mass(isoPart);
            namEl{j}(isoElem) = [];
            numEl{j}(isoElem) = [];
        end
    end
    
    if ~isempty(namEl{j}) && ~isNumerical
        for i = 1:length(namEl{j})
            ind = [namEl{j}{i},sprintf('%d',(numEl{j}(i)))];
            if ~isfield(dist,ind)
                if ~isfield(isoAbundStruct,ind)
                    useLimit = limit;
                    if limit < 1
                        useLimit = 10;
                    end
                    dist.(ind) = isoDalton_exact_mass(ind,useLimit);
                else
                    if limit == 0
                        useInds = 1:size(isoAbundStruct.(ind),1);
                    elseif limit <= 1
                        useInds = isoAbundStruct.(ind)(:,2) > limit;
                    else
                        useInds = 1:min(limit,size(isoAbundStruct.(ind),1));
                    end
                    dist.(ind) = isoAbundStruct.(ind)(useInds,:);
                end
            end
            if i == 1
                c = dist.(ind)(:,2);
                mz = dist.(ind)(:,1);
            else
                nmz = dist.(ind)(:,1);
                mz = mz(:,ones(size(nmz,1),1))+nmz(:,ones(size(mz,1),1))';
                mz = reshape(mz,numel(mz),1);
                c = c * dist.(ind)(:,2)';
                c = reshape(c,numel(c),1);
                if limit <= 1 && limit > 0
                    dEl = c<limit;
                    c(dEl) = [];
                    mz(dEl) = [];
                end
            end
        end
        if compound{j}(end) == 43
            mz = mz - electron;
        elseif compound{j}(end) == 45
            mz = mz + electron;
        end
        if isoPresent
            mz = mz + isoMass;
        end
        
    elseif isNumerical || isoPresent
        mz = krs_tof_exact_mass(compound{j});
        c = 1;
    else
        mz = compound{j};
        c = 1;
    end
    abundList = [mz,c];
    abundList = sortrows(abundList,-2);
    if limit > 1
        abundList = abundList(1:min(limit,size(abundList,1)),:);
    end
    if length(namEl) == 1
        out = abundList;
    else
        out{j} = abundList;
    end
end