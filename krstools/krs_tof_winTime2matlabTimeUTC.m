function tim=krs_tof_winTime2matlabTimeUTC(winTim)
%
% Convert windows time to matlab datanum
% Windows time is the 100 ns intervals since January 1, 1601 (UTC)
%
% tim=tof_winTime2matlabTime(winTim)
%


%Heikki Junninen
% Nov 2011

tim=(double(winTim)*100/1e9)/(60*60*24)+datenum(1601,1,1); %UTC
