function [par,ppm,mz,calib_best_mz,calib_best_sn]=krs_tof_mz_calib_parab(mz,spec,mzForm,a,b)
%
% mz-calibration to given set of compounds by detecting peaks from spectra.
% Best combination of sub-set of masses is evaluated by brute force method.
%
% Goodness of sub-set of peaks is measured with function:
%     min(ppms/acc+(1-mzRange/mxMz)+(1-totSig/sumY));
% Where,
% ppms - accuracy of mass calibration using selection of masses
% acc  - nominal accuracy of an instrument, set to 10ppm
% mzRange - range of sub-set of calibration masses
% mxMz - maximum mass in spectrum
% totSig - total signal of each sub-set
% sumY - total signal of all given calibration masses
%
% [par,ppm,mz,calib_best_mz,calib_best_sn]=tof_mz_calib_parab(mz,spec,mzParm)
%
% outputs:
% par - parameters for mass calibration, size depends on function used
% ppm - accuracy of mass calibration
% mz  - calibrated mass axes
% calib_best_mz - best masses used for calibration
% calib_best_sn - best sample numbers
%

%Heikki Junninen
% Dec 2009

doPlot = 0;
sn = 0;
if isstruct(mzForm)
    %in input is mzCalib_struct
    mzCalibStr=mzForm;
    a=mzCalibStr.a;
    b=mzCalibStr.b;
    equation=mzCalibStr.equation;
    mxNrMasses=mzCalibStr.mxNrMasses;
    mzForm=mzCalibStr.names;
    m=mzCalibStr.masses;
    ps = mzCalibStr.peakShape;
    usedEQ = mzCalibStr.usedEQ;
    funcArgs = usedEQ();
    usedPar = mzCalibStr.usedPar;
    doPlot = mzCalibStr.doPlot;
    if isfield(mzCalibStr,'sn')
        sn = mzCalibStr.sn;
        ymx = spec(round(sn));
    end
    if isempty(mxNrMasses)
        mxNrMasses=length(m)-1;
    end
elseif iscell(mzForm)
    m=krs_tof_exact_mass(mzForm);
    equation = @krs_tof_fit_tof_mz;
    mxNrMasses=length(m)-1;
elseif isnumeric(mzForm)
    m=mzForm;
    equation = @krs_tof_fit_tof_mz;
    mxNrMasses=length(m)-1;
end

m=m(:)';

% rx=round(mz);
% rg=round(m);

%instrument nominal accuracy
acc=10;

noiseLimCoef=3;

%most frequent value in spectrum, though as single count=noise level
freqVal=mode(spec(spec~=0));
%median of absolute differences of consecutive values=noise
medDif=median(abs(diff(spec)));

% take limit for exclusion as the bigger one of the noise estimate times noiseLimCoef
noise=max([freqVal,medDif])*noiseLimCoef;

if ~isempty(ps)
    param.R = [];
    param.peakShape = ps;
end

%if doPlot
%    figure;
%end

if ~isfield(mzCalibStr,'sn')
    
    for i=1:length(m)
        
        %     Isel=find(rx==rg(i));
        %Isel=find(mz>=m(i)-0.5 & mz<=m(i)+0.5);
        step = max(0.5);%,m(i)*2e-3);
        %disp('tof_mz_calib_parab: max peak search window 3amu!!!!!')
        fInd = ceil(funcArgs.inverseEQ(m(i)-step, usedPar));
        lInd = floor(funcArgs.inverseEQ(m(i)+step, usedPar));
        Isel = fInd:lInd;
        x=mz(Isel)';
        y=spec(Isel)';
        [ny,my]=size(y);
        nrP=1;
        %     [p1]=prctile(y,[75,98]);
        %     SN=mldivide(p1(2),p1(1));
        if isempty(ps)
            try
                peak_threshold = 0.5*max(y);
                peakInd = krs_peakFind(y', peak_threshold); % Find peaks
                [min_err, closeInd] = min(m(i)-x(peakInd)); % Select closest peak
                Imn = peakInd(closeInd);
                mx = y(Imn);
                Iselsel=[max(Imn-2,1):min(Imn+2,my)];
                %         sn=tof_peak_center([Isel(Imn)-2:Isel(Imn)+2]',y([Imn-2:min(Imn+2,m)]));
                sn=krs_tof_peak_center(Isel(Iselsel),y(Iselsel));
            catch
                disp(lasterr)
                sn=NaN;
            end
        else
            peak_threshold = 0.5*max(y);
            peakInd = krs_peakFind(y', peak_threshold); % Find peaks
            [min_err, closeInd] = min(m(i)-x(peakInd)); % Select closest peak
            Imn = peakInd(closeInd);
            mx = y(Imn);
            [ytot,~,~,~,par1] = krs_tof_fit_peaks(double(x),double(y),double(x(Imn)),param);
            if par1(2) > x(1) && par1(2) < x(end)
                sn = funcArgs.accurateInverseEQ(par1(2), usedPar);
            else
                sn = NaN;
            end
            %if doPlot
            %    subplot(1,length(m),i);plot(x,y,x,ytot);
            %end
        end
        peakD(i).xDat=x;
        peakD(i).yDat=y;
        peakD(i).sampleNums=sn;
        peakD(i).ymx=mx;
    end
    
    sn=double(cat(2,peakD.sampleNums));
    sn=sn(:)';
    ymx=cat(2,peakD.ymx);
    % remove peaks that are smaller than noise
    Isml=ymx<noise;
    if any(Isml)
        indsBad = find(Isml);
        dispMasses = [];
        for i = indsBad
            if isa(mzForm{i}, 'string')
                dispMasses = [dispMasses mzForm{i} ', '];
            else
                dispMasses = [dispMasses num2str(mzForm{i}) ', '];
            end
        end
        dispMasses(end-1:end) = [];
        disp(['tof_mz_calib_parab: masses close to noise level are excluded: ',dispMasses]);
        sn(Isml)=[];
        ymx(Isml)=[];
        m(Isml)=[];
    end
    
end



a_in=a;
b_in=b;
funcArgs = equation();

% Fix NaN bug
nanind = isnan(sn);
sn = sn(~nanind);
m = m(~nanind);
ymx = ymx(~nanind);
mxNrMasses = sum(~nanind);

if mxNrMasses>sum(~isnan(sn))
    par=NaN(1,funcArgs.numOfPars);
    ppm=NaN;
    calib_best_mz=NaN;
    calib_best_sn=NaN;
    disp('tof_mz_calib_parab: not enough peaks!')
    mz = NaN;
else
    if funcArgs.useOriginalParams
        if funcArgs.numOfInitValues == 2
            init = [a_in, b_in];
        else
            init = [a_in, b_in, 2];
        end
    else
        init = funcArgs.initValues;
    end
    %if nrParam==2
    %    init=[a_in,b_in];
    %else
    %    init=[a_in,b_in,2];
    %end
    [calib_best_mz,par,ppm,calib_best_sn]=best_combination(m, sn, ymx, init, mxNrMasses, equation, funcArgs,max(mz),acc);
    %a=par(1);
    %b=par(2);
    %if nrParam==2
    %    p=2;
    %else
    %    p=par(3);
    %end
    if doPlot
        mz = funcArgs.usedEQ(1:length(mz),par);
    else
        mz = funcArgs.usedEQ(length(mz),par);
    end
end


%% SUBFUNCTIONS
function [calib_best_mz,par,ppm_mn,calib_best_sn]=best_combination(ms, sn, ymx, init, mxNrMasses, equation, funcArgs,mxMz,acc)
%calc all combination of masses

nrPeaks=length(ms);
if nrPeaks>=mxNrMasses
    ch=nchoosek(1:nrPeaks,mxNrMasses);
else
    ch=1:nrPeaks;
end

%mz-tof fitting
nrCh=size(ch,1);
pars=NaN(nrCh,funcArgs.numOfPars);
ppms=NaN(nrCh,1);
%ppms_all=NaN(nrCh,1);
%{
if nrParam==2
    fun=@tof_fit_tof_mz;
    opt = optimset('Display','off',...
        'TolFun',1e-6,'TolX',1e-12,'MaxFunEvals',1000);
    %     init=[a_in,b_in];
else
    opt = optimset('GradObj','off','Display','off','Diagnostics','off',...
        'TolFun',1e-6,'TolX',1e-12,'MaxFunEvals',500,'Algorithm', 'active-set');
    fun=@tof_fit_tof_mz_3p;
    %     init=[a_in,b_in,2];
end
%}
if isfield(funcArgs,'options')
    opt = funcArgs.options;
else
    opt = optimset('Display','off','TolFun',1e-6,'TolX',1e-12,'MaxFunEvals',1000);
end
for i=1:nrCh
    par1=equation(ms(ch(i,:)),sn(ch(i,:)),init,opt);
    pars(i,:)=par1;
    % newMZ=(([1:nbrSamples]-b)/a).^2;
    %a=par1(1);
    %b=par1(2);
    %if nrParam==2
    %    p=2;
    %else
    %    p=par1(3);
    %end
    % goodness
    %test_mz=((sn(ch(i,:))-b)/a).^p;
    test_mz = funcArgs.usedEQ(sn(ch(i,:)),par1);
    ppms(i)=mean(abs(((ms(ch(i,:))-test_mz)./ms(ch(i,:)))*1000000));
    %all masses
    %test_mz=((sn-b)/a).^p;
    %test_mz = funcArgs.usedEQ(sn,par1);
    %ppms_all(i)=mean(abs(((ms-test_mz)./ms)*1000000));
end
sumY=sum(ymx);
if size(ch,1)>1
    mzRange=krs_tof_range(ms(ch),2);
    %     ppms_r=round(ppms/10)*10; %ppms in 10ppm step
    totSig=sum(ymx(ch),2);
    %     totSig=round(H_scale(totSig)*5)/5;
    %     [s,Is]=sortrows([ppms_r,1./mzRange,1./totSig]); %find smallest ppm (accuracy 5ppm) that has highest range
    %      [~,Imn]=min([H_scale(ppms)+H_scale(1./mzRange)+H_scale(1./totSig)]);
    [~,Imn]=min(ppms/acc+(1-mzRange/mxMz)+(1-totSig/sumY));
    %     Imn=Is(1);
    ppm_mn=ppms(Imn);
else
    Imn=1;
    ppm_mn=ppms;
end
par=pars(Imn,:);
% calib_best_nam=calib_nam(ch(Imn,:));
calib_best_mz=ms(ch(Imn,:));
calib_best_sn=sn(ch(Imn,:));