function out = krs_tof_fit_n_peaks(mz, spec, args)

% out = tof_fit_n_peaks(mz, spec, args)
% Determines how many peaks that can be fitted reliably to the given data
% and outputs the parameters used.
%
% In:
%     mz   - mass axis
%     spec - spectrum data
%     args - structure with the following fields:
%       'peakshape' - peak shape
%       'rfun'      - resolution function
%       'peaklist'  - peak list, these peaks are always included
%       'tolerance' - maximum uncertainty in mass for peaks not in the peak
%                     list
%       'limit'     - maximum number of peaks to fit to a single amu
%       'threshold' - a new peak is included only if the norm of the
%                     residual of the new fit is smaller than 'threshold'
%                     times the norm of the residual of the old previous
%                     fit
%       'init'      - initial guesses for peak positions
%
% Out:
%     out  - matrix containing information about the fitted peaks with the
%            following columns:
%              1) NaN (is set to spectrum # by tof_locate_and_fit_peaks)
%              2) mass
%              3) height
%              4) fwhm
%              5) area
%              6) sigma
%              7) residual norm to data norm ratio
%              8) 1 if peak was included in peak list, 0 otherwise
%
% Example usage:
%     

% Gustaf L�nn
% July 2012
if isfield(args,'peakshape')
    param.peakShape = args.peakshape;
end
param.R = args.rfun;
param.tol = args.tolerance;

if isfield(args,'peaklist') && ~isempty(args.peaklist)
    plInds = find(args.peaklist > min(mz) & args.peaklist < max(mz));
    if ~isempty(plInds)
        param.peakList = args.peaklist(plInds);
        init = [];
    else
        [~, mInd] = max(spec);
        init = double(mz(mInd));
    end
elseif isfield(args, 'init') && ~isempty(args.init)
    plInds = [];
    init = args.init;
else
    plInds = [];
    [~, mInd] = max(spec);
    init = double(mz(mInd));
end
i = 0;
fits = [];
noise = 5 * krs_tof_calc_noise(spec);
firstTime = true;

specNorm = norm(spec);
err = specNorm;

while i < args.limit || firstTime
    i = i+1;
    if ~firstTime
        if spec(mInd) < noise || (~firstTime && residual(mInd) < noise)
            break;
        end
    end
    [ytot, fval, y1, h_out, par] = krs_tof_fit_peaks(double(mz), double(spec), init, param);
    residual = spec - ytot(:, 1)';
    
    % Check ending condition
    newErr = norm(residual);
    if ~firstTime
        if (newErr > err * args.threshold)
        %if newErr < args.threshold % Oskari edited 1.8.17
            break;
        end
    end
    err = newErr;
    
    % Find the place to add next peak
    % Loop through already fitted peaks
    [maxRes, mInd] = max(residual);
    for j=1:length(par)-1
        while true
            ppos = par(j+1);
            hwhm = (ppos / par(1))/2;
            % If the maximum of the residual is within the fitted peak, set
            % it to 0 in order to ignore it
            if (ppos-(hwhm/2) < mz(mInd)) && (mz(mInd) < ppos+(hwhm/2)) && maxRes~=0
                residual(mInd) = 0;
            else
                break
            end
            [maxRes, mInd] = max(residual);
        end
    end
    % Set the position of next peak to the maximum of residual
    init(end+1) = double(mz(mInd));
    
    fits(i).norms = err;
    fits(i).par = par;
    fits(i).h_out = h_out;
    fits(i).heights = max(y1,[],1);
    firstTime = false;
end
if isempty(fits) || all([fits.norms] == Inf)
    out = [];
    return
end

[~, bestInd] = min([fits.norms]);
out = zeros(bestInd, 8);
par = [fits(bestInd).par args.peaklist(plInds)'];
for i = 1:length(par)-1
    mz = par(i+1);
    R = par(1);
    fwhm = mz / R;
    sig = fwhm / 2.35482;
    plPeak = i >= length(fits(bestInd).par);
    out(i, :) = [NaN mz fits(bestInd).heights(i) fwhm fits(bestInd).h_out(i) sig fits(bestInd).norms/specNorm plPeak];
end