function [resid,mz] = krs_tof_get_residual(fNam,timePoint)

% This is extracted from tof_plot_spectra, which is calculating residual
% for plotting, but doesn't give it out.
% With this one, you get it :)

% example usage: tof_get_residual(fNam,datenum(2011,4,15));

try
    pea = hdf5read(fNam,'/Peaks/PeakDataGauss');
catch
    disp('tof_get_residual: Dataset PeakDataGauss not present. Cannot calculate residual.');
    mz = [];
    resid = [];
    return
end

if nargin == 1
    timePoint = now;
end

dats = hdf5diskmap(fNam,'/tofData/signal');
tim = hdf5read(fNam,'/tofData/time');
fileLength = size(tim,1);

% check whether time or spectrum number given
if timePoint <= fileLength
    Imn = timePoint;
else
    [~,Imn] = min(abs(tim(1,:)-timePoint));
end
convcoef = krs_tof_get_convcoef(fNam);
convcoef = convcoef(Imn);
dat = dats(:,Imn)';
dat = dat * convcoef;
[pars,eq] = krs_tof_get_mz_calib_pars(fNam);
pars = pars(:,Imn(1));
mz = krs_tof_mass_axis(length(dat),pars,eq);
funcArgs = eq();
fitsum = zeros(1,length(mz));

try
    doPeakShape = hdf5read(fNam,'/Peaks/PeakDataGauss','peakShape');
catch
    doPeakShape = 0;
end
if doPeakShape
    peakShape = hdf5read(fNam,'/Peaks/PeakShape')';
    par1 = hdf5read(fNam,'/Peaks/PeakShape','par1');
    par2 = hdf5read(fNam,'/Peaks/PeakShape','par2');
    yi = peakShape(:,2)';
    rxi = peakShape(:,1)';
    if par1 ~= 0
        a = krs_tof_normpdf(rxi,par2,par1).*yi;
        a(isnan(a)) = 0;
    else
        a = yi;
    end
    a = krs_H_scale(a);
    pp = spline(rxi,a);
else
    rxi = -10:0.1:10;
    a = krs_H_scale(krs_tof_normpdf(rxi,0,1));
    pp = spline(rxi,a);
end

try
    % if the dataset PeakDataGaussIso exists, we concatenate the datasets
    peaIso = hdf5read(fNam,'/Peaks/PeakDataGaussIso');
    pea = [pea peaIso];
end

Isel = find(pea(1,:) == Imn);
ppos = pea(2,Isel);
phei = pea(3,Isel) * convcoef;
psig = pea(6,Isel);

for i=1:length(ppos)
    rxi2 = rxi*psig(i) + ppos(i);
    ind1 = floor(funcArgs.inverseEQ(rxi2(1),pars));
    ind2 = ceil(funcArgs.inverseEQ(rxi2(end),pars));
    if ~isnan(ind1) && ~isnan(ind2)
        if ind2 > length(mz)
            ind2 = length(mz);
        end
        partmz = mz(ind1:ind2);
        fit = ppval(pp, (partmz - ppos(i)) / psig(i));
        fit = fit * phei(i);        
        fitsum(ind1:ind2) = fitsum(ind1:ind2) + fit;
    end
end
resid = dat - fitsum;