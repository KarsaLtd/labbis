function [avrDat,mz,sumCoadds,avr_a,avr_b,tofPeriod,pol,singleIon]=krs_tof_load_and_average_nofilt(dateRange,flsNfo,mask,calcmz)
%
% Load data from HDR file and average it
% Output will be one spectra filtered in steps range(datarange)/res
%
% [avrDat,mz,coadds,avr_a,avr_b]=tof_load_and_average(daterange,res)
%
% res - resolution in minutes
%

%Heikki Junninen
%Mar 2010

if nargin == 3
    calcmz = false;
end

global tofPath

res=10; %load in in 10 min parts if longer than that
resDays=res/(60*24);

fullTime=unique([dateRange(1):resDays:dateRange(2),dateRange(2)]);

if nargin==1
    [fls,flsTim,nrFls,flsNfo]=krs_tof_scan_files(dateRange);
    mask=[];
elseif nargin==2
    mask=[];
    fls=flsNfo.flsList;
    nrFls=length(fls);
else
    fls=flsNfo.flsList;
    nrFls=length(fls);
end

flsInfo=flsNfo.flsParam;

if all(flsInfo.hasTofData)
%     dataSetNam='/FullSpectra/TofData/'; %not used
elseif all(flsInfo.hasEventList)
%     dataSetNam='/FullSpectra/EventList/'; %not used
else
    krs_tof_message('Data format not supported for one or all files in timeRange')
end

% Iok=find(flsInfo.stopTime>dateRange(1) & flsInfo.startTime<=dateRange(2));

Iok=find(flsInfo.startTime<=dateRange(1) & flsInfo.stopTime>dateRange(1) & flsInfo.startTime~=dateRange(2));

fls=fls(Iok);
nrFls=length(fls);
flsInfo = structfun(@(x)x(Iok), flsInfo, 'UniformOutput', false);

if nrFls~=0
    
    %dims=tof_get_dataset_size([tofPath,fls(1).name], dataSetNam);
    nrT_real=length(fullTime);
    nrT=max(length(fullTime)-1,1);
    
    dat(length(fullTime)-1,flsInfo.NbrSamples(1))=0;
    
    a = NaN(length(fullTime) - 1, 1);
    b = a;
    coadds(length(fullTime)-1,1)=0;
    pol=cell(size(a));
    singleIon = a;
    
    
    for t=1:nrT
        datR=[fullTime(t),fullTime(min(t+1,nrT_real))];
        args = {'dataSet',flsNfo.loadParam.dataSetFullSpectra{1}, 'mask', mask};
        [d,af,bf,timePointsUsed,fls,flsInfo,fTim]=krs_tof_load_raw_spectra(datR,args{:});
        if ~isempty(d)
            coadd=flsInfo.NbrWaveforms.*flsInfo.NbrBlocks.*flsInfo.NbrMemories;
            tofPeriod(t)=flsInfo.TofPeriod(1);
            pol{t}=flsInfo.IonMode{1};
            singleIon(t) = flsInfo.SI(1);
            %             coadds(t)=sum(coadd)*sum(cat(1,timePointsUsed{:}));
            coadds(t)=sum(coadd.*cellfun(@(x) sum(x(:)),timePointsUsed)');

            dat(t,:)=d;
            a(t)=krs_tof_nanmean(af);
            b(t)=krs_tof_nanmean(bf);
        else
            tofPeriod(t)=NaN;
        end
    end
    %     IzMed=datMed==0;
    %     Iz=dat==0;
    
    %     Is=sum(~IzMed,1)<=size(dat,1)*0.01;
    
    %     (sum(IzMed)-sum(Iz))
    %     dat(:,Is)=0;
    %convert to average per extraction mV
    sumCoadds=sum(coadds);
    [n,m]=size(dat);
    if n==1
        avrDat=dat/sumCoadds;
    else
        avrDat=sum(dat)/sumCoadds;
    end
    avr_a=krs_tof_nanmean(a);
    avr_b=krs_tof_nanmean(b);
    %remove line if data not present in file
    Ibad=isnan(tofPeriod);
    tofPeriod(Ibad)=[];
    pol(Ibad)=[];
    singleIon(Ibad) = [];
    if isempty(tofPeriod)
        tofPeriod=NaN;
        pol{1}=NaN;
        singleIon=NaN;
    end
    
    if calcmz
        [mz]=krs_tof_mass_axis(length(avrDat),[avr_a;avr_b]);
    else
        mz = [];
    end
else
    avrDat=[];
    mz=[];
    sumCoadds=[];
    avr_a=[];
    avr_b=[];
    tofPeriod=[];
    pol=[];
    singleIon=[];
end