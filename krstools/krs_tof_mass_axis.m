function mz = krs_tof_mass_axis(sn,pars,equation)

% Calculates mass axis from given parameters. When no equation is given,
% the scripts assumes an equation according to the number of parameters.
%
% in:
% sn       - sample value(s); if only one value given, mass axis will be
%            generated for 1:sn
% pars     - mass calibration parameters
% equation - function used for mass calibration
%
% out:
% mz       - mass axis (row vector)
%
% Gustaf L�nn
% October 2011

if nargin == 2
    if length(pars) == 2
        equation = @krs_tof_fit_tof_mz;
    elseif length(pars) == 3
        equation = @krs_tof_fit_tof_mz_3p;
    else
        equation = [];
    end
end
if ~strcmp(class(equation),'function_handle') || isempty(equation)
    disp('tof_mass_axis: Function has been updated. Correct usage is tof_mass_axis(length,pars,equation).');
    mz = NaN;
    return;
end

funcArgs = equation();

if length(sn) == 1
    mz = abs(funcArgs.usedEQ(1:sn,pars));
else
    mz = abs(funcArgs.usedEQ(sn,pars));
end