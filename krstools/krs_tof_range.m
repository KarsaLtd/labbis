function y = krs_tof_range(dat,dim)
% Calculate range of the data
% if dat is is vector difference between minimum and maximum values will be
% calculated. If dat is matrix it operates alongf the first dimension. 
%
% y = tof_range(dat,dim)
%
% y - range
% dat - data, vector or matrix
% dim - operates along given dimension, optional
%

% Heikki Junninen
% Aug 2017

if nargin == 1 
    y = max(dat) - min(dat);
else
    y = max(dat,[],dim) - min(dat,[],dim);
end
