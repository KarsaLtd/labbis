function [outlist err] = krs_tof_get_UR_sticks_timeseries(fNam, mz, varargin)

% gets unit mass resolution sticks timeseries from PeakData
%
% in:
% fNam     - name of file containing preprocessed data
% mz       - mass of the substance, can be given as a formula (eg. 'HSO4-')
%            or directly in Th
%
% in (optional):
% doPlot    - 1 plots the results, 0 doesn't
% plotSave  - saves all plotted graphs as figures
% simul     - plot all masses to the same figure
% normalize - scales all timeseries to [0,1]
% hold      - 1 doesn't erase current plot
% errors    - load precalculated errors
%
% out:
% outlist  - array consisting of the columns time, mass and height
% err      - errors
%
% usage:
% ts = tof_get_UR_sticks_timeseries('data.hdf','HSO4-','doPlot',1);
%
%

% Gustaf L�nn
% June 2011

doPlot = 0;
plotSave = 0;
normalize = 0;
timeRange = 0;
loadEr = 0;
hol = 0;
outlist = [];
err=[];

if ischar(mz)
    simul = 0;
elseif length(mz) > 1
    simul = 1;
else
    simul = 0;
end

for i = 1:length(varargin)/2
    switch(varargin{2*i-1})
        case {'doPlot'}
            doPlot = varargin{2 * i};
        case {'plotSave'}
            plotSave = varargin{2 * i};
        case {'simul'}
            simul = varargin{2 * i};
        case {'normalize'}
            normalize = varargin{2 * i};
        case {'timerange'}
            timeRange = varargin{2 * i};
        case {'hold'}
            hol = varargin{2 * i};
        case {'errors'}
            loadEr = varargin{2 * i};
    end
end

if iscell(mz)
    plotAmu = round(krs_tof_exact_mass(mz));
else
    if ischar(mz)
        plotAmu = round(krs_tof_exact_mass(mz));
    else
        plotAmu = round(mz);
    end
end

try
    data = hdf5diskmap(fNam,'/Sticks/signal');
    amuRange = hdf5read(fNam,'/Sticks','amu_range');
    tim = hdf5read(fNam,'/tofData/time');
    res = hdf5read(fNam,'/tofData','Averaging time step [min]');
    if loadEr
        er = hdf5diskmap(fNam,'/Sticks/error');
    end
catch ME
    ME.message
    return;
end

if doPlot && ~plotSave && ~hol
    if strcmp(get(gcf,'Tag'),'tof_progress_bar')
        figures = findall(0,'FileName','');
        figures(figures==gcf) = [];
        if ~isempty(figures)
            set(0,'CurrentFigure',figures(1));
            clf;
        else
            figure;
        end
    else
        clf;
    end
end

if plotSave
    dirName = fNam(1:length(fNam)-3);
    [~,~,~] = mkdir(dirName);
    if ~simul
        progressbar(0,[0.1 0.9]);
    end
    if ~doPlot
        doPlot = 1;
        %disp('Automatically set doPlot to 1.');
    end
end

outlist = zeros(length(tim),length(plotAmu)+1);
plotAmu(plotAmu<amuRange(1) | plotAmu>amuRange(2)) = NaN;
if loadEr
    err=outlist;
    
    for i = 1:length(tim)
        cdata = data(:,i);
        cerr=er(:,i);
        outlist(i,1) = tim(i);
        err(i,1)=tim(i);
        % NaN mass => NaN signal
        useMasses = ~isnan(plotAmu);
        useMasses = useMasses(:)';
        useMassesInc = [false useMasses];
        notUsedMassesInc = [false ~useMasses];
        outlist(i,useMassesInc) = cdata(plotAmu(useMasses));
        outlist(i,notUsedMassesInc) = NaN;
        
        err(i,useMassesInc) = cerr(plotAmu(useMasses));
        err(i,notUsedMassesInc) = NaN;
        
    end
    
else
    
    for i = 1:length(tim)
        cdata = data(:,i);
        outlist(i,1) = tim(i);
        % NaN mass => NaN signal
        useMasses = ~isnan(plotAmu);
        useMasses = useMasses(:)';
        useMassesInc = [false useMasses];
        notUsedMassesInc = [false ~useMasses];
        outlist(i,useMassesInc) = cdata(plotAmu(useMasses));
        outlist(i,notUsedMassesInc) = NaN;
    end
end
for u = 1:length(plotAmu)
    if (plotSave && ~simul) || (plotSave && simul && u == 1)
        h = figure;
    end
    if doPlot
        if isempty(get(0,'CurrentFigure'))
            figure
        end
        if ~simul
            clf
        end
        color = get(gca,'ColorOrder');
        %line(outlist{u}(:,1) - datenum(str2double(datestr(outlist{u}(1,1),10)),1,1),outlist{u}(:,3),'color',color(mod(u,7) + 1,:));
        %line(outlist{u}(:,1),(outlist{u}(:,3)./si)*tofFreq*(800/60)*250,'color',color(mod(u,7) + 1,:),'linewidth',2);
        if ~normalize
            line(outlist(:,1),outlist(:,u+1),'color',color(mod(u,7) + 1,:),'linewidth',2);
        else
            line(outlist(:,1),krs_H_scale(outlist(:,u+1)),'color',color(mod(u,7) + 1,:),'linewidth',2);
        end
        if timeRange
            xlim(timeRange);
            startDate = datestr(timeRange(1));
            stopDate = datestr(timeRange(2));
        else
            startDate = datestr(tim(1,1),0);
            stopDate = datestr(tim(1,size(tim,2)) + res/1440,0);
        end
        if simul
            titl = [{['Start: ',startDate,' Stop: ',stopDate]},{[' Time step: ',num2str(res),' min']}];
        else
            titl = [{['Start: ',startDate,' Stop: ',stopDate]},{[' Time step: ',num2str(res),' min Mass: ',num2str(plotAmu(u))]}];
        end
        %set(gca,'XTick',[outlist{u}(1,1):1:outlist{u}(end,1)]);
        set(gca,'FontSize',16);
        title(titl);
        if stopDate-startDate <= 3
            datetick('x','HH:MM','keeplimits');
        else
            datetick('x','dd','keeplimits');
        end
        xlabel('Time');
        ylabel('Signal (ions/sec)');
        set(gca,'xgrid','on');
        if ~plotSave && iscell(mz) && ~simul && length(mz) > 1 && u ~= length(plotAmu)
            pause
        end
        if simul
            [~,~,~,oldleg] = legend;
            oldleg{length(oldleg)+1} = num2str(plotAmu(u));
            legend(oldleg)
        end
    end
    if plotSave && ~simul
        saveas(h,[dirName,filesep,num2str(plotAmu(u)),'.fig']);
        close(h);
        progressbar(u/length(plotAmu),[0.1 0.9],'Save HR-sticks timeseries');
    end
    
end

if plotSave && simul
    saveas(h,[dirName,filesep,'simulplot',datestr(now,'yyyymmddHHMMSS'),'.fig']);
    close(h);
end

end