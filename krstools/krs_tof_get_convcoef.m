function [convcoef,si] = krs_tof_get_convcoef(fNam)

% gets the conversion coefficient needed to convert the raw signal to
% ions/sec from a preprocessed file

[~,datasetExists] = krs_tof_exist_in_file(fNam,'/tofData/SI');

if datasetExists
    si = hdf5read(fNam,'/tofData/SI');
else
    numspec = krs_tof_get_dataset_size(fNam,'/tofData/coadds');
    si = hdf5read(fNam,'/tofData','Single Ion (mVns)');
    si = repmat(si,1,numspec(1));
end

tofFreq = hdf5read(fNam,'/tofData','Tof Frequency'); %Hz
sr = hdf5read(fNam,'/tofData','Sampling Interval (s)') * 10^9; %in ns

convcoef = (sr * tofFreq) ./ si;