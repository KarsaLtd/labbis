function krs_tof_message(message, type)

% tof_message(message, type)
% Displays messages from tofTools functions. Writes the message to a log
% file in the tofTools-folder if the field 'keeplog' of 'tofToolsConstants'
% is set to true.
%
% In:
%    message - message string
%    type    - 'err', 'warn' or empty

% Gustaf L?nn
% September 2012

% determine type of message

if nargin == 2
    switch lower(type)
        case 'err'
            type = '(Error) ';
        case 'warn'
            type = '(Warning) ';
        otherwise
            type = [];
    end
else
    type = [];
end

% get name of caller function
[st, ~] = dbstack(1);
caller = st(1).file(1:regexp(st(1).file,'\.m') - 1);

% construct and display message
timestamp = datestr(now, 31);
message = ['[' timestamp '] ' type caller ': ' message];
%disp(message);

% add to log if flag set
const = getappdata(0, 'tofToolsConstants');
if ~isempty(const)
    if const.keeplog
        logDir=[fileparts(mfilename('fullpath')) filesep 'logs' filesep];
        if ~exist(logDir,'dir')
            mkdir(logDir);
        end
        logFile = [fileparts(mfilename('fullpath')) filesep 'logs' filesep 'tofTools_',datestr(floor(now),'yyyymmdd'),'.log'];
        fId = fopen(logFile, 'a');
        fprintf(fId, '%s\n', message);
        fclose(fId);
        %disp([logFile, ' updated'])
    end
end