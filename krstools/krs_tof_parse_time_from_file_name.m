function flsTim=krs_tof_parse_time_from_file_name(fileNam)
%
% Get time stamp from file name
%

%heikki Junninen


[PATHSTR,fNam,EXT] = fileparts(fileNam);
% ind = strfind(fNam,'2009');
% if isempty(ind)
%     ind = strfind(fNam,'2010');
% end
% DateString0 = fNam(ind:ind+18);  % 2009.04.27-22h40m34

c=regexp(fNam,'[0-9]+.[0-9]+.[0-9]+[-,_][0-9]+h[0-9]+m[0-9]+s','match');  % 2009.04.27-22h40m34
if ~isempty(c)
    DateString0 = c{1};
    
    if 0
        %matlab 7->
        DateString = DateString0(:,[1:4 6:7 9:10 12:13 15:16 18:19]); % 20090427224034
        flsTi = datenum(DateString,'yyyymmddHHMMSS');
    else
        %for matlab 6.5
        ys = str2num(DateString0(:,[1:4])); % 20090427224034
        ms = str2num(DateString0(:,[6:7])); % 20090427224034
        ds = str2num(DateString0(:,[9:10])); % 20090427224034
        Hs = str2num(DateString0(:,[12:13])); % 20090427224034
        Ms = str2num(DateString0(:,[15:16])); % 20090427224034
        Ss = str2num(DateString0(:,[18:19])); % 20090427224034
        flsTim = datenum(ys,ms,ds,Hs,Ms,Ss);
    end
else
    %try if file is saved by IgorDAQ in format IMSneg_20131015_130550
    c=regexp(fNam,'[0-9]+[0-9]+[0-9]+[-,_][0-9]+[0-9]+[0-9]+','match');
    
    if ~isempty(c)
        DateString0 = c{1};
        
        %for matlab 6.5
        ys = str2num(DateString0(:,[1:4])); % 20090427224034
        ms = str2num(DateString0(:,[5:6])); % 20090427224034
        ds = str2num(DateString0(:,[7:8])); % 20090427224034
        Hs = str2num(DateString0(:,[10:11])); % 20090427224034
        Ms = str2num(DateString0(:,[12:13])); % 20090427224034
        Ss = str2num(DateString0(:,[14:15])); % 20090427224034
        flsTim = datenum(ys,ms,ds,Hs,Ms,Ss);
    else
        flsTim=[];
    end
end
