function [Ig,Ids]=krs_tof_exist_in_file(fileName,nameToLook)
%
% [Ig,Ids]=tof_exist_in_file(fileName,nameToLook)
% returns 1 if nameToLook object is in file
%
% Ig for group
% Ids for dataset

%Heikki Junninen
%June 2009

%
% Open file.
%
file = H5F.open (fileName, 'H5F_ACC_RDONLY', 'H5P_DEFAULT');

%
% Begin iteration.
%

%H5G.iterate (file, '/',[] , @op_func);

I=0;
nrObj=H5G.get_num_objs(file);
[p,nameToLook_parts]=regexp(nameToLook,'/','match','split');

Ids=0;
h=0;
hg=h;
for i=1:nrObj
    nam=H5G.get_objname_by_idx(file,i-1);
    Ig(i)=strcmp(nam,nameToLook_parts{2}); %group exist or not
    
    statbuf=H5G.get_objinfo (file, nam, 0);
    
    switch (statbuf.type)
        case H5ML.get_constant_value('H5G_GROUP') && length(nameToLook_parts)>2
            %                 fprintf ('  Group: %s\n', nam);
            
            %                 fprintf ('Objects in root group:\n');
            if strcmp(nam,nameToLook_parts{2}) % only check if right group...?
                group_id = H5G.open(file, nam);
                nrObjG=H5G.get_num_objs(group_id);
                hg=hg+1;
                %                 collectGroupName{hg}=['/',nam];
                for j=1:nrObjG
                    h=h+1;
                    namD=H5G.get_objname_by_idx(group_id,j-1);
                    %                     collectDsName{h}=['/',nam,'/',namD];
                    Ids(i,j)=strcmp(namD,nameToLook_parts{3});
                end
            end
        otherwise
    end
    
end
Ig=any(Ig);
Ids=any(Ids(:));
% Im=strmatch(nameToLook,collectObjName);
% Im=ismember(nameToLook,collectObjName);


% I=Im~=0;
%
% Close and release resources.
%
H5F.close (file);

%% SUBFUNCTIONS
%=======================================================================

function status=op_func (loc_id, name)

%
% Get type of the object and display its name and type.
% The name of the object is passed to this function by
% the Library.
%
statbuf=H5G.get_objinfo (loc_id, name, 0);

switch (statbuf.type)
    case H5ML.get_constant_value('H5G_GROUP')
        fprintf ('  Group: %s\n', name);
        
        fprintf ('Objects in root group:\n');
        
    case H5ML.get_constant_value('H5G_DATASET')
        fprintf ('  Dataset: %s\n', name);
        
    case H5ML.get_constant_value('H5G_TYPE')
        fprintf ('  Datatype: %s\n', name);
        
    otherwise
        fprintf ( '  Unknown: %s\n', name);
end

status=0;
