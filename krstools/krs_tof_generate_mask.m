function [mask,mask_time] = krs_tof_generate_mask(timeRanges)

% Generates a mask that can be used when averaging for the raw files in the
% path set by tof_set_path. The length of the mask will depend on the
% earliest and latest time given.
%
% in:
%   timeRanges - two column array of times, the first column is the start
%                time, second stop time and each row represents a range to
%                be included in the mask
%
% out:
%   mask       - boolean of which raw spectra to include in averaging
%   mask_time  - contains the times of the raw spectra

% Gustaf L�nn
% March 2012

global tofPath

mask = [];
mask_time = [];

if isempty(timeRanges)
    return
end

if any(timeRanges(:,2) < timeRanges(:,1))
    errorInds = find(timeRanges(:,2) < timeRanges(:,1))';
    disp(['tof_generate_mask: Start time later than stop time in the following lines: ' num2str(errorInds)]);
    timeRanges(errorInds,:)=[];
    
    %return;
end

timeRanges = sortrows(timeRanges,1);
if exist([tofPath,'tofToolsindex.mat'],'file')
    load([tofPath,'tofToolsindex.mat']);
    Iok=find(flsNfo.flsParam.stopTime>timeRanges(1,1) & flsNfo.flsParam.startTime<=timeRanges(end,2));
    if ~any(Iok)
        Iok=find(flsNfo.flsParam.startTime<=timeRanges(1,1) & flsNfo.flsParam.stopTime>=timeRanges(end,2));
    end
    flsNfo.timVectors = flsNfo.timVectors(Iok);
else
    [~,~,~,flsNfo]=krs_tof_scan_files([timeRanges(1,1) timeRanges(end,2)]);
end

tim = cellfun(@(x)x(:),flsNfo.timVectors,'UniformOutput',false);
tim = cat(1,tim{:});
tim(tim < min(timeRanges(:))) = [];
tim(tim > max(timeRanges(:))) = [];

mask = false(length(tim),1);
for i = 1:size(timeRanges,1)
    useTimes = tim >= timeRanges(i,1) & tim <= timeRanges(i,2);
    mask(useTimes) = true;
end
mask_time = tim;