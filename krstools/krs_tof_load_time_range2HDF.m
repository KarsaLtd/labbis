function [fileName, fls]=krs_tof_load_time_range2HDF(dateRange,avr_par,fileName)
%
% Load tof raw spectra in time incriments and filter noise
% average to defined time steps. Averageing is done in tof-space,
% units mV
%
% [tofDat,mz]=tof_load_time_range2HDF(dateRange,res_final,res_initial)
%
%  tofDat      - tof data in res_final time resolution
%  mz          - mz
%  dateRange   - date range in datenum format
%  res_final   - resolution for the finel time series (minutes)
%                if set to 0 no average is calculated, data is just copied
%                to preprocess file
%  res_initial - time step at which the noise reduction is done (minutes)
%
%
% Example:
% get time series for day 1 May 2009 with 10 min interval, noise reduction
% is done in 2min interval
% avr_par=struct('data_timeRes',10,'noise_timeRes',2);
% tof_load_time_range([datenum(2009,5,1),datenum(2009,5,2)],avr_par)

%Heikki Junninen
%12.4.2010

global tofPath_save
global tofPath

d=datevec(dateRange(1));
% fileName=['tofTools_',sprintf('%4d.%02d.%02d-%02dh%02dm%02ds',d),'.h5'];
if nargin==2
    fileName=[tofPath_save,'tofTools_',sprintf('%4d.%02d.%02d-%02dh%02dm%02ds',d),'.h5'];
end

if nargin==1
    % averaging parameters
    avr_par=struct(...
        'data_timeRes',10,...
        'noise_timeRes',2,...
        'baselineCorrection',0,...
        'mask',[],...
        'mask_time',[]);
end

useMask = 0;
if ~isempty(avr_par.mask)
    useMask = 1;
end

%latence check
if ~isfield(avr_par,'append')
    %     default it to 0
    avr_par.append=0;
end

if ~isfield(avr_par,'data_processing_type')
    %     default it to 0
    avr_par.data_processing_type='ADC';
    %if set to 'ADC' reads the /FullSpectra otherwise /FullSpectra2
    
end

%check if processing type is known
if ~(strcmp(avr_par.data_processing_type,'ADC') || strcmp(avr_par.data_processing_type,'TDC'))
    krs_tof_message('Unknown data processing type. Options are: ''ADC'', ''TDC''','err')
end



diffSpec = false;
if isfield(avr_par, 'diff')
    if isempty(avr_par.mask) && avr_par.diff
        krs_tof_message('A mask is needed when calculating the difference between spectra. Averaging normally.', 'warn');
    else
        diffSpec = avr_par.diff;
    end
end

res_final=avr_par.data_timeRes;
res_initial=avr_par.noise_timeRes;

% disp('save daily files')
dataSetTofData_sig='/tofData/signal';
dataSetTofData_tm='/tofData/time';
dataSetTofData_coa='/tofData/coadds';
%dataSetCalib_a='/MassCalib/a';
%dataSetCalib_b='/MassCalib/b';
%dataSetCalib_p='/MassCalib/p';

% if average time step has set to 0, make no averaging

step=res_final/(60*24);
if exist([tofPath,'tofToolsindex.mat'],'file')
    % load from index
    load([tofPath,'tofToolsindex.mat']);
    Iok=find(flsNfo.flsParam.stopTime>dateRange(1) & flsNfo.flsParam.startTime<=dateRange(2));
    if ~any(Iok)
        Iok=find(flsNfo.flsParam.startTime<=dateRange(1) & flsNfo.flsParam.stopTime>=dateRange(2));
    end
    fls = fls(Iok);
    nrFls = length(fls);
    flsTim = flsTim(Iok);
    flsNfo.flsList = flsNfo.flsList(Iok);
    flsNfo.timVectors = flsNfo.timVectors(Iok);
    flsNfo.timDims = flsNfo.timDims(Iok);
    flsNfo.tpsVoltages = flsNfo.tpsVoltages(Iok);
    flsNfo.tpsVolNames = flsNfo.tpsVolNames(Iok);
    flsNfo.flsParam=structfun(@(x) (x(Iok)), flsNfo.flsParam,'UniformOutput', false);
else
    [fls,flsTim,nrFls,flsNfo]=krs_tof_scan_files(dateRange);
end

if res_final==0
    startTime=flsTim(1);
    stopTime=flsNfo.timVectors{end}(end);
else
    startTime=dateRange(1);
    stopTime=dateRange(2);
end



if isempty(fls)
    %     disp('tof_load_time_range_2: No files')
    fileNames=[];
    return;
end
[pth fNam ext]=fileparts(fileName);
fileNam=[];
fileNames = [];

% check if parameters have changed
% also check if asked data processing type exist
fNams=cell(nrFls,1);
par_change(1)=max(startTime,flsTim(1));
tofPers(1)=flsNfo.flsParam.TofPeriod(1);
sis(1)=flsNfo.flsParam.SI(1);
sInt(1)=flsNfo.flsParam.SampleInterval(1);
prType(1)=flsNfo.flsParam.NdigoDataProcessingType(1);
prTypeStrs={'ADC','TDC','Hybrid'};

% define which dataprocessing mode to read
if strcmp(avr_par.data_processing_type,'ADC')
    requestedPrType=1;
    dataSetFullSpectra='/FullSpectra';
elseif strcmp(avr_par.data_processing_type,'TDC')
    dataSetFullSpectra='/FullSpectra';
    requestedPrType=2;
end
if prType(1)==3
    if strcmp(avr_par.data_processing_type,'TDC')
        dataSetFullSpectra='/FullSpectra2';
        requestedPrType=2;
    end
end

h=1;
for i=1:nrFls
    %assumes that all the files in the data range have measure with the
    %same dataProcessingType!!!!
    flsNfo.loadParam.dataSetFullSpectra{i}=dataSetFullSpectra;
    if flsNfo.flsParam.NdigoDataProcessingType(i)==-1
        fNams{i}=sprintf(['%06d_%s'],round([flsNfo.flsParam.NbrSamples(i)]),flsNfo.flsParam.IonMode{i});
    else
        if flsNfo.flsParam.NdigoDataProcessingType(i)==3
            prTypeStr=prTypeStrs{requestedPrType};
        else
            if flsNfo.flsParam.NdigoDataProcessingType(i) == requestedPrType
                prTypeStr=prTypeStrs{flsNfo.flsParam.NdigoDataProcessingType(i)};
            else
                krs_tof_message(['File (',flsNfo.flsList(1).name,') did not contain data in processed type: ',prTypeStrs{requestedPrType}],'warn')
                prTypeStr=prTypeStrs{flsNfo.flsParam.NdigoDataProcessingType(i)};
                krs_tof_message(['Using type: ',prTypeStr],'warn')
            end
        end
        fNams{i}=sprintf(['%s_%06d_%s'],prTypeStr,round([flsNfo.flsParam.NbrSamples(i)]),flsNfo.flsParam.IonMode{i});
    end
    if i==1
        par_change_fNam{1}=fNams{i};
    else
        if ~strcmp(fNams{i-1},fNams{i})
            h=h+1;
            par_change(h)=flsTim(i);
            par_change_fNam{h}=fNams{i};
            tofPers(h)=flsNfo.flsParam.TofPeriod(i);
            sis(h)=flsNfo.flsParam.SI(i);
            sInt(h)=flsNfo.flsParam.SampleInterval(i);
            prTypes(h)=flsNfo.flsParam.NdigoDataProcessingType(i);
            
        end
    end
end

if res_final==0
    %tim=unique([cat(2,flsNfo.timVectors{:}),par_change]);
    temptim = cat(2,flsNfo.timVectors{:});
    tim=uniquetol([reshape(temptim,1,numel(temptim)),par_change]);
    %remove time points that are outside requested range
    [~,t0ind] = min(abs(tim-dateRange(1)));
    [~,t1ind] = min(abs(tim-dateRange(2)));
    if t1ind<length(tim)
        tim(t1ind+1:end)=[];
    end
    if t0ind>1
        tim(1:t0ind)=[];
    end
    tim(isnan(tim))=[];
    fullTime=uniquetol([tim,flsTim']);
    
else
    tim=uniquetol([startTime:step:stopTime,stopTime,par_change]);
    tim(tim < startTime) = [];
    fullTime=uniquetol([startTime:step:stopTime,stopTime,flsTim']);
end

[mx,Imx]=krs_tof_nanmax(flsNfo.timVectors{1}(:));
tm1=flsNfo.timVectors{1}([1,Imx]);
%tm2=[0,(flsNfo.timVectors{1}(2)-flsNfo.timVectors{1}(1))];
%flRange=(tm1(:)+tm2(:));
flRange=tm1(:);
loadSumSpectrum=0;
if nrFls==1 && length(tim)==2 && abs(flRange(1)-tim(1))<1/(60*60*24)
    
    % if estimated file stop time is within second, assume whole file averages
    % is requested and dont recalculate, instead load the sumSpectrum
    if abs(flRange(2)-tim(2))<1/(60*60*24)
        loadSumSpectrum=1;
        nrTimePointsMeasured=Imx;
    end
    
end
writeFile = 1;
anyWrite = 0;
fileNameOld='';
timFromFile=[];
IparsOk=0; %given parameters and the ones in file and the same
al.time=[]; %logs
al.msg=[]; %logs

hf=0;
tim(tim<par_change(1))=[];
createdFiles = {};
lastWrittenFile = '';

if ~isempty(avr_par.mask) % remove unused mask points, useful since the mask is saved
    delMaskInd = avr_par.mask_time > stopTime | avr_par.mask_time < startTime;
    avr_par.mask_time(delMaskInd) = [];
    avr_par.mask(delMaskInd) = [];
end

for t=1:length(tim)-1
    %check if timestep is shorter than required skip it
    %due to rounding error direct step comparison cant be done
    % if abs((tim(t+1)-tim(t))-step)<step*1e-6
    
    %create filename
    %generate file name for each spectrum, this will push the result
    % to the right file if the same setting is coming again later.
    % New file will be generated only when parameters change first time
    if t==1
        Ipar_change=1;
    else
        Ipar_change=tim(t)==par_change;
    end
    %{
    if any(Ipar_change)
        newNam=fullfile(pth,[fNam,'_',par_change_fNam{Ipar_change},'.h5']);
    end
    
    if any(Ipar_change) && ~strcmp(newNam,fileNam)
        hf=hf+1;
        fileName=fullfile(pth,[fNam,'_',par_change_fNam{Ipar_change},'.h5']);
        fileNames{hf}=fileName;
    elseif t==1
        hf=hf+1;
        Ipar_change=1;
        fileName=fullfile(pth,[fNam,'_',par_change_fNam{Ipar_change},'.h5']);
        fileNames{hf}=fileName;
        fileNameOld=fileName;
    end
    %}
    % Oskari edited 8.1.18
    fileName = fullfile(pth,[fNam '.h5']);
    fileNameOld=fileName;
    hf = hf+1;
    fileNames{hf}=fileName;
    % UNTIL HERE
    
    % find if file exist if appending, requested time point existing and
    % coadds is set. If 0 it means spectra has not been calculated
    doCalc=1;
    if avr_par.append
        if exist(fileName,'file')
            if ~strcmp(fileNameOld,fileName), %load only if previous file was not the same
                timFromFile=hdf5read(fileName,'/tofData/time');
                cadFromFile=hdf5read(fileName,'/tofData/coadds');
                
                %read parameters from file
                avr_par_file=avr_par;
                
                avr_par_file.data_timeRes=hdf5read(fileName,'/tofData','Averaging time step [min]');
                avr_par_file.noise_timeRes=hdf5read(fileName,'/tofData','Noise reduction time step [min]');
                avr_par_file.baselineCorrection=hdf5read(fileName,'/tofData','baselineCorrection');
                
                %check if given parameters and the ones in file and the same
                IparsOk=isequal(avr_par,avr_par_file);
            end
            
            %do not calculations if time point already exist and coadds is
            %not zero
            %Itm=timFromFile==tim(t); %absolute mach
            Itm=abs((timFromFile-tim(t)))<1/(24*60*60*10000); %match within 0.100ms
            if any(Itm) & IparsOk
                doCalc=cadFromFile(Itm)==0;
            else
                doCalc=1;
            end
        else
            doCalc=1;
            fileNameOld=fileName;
            
        end %file exist
    end
    
    if doCalc
        I=find(fullTime>=tim(t) & fullTime<tim(t+1));
        mask=[];
        if ~isempty(avr_par.mask)
            Imask=find(avr_par.mask_time>=tim(t) & avr_par.mask_time<=tim(t+1));
            mask_val=avr_par.mask(Imask);
            mask_time=avr_par.mask_time(Imask);
            %         tic=double(avr_par.tic(Imask));
            %         mask=[mask_time,mask_val,tic];
            if diffSpec
                diffMask_val = ~mask_val;
                diffMask_time = mask_time;
                
                % every time point inside the current averaging window that
                % is either non-existent or false in the mask should be
                % true in the diffMask, therefore, if the mask doesn't fill
                % the current averaging window, extend it
                temptim = cat(2, flsNfo.timVectors{:}); % time for raw spectra
                temptim = temptim(:);
                if max(mask_time) < tim(t + 1) % mask ends too early
                    temptim2 = temptim(temptim > max(mask_time) & temptim <= tim(t + 1));
                    diffMask_time = [diffMask_time; temptim2];
                    diffMask_val = [diffMask_val; ones(length(temptim2), 1)];
                end
                if min(mask_time) > tim(t) % mask begins too late
                    temptim2 = temptim(temptim < min(mask_time) & temptim >= tim(t));
                    diffMask_time = [temptim2; diffMask_time];
                    diffMask_val = [ones(length(temptim2), 1); diffMask_val];
                end
                diffMask = [diffMask_time, diffMask_val];
            end
            mask=[mask_time,mask_val];
            if useMask && isempty(mask)
                mask = [0,0];
                loadSumSpectrum=0;
            end
        end
        h=0;
        tof_dat_tmp = [];
        coadds=[];
        hasIMS = 1;
        for i=1:max(length(I),1)
            
            %check if tofData dataset exists,
            %if not check, if eventList exists
            
            %[Ig,Ids]=tof_exist_in_file(fn,'/FullSpectra/TofData');
            
            
            %If every extraction have been saved, it is most likely IMS.
            %If only one file is opened.
            %In this case read in only the sum spectrum
            
            %                 isIMS=flsNfo.flsParam.NbrWaveforms==1 & length(flsNfo.flsList)==1;
            %                 if isIMS
            %
            %                     fN=[tofPath,flsNfo.flsList.name];
            %                     %[mz,singleIon,NbrWaveforms,NbrSamples,SampleInterval,a,b]=tof_mass_axis_old(fN);
            %                     avrDat=hdf5read(fN,'/FullSpectra/SumSpectrum');
            %                     mz=hdf5read(fN,'/FullSpectra/MassAxis');
            %
            %                     sumCoadds=flsNfo.flsParam.NbrWaveforms.*flsNfo.flsParam.NbrBlocks.*flsNfo.flsParam.NbrMemories;
            %                     a = hdf5read(fN,'/FullSpectra','MassCalibration a');
            %                     b = hdf5read(fN,'/FullSpectra','MassCalibration b');
            %                     tofPeriod=flsNfo.flsParam.TofPeriod;
            %                     singleIon=flsNfo.flsParam.SI;
            %                     pol=flsNfo.flsParam.IonMode;
            %
            %                 else
            if loadSumSpectrum
                fN=fullfile(flsNfo.flsList.folder, flsNfo.flsList.name);
                %[mz,singleIon,NbrWaveforms,NbrSamples,SampleInterval,a,b]=tof_mass_axis_old(fN);
                avrDat=h5read(fN,[dataSetFullSpectra '/SumSpectrum']);
                mz=h5read(fN,[dataSetFullSpectra '/MassAxis']);
                
                sumCoadds=flsNfo.flsParam.NbrWaveforms.*flsNfo.flsParam.NbrBlocks.*flsNfo.flsParam.NbrMemories*nrTimePointsMeasured;
                avrDat=avrDat/sumCoadds; %mV/extraction
                a = h5readatt(fN,dataSetFullSpectra, 'MassCalibration a');
                b = h5readatt(fN,dataSetFullSpectra, 'MassCalibration b');
                if a < 1
                    a = h5readatt(fN,dataSetFullSpectra,'MassCalibration p1');
                    b = h5readatt(fN,dataSetFullSpectra,'MassCalibration p2');
                end
                tofPeriod=flsNfo.flsParam.TofPeriod;
                singleIon=flsNfo.flsParam.SI;
                pol=flsNfo.flsParam.IonMode;
                acqLog=krs_tof_read_log(fN);
                
                krs_tof_message('Loaded SumSpectrum');
            else
                if res_initial==0
                    [avrDat,mz,sumCoadds,a,b,tofPeriod,pol,singleIon]=krs_tof_load_and_average_nofilt([fullTime(I(i)),fullTime(I(i)+1)],flsNfo,mask);
                    if diffSpec
                        [avrDatDiff, mzDiff, sumCoaddsDiff, aDiff, bDiff, tofPeriodDiff, polDiff, singleIonDiff] = krs_tof_load_and_average_nofilt([fullTime(I(i)),fullTime(I(i)+1)],flsNfo,diffMask);
                    end
                else
                    [avrDat,mz,sumCoadds,a,b,tofPeriod,pol,singleIon]=krs_tof_load_and_average([fullTime(I(i)),fullTime(I(i)+1)],res_initial,flsNfo,mask);
                end
            end
            %                end
            if ~isempty(avrDat) | avrDat==0
                if ~all(isnan(avrDat))
                    writeFile = 1;
                    anyWrite = 1;
                end
                avrDl = length(avrDat);
                h=h+1;
                if h==1
                    tof_dat_tmp=NaN(length(I),length(avrDat));
                    coadds=NaN(length(I),1);
                    as=coadds;
                    bs=coadds;
                    ps=coadds;
                    tofPeriods=coadds;
                    pols=cell(size(coadds));
                    singleIons=coadds;
                end
                tof_dat_tmp(h,:)=avrDat*sumCoadds;
                coadds(h)=sumCoadds;
                if diffSpec
                    coadds(h) = coadds(h) + sumCoaddsDiff;
                    if sumCoadds == 0 % if no data in avrDat, do this to avoid NaNs
                        tof_dat_tmp(h, :) = -avrDatDiff * sumCoaddsDiff;
                        writeFile = 1; % set flag that we have data (not in avrDat, but in avrDatDiff)
                        anyWrite = 1;
                        a = aDiff; % in case there is no avrDat for this averaging window, use these parameters instead to avoid NaNs
                        b = bDiff;
                        tofPeriod = tofPeriodDiff;
                        pol = polDiff;
                        singleIon = singleIonDiff;
                        sumCoadds = sumCoaddsDiff; % to make singleIon calculation correct, not used for anything else later
                    elseif sumCoaddsDiff ~= 0 % only do if data in avrDatDiff to avoid NaNs
                        tof_dat_tmp(h, :) = tof_dat_tmp(h, :) - avrDatDiff * sumCoaddsDiff;
                    end
                end
                
                
                as(h)=a;
                bs(h)=b;
                ps(h)=2;
                if ~all(flsNfo.flsParam.hasIMS)
                    hasIMS = 0;
                end
                tofPeriods(h)=unique(tofPeriod); %all must be the same
                pols{h}=pol{1}; % select just one, since all must be the same, and previous line will crash if no
                singleIons(h) = singleIon(1)*sumCoadds;
            end
        end
        
        if isempty(coadds)
            %             tof_dat = NaN(1,avrDl);
            tof_dat = NaN;
            as = NaN;
            bs = NaN;
            ps = NaN;
            coadds = NaN;
            writeFile=0;
        else
            if max(size(coadds)) > 1%h>1
                tof_dat=krs_tof_nansum(tof_dat_tmp)/krs_tof_nansum(coadds);
                singleIon = krs_tof_nansum(singleIons)/krs_tof_nansum(coadds);
            else
                tof_dat=tof_dat_tmp/coadds;
                singleIon = krs_tof_nansum(singleIons)/krs_tof_nansum(coadds);
            end
        end
        if avr_par.baselineCorrection
            mz = krs_tof_mass_axis(length(tof_dat), [krs_tof_nanmean(as) krs_tof_nanmean(bs)]);
            if ~all(isnan(mz))
                [tof_dat, bl] = krs_tof_remove_baseline(mz,tof_dat);
            else
                bl = NaN(1,size(tof_dat,2));
            end
        end
        
        %% save to HDF
        
        if writeFile
            if any(Ipar_change) && ~any(strcmp(createdFiles(:),fileName))
                createdFiles{end+1} = fileName;
                if exist('ids','var')
                    krs_tof_close_HDF(ids);
                end
                ids = struct('fId',{[],[],[],[],[],[],[],[],[],[]},'dId',[],'dims_old',[]);
                %         if ~exist(fileName,'file')
                %hf=hf+1;
                %                 fileNames{hf}=fileName;
                
                ids(1) = krs_tof_write_HDF(fileName,dataSetTofData_sig,tof_dat,'keepopen',true,'memType','H5T_NATIVE_FLOAT','mode','create');
                %attributes
                if Ipar_change == 0
                    tof_freq=unique(1./(flsNfo.flsParam.TofPeriod*1e-9)); %Hz
                    sInts=unique(flsNfo.flsParam.SampleInterval);
                    DCcard=unique(flsNfo.flsParam.DC_card); %assumes DC card remains the same
                    prType=unique(flsNfo.flsParam.NdigoDataProcessingType);
                else
                    tof_freq=1./(tofPers(Ipar_change)*1e-9);
                    sInts = sInt(Ipar_change);
                    DCcard=flsNfo.flsParam.DC_card{Ipar_change}; %assumes DC card remains the same
                    prType=flsNfo.flsParam.NdigoDataProcessingType(Ipar_change);
                end
                attrs = {'Noise reduction time step [min]', res_initial, 'Averaging time step [min]', res_final, ...
                    'Signal Unit', 'mV/ext', 'Start Time', dateRange(1), 'Stop Time', dateRange(2), 'Tof Frequency', tof_freq, ...
                    'Sampling Interval (s)', sInts, 'Polarity', pol{1}, 'baselineCorrection', avr_par.baselineCorrection, ...
                    'hasIMS', hasIMS, 'diffSpec', double(diffSpec),'DC_card', DCcard,'NdigoProcessingType',prType,'UsedProcessingType',avr_par.data_processing_type};
                krs_tof_write_attributes(fileName, attrs, '/tofData');
                if ~isempty(mask)
                    krs_tof_write_HDF(fileName, '/tofData/mask', [avr_par.mask_time(:) avr_par.mask(:)], 'memType', 'H5T_NATIVE_DOUBLE', 'chunk', [size(mask, 1) 2]);
                end
            else
                if ~strcmp(lastWrittenFile,fileName) && ~isempty(lastWrittenFile)
                    krs_tof_close_HDF(ids);
                    ids = struct('fId',{[],[],[],[],[],[],[],[],[],[]},'dId',[],'dims_old',[]);
                elseif isempty(lastWrittenFile)
                    ids = struct('fId',{[],[],[],[],[],[],[],[],[],[]},'dId',[],'dims_old',[]);
                end
                ids(1) = krs_tof_write_HDF(fileName,dataSetTofData_sig,tof_dat,'keepopen',true,'ids',ids(1),'memType','H5T_NATIVE_FLOAT');
                %if ~exist('fId','var') || fId.double == -1
                %    fId = H5F.open(fileName,'H5F_ACC_RDWR','H5P_DEFAULT');
                %end
            end
            ids(2) = krs_tof_write_HDF(fileName,dataSetTofData_tm,tim(t),'memType','H5T_NATIVE_DOUBLE','keepopen',true,'ids',ids(2),'chunk',[length(tim)-1 1]); %time has to be in double
            ids(3) = krs_tof_write_HDF(fileName,'/MassCalib/a_log',krs_tof_nanmean(as),'memType','H5T_NATIVE_FLOAT','keepopen',true,'ids',ids(3),'chunk',[length(tim)-1 1]);
            ids(4) = krs_tof_write_HDF(fileName,'/MassCalib/b_log',krs_tof_nanmean(bs),'memType','H5T_NATIVE_FLOAT','keepopen',true,'ids',ids(4),'chunk',[length(tim)-1 1]);
            ids(5) = krs_tof_write_HDF(fileName,'/MassCalib/ps_log',krs_tof_nanmean(ps),'memType','H5T_NATIVE_FLOAT','keepopen',true,'ids',ids(5),'chunk',[length(tim)-1 1]);
            ids(6) = krs_tof_write_HDF(fileName,'/tofData/SI',singleIon,'memType','H5T_NATIVE_FLOAT','keepopen',true,'ids',ids(6),'chunk',[length(tim)-1 1]);
            ids(7) = krs_tof_write_HDF(fileName,dataSetTofData_coa,krs_tof_nansum(coadds),'memType','H5T_NATIVE_FLOAT','keepopen',true,'ids',ids(7),'chunk',[length(tim)-1 1]);
            if avr_par.baselineCorrection
                ids(8) = krs_tof_write_HDF(fileName,'/tofData/baseline',bl,'memType','H5T_NATIVE_FLOAT','keepopen',true,'ids',ids(8));
            end
            
            %write log messages from this time period
            %             al.time=[al.time;cell2mat(acqLog(:,1))];
            %             if isempty(al.msg)
            %                 al.msg={acqLog{:,3}};
            %             else
            %                 al.msg=[al.msg;acqLog{:,3}];
            %             end
            %             ids(9) = tof_write_HDF(fileName,'/Logs/AcquisitionLog',al,'keepopen',true,'ids',ids(9),'chunk',[1 1]);
            
            %             for lt=1:size(acqLog,1)
            %             ids(9) = tof_write_HDF(fileName,'/Logs/AcquisitionLog_time',acqLog{lt,1},'memType','H5T_NATIVE_DOUBLE','keepopen',true,'ids',ids(9),'chunk',[1 1]);
            %             ids(10) = tof_write_HDF(fileName,'/Logs/AcquisitionLog_msg',acqLog{lt,3},'memType','H5T_NATIVE_CHAR','keepopen',true,'ids',ids(10),'chunk',[1 1]);
            %             end
            lastWrittenFile = fileName;
        end
        
    end %if doCalc
    %krs_tof_progress_bar(progbar,'av',t/(length(tim)-1),fileName);
    if t==1
        filenos = regexp(avr_par.labapp.progbar.Message,'\d*','Match');
        filei = str2double(filenos{1});
    end
    progBar(avr_par.labapp, 'value', ...
        ((filei-1)/avr_par.noEvents)+(t/(length(tim)-1))/avr_par.noEvents);
    %end
    
end

%write the logs to file
try
    acqLog=krs_tof_read_log([fullTime(1),fullTime(end)]);
    al.time=cell2mat(acqLog(:,1));
    al.msg={acqLog{:,3}}';
catch
    acqLog=[];
    krs_tof_message('Reading acqusition log failed')
    al.time=1;
    al.msg={'No acqusition log'};
end
if isempty(acqLog)
    krs_tof_message('No acqusition log')
    al.time=1;
    al.msg={'No acqusition log'};
end

krs_tof_write_HDF_logs(fileName, '/Logs/AcquisitionLog', al);

if anyWrite
    krs_tof_close_HDF(ids);
else
    disp('tof_load_time_range2HDF.m: No new data');
end