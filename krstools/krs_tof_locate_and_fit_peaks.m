function outList = krs_tof_locate_and_fit_peaks(fNam, varargin)

% outList = tof_locate_and_fit_peaks(fNam, varargin)
% Finds and fits peaks using peak shape and resolution function.
% 
% In:
%     fNam     - path to the preprocessed file
%     varargin - in the form of "property, value", properties are:
%       'peakshape' - path of peak shape file to use, if none is given, the
%                     script will try to read the peak shape from the
%                     preprocessed file; set to false to use a Gaussian
%                     peak shape
%       'peaklist'  - peak list database or cell of compounds/masses,
%                     these peaks are always fitted at their exact mass
%       'threshold' - a new peak is included only if the norm of the
%                     residual of the new fit is smaller than 'threshold'
%                     times the norm of the residual of the old previous
%                     fit
%       'range'     - amu range to include, [min max]
%       'write'     - true writes the found and fitted peaks to the
%                     preprocessed file, false only outputs them
%       'rfun'      - resolution function to use, can be an anonymous
%                     function with mass as a argument or a scalar
%       'limit'     - maximum number of peaks to fit to a single amu
%       'tolerance' - maximum uncertainty in mass for non-peak list peaks
%       'spec'      - numbers of the spectra to be included
%       'verbose'   - displays progress
%       'init'   - Initial guesses for peak position
%
% Out:
%     outList  - matrix containing information about the fitted peaks with
%                the following columns:
%                  1) spectrum number
%                  2) mass
%                  3) height
%                  4) fwhm
%                  5) area
%                  6) sigma
%                  7) residual norm to data norm ratio
%                  8) 1 if peak was included in peak list, 0 otherwise
%
% Example usage:
%     

% Gustaf L�nn
% July 2012

outList = [];
isoList = [];
dataset = '/Peaks/LocateAndFit';
datasetIso = '/Peaks/LocateAndFitIso';
isotope_limit = 0.002;

% interpret input

if nargin == 2 && isstruct(varargin{1}) % arguments already in correct form
    args = varargin{1};
elseif round(nargin / 2) == nargin / 2 % argument pairs not matching, number should be odd
    krs_tof_message('Incorrect number of input arguments.', 'err');
    return
else % set defaults and interpret varargin
    args.peakshape = [];
    args.peaklist = [];
    args.threshold = 0.8;
    args.range = [50 200];
    args.write = true;
    args.rfun = [];
    args.limit = 4;
    args.tolerance = 0.25;
    args.verbose = true;
    args.spec = 0;
    args.init = [];
    for i = 1:2:(nargin - 1)
        field = lower(varargin{i});
        if isvarname(field)
            args.(field) = varargin{i + 1};
        end
    end
end

if ~isfield(args, 'init')
    args.init = [];
end

if ~exist(fNam, 'file')
    krs_tof_message(['Could not find the file ''' fNam '''.'], 'err');
    return
end

[~, Dexist] = krs_tof_exist_in_file(fNam, '/tofData/signal');
if ~Dexist
    krs_tof_message(['The file ''' fNam ''' is not a tofTools preprocessed file.'], 'err');
    return
end

% peak list

if ischar(args.peaklist) && exist(args.peaklist, 'file')
    pl = krs_tof_read_peakList(args.peaklist);
    peaklist = sort([pl.mass]);
    isotopicAbundances = cell(1, length(pl));
    swapped = false(1, length(pl));
    for i = 1:length(pl)
        isotopicAbundances{i} = pl(i).isoAbund;
        [isotopicAbundances{i}, inds] = sortrows(isotopicAbundances{i}, 1);
        if inds(1) ~= 1
            swapped(i) = true; % keep track of when the main isotope isn't the lightest one
        end
        isotopicAbundances{i}(:, 2) = isotopicAbundances{i}(:, 2) / isotopicAbundances{i}(1, 2);
    end
    usePeakList = true;
elseif ~isempty(args.peaklist)
    peaklist = krs_tof_exact_mass(args.peaklist);
    isotopicAbundances = cell(1, length(args.peaklist));
    swapped = false(1, length(args.peaklist));
    for i = 1:length(args.peaklist)
        isotopicAbundances{i} = krs_tof_exact_isotope_masses(args.peaklist{i}, 10);
        [isotopicAbundances{i}, inds] = sortrows(isotopicAbundances{i}, 1);
        if inds(1) ~= 1
            swapped(i) = true; % keep track of when the main isotope isn't the lightest one
        end
        isotopicAbundances{i}(:, 2) = isotopicAbundances{i}(:, 2) / isotopicAbundances{i}(1, 2);
    end
    usePeakList = true;
else
    usePeakList = false;
end

% peak shape

[~, PSexist] = krs_tof_exist_in_file(fNam, '/Peaks/PeakShape');
if ischar(args.peakshape) % load peak shape from file, overwrite
    load(args.peakshape);
    args.peakshape = peakShape;
    if ~isfield(args.peakshape, 'id')
        args.peakshape.id = krs_tof_ps_generate_id(args.peakshape.dat(:, 2));
    end
    usePeakShape = true;
elseif ~isequal(args.peakshape, false) && PSexist % use peak shape in preprocessed file
    args.peakshape.dat = double(hdf5read(fNam, '/Peaks/PeakShape'))';
    args.peakshape.id = krs_tof_ps_generate_id(args.peakshape.dat(:, 2));
    usePeakShape = true;
else % do not use peak shape
    rxi = (-30:0.1:30)';
    args.peakshape.dat = [rxi normpdf(rxi,0,1)];
    args.peakshape.id = '0';
    usePeakShape = false;
end
step = round((args.peakshape.dat(2,1)-args.peakshape.dat(1,1))*100)/100;
rxpp = args.peakshape.dat(1,1)-10:step:args.peakshape.dat(end, 1)+10;
bpp = H_scale([zeros(1,round(10/step)) args.peakshape.dat(:,2)' zeros(1,round(10/step))]);
%args.peakshape.dat(:,2) = H_scale(args.peakshape.dat(:,2));
args.peakshape.sp = spline(rxpp,bpp);
if usePeakShape
    try
        calibPeakShapeId = hdf5read(fNam, '/MassCalib', 'peakShapeId');
        calibPeakShapeId = calibPeakShapeId.Data;
        if ~strcmp(calibPeakShapeId, {args.peakshape.id, '0'})
            krs_tof_message('Different peak shapes used for mass calibration and peak fitting.', 'warn');
        end
    end
end

% delete previous datasets

fId = H5F.open(fNam, 'H5F_ACC_RDWR', 'H5P_DEFAULT');
[~, Dexist] = krs_tof_exist_in_file(fNam, dataset);
if Dexist
    H5L.delete(fId, dataset, 'H5P_DEFAULT');
end
[~, Dexist] = krs_tof_exist_in_file(fNam, datasetIso);
if Dexist
    H5L.delete(fId, datasetIso , 'H5P_DEFAULT');
end
H5F.close(fId);

% set values

signal = hdf5diskmap(fNam, '/tofData/signal');
nSpec = size(signal, 1);
if ~isequal(args.spec, 0)
    if any(args.spec > nSpec || args.spec < 1)
        krs_tof_message('Spectrum number out of range.', 'err');
        return;
    else
        loopSpec = args.spec;
    end
else
    loopSpec = 1:nSpec;
end
nSample = size(signal, 2);
[pars, eq] = krs_tof_get_mz_calib_pars(fNam);
funcArgs = eq();

if length(args.range) == 2
    loopAmu = args.range(1):args.range(2);
else
    loopAmu = args.range(args.range ~= 0);
end

if args.verbose
    fprintf('Processing... ');
    reprint;
end

% main loop
outListLength = 0;
for i = loopSpec
    spec = signal(:, i)';
    mz = krs_tof_mass_axis(nSample, pars(:, i), eq);
    maxmz = ceil(max(mz));    
    if ~isnan(maxmz) && ~all(pars(:, i) == -999)
        isotopeData = cell(1, maxmz);
        for j = loopAmu
            fInd = floor(funcArgs.inverseEQ(j - 0.5, pars(:, i)));
            lInd = ceil(funcArgs.inverseEQ(j + 0.5, pars(:, i)));
            inds = fInd:lInd;
            if usePeakList
                plInds = find(peaklist > j - 0.5 & peaklist < j + 0.5);
                args.peaklist = peaklist(plInds);
            end
            partMz = mz(inds);
            partSpec = spec(inds);
            if ~isempty(isotopeData{j})
                for k = 1:size(isotopeData{j}, 1)
                    height = isotopeData{j}(k, 2);
                    if height > isotope_limit * max(partSpec) % only include if isotope is significant in its region
                        m = isotopeData{j}(k, 1);
                        area = isotopeData{j}(k, 4);
                        pId = isotopeData{j}(k, 3);
                        w = isotopeData{j}(k, 5);
                        peakProfile = ppval(args.peakshape.sp, (partMz - isotopeData{j}(k, 1)) / w) * height;
                        partSpec = partSpec - peakProfile;
                        isoList = [isoList; NaN m height NaN area w NaN pId];
                    end
                end
            end
            out = krs_tof_fit_n_peaks(partMz, partSpec, args);
            %figure(8),clf,plot(partMz,partSpec)
            if usePeakList && length(plInds) > 0
                outInds = find(out(:, 8) == 1); % find indices of peaks from peak list
                for k = 1:length(plInds)
                    out(outInds(k), 8) = plInds(k); % sets unique peak identifier
                    for l = 2:size(isotopicAbundances{plInds(k)}, 1)
                        add = [isotopicAbundances{plInds(k)}(l, :) plInds(k) out(outInds(k), 5) out(outInds(k), 6)]; % mass height peakId area sigma
                        add(2) = add(2) * out(outInds(k), 3); % scale height and area using the main peak height
                        add(4) = add(4) * out(outInds(k), 3);
                        isotopeData{round(isotopicAbundances{plInds(k)}(l, 1))} = [isotopeData{round(isotopicAbundances{plInds(k)}(l, 1))}; add]; % add isotope data
                    end
                end
            end
            if args.verbose
                reprint(sprintf('(%d) %d', i, j));
            end
            outList = [outList; out];
        end
        if usePeakList && ~isempty(outList) && ~isempty(isoList)
            for k = find(swapped) % swap back swapped isotopes
                outInd = find(outList(:, 8) == k);
                isoInds = find(isoList(:, 8) == k);
                [~, mInd] = max([outList(outInd, 3);isoList(isoInds, 3)]);
                if mInd ~= 1
                    temp = outList(outInd, :);
                    outList(outInd, :) = isoList(isoInds(mInd - 1), :);
                    isoList(isoInds(mInd - 1), :) = temp;
                end
            end
        end
        if ~isempty(outList)
            outList(outListLength + 1:end, 1) = i;
            if args.write
                outList = sortrows(outList, 2);
                krs_tof_write_HDF(fNam, dataset, outList, 'memType', 'H5T_NATIVE_FLOAT');
                outList = [];
            else
                outListLength = size(outList, 1);
            end
        end
        if ~isempty(isoList)
            isoList(outListLength + 1:end, 1) = i;
            if args.write
                isoList = sortrows(isoList, 2);
                krs_tof_write_HDF(fNam, datasetIso, isoList, 'memType', 'H5T_NATIVE_FLOAT');
                isoList = [];
            else
                outListLength = size(outList, 1);
            end
        end
    end
end

if args.write
    if ~isempty(args.rfun)
        if isa(args.rfun, 'function_handle')
            args.rfun = func2str(args.rfun);
        else
            args.rfun = num2str(args.rfun);
        end
    else
        args.rfun = '';
    end
    attrs = {'peakShape', double(usePeakShape), 'Rfun', args.rfun, 'peakShapeId', args.peakshape.id};
    krs_tof_write_attributes(fNam, attrs, dataset);
    if usePeakShape
        krs_tof_ps_write(fNam, args.peakshape);
    end
end