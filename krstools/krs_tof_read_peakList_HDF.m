function peakList = krs_tof_read_peakList_HDF(fNam,varargin)
%
% Read peakList file saved by function tof_write_peakList
%
%Heikki Junninen
%july 2011

loadElComp = 0;
loadCharge = 0;
loadIsoAbund = 0;
nrEl = 0;
amurange=[];
peakList = [];

i=1;
while i<=length(varargin)
    argok = 1;
    if ischar(varargin{i})
        switch varargin{i}
            % argument IDs
            case 'amurange',       i=i+1; amurange = varargin{i};
            case 'elemental_composition',   loadElComp = 1;
            case 'charge',   loadCharge = 1;
            case 'isotopic_abundances',   loadIsoAbund = 1;
            case 'nrElements',   nrEl = 1;
            case 'all',    loadElComp = 1; loadCharge = 1; loadIsoAbund = 1; nrEl = 1;
                
            otherwise, argok=0;
        end
    else
        argok = 0;
    end
    if ~argok
        disp(['tof_read_peakList_HDF.m: Ignoring invalid argument #' num2str(i+1)]);
    end
    i = i+1;
end

%check what groups are present
nfo=hdf5info(fNam);
nrG=size(nfo.GroupHierarchy.Groups,2);

grpNamsInFile = cell(1,nrG);
for i=1:nrG
    grpNamsInFile{i}=nfo.GroupHierarchy.Groups(i).Name;
end
grpNamsInFile(~contains(grpNamsInFile,'/00')) = [];

fid = H5F.open(fNam,'H5F_ACC_RDWR','H5P_DEFAULT');



if isempty(amurange)
    grpNams=grpNamsInFile;
    if isempty(grpNams)
        return
    end
    amurange(1)=str2double(grpNams{1}(2:end));
    amurange(2)=str2double(grpNams{end}(2:end))+100;
else
    grpNamsDat=floor(amurange(1)/100)*100:100:floor(amurange(2)/100)*100;
    nrG=size(grpNamsDat,2);
    grpNams = [];
    for i=1:nrG
        grpNam=sprintf('/%05d',floor(grpNamsDat(i)/100)*100);
        if any(strcmp(grpNamsInFile,grpNam))
            grpNams{end+1}=grpNam;
        end
    end
end

if isempty(grpNams)
    disp('tof_read_peakList_HDF.m: no peaks at given amurange')
    return
end
iMh=0;
iEh=0;
molComp=[];
elComp=[];
mass=[];
charge=[];
nrEls=[];
isoAbund=[];

for i=1:length(grpNams)
    
    m=hdf5read(fNam,[grpNams{i},'/mass']);
    Isel = m>=amurange(1) & m<=amurange(2);
    mass=[mass,m(Isel)];
    if any(Isel)
        if loadElComp
            els=readdataset(fid,grpNams{i},'elemental_composition')';
            el=char(els(Isel,:));
            chars=hdf5read(fNam,[grpNams{i},'/charge']);
            tcharge=char(chars(Isel));
            
            for iE=1:size(el,1)
                iEh=iEh+1;
                elComp{iEh}=deblank([deblank(el(iE,:)),tcharge(iE)]);
            end
        end

        mols=readdataset(fid,grpNams{i},'molecular_composition')';
        mol=char(round(mols(Isel,:)));
        for iM=1:size(mol,1)
            iMh=iMh+1;
            molComp{iMh}=deblank(mol(iM,:));
        end        
        
        if loadCharge
            if ~loadElComp
                chars=hdf5read(fNam,[grpNams{i},'/charge']);
            end
            charge = [charge char(chars(Isel))];
            
        end
        
        if loadIsoAbund
            isoAbun = hdf5read(fNam,[grpNams{i},'/isotopic_abundances']);
            isoAbun = isoAbun(:,:,Isel);
            isoAbund = cat(3,isoAbund,isoAbun);
        end
        
        if nrEl
            nrElr = hdf5read(fNam,[grpNams{i},'/nrEl']);
            nrEls = [nrEls,nrElr(Isel)];
        end
    end
end
H5F.close(fid);
[mass,ind] = sort(mass);
peakList=struct('mass',num2cell(mass)');
[peakList.molComp]=molComp{ind};
if loadElComp
    [peakList.elComp]=elComp{ind};
end
if loadCharge
    c = regexp(charge,'.','match');
    [peakList.charge]=c{ind};
end
if loadIsoAbund
    isoAbund=permute(isoAbund,[3 2 1]);
    for i = 1:length(mass)
        peakList(i).isoAbund = reshape(isoAbund(i,:,:),[10 2]);
    end
end
if nrEl
    c = num2cell(nrEls(ind));
    [peakList.nrEl] = c{:};
end

function data = readdataset(fid,group,dataset)
gid = H5G.open(fid,group);
did = H5D.open(gid,dataset);
data = H5D.read(did,'H5ML_DEFAULT','H5S_ALL','H5S_ALL','H5P_DEFAULT');
H5D.close(did);
H5G.close(gid);