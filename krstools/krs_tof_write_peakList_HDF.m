function krs_tof_write_peakList_HDF(fNam,peakList,verbose)
%
% Write peakList to HDF file
%
% tof_write_peakList(fNam,peakList)
% fNam - file name
% peakList - cell with molecular compositions of peaks
%
% File has own group for each 100 amu so that masses below 100Th are saved
% to group 00000 and below 200Th to group 00100 and so on.
% Each group has datasets:
%  mass - mass of the peak
%  nrElements - number of elements
%  charge - charge (saved in single, so after reading in, have to be converted to char)
%  elemental_composition - elemental composition of the molecule
%                  (saved in single, so after reading in, have to be converted to char)
%  molecular_composition - molecular composition (same as input)
%                  (saved in single, so after reading in, have to be converted to char)

%Heikki Junninen
%july 2011

% initialize


if nargin < 3
    verbose = 0;
end

nrp = 0;
append = 0;
plReady = 0;
fw = 1;

if isstruct(peakList)
    plReady = 1;
    mss = peakList.mass;
elseif ~iscell(peakList)
    peakList = {peakList};    
end

if ~plReady
    if exist(fNam,'file')
        if verbose
            disp(['tof_write_peakList_HDF: Appending to file ' fNam '.']);
        end
        append = 1;
    else
        if verbose
            disp(['tof_write_peakList_HDF: Creating file ' fNam '.']);
        end
    end
    [mss,elNam,elNum]=krs_tof_exact_mass(peakList);
    dEl = mss == 0;
    if any(dEl)
        disp('tof_write_peakList_HDF.m: Removing the following unidentifiable compounds:');
        disp(peakList(dEl));
        mss(dEl) = [];
        elNam(dEl) = [];
        elNum(dEl) = [];
        peakList(dEl) = [];
    end
end

maxind = floor(max(mss)/100)+1;
electron = 0.0005485799;
mass = cell(maxind,1);
MolComp = mass;
NrEl = mass;
charge = mass;
ElComp = mass;
IsoAbund = mass;

if plReady
    mss = floor(mss/100)+1;
    [~,ind] = unique(mss,'first');
    ind = [ind;length(mss)+1];
    for i = 1:length(ind)-1
        mass{i} = peakList.mass(ind(i):ind(i+1)-1)';
        charge{i} = peakList.charge(ind(i):ind(i+1)-1)';
        NrEl{i} = peakList.charge(ind(i):ind(i+1)-1)';
        MolComp{i} = char(peakList.molecComp(ind(i):ind(i+1)-1,:));
        ElComp{i} = char(peakList.elemComp(ind(i):ind(i+1)-1,:));
        IsoAbund{i} = peakList.isoAbund(ind(i):ind(i+1)-1,:,:);
    end
else
    if append
        curDataEl = cell(maxind,1);
    end
    for i = 1:maxind
        IsoAbund{i} = zeros(1,10,2);
    end
    
    % process and structurize data
    
    for iP=1:length(mss)
        add = 1;
        upd = 0;
        ind = floor(mss(iP)/100)+1;
        if append
            if isempty(curDataEl{ind})
                try
                    curDataEl{ind} = readdataset(fNam,sprintf('%05d',(ind-1)*100),'elemental_composition');
                    tDataEl = cellstr(char(curDataEl{ind}'));
                catch
                    tDataEl = [];
                end
            else
                tDataEl = cellstr(char(curDataEl{ind}'));
            end
        end
        nrE = length(elNum{iP});
        tElComp=[];
        for iE=1:nrE
            tElComp=[tElComp,sprintf('%s%d',elNam{iP}{iE},elNum{iP}(iE)),' '];
        end
        tMolComp = peakList{iP};
        tcharge=regexp(tMolComp,'-|+','match');
        if isempty(tcharge)
            tcharge{1}=' ';
        end
        tcharge = tcharge{1};
        if append
            ttElComp = deblank(tElComp);
            if ~isempty(tDataEl)
                indp = find(cellfun(@(x) strcmp(x,ttElComp),tDataEl))';
                if ~isempty(indp)
                    add = 0;
                end
            end
        end
        if add
            try
                tIso = isoDalton_exact_mass(tElComp,10);
            catch
                tIso = [];
            end
            if tcharge == '-'
                tIso(:,1) = tIso(:,1) + electron;
            elseif tcharge == '+'
                tIso(:,1) = tIso(:,1) - electron;
            end
            if isempty(mass{ind})
                nrp = nrp + 1;
                mass{ind} = mss(iP);
                NrEl{ind} = nrE;
                charge{ind} = tcharge;
                MolComp{ind} = tMolComp;
                ElComp{ind} = tElComp;
                IsoAbund{ind}(1,1:size(tIso,1),1:2) = tIso;
            else
                nrp = nrp + 1;
                mass{ind}(end+1) = mss(iP);
                NrEl{ind}(end+1) = nrE;
                charge{ind}(end+1) = tcharge;
                MolComp{ind}(end+1,1:length(tMolComp)) = tMolComp;
                ElComp{ind}(end+1,1:length(tElComp)) = tElComp;
                IsoAbund{ind}(end+1,1:size(tIso,1),1:2) = tIso;
            end
        end
    end
end

% write data to file
ind = [];

for i = 1:maxind
    if ~isempty(mass{i})
        ind = sprintf('%05d',(i-1)*100);
        datasetMass=[ind '/mass'];
        datasetNrEl=[ind '/nrEl'];
        datasetCharge=[ind '/charge'];
        datasetMolComp=[ind '/molecular_composition'];
        datasetElComp=[ind '/elemental_composition'];
        datasetIsoAbund=[ind '/isotopic_abundances'];

        if ~exist(fNam,'file')
            krs_tof_create_HDF(fNam,datasetMass,mass{i}','H5T_NATIVE_DOUBLE',fliplr(size(mass{i})));
            fId = H5F.open(fNam,'H5F_ACC_RDWR','H5P_DEFAULT');
        else
            if fw
                fId = H5F.open(fNam,'H5F_ACC_RDWR','H5P_DEFAULT');
            end
            krs_tof_append_HDF(fNam,datasetMass,mass{i}','H5T_NATIVE_DOUBLE',fliplr(size(mass{i})),fId);
        end
        krs_tof_append_HDF(fNam,datasetNrEl,uint8(NrEl{i}'),'H5T_NATIVE_CHAR',fliplr(size(NrEl{i})),fId);
        krs_tof_append_HDF(fNam,datasetCharge,uint8(charge{i}'),'H5T_NATIVE_CHAR',fliplr(size(charge{i})),fId);
        krs_tof_append_HDF(fNam,datasetMolComp,uint8(MolComp{i}),'H5T_NATIVE_CHAR',size(MolComp{i}),fId);
        if ~isempty(ElComp{i})
            krs_tof_append_HDF(fNam,datasetElComp,uint8(ElComp{i}),'H5T_NATIVE_CHAR',size(ElComp{i}),fId);
        end
        krs_tof_append_HDF(fNam,datasetIsoAbund,IsoAbund{i},'H5T_NATIVE_DOUBLE',size(IsoAbund{i}),fId);
        fw = 0;
    end
end

if ~fw
    H5F.close(fId);
end

if ~plReady
    if verbose
        disp([num2str(nrp) ' peak(s) added.']);
    end
end

function data = readdataset(fid,group,dataset)
fid = H5F.open(fid,'H5F_ACC_RDWR','H5P_DEFAULT');
gid = H5G.open(fid,group);
did = H5D.open(gid,dataset);
data = H5D.read(did,'H5ML_DEFAULT','H5S_ALL','H5S_ALL','H5P_DEFAULT');
H5D.close(did);
H5G.close(gid);
H5F.close(fid);