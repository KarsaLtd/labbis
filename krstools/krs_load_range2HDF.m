function fileNames=krs_load_range2HDF(dateRange,avr_par,fileName)
%

global tofPath

useMask = 0;
if ~isempty(avr_par.mask)
    useMask = 1;
end

%latence check
if ~isfield(avr_par,'append')
    %     default it to 0
    avr_par.append=0;
end

if ~isfield(avr_par,'data_processing_type')
    avr_par.data_processing_type='ADC';
    %if set to 'ADC' reads the /FullSpectra otherwise /FullSpectra2
end

%check if processing type is known
if ~(strcmp(avr_par.data_processing_type,'ADC') || strcmp(avr_par.data_processing_type,'TDC'))
    krs_tof_message('Unknown data processing type. Options are: ''ADC'', ''TDC''','err')
end

res_final=avr_par.data_timeRes;
res_initial=avr_par.noise_timeRes;

dataSetTofData_sig='/tofData/signal';
dataSetTofData_tm='/tofData/time';
dataSetTofData_coa='/tofData/coadds';

% if average time step has set to 0, make no averaging

step=res_final/(60*24);

[fls,flsTim,nrFls,flsNfo]=krs_tof_scan_files(dateRange);

if res_final==0
    startTime=flsTim(1);
    stopTime=flsNfo.timVectors{end}(end);
else
    startTime=dateRange(1);
    stopTime=dateRange(2);
end

if isempty(fls)
    fileNames=[];
    return
end

[pth fNam ext]=fileparts(fileName);
fileNam=[];
fileNames = [];

% check if parameters have changed
% also check if asked data processing type exist
fNams=cell(nrFls,1);
par_change(1)=max(startTime,flsTim(1));
tofPers(1)=flsNfo.flsParam.TofPeriod(1);
sis(1)=flsNfo.flsParam.SI(1);
sInt(1)=flsNfo.flsParam.SampleInterval(1);
prType(1)=flsNfo.flsParam.NdigoDataProcessingType(1);
prTypeStrs={'ADC','TDC','Hybrid'};

% define which dataprocessing mode to read
if strcmp(avr_par.data_processing_type,'ADC')
    requestedPrType=1;
    dataSetFullSpectra='/FullSpectra';
elseif strcmp(avr_par.data_processing_type,'TDC')
    dataSetFullSpectra='/FullSpectra';
    requestedPrType=2;
end
if prType(1)==3
    if strcmp(avr_par.data_processing_type,'TDC')
        dataSetFullSpectra='/FullSpectra2';
        requestedPrType=2;
    end
end

par_change = 0; % <--------------- NOTE

if res_final==0
    %tim=unique([cat(2,flsNfo.timVectors{:}),par_change]);
    temptim = cat(2,flsNfo.timVectors{:});
    tim=unique([reshape(temptim,1,numel(temptim)),par_change]);
    %remove time points that are outside requested range
    tim(tim>dateRange(2))=[];
    tim(tim<dateRange(1))=[];
    tim(isnan(tim))=[];
    fullTime=unique([tim,flsTim']);
else
    tim=unique([startTime:step:stopTime,stopTime,par_change]);
    tim(tim < startTime) = [];
    fullTime=unique([startTime:step:stopTime,stopTime,flsTim']);
end

[mx,Imx]=krs_tof_nanmax(flsNfo.timVectors{1}(:));
tm1=flsNfo.timVectors{1}([1,Imx]);


loadSumSpectrum=0;
writeFile = 1;
anyWrite = 0;
fileNameOld='';
timFromFile=[];
IparsOk=0; %given parameters and the ones in file and the same
al.time=[]; %logs
al.msg=[]; %logs

hf=0;
tim(tim<par_change(1))=[];
createdFiles = {};
lastWrittenFile = '';

if ~isempty(avr_par.mask) % remove unused mask points, useful since the mask is saved
    delMaskInd = avr_par.mask_time > stopTime | avr_par.mask_time < startTime;
    avr_par.mask_time(delMaskInd) = [];
    avr_par.mask(delMaskInd) = [];
end

for t=1:length(tim)-1
    %check if timestep is shorter than required skip it
    %due to rounding error direct step comparison cant be done
    % if abs((tim(t+1)-tim(t))-step)<step*1e-6
    
    %create filename

    % find if file exist if appending, requested time point existing and
    % coadds is set. If 0 it means spectra has not been calculated
    doCalc=1;
    
    if doCalc
        I=find(fullTime>=tim(t) & fullTime<tim(t+1));
        mask=[];
        if ~isempty(avr_par.mask)
            Imask=find(avr_par.mask_time>=tim(t) & avr_par.mask_time<=tim(t+1));
            mask_val=avr_par.mask(Imask);
            mask_time=avr_par.mask_time(Imask);
            mask=[mask_time,mask_val];
            if useMask && isempty(mask)
                mask = [0,0];
                loadSumSpectrum=0;
            end
        end
        h=0;
        tof_dat_tmp = [];
        coadds=[];
        hasIMS = 1;
        for i=1:max(length(I),1)
            if loadSumSpectrum
                fN=[tofPath,flsNfo.flsList.name];
                
                avrDat=hdf5read(fN,dataSetFullSpectra,'/SumSpectrum');
                mz=hdf5read(fN,dataSetFullSpectra,'/MassAxis');
                
                sumCoadds=flsNfo.flsParam.NbrWaveforms.*flsNfo.flsParam.NbrBlocks.*flsNfo.flsParam.NbrMemories*nrTimePointsMeasured;
                avrDat=avrDat/sumCoadds; %mV/extraction
                a = hdf5read(fN,dataSetFullSpectra,'MassCalibration a');
                b = hdf5read(fN,dataSetFullSpectra,'MassCalibration b');
                if a==0
                    a = hdf5read(fN,dataSetFullSpectra,'MassCalibration p1');
                    b = hdf5read(fN,dataSetFullSpectra,'MassCalibration p2');
                end
                tofPeriod=flsNfo.flsParam.TofPeriod;
                singleIon=flsNfo.flsParam.SI;
                pol=flsNfo.flsParam.IonMode;
                acqLog=krs_tof_read_log(fN);
            else
                if res_initial==0
                    [avrDat,mz,sumCoadds,a,b,tofPeriod,pol,singleIon]=krs_tof_load_and_average_nofilt([fullTime(I(i)),fullTime(I(i)+1)],flsNfo,mask);
                else
                    [avrDat,mz,sumCoadds,a,b,tofPeriod,pol,singleIon]=krs_tof_load_and_average([fullTime(I(i)),fullTime(I(i)+1)],res_initial,flsNfo,mask);
                end
            end
            if ~isempty(avrDat) | avrDat==0
                if ~all(isnan(avrDat))
                    writeFile = 1;
                    anyWrite = 1;
                end
                avrDl = length(avrDat);
                h=h+1;
                if h==1
                    tof_dat_tmp=NaN(length(I),length(avrDat));
                    coadds=NaN(length(I),1);
                    as=coadds;
                    bs=coadds;
                    ps=coadds;
                    tofPeriods=coadds;
                    pols=cell(size(coadds));
                    singleIons=coadds;
                end
                tof_dat_tmp(h,:)=avrDat*sumCoadds;
                coadds(h)=sumCoadds;
                if diffSpec
                    coadds(h) = coadds(h) + sumCoaddsDiff;
                    if sumCoadds == 0 % if no data in avrDat, do this to avoid NaNs
                        tof_dat_tmp(h, :) = -avrDatDiff * sumCoaddsDiff;
                        writeFile = 1; % set flag that we have data (not in avrDat, but in avrDatDiff)
                        anyWrite = 1;
                        a = aDiff; % in case there is no avrDat for this averaging window, use these parameters instead to avoid NaNs
                        b = bDiff;
                        tofPeriod = tofPeriodDiff;
                        pol = polDiff;
                        singleIon = singleIonDiff;
                        sumCoadds = sumCoaddsDiff; % to make singleIon calculation correct, not used for anything else later
                    elseif sumCoaddsDiff ~= 0 % only do if data in avrDatDiff to avoid NaNs
                        tof_dat_tmp(h, :) = tof_dat_tmp(h, :) - avrDatDiff * sumCoaddsDiff;
                    end
                end
                
                
                as(h)=a;
                bs(h)=b;
                ps(h)=2;
                if ~all(flsNfo.flsParam.hasIMS)
                    hasIMS = 0;
                end
                tofPeriods(h)=unique(tofPeriod); %all must be the same
                pols{h}=pol{1}; % select just one, since all must be the same, and previous line will crash if no
                singleIons(h) = singleIon(1)*sumCoadds;
            end
        end
        
        if isempty(coadds)
            %             tof_dat = NaN(1,avrDl);
            tof_dat = NaN;
            as = NaN;
            bs = NaN;
            ps = NaN;
            coadds = NaN;
            writeFile=0;
        else
            if max(size(coadds)) > 1%h>1
                tof_dat=krs_tof_nansum(tof_dat_tmp)/tof_nansum(coadds);
                singleIon = krs_tof_nansum(singleIons)/krs_tof_nansum(coadds);
            else
                tof_dat=tof_dat_tmp/coadds;
                singleIon = krs_tof_nansum(singleIons)/krs_tof_nansum(coadds);
            end
        end
        if avr_par.baselineCorrection
            mz = krs_tof_mass_axis(length(tof_dat), [krs_tof_nanmean(as) krs_tof_nanmean(bs)]);
            if ~all(isnan(mz))
                [tof_dat, bl] = krs_tof_remove_baseline(mz,tof_dat);
            else
                bl = NaN(1,size(tof_dat,2));
            end
        end
        
        %% save to HDF
        
        if writeFile
            if any(Ipar_change) && ~any(strcmp(createdFiles(:),fileName))
                createdFiles{end+1} = fileName;
                if exist('ids','var')
                    krs_tof_close_HDF(ids);
                end
                ids = struct('fId',{[],[],[],[],[],[],[],[],[],[]},'dId',[],'dims_old',[]);
                %         if ~exist(fileName,'file')
                %hf=hf+1;
                %                 fileNames{hf}=fileName;
                
                ids(1) = krs_tof_write_HDF(fileName,dataSetTofData_sig,tof_dat,'keepopen',true,'memType','H5T_NATIVE_FLOAT','mode','create');
                %attributes
                if Ipar_change == 0
                    tof_freq=unique(1./(flsNfo.flsParam.TofPeriod*1e-9)); %Hz
                    sInts=unique(flsNfo.flsParam.SampleInterval);
                    DCcard=unique(flsNfo.flsParam.DC_card); %assumes DC card remains the same
                    prType=unique(flsNfo.flsParam.NdigoDataProcessingType);
                else
                    tof_freq=1./(tofPers(Ipar_change)*1e-9);
                    sInts = sInt(Ipar_change);
                    DCcard=flsNfo.flsParam.DC_card{Ipar_change}; %assumes DC card remains the same
                    prType=flsNfo.flsParam.NdigoDataProcessingType(Ipar_change);
                end
                attrs = {'Noise reduction time step [min]', res_initial, 'Averaging time step [min]', res_final, ...
                    'Signal Unit', 'mV/ext', 'Start Time', dateRange(1), 'Stop Time', dateRange(2), 'Tof Frequency', tof_freq, ...
                    'Sampling Interval (s)', sInts, 'Polarity', fileName(end-10:end-3), 'baselineCorrection', avr_par.baselineCorrection, ...
                    'hasIMS', hasIMS, 'diffSpec', double(diffSpec),'DC_card', DCcard,'NdigoProcessingType',prType,'UsedProcessingType',avr_par.data_processing_type};
                krs_tof_write_attributes(fileName, attrs, '/tofData');
                if ~isempty(mask)
                    krs_tof_write_HDF(fileName, '/tofData/mask', [avr_par.mask_time(:) avr_par.mask(:)], 'memType', 'H5T_NATIVE_DOUBLE', 'chunk', [size(mask, 1) 2]);
                end
            else
                if ~strcmp(lastWrittenFile,fileName) && ~isempty(lastWrittenFile)
                    krs_tof_close_HDF(ids);
                    ids = struct('fId',{[],[],[],[],[],[],[],[],[],[]},'dId',[],'dims_old',[]);
                elseif isempty(lastWrittenFile)
                    ids = struct('fId',{[],[],[],[],[],[],[],[],[],[]},'dId',[],'dims_old',[]);
                end
                ids(1) = krs_tof_write_HDF(fileName,dataSetTofData_sig,tof_dat,'keepopen',true,'ids',ids(1),'memType','H5T_NATIVE_FLOAT');
                %if ~exist('fId','var') || fId.double == -1
                %    fId = H5F.open(fileName,'H5F_ACC_RDWR','H5P_DEFAULT');
                %end
            end
            ids(2) = krs_tof_write_HDF(fileName,dataSetTofData_tm,tim(t),'memType','H5T_NATIVE_DOUBLE','keepopen',true,'ids',ids(2),'chunk',[length(tim)-1 1]); %time has to be in double
            ids(3) = krs_tof_write_HDF(fileName,'/MassCalib/a_log',tof_nanmean(as),'memType','H5T_NATIVE_FLOAT','keepopen',true,'ids',ids(3),'chunk',[length(tim)-1 1]);
            ids(4) = krs_tof_write_HDF(fileName,'/MassCalib/b_log',krs_tof_nanmean(bs),'memType','H5T_NATIVE_FLOAT','keepopen',true,'ids',ids(4),'chunk',[length(tim)-1 1]);
            ids(5) = krs_tof_write_HDF(fileName,'/MassCalib/ps_log',krs_tof_nanmean(ps),'memType','H5T_NATIVE_FLOAT','keepopen',true,'ids',ids(5),'chunk',[length(tim)-1 1]);
            ids(6) = krs_tof_write_HDF(fileName,'/tofData/SI',singleIon,'memType','H5T_NATIVE_FLOAT','keepopen',true,'ids',ids(6),'chunk',[length(tim)-1 1]);
            ids(7) = krs_tof_write_HDF(fileName,dataSetTofData_coa,krs_tof_nansum(coadds),'memType','H5T_NATIVE_FLOAT','keepopen',true,'ids',ids(7),'chunk',[length(tim)-1 1]);
            if avr_par.baselineCorrection
                ids(8) = krs_tof_write_HDF(fileName,'/tofData/baseline',bl,'memType','H5T_NATIVE_FLOAT','keepopen',true,'ids',ids(8));
            end
            
            %write log messages from this time period
            %             al.time=[al.time;cell2mat(acqLog(:,1))];
            %             if isempty(al.msg)
            %                 al.msg={acqLog{:,3}};
            %             else
            %                 al.msg=[al.msg;acqLog{:,3}];
            %             end
            %             ids(9) = tof_write_HDF(fileName,'/Logs/AcquisitionLog',al,'keepopen',true,'ids',ids(9),'chunk',[1 1]);
            
            %             for lt=1:size(acqLog,1)
            %             ids(9) = tof_write_HDF(fileName,'/Logs/AcquisitionLog_time',acqLog{lt,1},'memType','H5T_NATIVE_DOUBLE','keepopen',true,'ids',ids(9),'chunk',[1 1]);
            %             ids(10) = tof_write_HDF(fileName,'/Logs/AcquisitionLog_msg',acqLog{lt,3},'memType','H5T_NATIVE_CHAR','keepopen',true,'ids',ids(10),'chunk',[1 1]);
            %             end
            lastWrittenFile = fileName;
        end
        
    end %if doCalc
    krs_tof_progress_bar(progbar,'av',t/(length(tim)-1),fileName);
    %end
    
end

%write the logs to file
try
    acqLog=krs_tof_read_log([fullTime(1),fullTime(end)]);
    al.time=cell2mat(acqLog(:,1));
    al.msg={acqLog{:,3}}';
catch
    acqLog=[];
    krs_tof_message('Reading acqusition log failed')
    al.time=1;
    al.msg={'No acqusition log'};
end
if isempty(acqLog)
    krs_tof_message('No acqusition log')
    al.time=1;
    al.msg={'No acqusition log'};
end

krs_tof_write_HDF_logs(fileName, '/Logs/AcquisitionLog', al);

if anyWrite
    krs_tof_close_HDF(ids);
else
    disp('tof_load_time_range2HDF.m: No new data');
end