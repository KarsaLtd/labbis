function dims=krs_tof_get_dataset_size(fNam, dataSetNam, dim)
%
% dims=tof_get_dataset_size(fNam, dataSet, dim)
%
% get dataset dimensions
%
try
    a=h5info(fNam,dataSetNam);
    dims=fliplr(a.Dataspace.Size);
    
catch
    %old version
    % fileID = H5F.open([fNam],'H5F_ACC_RDWR','H5P_DEFAULT');
    fileID = H5F.open([fNam],'H5F_ACC_RDONLY','H5P_DEFAULT');
    
    datasetID = H5D.open(fileID,dataSetNam);
    dataspaceID = H5D.get_space(datasetID);
    
    % [numdims, dims] = H5ML.hdf5('H5Sget_simple_extent_dims', dataspaceID);
    [numdims, dims, maxdims] = H5S.get_simple_extent_dims(dataspaceID);
    
    
    H5S.close(dataspaceID);
    H5D.close(datasetID);
    H5F.close(fileID);
end
%end

if nargin == 3
    dims = dims(dim);
end
