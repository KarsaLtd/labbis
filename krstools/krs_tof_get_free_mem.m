function freeMem = krs_tof_get_free_mem

% Returns free memory in bytes, uses different methods on different
% platforms.
%
% Usage:
%   freeMem = tof_get_free_mem;

% Gustaf L�nn
% April 2011

v = version('-date');
vd = datenum(v);
vd1 = datenum(2008,2,10); % release date of version 7.6.0.324 (R2008a)

if isunix && ~ismac
    fid = fopen('/proc/meminfo');
    memdata = textscan(fid, '%s');
    freeMem = str2double(memdata{1}{(find(strcmpi(memdata{1},'memfree:')) + 1)}) * 1024;
    fclose(fid);
elseif ispc
    if vd < vd1
        freeMem = feature('memstats');
    else
        freeMem = memory;
        freeMem = freeMem.MaxPossibleArrayBytes;
    end
else % ismac
    [~, memData] = unix('vm_stat');
    memData = textscan(memData, '%s');
    pageSize = str2double(memData{1}{find(strcmp(memData{1}, 'of'), 1) + 1});
    freeMem = str2double(memData{1}{find(strcmp(memData{1}, 'free:'), 1) + 1}(1:end-1)); % ignore the dot at the end
    freeMem = freeMem * pageSize;
end