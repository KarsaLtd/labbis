function peaksOut = krs_tof_read_peakList(db,varargin)

% Reads peaks from a SQLite database written by tof_write_peakList.
%
% Input arguments:
%   db - file name of database
%   varargin - 'amurange': mass range to load peaks from
%              'query': custom SQL query, query is made in the following
%                       form: 'SELECT * FROM peaks WHERE [query]'
%              'simple': true => only retrieves data from the main table,
%                        none of the fields below are read
%              Fields to be included:
%              'location': true/false, default true
%              'environment': true/false, default true
%              'isotopes': true/false, default true
%              'comment': true/false, default true
%
% Example usage:
% tof_read_peakList('peaklist','amurange',[30 300],'query','nrEl = 3')
%
% Gustaf L�nn
% September 2011

i = 1;
amurange = [];
query = [];
simple = 0;
location = 1;
environment = 1;
comment = 1;
isotopes = 1;
calibration=1;
while i <= length(varargin)
    argok = 1;
    if ischar(varargin{i})
        switch lower(varargin{i})
            case 'amurange', i = i + 1; amurange = varargin{i};
            case 'simple', i = i + 1; if varargin{i};isotopes=0;comment=0;location=0;environment=0;end;
            case 'query', i = i + 1; query = varargin{i};
            case 'location', i = i + 1; location = varargin{i};
            case 'environment', i = i + 1; environment = varargin{i};
            case 'comment', i = i + 1; comment = varargin{i};
            case 'isotopes', i = i + 1; isotopes = varargin{i};
            case 'calibration', i = i + 1; calibration = varargin{i};
            otherwise, argok = 0;
        end
    else
        argok = 0;
    end
    if ~argok,
        disp(['tof_read_peakList: Ignoring invalid argument #' num2str(i + 1)]);
    end
    i = i + 1;
end

if isempty(regexp(db,'[.]','ONCE'))
    db = [db '.db'];
end

if ~exist(db, 'file')
    disp('tof_read_peakList.m: The given database does not exist.');
    return;
end
dbid = mksqlite(0, 'open', db);
if isempty(amurange)
    if isempty(query)
        peaksOut = mksqlite(dbid, 'SELECT * FROM peaks');
    else
        peaksOut = mksqlite(dbid, ['SELECT * FROM peaks WHERE ' query]);
    end
else
    peaksOut = mksqlite(dbid, ['SELECT * FROM peaks WHERE mass > ' num2str(amurange(1)) ' AND mass < ' num2str(amurange(2))]);
end

if isotopes
    isotopes = mksqlite(dbid, 'SELECT pId, mass, abundance FROM isotopes');
    if ~isempty(isotopes)
        pId = [isotopes.pId];
        for i = 1:length(peaksOut)
            ind = find(pId == i);
            peaksOut(i).isoAbund = [[isotopes(ind).mass]' [isotopes(ind).abundance]'];
        end
    else
        tmpField = cell(size(peaksOut));
        [peaksOut.isoAbund] = tmpField{:};
    end
end

if location
    locations = mksqlite(dbid, 'SELECT pId, name FROM locations JOIN locationlist ON locations.prId = locationlist.prId');
    if ~isempty(locations)
        pId = [locations.pId];
        for i = 1:length(peaksOut)
            ind = find(pId == i);
            peaksOut(i).location = {locations(ind).name};
        end
    else
        if ~isempty(peaksOut)
            tmpField = cell(size(peaksOut));
            [peaksOut.location] = tmpField{:};
        end
    end
end

if environment
    environments = mksqlite(dbid, 'SELECT pId, name FROM environments JOIN environmentlist ON environments.enId = environmentlist.enId');
    if ~isempty(environments)
        pId = [environments.pId];
        for i = 1:length(peaksOut)
            ind = find(pId == i);
            peaksOut(i).environment = {environments(ind).name};
        end
    else
        if ~isempty(peaksOut)
            tmpField = cell(size(peaksOut));
            [peaksOut.environment] = tmpField{:};
        end
    end
end

if comment
    if ~isempty(peaksOut)
        tmpField = repmat({''}, size(peaksOut));
        [peaksOut.comment] = tmpField{:}; % fill with empty strings
    end
    tables = mksqlite(dbid, 'SHOW TABLES');
    if any(strcmp('comments', {tables.tablename})) % check if 'comments'-table present
        comments = mksqlite(dbid, 'SELECT pId, data FROM comments');
        if ~isempty(comments)
            pId = [comments.pId];
            for i = 1:length(peaksOut)
                ind = find(pId == i);
                if ~isempty(ind)
                    peaksOut(i).comment = {comments(ind).data};
                end
            end
        end
    end
end


if calibration
    if ~isempty(peaksOut)
        tmpField = repmat({''}, size(peaksOut));
        [peaksOut.calibration] = tmpField{:}; % fill with empty strings
    end
    tables = mksqlite(dbid, 'SHOW TABLES');
    if any(strcmp('calibration', {tables.tablename})) % check if 'comments'-table present
        calibData = mksqlite(dbid, 'SELECT pId, CC1, CC2, eq, unit FROM calibration');
        if ~isempty(calibData)
            pId = [calibData.pId];
            for i = 1:length(peaksOut)
                ind = find(pId == i);
                if ~isempty(ind)
                    peaksOut(i).calibration = {calibData(ind).CC1,calibData(ind).CC2,calibData(ind).eq,calibData(ind).unit};
                    
                end
            end
        end
    end
end


mksqlite(dbid, 'close');

%[~,ind] = sort([peaksOut.mass]);
%peaksOut = peaksOut(ind);