function [pars,eq] = krs_tof_get_mz_calib_pars(fNam)

% Reads mass calibration parameters from a preprocessed file. Handles three
% different cases:
%  1) no mass calibration done
%  2) calibration done using old parameter style
%  3) calibration done using new parameter style
%
% in:
% fNam    - name of preprocessed file
%
% out:
% pars    - mass calibration parameters, one column per spectrum
% eq      - function handle of the function used for mass calibration
%
% Gustaf L�nn
% October 2011

% check if mass calibration done

try
    dummy = hdf5read(fNam,'/MassCalib','masses');
catch
    a = hdf5read(fNam,'/MassCalib/a_log');
    b = hdf5read(fNam,'/MassCalib/b_log');
    pars = [a;b];
    eq = @krs_tof_fit_tof_mz;
    return
end


% check if file is using old parameter format

try
    eq = hdf5read(fNam,'/MassCalib','equation');
catch
    nrParam = hdf5read(fNam,'/MassCalib','nrParam');
    a = hdf5read(fNam,'/MassCalib/a');
    b = hdf5read(fNam,'/MassCalib/b');
    if nrParam == 2
        eq = @krs_tof_fit_tof_mz;
        pars = [a;b];
    else
        p = hdf5read(fNam,'/MassCalib/p');
        pars = [a;b;p];
        eq = @krs_tof_fit_tof_mz_3p;
    end
    return
end

% new parameter format

pars = hdf5read(fNam,'/MassCalib/pars');
eq = str2func(eq.Data);

% Oskari added 20.12.17...
if all(pars==0)
    a = hdf5read(fNam,'/MassCalib/a_log');
    b = hdf5read(fNam,'/MassCalib/b_log');
    pars = [a;b];
    eq = @krs_tof_fit_tof_mz;
end
% ...Until here