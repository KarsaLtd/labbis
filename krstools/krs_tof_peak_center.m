function [z,p]=krs_tof_peak_center(x,y)
%
% find peak center point using polynomial fit
%

x=x(:);
y=log10(y(:));
Isel=~isinf(y);
x=x(Isel);
y=y(Isel);
if length(y)==1
    z=x;
else
    %p=polyfit(x-x(1),y,2);
    A = [ones(length(x),1) x-x(1) (x-x(1)).^2];
    p = A\y;
    z=-p(2)/(2*p(3))+x(1);
end