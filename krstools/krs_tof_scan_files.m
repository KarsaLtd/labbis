function [fls,flsTim,nrFls,flsStruct]=krs_tof_scan_files(dateRange,pathF,verbose)
%
% find files in dateRange
%  [fls,tim]=tof_scan_files(dateRange)
%output will be sorted

%Heikki Junninen, Mikael Ehn
% May 2009

global tofPath

scan_subfolders = 1;

if nargin==1
    pathF=tofPath;
    verbose = false;
end

if nargin == 2
    verbose = false;
end

if dateRange(1)==dateRange(2)
    dateRange(2)=dateRange(2)+datenum(0,0,0,23,59,59.9);
end

% include also 3 hours before and after
dateRangeEx=dateRange+[-5/24,5/24];

if isempty(pathF)
    krs_tof_message('tofPath not set. Assume the files are on current folder')
end
fls=[];
dateRangeEx1=unique(floor([dateRangeEx(1):dateRangeEx(2),dateRangeEx(2)]));
for d=1:length(dateRangeEx1)
    daten=dateRangeEx1(d);
    fls_tmp=dir([pathF,'*',krs_H_datestr(daten,39),'*.h5']);
    fls=[fls;fls_tmp];
    if scan_subfolders
        subf = dir(pathF);
        subf = subf([subf.isdir]);
        subf = subf(3:end); % Remove . and ..
        for i=1:length(subf)
            sfls = dir(fullfile(pathF, subf(i).name, ['*', krs_H_datestr(daten,39), '*.h5']));
            fls = [fls; sfls];
        end
    end
end

if nargout>1
    if length(fls)==0
        fls=[];
        flsTim=[];
        nrFls=0;
        flsStruct=[];
        %krs_tof_message(sprintf('No files found between %s and %s in %s.', datestr(dateRangeEx(1)), datestr(dateRangeEx(2)), pathF), 'warn');
    else
        [fls,flsTim,nrFls]=krs_tof_sort_files(fls);
        
        flsInfo=krs_tof_get_file_info(fls, verbose);
        Iok=find(flsInfo.stopTime>dateRange(1) & flsInfo.startTime<=dateRange(2));
        
        %calclulate file duration in sec
        %         flsDuration=flsInfo.TofPeriod*1e-9.*flsInfo.NbrSegments...
        %             .*flsInfo.NbrBufs.*flsInfo.NbrWaveforms.*flsInfo.NbrWrites.*flsInfo.NbrMemories;
        %         flsStopTim=flsTim+flsDuration/(60*60*24);
        %         %remove files that are outside the original window
        %         Iok=find(flsStopTim>dateRange(1) & flsTim<=dateRange(2));
        %         flsInfo.startTime=flsTim;
        %         flsInfo.stopTime=flsStopTim;
        %
        
        %if dateRange is smaller than time between files
        if ~any(Iok)
            %         t1=min(flsTim-dateRange(1),0);
            %         t1(t1==0)=NaN;
            %         [mx Is]=max(t1);
            %
            %         t1=min(flsTim-dateRange(2),0);
            %         t1(t1==0)=NaN;
            %         [mx Ie]=max(t1);
            %
            %         if Is==Ie
            %             Iok=Ie;
            %         end
            Iok=find(flsTim<=dateRange(1) & flsInfo.stopTime>=dateRange(2));
        end
        
        if isempty(Iok)
            fls=[];
            flsTim=[];
            nrFls=0;
            flsStruct=[];
            %krs_tof_message(sprintf('No files found between %s and %s in %s.', datestr(dateRange(1)), datestr(dateRange(2)), pathF), 'warn');
            return
        end
        fls=fls(Iok);
        flsTim=flsTim(Iok);
        nrFls=length(Iok);
        flsInfo=structfun(@(x) (x(Iok)), flsInfo,'UniformOutput', false);
        
        flsStruct.flsList=fls;
        flsStruct.flsParam=flsInfo;
        
        hasTofData=zeros(nrFls,1);
        hasEventList=zeros(nrFls,1);
        for f=1:nrFls
            path_fNam=fullfile(fls(f).folder, fls(f).name);
            
            [Ig,Ids]=krs_tof_exist_in_file(path_fNam,'/FullSpectra/TofData');
            hasTofData(f)=Ids;
            [Ig,Ids]=krs_tof_exist_in_file(path_fNam,'/FullSpectra/EventList');
            hasEventList(f)=Ids;
        end
        
        Ibad=~hasTofData & ~hasEventList;
        if any(Ibad)
            nrFls=nrFls-sum(Ibad);
            hasEventList(Ibad)=[];
            hasTofData(Ibad)=[];
            
            fls(Ibad)=[];
            flsTim(Ibad)=[];
            flsInfo=structfun(@(x) (x(~Ibad)), flsInfo,'UniformOutput', false);
            
            flsStruct.flsList=fls;
            flsStruct.flsParam=flsInfo;
            
        end
        
        %load tim vector
        timVectors=cell(nrFls,1);
        timDims=timVectors;
        tpsVolDat=timVectors;
        tpsVolHdr=timVectors;
        
        if verbose
            disp(sprintf('Reading data: '));
            reprint;
            reprint('0 %');
        end
        for f=1:nrFls
            path_fNam=fullfile(fls(f).folder, fls(f).name);
            try
                [tim,timDim]=krs_tof_get_time(path_fNam,'2D');
            catch
                krs_tof_message(['problem with file:',path_fNam],'error')
                krs_tof_message(lasterr,'error')
                return
            end
            timVectors{f}=tim;
            timDims{f}=timDim;
            %load TPS voltages if present
            try
                %no memory check, hope that all fits to memory
                twDat=h5read(path_fNam,'/TPS2/TwData');
                %search for different settings
                %             twDat_u=unique(twDat,'rows');
                twInf=h5read(path_fNam,'/TPS2/TwInfo');
                nrH=length(twInf);
                twHdr=cell(1,nrH);
                for i=1:nrH
                    twHdr{i}=deblank(twInf{i});
                end
                tpsVolDat{f}=twDat;
                tpsVolHdr{f}=twHdr;
            catch ER
                %disp('tof_scan_files:')
                %disp(ER.message)
            end
            [Ig,Ids]=krs_tof_exist_in_file(path_fNam,'/FullSpectra/TofData');
            hasTofData(f)=Ids;
            [Ig,Ids]=krs_tof_exist_in_file(path_fNam,'/FullSpectra/EventList');
            hasEventList(f)=Ids;
            if verbose
                perc = ceil(f / nrFls * 100);
                if mod(perc, 5) == 0
                    reprint(sprintf('%d %%', perc));
                end
            end
        end
        flsStruct.timVectors=timVectors;
        flsStruct.timDims=timDims;
        flsStruct.tpsVoltages=tpsVolDat;
        flsStruct.tpsVolNames=tpsVolHdr;
        flsStruct.flsParam.hasTofData=hasTofData';
        flsStruct.flsParam.hasEventList=hasEventList';
        if 0
            %     Find if any file has too many writes in attribute, but actually time
            %     dimentions are smaller. These files are probably stopped before first
            %     buf dimension has been acquired
            
            
            timDims_mat=cat(1,timDims{:});
            %chck if the last dimension (writes) matches
            Ibad=(timDims_mat(:,end)<=flsInfo.NbrWrites-1);
            flsStruct.flsList(Ibad)=[];
            flsStruct.flsParam=structfun(@(x) x(~Ibad),flsStruct.flsParam,'UniformOutput', false);
            flsStruct.timVectors(Ibad)=[];
            flsStruct.timDims(Ibad)=[];
            flsStruct.tpsVoltages(Ibad)=[];
            flsStruct.tpsVolNames(Ibad)=[];
            
            fls(Ibad)=[];
            flsTim(Ibad)=[];
            nrFls=sum(~Ibad);
        end
    end
    
    
end