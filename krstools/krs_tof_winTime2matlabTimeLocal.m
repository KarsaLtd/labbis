function tim=krs_tof_winTime2matlabTimeLocal(winTim,timStr)
%
% Convert windows timestamp in FILETIME format and time String in Local
% time to matlab format local time
%
% tim=tof_winTime2matlabTimeLocal(winTim,timStr)
%
%

% Heikki Junninen
% Feb 2017


    timUTC=krs_tof_winTime2matlabTimeUTC(winTim);
    
    %check if string is one character too short and AM and PM are
    %wrong
    if strcmp(timStr(end),'A') || strcmp(timStr(end),'P')
        timStr=[timStr,'M'];
    end
    dvecUTC=datevec(timUTC);
    try
        dvecStr=datevec(timStr);
    catch
        % New instruments have different datetime format in acquisition log
        dvecStr=datevec(timStr, 'yyyy-mm-ddTHH:MM:SS');
    end
    tim=datenum(dvecStr(1),dvecStr(2),dvecStr(3),dvecStr(4),dvecStr(5),dvecUTC(6));

    