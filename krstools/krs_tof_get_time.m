function [tim,dims,logs,log_tims]=krs_tof_get_time(path_fNam,shape)
%
% get time vector from file
%  [tim,dims,logs,log_tims]=tof_get_time(path_fNam,[shape])
% shape - time matrix formated to 1D matrix or left to original 2D
%           '1D' (default) or '2D'
% tim - time when spectra has been recodred
% dims - dimensions of time matrix
% logs - log entries
% log_tims - times of the log entries
%

%heikki Junninen
%May 2009

if nargin == 1
    shape='1D';
end

try
    imsm=h5readatt(path_fNam,'/TimingData','ImsOperationMode');
    isIMS=true;
catch
    isIMS=false;
end

readLog=0;
if nargout>2
    readLog=1;
end
dataSetNam='/TimingData/BufTimes/';
dataSetNam2='/AcquisitionLog/Log/';

if ~exist(path_fNam,'file')
    warning(['tof_get_time: Could not open file ',path_fNam,'.']);
    tim = [];
    return;
end

fileID = H5F.open(path_fNam,'H5F_ACC_RDONLY','H5P_DEFAULT');

if fileID == -1
    warning(['tof_get_time: Could not open file ',path_fNam,'.']);
    H5F.close(fileID);
    tim=[];
else
    try
        datasetID = H5D.open(fileID,dataSetNam);
    catch
        warning(['tof_get_time: Could not open ',dataSetNam,' in ',path_fNam,'.']);
        tim = [];
        dims = [];
        return;
    end
    dataspaceID = H5D.get_space(datasetID);
    [~, dims, ~] = H5S.get_simple_extent_dims(dataspaceID);
    segmentsID = H5A.open(fileID,'NbrSegments','H5P_DEFAULT');
    nbrSegments = H5A.read(segmentsID,'H5T_NATIVE_DOUBLE');
    H5A.close(segmentsID);
    if nbrSegments ~= 1
        % load values needed for time calculation
        waveformsID = H5A.open(fileID,'NbrWaveforms','H5P_DEFAULT');
        nbrWaveforms = H5A.read(waveformsID,'H5T_NATIVE_DOUBLE');
        H5A.close(waveformsID);
        groupID = H5G.open(fileID,'TimingData');
        tofperiodID = H5A.open(groupID,'TofPeriod','H5P_DEFAULT');
        tofPeriod = H5A.read(tofperiodID,'H5T_NATIVE_DOUBLE');
        H5A.close(tofperiodID);
        H5G.close(groupID);
    end
    if min(dims)==0
        warning(['tof_get_time: Dataset ',dataSetNam,' is empty in ', path_fNam,'.'])
        H5S.close(dataspaceID);
        H5D.close(datasetID);
        H5F.close(fileID);
        tim=[];
    else
        dat=H5D.read(datasetID,'H5T_NATIVE_DOUBLE','H5S_ALL','H5S_ALL','H5P_DEFAULT');
        if readLog
            datasetID2 = H5D.open(fileID,dataSetNam2);
            dat2=H5D.read(datasetID2,'H5ML_DEFAULT','H5S_ALL','H5S_ALL','H5P_DEFAULT');
            H5D.close(datasetID2);
        end
        H5S.close(dataspaceID);
        H5D.close(datasetID);
        H5F.close(fileID);
%         srtTim=tof_parse_time_from_file_name(path_fNam);
        srtTim=krs_tof_get_file_start_time(path_fNam);
        dims=size(dat);
        switch shape
            case '1D'
                tim=srtTim+reshape(dat,dims(1)*dims(2),1)/(60*60*24);
            case '2D'
                tim=srtTim+dat/(60*60*24);
        end
        %find if any uncomplete bufs. If acqusition was stopped too early
        %buftimes for this write is filled with zeros
        Iz=dat==0;
        Iz(1)=0;
        tim(Iz)=NaN;
        
        if nbrSegments ~= 1 & ~isIMS
            dims = [nbrSegments dims];
            switch shape
                case '1D'
                    segtim = zeros(size(tim,1)*nbrSegments,1);
                    segtim(1:nbrSegments:end) = tim(:);
                    for i = 2:nbrSegments
                        segtim(i:nbrSegments:end) = segtim(i-1:nbrSegments:end) + (tofPeriod * nbrWaveforms * 1e-9) / (60*60*24);
                    end
                    tim = segtim;
                case '2D'
                    segtim = zeros(dims);
                    segtim(1,:,:) = tim(:,:);
                    for i = 2:nbrSegments
                        segtim(i,:,:) = segtim(i - 1,:,:) + (tofPeriod * nbrWaveforms * 1e-9) / (60*60*24);
                    end
                    tim = segtim;
            end
        end
        
        if readLog
%             %convert log entries
%             log_tims=datenum(dat2.timestring','mm/dd/yyyy HH:MM:SS PM');
%             logs=cell(length(dat2.timestamp),1);
%             for i=1:length(dat2.timestamp)
%                 logs{i}=deblank(dat2.logtext(:,i)');
%             end
%             
            logs=krs_tof_read_log(path_fNam);
            log_tims=cell2mat(logs(:,1));
            
        end
    end
    %while isnan(tim(end))
    %    tim = tim(1:end-1);
    %end
end