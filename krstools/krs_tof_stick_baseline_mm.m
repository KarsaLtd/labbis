function krs_tof_stick_baseline_mm(fileName,stick_par)
%
%
%
%

% Heikki Junninen
% Apr.2010

if nargin==1
    stick_par=struct('amu_range',[16,4000],'smooth_step',7,...
        'stick_int_region',0.4,'noise_int_region',0.4,'mass_defect','C',...
        'baselineCorrection',0,'polarity','negative','doPlot',0);
end

%averaging time in seconds
avTm = h5readatt(fileName,'/tofData','Data time resolution [s]');

% Check if data is contained within the file or load from raw file
[~ ,Ids] = krs_tof_exist_in_file(fileName,'/tofData/signal');
if Ids
    dat = h5read(fileName, '/tofData/signal');
else
    % Load from raw file, handle windowing and data dimensions
    dat = krs_get_signal(fileName);
end

%check if polarity is correct
polStr=hdf5read(fileName,'/tofData/','Polarity');
polarity=polStr.Data;
if length(stick_par)==2
    Ipol(1)=strcmp(polarity,stick_par(1).polarity);
    Ipol(2)=strcmp(polarity,stick_par(2).polarity);
    
    stick_par=stick_par(Ipol);
    
else
    
    if ~strcmp(polarity,stick_par.polarity)
        disp('tof_stick_baseline_mm: calibration parameters for wrong polarity')
        return
    end
end

useParEdges = 0;
if isfield(stick_par,'noise_edges1')
    useParEdges = 1;
    stc_edges1 = stick_par.stick_edges1(:)';
    stc_edges2 = stick_par.stick_edges2(:)';
    noise_edges1 = stick_par.noise_edges1(:)';
    noise_edges2 = stick_par.noise_edges2(:)';
end

doPlot=stick_par.doPlot;

mnamu=stick_par.amu_range(1);
mxamu=stick_par.amu_range(2);
% doSmoothing=stick_par.smooth_step~=0;
sir=stick_par.stick_int_region;
nir=stick_par.noise_int_region;

if sir+nir>1
    disp('tof_stick_baseline_mm: signal+noise region is more than 1 amu')
end

m=krs_tof_exact_mass(stick_par.mass_defect); %exact mass
nm=krs_tof_exact_mass(stick_par.mass_defect,1); %nominal mass
mass_defect_coef=m/nm;

try
    base = hdf5diskmap(fileName, '/tofData/baseline');
catch
    base = [];
end

[pars,eq] = krs_tof_get_mz_calib_pars(fileName);
funcArgs = eq();

% check if amu_range is ok
ch = 0;
Isel = pars(1,:)~=-999;
mhmin = -Inf;
mhmax = Inf;

cmax = floor(abs(funcArgs.usedEQ(size(dat,1),pars))/max(1,mass_defect_coef))-2;
if cmax < mhmax
    mhmax = cmax;
end
cmin = ceil(abs(funcArgs.usedEQ(1,pars))/min(1,mass_defect_coef))+2;
if cmin > mhmin
    mhmin = cmin;
end

if ~isinf(mhmin)
    if mhmin > mnamu
        mnamu = mhmin;
        stick_par.amu_range(1) = mhmin;
        ch = 1;
    end
end

if ~isinf(mhmax)
    if mhmax < mxamu
        mxamu = mhmax;
        stick_par.amu_range(2) = mhmax;
        ch = 1;
    end
end
if ch
    disp(['tof_stick_baseline_mm: amu_range too wide, using [',num2str(mnamu),' ',num2str(mxamu),'] instead.']);
end
[convcoef, si]= krs_tof_get_convcoef(fileName);
convcoef = convcoef(1);
si = si(1);
% mxtof=size(dat,1);
% x=1:mxtof;

% unlink Sticks-dataset if present
krs_unlinkHDFDataset(fileName, 'Sticks', [], true);

amus=single(mnamu:mxamu)*mass_defect_coef;
%amus_r=round(amus);
if 0
winit=0.00002*amus+0.001; %empirical
sir=min(sir*max(winit/(winit(100)),1),1);
nir=min(1-sir,nir);
end
nrAmus=length(amus);
nrTim=size(dat,2);
ids = struct('fId',{[],[],[],[],[],[],[],[]},'dId',[],'dims_old',[]);
for t=1:nrTim
    sticks=single(NaN(1,mxamu));
    noise=sticks;
    tof_count_noise=sticks;
    tof_count_stcks=sticks;
    
    par = pars;
    spec=dat(:,t)';
    if ~isempty(base) % if signal is baseline corrected, add baseline
        bl = base(:,t)';
        spec = spec + bl;
    end
    
    %         tof_amu=round([a.*amus.^(1/p)+b]);
    %stick edges
    if ~useParEdges
        stc_edges1=amus-sir/2;
        stc_edges2=amus+sir/2;
        half_amus=amus-0.5;
        noise_edges1=half_amus-nir/2;
        noise_edges2=half_amus+nir/2;
    end
    tof_stc_edges1=round(funcArgs.inverseEQ(stc_edges1,par));
    tof_stc_edges2=round(funcArgs.inverseEQ(stc_edges2,par));
    %noise edges
    %         noise_edges1=[half_amus-nir/2];
    %         noise_edges2=[half_amus+nir/2];
    tof_noise_edges1=round(funcArgs.inverseEQ(noise_edges1,par));
    tof_noise_edges2=round(funcArgs.inverseEQ(noise_edges2,par));
    
    if doPlot
        figure(11011)
        
        tcolor(1,1,1:3) = [0.7 0.7 0.7];
        tcolor(1,2,1:3) = [.7 .7 .7];
        plot(spec);
        
        
        for i=1:nrAmus,
            %                     if i>=rng(si,1) && i<=rng(si,2)
            mx=max(spec(tof_stc_edges1(i):tof_stc_edges2(i)));
            x=([tof_stc_edges1(i),tof_stc_edges1(i);tof_stc_edges1(i),tof_stc_edges2(i);tof_stc_edges2(i),tof_stc_edges2(i);tof_stc_edges2(i),tof_stc_edges1(i)]);
            y=([0 0;0 0;0 mx;mx mx]);
            patch(x,y,tcolor,'edgealpha',0);
        end
        drawnow
        disp('press a key to continue')
        pause
    end
    
    %calculate sum of sepectra
    %from fixed amu range
    
    %         for i=1:nrAmus,
    %             sticks(1,amus_r(i))=(sum(spec(tof_stc_edges1(i):tof_stc_edges2(i)))/si)*tofFreq; %ions/s
    %             noise(1,amus_r(i))=(sum(spec(tof_noise_edges1(i):tof_noise_edges2(i)))/si)*tofFreq; %ions/s
    %             tof_count_noise(1,amus_r(i))=length(tof_noise_edges1(i):tof_noise_edges2(i));
    %             tof_count_stcks(1,amus_r(i))=length(tof_stc_edges1(i):tof_stc_edges2(i));
    %             %             plot(tof_stc_edges1(i):tof_stc_edges2(i),spec(tof_stc_edges1(i):tof_stc_edges2(i)),'r')
    %             %             plot(tof_noise_edges1(i):tof_noise_edges2(i),spec(tof_noise_edges1(i):tof_noise_edges2(i)),'k')
    %         end
        
        
    if any(tof_stc_edges1)
        %             tic
        %             for i=1:nrAmus,
        %                 sticks(1,i+mnamu-1)=sum(spec(tof_stc_edges1(i):tof_stc_edges2(i))) * convcoef(t); %ions/s
        %                 noise(1,i+mnamu-1)=sum(spec(tof_noise_edges1(i):tof_noise_edges2(i))) * convcoef(t); %ions/s
        %                 tof_count_noise(1,i+mnamu-1)=tof_noise_edges2(i)-tof_noise_edges1(i)+1;
        %                 tof_count_stcks(1,i+mnamu-1)=tof_stc_edges2(i)-tof_stc_edges1(i)+1;
        %                 %             plot(tof_stc_edges1(i):tof_stc_edges2(i),spec(tof_stc_edges1(i):tof_stc_edges2(i)),'r')
        %                 %             plot(tof_noise_edges1(i):tof_noise_edges2(i),spec(tof_noise_edges1(i):tof_noise_edges2(i)),'k')
        %             end
        %             toc
        for i=1:nrAmus,
            sticks(1,i+mnamu-1)=sum(spec(tof_stc_edges1(i):tof_stc_edges2(i))); %mV/ext
            noise(1,i+mnamu-1)=sum(spec(tof_noise_edges1(i):tof_noise_edges2(i))); %mV/ext
            tof_count_noise(1,i+mnamu-1)=tof_noise_edges2(i)-tof_noise_edges1(i)+1;
            tof_count_stcks(1,i+mnamu-1)=tof_stc_edges2(i)-tof_stc_edges1(i)+1;
            %             plot(tof_stc_edges1(i):tof_stc_edges2(i),spec(tof_stc_edges1(i):tof_stc_edges2(i)),'r')
            %             plot(tof_noise_edges1(i):tof_noise_edges2(i),spec(tof_noise_edges1(i):tof_noise_edges2(i)),'k')
        end
        sticks=sticks*convcoef; %ions/s
        noise=noise*convcoef; %ions/s
        
        stc_ion_count=sticks*avTm; %ions
        noise_ion_count=noise*avTm; %ions
        %calculate error for PMF
        if nir(1)==0
            err=(sqrt(sqrt(stc_ion_count).^2)/avTm);
        else
            err=(sqrt(sqrt(stc_ion_count).^2+sqrt(noise_ion_count).^2)/avTm);
        end
        
    end
    
    % Noise at an integer mass calculated as an average from noise on both sides of that mass
    noise(1:end-1) = (noise(1:end-1) + noise(2:end)) / 2;
    
    %moving average
    if ~all(isnan(noise))
        if nir(1)==0
            sticks_corr=sticks;
            bline_cps=zeros(size(sticks_corr));
        else         %             bline=movavr(medfilt1(noise,3),stick_par.smooth_step);
            bline1=medfilt1(noise,stick_par.smooth_step);
            bline1(bline1<noise*0.9)=NaN;
            bline1(mnamu)=noise(mnamu);
            bline1(end)=noise(end);
            bline=MDrepl1(bline1',NaN,'nearest')';
            bline_cps=(tof_count_stcks.*bline./tof_count_noise); %cps
            
            sticks_corr=sticks-bline_cps;
        end
    end
        
    if t==1
        try
            outamus = single(NaN(1,mxamu));
            outamus(mnamu:mxamu) = amus;
            ids(1) = krs_tof_write_HDF(fileName,'/Sticks/amus',outamus,'memType','H5T_NATIVE_FLOAT','keepopen',true);
            
            attr = 'ions/sec';
            attr_details.Name = 'Signal Unit';
            attr_details.AttachedTo = '/Sticks';
            attr_details.AttachType = 'group';
            hdf5write(fileName, attr_details, attr, 'WriteMode', 'append');
            
            attr = 'Baseline corrected signal';
            attr_details.Name = 'Signal Info';
            attr_details.AttachedTo = '/Sticks';
            attr_details.AttachType = 'group';
            hdf5write(fileName, attr_details, attr, 'WriteMode', 'append');
        end
        if useParEdges
            stick_par.noise_edges1 = [];
            stick_par.noise_edges2 = [];
            stick_par.stick_edges1 = [];
            stick_par.stick_edges2 = [];
            stick_par.customRegions = 1;
        else
            stick_par.customRegions = 0;
        end
        krs_tof_write_attributes(fileName,stick_par,'Sticks');
        
    end %if t==1
    %offset=[t,1]; %whole row
    ids(2) = krs_tof_write_HDF(fileName,'/Sticks/signal',sticks_corr,'memType','H5T_NATIVE_FLOAT','keepopen',true,'ids',ids(2));
    ids(3) = krs_tof_write_HDF(fileName,'/Sticks/baseline',bline_cps,'memType','H5T_NATIVE_FLOAT','keepopen',true,'ids',ids(3));
    
    % add NaNs to the beginning of the datasets for a more logical order
    tne1Out = single(NaN(1,mxamu));
    tne2Out = tne1Out;
    tse1Out = tne1Out;
    tse2Out = tne1Out;
    tse3Out = tne1Out;
    tne1Out(mnamu:mxamu) = tof_noise_edges1;
    tne2Out(mnamu:mxamu) = tof_noise_edges2;
    tse1Out(mnamu:mxamu) = tof_stc_edges1;
    tse2Out(mnamu:mxamu) = tof_stc_edges2;
    tse3Out = err;
    
    ids(4) = krs_tof_write_HDF(fileName,'/Sticks/baseline_edge1',tne1Out,'memType','H5T_NATIVE_FLOAT','keepopen',true,'ids',ids(4));
    ids(5) = krs_tof_write_HDF(fileName,'/Sticks/baseline_edge2',tne2Out,'memType','H5T_NATIVE_FLOAT','keepopen',true,'ids',ids(5));
    ids(6) = krs_tof_write_HDF(fileName,'/Sticks/stick_edge1',tse1Out,'memType','H5T_NATIVE_FLOAT','keepopen',true,'ids',ids(6));
    ids(7) = krs_tof_write_HDF(fileName,'/Sticks/stick_edge2',tse2Out,'memType','H5T_NATIVE_FLOAT','keepopen',true,'ids',ids(7));
    ids(8) = krs_tof_write_HDF(fileName,'/Sticks/error',err,'memType','H5T_NATIVE_FLOAT','keepopen',true,'ids',ids(8));
end %for time

krs_tof_close_HDF(ids);

%remove stick baseline also from raw data
if isfield(stick_par,'baselineCorrection')
    if stick_par.baselineCorrection
        krs_tof_remove_baseline_stc(fileName);
    end
end