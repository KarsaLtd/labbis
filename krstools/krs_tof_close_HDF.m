function krs_tof_close_HDF(ids)

% tof_close_HDF(ids)
% Closes the given HDF dataset and file handles.
%
% In:
%     ids - ids-structure (length can be > 1) given by tof_write_HDF
%
% Usage:
%     tof_close_HDF(ids);

% Gustaf L�nn
% June 2012

% id-class takes care of closing the handles properly
clear ids

% method below doesn't work on R2009b 32-bit Mac
%{
dIds = unique([ids.dId]);
for i = 1:length(dIds)
    try
        H5D.close(dIds(i));
    catch M
        M.message
    end
end

fIds = unique([ids.fId]);
for i = 1:length(fIds)
    try
        H5F.close(fIds(i));
    catch M
        M.message
    end
end
%}