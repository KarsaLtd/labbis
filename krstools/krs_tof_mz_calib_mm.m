function krs_tof_mz_calib_mm(fileName,mzCalib_par)
% Robust mass calibration
% Using predifined peak list and uses only sub set for actual calibration
%
%
% tof_mz_calib_mm(fileName)

%check if polarity is correct
polStr=hdf5read(fileName,'/tofData/','Polarity');
polarity=polStr.Data;
if length(mzCalib_par)==2
    Ipol(1)=strcmp(polarity,mzCalib_par(1).polarity);
    Ipol(2)=strcmp(polarity,mzCalib_par(2).polarity);
    mzCalib_par=mzCalib_par(Ipol);
else
    if ~strcmp(polarity,mzCalib_par.polarity)
        disp('tof_mz_calib_mm: calibration parameters for wrong polarity')
        return
    end
end

%latency check
if ~isfield(mzCalib_par,'mode')
    mzCalib_par.mode='moving';
    disp(['tof_mz_calib_mm: mode is not set for ',mzCalib_par.polarity,'. Using ''moving'''])
elseif isempty(mzCalib_par.mode)
    mzCalib_par.mode='moving';
    disp(['tof_mz_calib_mm: mode is not set for ',mzCalib_par.polarity,'. Using ''moving'''])
end
if isfield(mzCalib_par,'nrParam')
    if mzCalib_par.nrParam == 2
        mzCalib_par.equation = @krs_tof_fit_tof_mz;
    elseif mzCalib_par.nrParam == 3
        mzCalib_par.equation = @krs_tof_fit_tof_mz_3p;
    end
end
if ~isfield(mzCalib_par,'peakShape')
    mzCalib_par.peakShape = [];
elseif ischar(mzCalib_par.peakShape)
    try
        load(mzCalib_par.peakShape);
        mzCalib_par.peakShape = peakShape;
    catch
        disp('tof_mz_calib_mm: Couldn''t load peak shape file. Using Gaussian.');
        mzCalib_par.peakShape = [];
    end
end
if ~isfield(mzCalib_par, 'useLoggedParameters')
    mzCalib_par.useLoggedParameters = 1;
end
if ~isfield(mzCalib_par,'sparse')
    mzCalib_par.sparse=0;
    disp(['tof_mz_calib_mm: sparse setting is not set for ',mzCalib_par.polarity,'. Not using ''sparse'''])
elseif isempty(mzCalib_par.mode)
    mzCalib_par.mode=0;
    disp(['tof_mz_calib_mm: sparse setting is not set for ',mzCalib_par.polarity,'. Using ''sparse'''])
end


funcArgs = mzCalib_par.equation();

doPlot=mzCalib_par.doPlot;
doSparse=mzCalib_par.sparse;
o=1;
try
    dat = h5read(fileName, '/tofData/SumSpectrum');
catch
    dat = krs_get_signal(fileName);
    dat = sum(dat,2);
end

as_log = hdf5read(fileName, '/MassCalib/a_log');
bs_log = hdf5read(fileName, '/MassCalib/b_log');
ps_log = hdf5read(fileName, '/MassCalib/ps_log');
as_log = as_log(1);
bs_log = bs_log(1);
ps_log = ps_log(1);

if mzCalib_par.useLoggedParameters
    pars = [as_log;bs_log];
    eq = @krs_tof_fit_tof_mz;
else
    [pars, eq] = krs_tof_get_mz_calib_pars(fileName);
end

%remove previous calibration results from 'MassCalib' group in the file
krs_unlinkHDFDataset(fileName, 'MassCalib', [], true);

sz=size(dat);
nrR=1;
nrSn=sz(sz~=nrR);
lgpar = zeros(1,funcArgs.numOfPars);
krs_tof_write_HDF(fileName,'/MassCalib/a_log',as_log','chunk',[nrR 1]);
krs_tof_write_HDF(fileName,'/MassCalib/b_log',bs_log','chunk',[nrR 1]);
krs_tof_write_HDF(fileName,'/MassCalib/ps_log',ps_log','chunk',[nrR 1]);

mzCalib_par_tmp=mzCalib_par;
mzCalib_par_tmp.usedEQ = eq;
fId = [];

ids = struct('fId',{[],[],[],[]},'dId',[],'dims_old',[]);
%     memory
if ~isnan(pars(1)) && pars(1)~=-999
    mz=krs_tof_mass_axis(nrSn, pars, eq);
    mzCalib_par_tmp.usedPar = pars;

    spec = dat;
    %else
    if any(isnan(spec))
        spec(isnan(spec)) = 0;
    end

    a_log=double(as_log);
    b_log=double(bs_log);
    
    if all(mzCalib_par_tmp.usedPar == 0)
        par = NaN;
        ppm = NaN;
        mz = NaN;
        calib_best_mz = NaN;
        calib_best_sn = NaN;
    else
        if ~isfield(mzCalib_par, 'a')
            mzCalib_par_tmp.a = a_log;
        end
        if ~isfield(mzCalib_par, 'b')
            mzCalib_par_tmp.b = b_log;
        end
        mzCalib_par_tmp.b = b_log;
        
        [par,ppm,mz,calib_best_mz,calib_best_sn]=krs_tof_mz_calib_parab(mz,spec,mzCalib_par_tmp);
    end
    
    max_log = max(krs_tof_mass_axis([1 size(spec, 1)],[a_log;b_log]));
    max_new=max(mz);
    %if difference is more than 500Th use the logged one as starting
    %point
    if abs(max_log-max_new)>500 || isnan(max_new)
        ppm=NaN;
    else
        lgpar = par;
    end
    if isnan(ppm) || ppm > 10
        atemp = mzCalib_par_tmp.a;
        btemp = mzCalib_par_tmp.b;
        mzCalib_par_tmp.a=a_log;
        mzCalib_par_tmp.b=b_log;
        
        mz_log=krs_tof_mass_axis(size(spec,1),[a_log;b_log])';
        mzCalib_par_tmp.usedEQ = @krs_tof_fit_tof_mz;
        mzCalib_par_tmp.usedPar = [a_log;b_log];
        
        [parn,ppmn,mzn,calib_best_mzn,calib_best_snn]=krs_tof_mz_calib_parab(mz_log,spec,mzCalib_par_tmp);
        mzCalib_par_tmp.usedEQ = eq;
        mzCalib_par_tmp.a = atemp;
        mzCalib_par_tmp.b = btemp;
        max_new=max(mzn);
        if ~isnan(ppmn) && (isnan(ppm) || ppmn < ppm) && abs(max_log - max_new) < 500 && ~isnan(max_new)
            ppm = ppmn;
            par = parn;
            mz = mzn;
            calib_best_mz = calib_best_mzn;
            calib_best_sn = calib_best_snn;
            lgpar = par;
        end
        if abs(max_log-max_new)>500 || isnan(ppm)
            disp(['tof_mz_calib_mm: Mass calibration failed for spectra ',num2str(1),'. Using values from previous successful calibration.']);
            par = lgpar;
            ppm=NaN;
            calib_best_mz=NaN(1,mzCalib_par.mxNrMasses);
            calib_best_sn=calib_best_mz;
        end
    end
    
else
    ppm=-999;
    calib_best_mz=ones(1,mzCalib_par.mxNrMasses)*ppm;
    calib_best_sn=calib_best_mz;
    par = ones(1,funcArgs.numOfPars)*ppm;
end

dims = krs_tof_get_dataset_size(fileName, '/tofData/time');
specNo = dims(1);
par = repmat(par,specNo,1);
calib_best_mz = repmat(calib_best_mz,specNo,1);
calib_best_sn = repmat(calib_best_sn,specNo,1);

try
    ids(1) = krs_tof_write_HDF(fileName,'/MassCalib/pars',par,'ids',ids(1),'memType','H5T_NATIVE_FLOAT','keepopen',true,'chunk',[nrR funcArgs.numOfPars]);
    %ids(2) = krs_tof_write_HDF(fileName,'/MassCalib/ppm',ppm,'ids',ids(2),'memType','H5T_NATIVE_FLOAT','keepopen',true,'chunk',[nrR 1]);
    ids(3) = krs_tof_write_HDF(fileName,'/MassCalib/masses',calib_best_mz,'ids',ids(3),'memType','H5T_NATIVE_FLOAT','keepopen',true,'chunk',[nrR mzCalib_par.mxNrMasses]);
    ids(4) = krs_tof_write_HDF(fileName,'/MassCalib/sn',calib_best_sn,'ids',ids(4),'memType','H5T_NATIVE_FLOAT','keepopen',true,'chunk',[nrR mzCalib_par.mxNrMasses]);
catch
    disp(lasterr)
end

krs_tof_close_HDF(ids);

% G: write attributes

isn = cellfun(@isnumeric,mzCalib_par.names);
if any(isn)
    mzCalib_par.names(isn) = cellfun(@num2str,mzCalib_par.names(isn),'UniformOutput',false);
end

if isempty(mzCalib_par.peakShape)
    mzCalib_par.peakShape = 0;
    mzCalib_par.peakShapeId = '0';
else
    if ~isfield(mzCalib_par.peakShape, 'id')
        mzCalib_par.peakShape.id = krs_tof_ps_generate_id(mzCalib_par.peakShape.dat(:,2));
    end
    krs_tof_ps_write(fileName, mzCalib_par.peakShape);
    mzCalib_par.peakShapeId = mzCalib_par.peakShape.id;
    mzCalib_par.peakShape = 1;
end

usedInd = zeros(length(mzCalib_par.masses), 1);
for i=1:size(calib_best_mz, 2)
    usedInd = usedInd |  mzCalib_par.masses==calib_best_mz(1, i);
end
mzCalib_par.masses = mzCalib_par.masses(usedInd);
mzCalib_par.names = mzCalib_par.names(usedInd);
krs_tof_write_attributes(fileName,mzCalib_par,'MassCalib');
