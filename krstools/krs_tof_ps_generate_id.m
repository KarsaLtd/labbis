function id = krs_tof_ps_generate_id(peakData)

mdEngine = java.security.MessageDigest.getInstance('MD5');
mdEngine.update(typecast(single(peakData), 'uint8'));
hash = mdEngine.digest;
id = reshape(dec2hex(hash + 128), 1, 32);