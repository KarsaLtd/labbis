function [dataAvr]=krs_tof_get_average_spectrum(fNam,timeRange)
%
% Get average spectrum from a given (tofTools) file
%
% [dataAvr]=tof_get_average_spectrum(fNam);
%
% unit: ions/sec

% Heikki Junninen
% Nov 2011
try
    dataSetDim=krs_tof_get_dataset_size(fNam, '/tofData/signal');
catch
    error('tof_get_average_spectrum: provided file is not a tofTools HDF-file');
end

convcoef = krs_tof_get_convcoef(fNam);
coadds = hdf5read(fNam,'/tofData/coadds');
sumcoadds = 0;

if nargin==1
    Istrt=1;
    nbrTims=dataSetDim(1);
else
    
    if timeRange(2) <= dataSetDim(1)
        Istrt = timeRange(1);
        Iend = timeRange(2);
    elseif timeRange(2)>datenum(1900,1,1)
        tm=hdf5read(fNam,'/tofData/time');
        [~,Istrt]=min(abs(tm-timeRange(1)));
        %Istrt=min(abs(tm-timeRange(1)));
        [~,Iend]=min(abs(tm-timeRange(2)));
    else
        disp('tof_get_average_spectrum: strange timeRange value. Too high to be a number of spectrum and to low to be time')
    end
    
    nbrTims=max(Iend-Istrt+1,1);
end

% row by row (slow)
if 0
    h=0;
    dataSum=0;
    for i=Istrt:Iend
        h=h+1;
        data = hdf5readslab(fNam, '/tofData/signal', [h-1 0], [1 dataSetDim(2)]);
        dataSum=dataSum*coadds(h)*convcoef(h)+data;
        sumcoadds = sumcoadds + coadds(h);
    end
    dataAvr=dataSum/sumcoadds;
end

%all together (fast,memory consuming)
if 0
    data = hdf5readslab(fNam, '/tofData/signal', [Istrt-1 0], [dataSetDim(1) dataSetDim(2)]);
    data = bsxfun(@times, data, coadds(Istrt:(Istrt+dataSetDim(1)-1)).*convcoef(Istrt:(Istrt+dataSetDim(1)-1)));
    dataAvr=krs_tof_nanmean(data,2)/mean(coadds(Istrt:(Istrt+dataSetDim(1)-1)));
end

%max nbr rows that fit to memory (fast and memory friendly)
memAvailab = krs_tof_get_free_mem / 4; % for data in single
memReq=prod(dataSetDim)*2;
dataSum=0;
if memReq<memAvailab
    data = hdf5readslab(fNam, '/tofData/signal', [Istrt-1 0], [nbrTims dataSetDim(2)]);
    data = bsxfun(@times, data, coadds(Istrt:(Istrt+nbrTims-1)).*convcoef(Istrt:(Istrt+nbrTims-1)));
    coadds(coadds==0) = NaN;
    dataAvr=krs_tof_nanmean(data,2)/krs_tof_nanmean(coadds(Istrt:(Istrt+nbrTims-1)));
else
    nbrTimsMem=floor(3*memAvailab./(4*memReq./nbrTims)); %max time points that fits to memory
    %     nbrTimsMem=1;
    timBreaks=unique([Istrt:nbrTimsMem:Istrt+nbrTims,Istrt+nbrTims]);
    for mi=1:length(timBreaks)-1
        try
            dat = hdf5readslab(fNam, '/tofData/signal', [timBreaks(mi)-1 0], [timBreaks(mi+1)-timBreaks(mi) dataSetDim(2)]);
            dat = bsxfun(@times, dat, coadds(timBreaks(mi):timBreaks(mi+1)-1).*convcoef(timBreaks(mi):timBreaks(mi+1)-1));
            sumcoadds = sumcoadds + sum(coadds(timBreaks(mi):timBreaks(mi+1)-1));
            dataSum=dataSum+sum(dat,2);
        catch
            disp(fNam)
            disp(lasterr)
        end
    end
    dataAvr=dataSum/sumcoadds;
end