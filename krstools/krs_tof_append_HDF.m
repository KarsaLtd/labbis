function krs_tof_append_HDF(fileName,dataSetName,data,memType,chunk,file)
%
% Append data to existing HDF file
% Dimensions must match, No error checking
% Converts data to single to save space unless memorytype is given
% Chunk size is one row

%Heikki Junninen
%Apr 2010

if nargin==3
    memType='H5T_NATIVE_FLOAT';
end
if nargin==4
    if isempty(memType)
        memType='H5T_NATIVE_FLOAT';
    end
end
if nargin < 6
    cl = 1;
end
if nargin == 6
    cl = 0;
    if isempty(memType)
        memType = 'H5T_NATIVE_FLOAT';
    end
end
FILE    = fileName;
if ~strcmp('/',dataSetName(1))
    DATASET = ['/',dataSetName];
else
    DATASET = dataSetName;
end
%convert data to single (float)
if strcmp(memType,'H5T_NATIVE_FLOAT')
    data=single(data);
end

dims = fliplr(size(data));
if nargin < 5 || isempty(chunk)
    chunk = [dims(1), ones(1,length(dims)-1)];
else
    chunk = fliplr(chunk);
end


%% Open file and dataset using the default properties.
%
if cl == 1
    file = H5F.open(FILE,'H5F_ACC_RDWR','H5P_DEFAULT');
end
group=[];
try
    
    %[Gexist,DSexist]=tof_exist_in_file(FILE,DATASET);
    % use H5L.exists(file,DATASET,'H5P_DEFAULT') instead?
    t = regexp(DATASET,'/','split');
    Gexist = H5L.exists(file,t{2},'H5P_DEFAULT');
    if Gexist
        DSexist = H5L.exists(file,DATASET,'H5P_DEFAULT');
    else
        DSexist = 0;
    end
    if ~DSexist && Gexist || ~Gexist
        [dset,group]=create_new_dataset(file,FILE,DATASET,memType,dims,chunk,Gexist);
        dims_old=zeros(1,length(dims));
    else
        dset = H5D.open(file,DATASET);
        space = H5D.get_space(dset);
        %     [numdims, dims_old] = H5ML.hdf5('H5Sget_simple_extent_dims', space);
        % dims=tof_get_dataset_size(fNam, dataSetNam)
        [~, dims_old] = H5S.get_simple_extent_dims(space);
    end
    
catch ME
    %check if dataset exist
    %     DSexist=tof_exist_in_file(FILE,DATASET);
    %     if ~DSexist
    %         [dset,group]=create_new_dataset(file,FILE,DATASET,memType,dims,chunk);
    %         space = H5D.get_space(dset);
    %         numdims=2;
    %         dims_old=[0 0];
    %     else
    ME.message
    return
    %     end
end
% tid = H5D.get_type(dset); %dataset memory type id

%% Extend the dataset.
%
if length(dims)==3
    dims_extended=[dims(1),dims(2),dims_old(1)+dims(3)];
else
    dims_extended=[dims(1),dims_old(1)+dims(2)];
end
H5D.extend(dset,fliplr(dims_extended));

%% Retrieve the dataspace for the newly extended dataset.
%
space = H5D.get_space(dset);

%% Hyperslab
% Subtract a hyperslab reflecting the original dimensions from the
% selection.  The selection now contains only the newly extended
% portions of the dataset.
%
start = [zeros(1,length(dims)-1), dims_old(1)];
count=dims;

H5S.select_hyperslab(space,'H5S_SELECT_SET',fliplr(start),[],fliplr(count),[]);

% create space for the hyperslab in memory
memspaceID = H5S.create_simple(length(dims), dims, dims);

%% Write the data to the selected portion of the dataset.
%
% if ~ischar(data)
if length(dims)==3
    data = permute(data,[3 2 1]);
else
    data=data';
end
% end
stype = H5T.copy(memType);
H5D.write(dset,stype,memspaceID,space,'H5P_DEFAULT',data);
% H5D.write(dset,'H5T_NATIVE_FLOAT',memspaceID,space,'H5P_DEFAULT',data);

%% Close and release resources.
%
% H5ML.HDF5lib('H5Sclose',memspaceID);
% H5ML.HDF5lib('H5Sclose',space);
% H5ML.HDF5lib('H5Dclose',dset);

H5S.close(memspaceID);
H5S.close(space);
H5D.close(dset);
if ~isempty(group)
    H5G.close (group);
end
if cl
    H5F.close(file);
end

%% SUBFUNCTIONS

function [dset, group]=create_new_dataset(file,fNam,DATASET,memType,dims,chunk,Ig)

compress=1;
avail = H5Z.filter_avail('H5Z_FILTER_DEFLATE');
if (~avail)
    fprintf('gzip filter not available.\n');
    %     return;
    compress=0;
end
filter_info = H5Z.get_filter_info('H5Z_FILTER_DEFLATE');
if (filter_info ~= 3)
    fprintf('gzip filter not available for encoding and decoding.\n');
    %     return;
    compress=0;
end

%% Create dataspace with unlimited dimensions.
%
H5S_UNLIMITED = H5ML.get_constant_value('H5S_UNLIMITED');
maxdims = ones(1,length(dims)).*H5S_UNLIMITED;
space = H5S.create_simple(length(dims), fliplr(dims), fliplr(maxdims));

%% create group if needed
%
Ikono=strfind(DATASET,'/');
groupName=[];
if length(Ikono)==2
    groupName=DATASET(Ikono(1)+1:Ikono(2)-1);
end

group=[];
if ~isempty(groupName)
    %open group if exist, otherwise create one
    %[Ig,~]=tof_exist_in_file(fNam,DATASET);
    if Ig
        group = H5G.open(file, ['/',groupName]);
    else
        group = H5G.create(file, ['/',groupName], 1);
    end
end


%% Create the dataset creation property list, and set the chunk
% size and compression if available
%
dcpl = H5P.create('H5P_DATASET_CREATE');
H5P.set_chunk (dcpl, fliplr(chunk));
if compress
    H5P.set_deflate(dcpl, 9);
end

%% Create the unlimited dataset.
%
dset = H5D.create(file,DATASET,memType,space,dcpl);
% dset = H5D.create(file,DATASET,memType,space,'H5P_DEFAULT', dcpl);

% dset = H5D.create(file,DATASET,'H5T_STD_I32LE',space,dcpl);

