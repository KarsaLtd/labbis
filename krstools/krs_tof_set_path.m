function krs_tof_set_path(datPath, savePath)

% tof_set_path(datPath, savePath)
% Set global variables to locate files.
% 
% dataPath - path to hdf-files
% savePath - path where to save processed files

% Heikki Junninen
% May 2009

krs_tof_initialize;

global tofPath
global tofPath_save

if datPath(end) ~= filesep
    datPath = [datPath filesep];
end
if savePath(end) ~= filesep
    savePath = [savePath filesep];
end
tofPath = datPath;
tofPath_save = savePath;

if ~exist(tofPath, 'dir')
    error('The given raw data path doesn''t exist.');
end

if ~exist(tofPath_save, 'dir')
try
    mkdir(tofPath_save)
catch ER
    warning('tof_set_path: Creating the preprocessing folder failed. Create folder manually or define a location with write permissions.')
    disp(ER.message)
end
end