function hdf5writeslab(filename, data,  datasetname, offset,slabsize)

%hdf5readslab Reads a hyperslab from HDF5 files.
%
% hdf5readslab reads data from a data set in an HDF5 file.
%
% hdf5writeslab(filename, data,  datasetname, offset,slabsize)
% all data from the file FILENAME for the data set named DATASETNAME,
% which reads and array of size SLABSIZE starting from index OFFSET.
%
% if slabsize one dimension is 0 all will be read
%
% Example: read a 3x3 array from a dataset called 'u' in file 'sds.h5',
% starting from location [3, 3]:
% data=hdf5readslab('sds.h5', 'u', [3, 3], [3,3]
%

%Open the HDF File
fileID = H5F.open(filename, 'H5F_ACC_RDONLY', 'H5P_DEFAULT');

%Open the Dataset
datasetID = H5D.open(fileID, datasetname);

%Get dataspace ID
dataspaceID = H5D.get_space(datasetID);

% % to get dimensions of dataset

if any(slabsize==0)
    [rank dims] = H5S.get_simple_extent_dims(dataspaceID);
    %
    if slabsize(1)==0
        slabsize(1)=dims(1);
    else
        slabsize(2)=dims(2);
    end
end

% select the hyperslab
stride = ones(size(offset));
count = ones(size(offset));
H5S.select_hyperslab(dataspaceID, 'H5S_SELECT_SET', offset,stride, count, slabsize);

% create space for the hyperslab in memory
memspaceID = H5S.create_simple(length(slabsize), slabsize,slabsize);

%Read data with offset
% data = H5D.read(datasetID, 'H5ML_DEFAULT', memspaceID, dataspaceID, 'H5P_DEFAULT');
H5D.write (datasetID,'H5T_NATIVE_DOUBLE',memspaceID,dataspaceID,'H5P_DEFAULT',double(data)');

H5S.close(dataspaceID);
% H5S.close(memspaceID);
H5D.close(datasetID);
H5F.close(fileID);
end