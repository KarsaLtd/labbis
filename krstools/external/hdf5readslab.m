function data = hdf5readslab(filename, datasetname, offset,slabsize)

%hdf5readslab Reads a hyperslab from HDF5 files.
%
% hdf5readslab reads data from a data set in an HDF5 file.
%
% DATA = HDF5READ(FILENAME,DATASETNAME, OFFSET, SLABSIZE) returns in the variable DATA
% all data from the file FILENAME for the data set named DATASETNAME,
% which reads and array of size SLABSIZE starting from index OFFSET.
%
% if slabsize one dimension is 0 all will be read
%
% Example: read a 3x3 array from a dataset called 'u' in file 'sds.h5',
% starting from location [3, 3]:
% data=hdf5readslab('sds.h5', 'u', [3, 3], [3,3]
%

%Open the HDF File
fileID = H5F.open(filename, 'H5F_ACC_RDONLY', 'H5P_DEFAULT');

%Open the Dataset
datasetID = H5D.open(fileID, datasetname);

%Get dataspace ID
dataspaceID = H5D.get_space(datasetID);

% % to get dimensions of dataset

if any(slabsize==0)
    [rank dims] = H5S.get_simple_extent_dims(dataspaceID);
    slabsize(slabsize == 0) = dims(slabsize == 0);
end

% select the hyperslab
stride = ones(size(offset));
count = ones(size(offset));
H5S.select_hyperslab(dataspaceID, 'H5S_SELECT_SET', offset,stride, count, slabsize);

% create space for the hyperslab in memory
memspaceID = H5S.create_simple(length(slabsize), slabsize,slabsize);

% memory type
% memtype = H5T.vlen_create ('H5T_NATIVE_UINT32');
memtype='H5ML_DEFAULT';
%Read data with offset

data = H5D.read(datasetID, memtype, memspaceID, dataspaceID, 'H5P_DEFAULT');

H5S.close(dataspaceID);
H5S.close(memspaceID);
H5D.close(datasetID);
H5F.close(fileID);
end