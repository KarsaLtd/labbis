function output_args = subsref(input_args,S )
%SUBSREF Summary of this function goes here
%   Detailed explanation goes here

switch S.type
    case '()'
        if any(cellfun('isempty', S.subs))
            error('Empty subscripts no supported')
        end
        if isempty(input_args.datasetID) || isempty(input_args.fileID) || isempty(input_args.filepath) || isempty(input_args.size)
            error('Object not complete')
        else
            dataspaceID = H5ML.HDF5lib('H5Dget_space',input_args.datasetID.identifier);
            if length(S.subs) == length(input_args.size)
                if any(cellfun('isclass', S.subs, 'char'))
                    if all(cellfun('isclass', S.subs, 'char')) % Handle (:,:) get all data
                        if strcmp(S.subs{1},':') && strcmp(S.subs{2},':')
                            output_args = H5D.read(input_args.datasetID,'H5ML_DEFAULT','H5S_ALL','H5S_ALL','H5P_DEFAULT');
                            if input_args.size(2) == 1
                                output_args = output_args';
                            end
                        else
                            error('invalid subscript')
                        end
                    else
                        switch find(cellfun('isclass', S.subs, 'char')) % Handle (x:x,:) and (:,x:x)
                            case 1
                                if strcmp(S.subs{1},':') % Handle (:,x:x)
                                    mem_space_id = H5ML.HDF5lib('H5Screate_simple',length(S.subs),[length(S.subs{2}) input_args.size(2)],[]);
                                    Datatype = H5ML.HDF5lib('H5MLget_mem_datatype',input_args.datasetID.identifier);
                                    if iscontiguous(S.subs{2}) % Selected block is contiguous
                                        H5ML.HDF5lib('H5Sselect_hyperslab',dataspaceID,'H5S_SELECT_SET',[S.subs{2}(1)-1 0],[],[length(S.subs{2}) input_args.size(2)],[]);
                                        [err output_args] = H5ML.HDF5lib('H5Dread',input_args.datasetID.identifier,Datatype,mem_space_id,dataspaceID,'H5P_DEFAULT');
                                    else
                                        H5ML.HDF5lib('H5Sselect_hyperslab',dataspaceID,'H5S_SELECT_SET',[S.subs{2}(1)-1 0],[],[1 input_args.size(2)],[]);
                                        for  i = 2 : length(S.subs{2})
                                            H5ML.HDF5lib('H5Sselect_hyperslab',dataspaceID,'H5S_SELECT_OR',[S.subs{2}(i)-1 0],[],[1 input_args.size(2)],[]);
                                        end
                                        [junk,Idx] = sort(S.subs{2});
                                        [err output_args] = H5ML.HDF5lib('H5Dread',input_args.datasetID.identifier,Datatype,mem_space_id,dataspaceID,'H5P_DEFAULT');
                                        if input_args.size(2) == 1
                                            output_args = output_args(Idx)';
                                        else
                                            output_args = output_args(:,Idx);
                                        end
                                    end
                                    H5ML.HDF5lib('H5Sclose',mem_space_id);
                                    H5ML.HDF5lib('H5Tclose',Datatype);
                                else
                                    error('invalid subscript')
                                end
                            case 2
                                if strcmp(S.subs{2},':') % Handle (x:x,:)
                                    mem_space_id = H5ML.HDF5lib('H5Screate_simple',length(S.subs),[input_args.size(1) length(S.subs{1})],[]);
                                    Datatype = H5ML.HDF5lib('H5MLget_mem_datatype',input_args.datasetID.identifier);
                                    if iscontiguous(S.subs{1}) % Selected block is contiguous
                                        H5ML.HDF5lib('H5Sselect_hyperslab',dataspaceID,'H5S_SELECT_SET',[0 S.subs{1}(1)-1],[],[input_args.size(1) length(S.subs{1})],[]);
                                        [err output_args] = H5ML.HDF5lib('H5Dread',input_args.datasetID.identifier,Datatype,mem_space_id,dataspaceID,'H5P_DEFAULT');
                                        if length(S.subs{1}) == 1
                                            output_args = output_args';
                                        end
                                    else
                                        H5ML.HDF5lib('H5Sselect_hyperslab',dataspaceID,'H5S_SELECT_SET',[0 S.subs{1}(1)-1],[],[input_args.size(1) 1],[]);
                                        for  i = 2 : length(S.subs{1})
                                            H5ML.HDF5lib('H5Sselect_hyperslab',dataspaceID,'H5S_SELECT_OR',[0 S.subs{1}(i)-1],[],[input_args.size(1) 1],[]);
                                        end
                                        [junk,Idx] = sort(S.subs{1});
                                        [err output_args] = H5ML.HDF5lib('H5Dread',input_args.datasetID.identifier,Datatype,mem_space_id,dataspaceID,'H5P_DEFAULT');
                                        output_args = output_args(Idx,:);
                                    end
                                    H5ML.HDF5lib('H5Sclose',mem_space_id);
                                    H5ML.HDF5lib('H5Tclose',Datatype);
                                else
                                    error('invalid subscript')
                                end
                            otherwise
                                error('Imposible to reach here!')
                        end
                    end
                else %both subscripts are numerical
                    mem_space_id = H5ML.HDF5lib('H5Screate_simple',length(S.subs),[length(S.subs{2}) length(S.subs{1})],[]);
                    Datatype = H5ML.HDF5lib('H5MLget_mem_datatype',input_args.datasetID.identifier);
                    if iscontiguous(S.subs{1}) && iscontiguous(S.subs{2}) 
                        H5ML.HDF5lib('H5Sselect_hyperslab',dataspaceID,'H5S_SELECT_SET',[S.subs{2}(1)-1 S.subs{1}(1)-1],[],[length(S.subs{2}) length(S.subs{1})],[]);
                        [err output_args] = H5ML.HDF5lib('H5Dread',input_args.datasetID.identifier,Datatype,mem_space_id,dataspaceID,'H5P_DEFAULT');
                        if length(S.subs{1}) == 1
                            output_args = output_args';
                        end
                    elseif iscontiguous(S.subs{1}) && ~iscontiguous(S.subs{2})
                        H5ML.HDF5lib('H5Sselect_hyperslab',dataspaceID,'H5S_SELECT_SET',[S.subs{2}(1)-1 S.subs{1}(1)-1],[],[1 length(S.subs{1})],[]);
                        for  i = 2 : length(S.subs{2})
                            H5ML.HDF5lib('H5Sselect_hyperslab',dataspaceID,'H5S_SELECT_OR',[S.subs{2}(i)-1 S.subs{1}(1)-1],[],[1 length(S.subs{1})],[]);
                        end
                        [junk,Idx] = sort(S.subs{2});
                        [err output_args] = H5ML.HDF5lib('H5Dread',input_args.datasetID.identifier,Datatype,mem_space_id,dataspaceID,'H5P_DEFAULT');
                        if length(S.subs{1}) == 1
                            output_args = output_args(Idx)';
                        else
                            output_args = output_args(:,Idx);
                        end
                    elseif ~iscontiguous(S.subs{1}) && iscontiguous(S.subs{2})
                        H5S.select_hyperslab(dataspaceID,'H5S_SELECT_SET',[S.subs{2}(1)-1 S.subs{1}(1)-1],[],[length(S.subs{2}) 1],[]);
                        for  i = 2 : length(S.subs{1})
                            H5S.select_hyperslab(dataspaceID,'H5S_SELECT_OR',[S.subs{2}(1)-1 S.subs{1}(i)-1],[],[length(S.subs{2}) 1],[]);
                        end
                        [junk,Idx] = sort(S.subs{1});
                        output_args = H5D.read(input_args.datasetID,'H5ML_DEFAULT',mem_space_id,dataspaceID,'H5P_DEFAULT');
                        output_args = output_args(Idx,:);
                    else
                        warning('hdf5diskmap:select_elements','Implementing Matlab meshgrid and H5S.select_elements, opperation potentially slow.');
                        [X,Y] = meshgrid(S.subs{2},S.subs{1});
                        mem_space_id = H5S.create_simple(length(S.subs),[length(S.subs{2}) length(S.subs{1})],[]);
                        H5S.select_elements(dataspaceID,'H5S_SELECT_SET',[X(:) Y(:)]'-1);
                        output_args = H5D.read(input_args.datasetID,'H5ML_DEFAULT',mem_space_id,dataspaceID,'H5P_DEFAULT');
                    end
                    H5ML.HDF5lib('H5Sclose',mem_space_id);
                    H5ML.HDF5lib('H5Tclose',Datatype);
                end
            else
                error('incompatible number of dims')
            end
            H5ML.HDF5lib('H5Sclose',dataspaceID);
        end
    otherwise
        error('Unsuported subs reference type')
end
