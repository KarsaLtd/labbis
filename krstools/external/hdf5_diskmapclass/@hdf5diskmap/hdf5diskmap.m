function [ output_args ] = hdf5diskmap(varargin)
%HDF5DISKMAP HDF5 Matlab class to handle large data files.
%
%   Matlab class to handle large data files in a memory map
%   fashion similar to Matlab's memmapfile.
%
%  To declare a new variable (and corresponding HDF5 file)
%  Variable decleration syntax:  
%           Var = hdf5diskmap( filepath )
%           Var = hdf5diskmap( filepath, datasetname )
%           Var = hdf5diskmap( filepath, datasetname, chunksize )
%
%   Var = hdf5diskmap( filepath ) Opens an existing HDF5 file located at
%   filepath and opens the first dataset withing this file mapping to the
%   output argument Var. The file is opened in read and write mode.
%
%   Var = hdf5diskmap( filepath, datasetname ) Opens an HDF5 file
%   located at filepath and opens the dataset with name datasetname, 
%   mapping it to output argument Var. If the file does not exsit it is
%   created along with the required dataset in read and write mode.
%
%   Var = hdf5diskmap( filepath, datasetname, chunksize ) As above but
%   additionaly specifying the dataset data chunking size. This is useful
%   for speeding up many read opperations. See the HDF5 documentation.
%
%   Once the first assignment of a hdf5diskmap variable is made the array
%   precision, i.e. double/single/uint8, cannot be changed and subsequent
%   assignments must be of the same type.
%
%   To use the resulting 'hdf5diskmap' object Var as a matlab variable
%   most subscript assignment and referencing syntax of normal matlab
%   variables is possible.
%
%   Usage syntax: assignment
%
%   Var(:,:) = [ any size matrix data, double/single/uint8 precision (max 2 dimensions) ]
%   Var([x],:) = [any x by size(Var,2) scalar array]
%   Var(:,[x]) = [any x by size(Var,1) scalar array]
%   Var([x],[y]) = [any x by y scalar array]
%
%   x and y can be non consecutive, unordered, and non-contigious, however
%   multiple and repetitive read or write access in this manner can be
%   slow. Setting proper chunking blocks in dataset creatiion will
%   considerably aleviate this however. The default fill value for
%   unassigned indecies is 0.
%
%   Usage syntax: referencing
%
%   As above with assignment, i.e. Var([x],:), Var(:,[x]), Var([x],[y]),
%   however to return the full data of the variable the explicit Var(:,:)
%   is required and must be assigned to a new matlab array to be used in
%   in-place artihmetic opperations. This is due to some methods not being
%   defined as yet (see below).
%
%   NOTE: full error handling is not implemented in part due to keeping
%   speed and will be added in future releases. Aditionally subsref and
%   hence referencing has been implemented with direct calls to the HDF5
%   mex functions making it considerably faster for repetative disk access.
%   This is not so for subsasign and hence reptative assignments will be
%   slower(to be apdated in next release)

%   Suports Matlab 2008a and earlier, using old class definition syntax.
%
%       Methods implimented:
%
%           constructor(this), size, subsasgn, subsref, display, disp, numel
%
%       Methods not implimented yet:
%   
%           end, as in Var(end,:), plus, minus, divide, multiply, others...
%           
%       NOTE: Var cannot be used in-place for arrithmetic opperations and
%       os must be assigned to a normal scalar array via the explicit
%       referencing assignment. For example A = Var(:,1:10), A = A + 1 ....
%
%
%   This code is copywrite to Maria Pavlou, playfull_mf@yahoo.co.uk
%   Contributions welcome. No responsibility is taken for any loss of data
%   or damage to hardware/software. Use at your own risk.

if nargin == 1
    if isa(varargin,'hdf5diskmap')
        output_args = varargin;
    elseif ischar(varargin{1})
        if (exist(varargin{1},'file') == 2)
            try
                fileID = H5F.open(varargin{1},'H5F_ACC_RDWR','H5P_DEFAULT');
                if H5G.get_num_objs(fileID) < 1
                    error('No datasets in file');
                end
                FirstDatasetName = H5G.get_objname_by_idx(fileID,0);
                datasetID = H5D.open(fileID,FirstDatasetName);
                dataspaceID = H5D.get_space(datasetID);
                [dataspacendims,dataspacedims] = H5S.get_simple_extent_dims(dataspaceID);
            catch ME
                error('Unable to open file or first dataset. Posible non-standard file format or an empty HDF5 file.',0);
            end
            output_args.filepath = varargin{1};
            output_args.fileID = fileID;
            output_args.datasetID = datasetID;
            output_args.datasetName = FirstDatasetName;
            output_args.size = dataspacedims;
            output_args.currentselectcion = 'full';
            output_args.currentoutputdims = dataspacedims;
            output_args = class(output_args,'hdf5diskmap');
        else
            error('Unable to open file: %s \n   Please check the file is within the local path or specify a full path.',varargin{1})
        end
    else
        error('Wrong argument type')
    end
    return
end

if nargin >= 2
    if ischar(varargin{1}) && ischar(varargin{2})
        if (exist(varargin{1},'file') == 2)
%             disp(['File exists opening in read/write mode: ' varargin{1}])
            if (nargin > 2) && (isnumeric(varargin{3}))
                ChunkDims = fliplr(varargin{3});
                fa_plist = H5P.create('H5P_FILE_ACCESS');
                [mdc_nelmts rdcc_nelmts rdcc_nbytes rdcc_w0] = H5P.get_cache(fa_plist);
                rdcc_nbytes = ChunkDims(end-1)*ChunkDims(end)*2*8;
                H5P.set_cache(fa_plist,mdc_nelmts,rdcc_nelmts,rdcc_nbytes,rdcc_w0);
                try
                    fileID = H5F.open(varargin{1},'H5F_ACC_RDWR',fa_plist);
                    datasetID = H5D.open(fileID,varargin{2});
                    dataspaceID = H5D.get_space(datasetID);
                    [dataspacendims,dataspacedims] = H5S.get_simple_extent_dims(dataspaceID);
                catch
                    error(['Unable to open file or select the specified dataset: ' varargin{1} '/' varargin{2}]);
                end
                output_args.chunk_size = ChunkDims;
            else
                try
                    fileID = H5F.open(varargin{1},'H5F_ACC_RDWR','H5P_DEFAULT');
                    datasetID = H5D.open(fileID,varargin{2});
                    dataspaceID = H5D.get_space(datasetID);
                    [dataspacendims,dataspacedims] = H5S.get_simple_extent_dims(dataspaceID);
                catch
                    error(['Unable to open file or select the specified dataset: ' varargin{1} '/' varargin{2}]);
                end
                output_args.chunk_size = 'notset';
            end
            output_args.size = dataspacedims;
            output_args.filepath = varargin{1};
            output_args.fileID = fileID;
            output_args.datasetID = datasetID;
            output_args.datasetName = varargin{2};
            output_args.currentselection = 'full';
            output_args.currentoutputdims = dataspacedims;
            output_args = class(output_args,'hdf5diskmap');
        else
            disp(['Creating new file in read/write mode: ' varargin{1}])

            if (nargin > 2) && (isnumeric(varargin{3}))
                ChunkDims = fliplr(varargin{3});
                fa_plist = H5P.create('H5P_FILE_ACCESS');
                [mdc_nelmts rdcc_nelmts rdcc_nbytes rdcc_w0] = H5P.get_cache(fa_plist);
                rdcc_nbytes = ChunkDims(end-1)*ChunkDims(end)*2*8;
                H5P.set_cache(fa_plist,mdc_nelmts,rdcc_nelmts,rdcc_nbytes,rdcc_w0);
                try
                    fileID = H5F.create(varargin{1},'H5F_ACC_TRUNC','H5P_DEFAULT',fa_plist);
                catch ME
                    error('Unable to creat file: %s',varargin{1});
                end
                output_args.chunk_size = ChunkDims;
            else
                try
                    fileID = H5F.create(varargin{1},'H5F_ACC_TRUNC','H5P_DEFAULT','H5P_DEFAULT');
                catch ME
                    error('Unable to creat file: %s',varargin{1});
                end
                output_args.chunk_size = 'notset';
            end
            output_args.size = [];
            output_args.filepath = varargin{1};
            output_args.fileID = fileID;
            output_args.datasetID = [];
            output_args.datasetName = varargin{2};
            output_args.currentselection = [];
            output_args.currentoutputdims = [];
            output_args = class(output_args,'hdf5diskmap');
        end
    else
        error('Wrong argument types combination')
    end
    return
end