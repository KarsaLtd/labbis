function input_args = subsasgn(input_args, S, B)
switch S.type
    case '()'
        if (isempty(input_args.fileID) || isempty(input_args.filepath))  %Object not complete
            error('Object not complete')
        elseif all(~cellfun('isempty',{input_args.datasetID input_args.fileID input_args.filepath input_args.size})) %Object is fully inistialized

            dataspaceID = H5D.get_space(input_args.datasetID);
            datatypeID =  H5D.get_type(input_args.datasetID);

            switch class(B)
                case 'uint8'
                    indatatypeID = H5T.copy('H5T_NATIVE_UCHAR');
                case 'double'
                    indatatypeID = H5T.copy('H5T_NATIVE_DOUBLE');
                case 'single'
                    indatatypeID = H5T.copy('H5T_NATIVE_FLOAT');
                otherwise
                    error('unsuported data types')
            end

            if ~H5T.equal(datatypeID,indatatypeID)
                error('incompatible data types')
            end

            [rowsB,colsB] = size(B);
            [dataspacendims,dataspacedims,dataspacemaxdims] = H5S.get_simple_extent_dims(dataspaceID);
            if length(S.subs) == length(input_args.size) %Make sure input subsript dimensions are same a dataset dimensions
                if any(cellfun('isclass', S.subs, 'char'))
                    if all(cellfun('isclass', S.subs, 'char')) % Handle (:,:), write all subscripts
                        if strcmp(S.subs{1},':') && strcmp(S.subs{2},':')
                            if isequal(flipud(dataspacedims),[rowsB colsB]') % Input is same size and original data
                                mem_space_id = H5S.create_simple(length(S.subs),[colsB rowsB],[]);
                            elseif sum([rowsB colsB]) == 2 % Input is single variable
                                mem_space_id = H5S.create_simple(length(S.subs),dataspacedims,[]);
                                B = B*ones(dataspacedims(2),dataspacedims(1));
                            else
                                error('unequal subscript assigment')
                            end
                            H5D.write(input_args.datasetID,indatatypeID,mem_space_id,dataspaceID,'H5P_DEFAULT',B)
                        else
                            error('invalid subscript')
                        end
                    else % Handle mixed case of semicolon and subscript indeces
                        switch find(cellfun('isclass', S.subs, 'char')) % Handle (x:x,:) and (:,x:x)
                            case 1 % Handle (:,x:y)
                                if strcmp(S.subs{1},':')
                                    if isequal(dataspacedims(2),rowsB) || isequal([rowsB colsB],[1 1]) % Have same colon dimension or Single value input
                                        if isequal([rowsB colsB],[1 1])  % Single value input
                                            B = B*ones(dataspacedims(2),length(S.subs{2}));
                                            [rowsB,colsB] = size(B);
                                        end

                                        if ~isequal(length(S.subs{2}),colsB) % Check input subscript length is same as asignment input B
                                            error('unequal subscript assigment')
                                        end

                                        if  max(S.subs{2}) > dataspacedims(1) % Extend dataset dimensions if needed
                                            H5D.extend(input_args.datasetID,[max(S.subs{2}) dataspacedims(2)]);

                                            dataspaceID = H5D.get_space(input_args.datasetID);

                                            [dataspacendims,dataspacedims] = H5S.get_simple_extent_dims(dataspaceID);
                                            input_args.size = dataspacedims;
                                            input_args.currentoutputdims = dataspacedims;
                                        end

                                        if iscontiguous(S.subs{2})      % Selected block is contiguous
                                            mem_space_id = H5S.create_simple(length(S.subs),[colsB rowsB],[]);
                                            H5S.select_hyperslab(dataspaceID,'H5S_SELECT_SET',[S.subs{2}(1)-1 0],[],[length(S.subs{2}) input_args.size(2)],[]);
                                            H5D.write(input_args.datasetID,indatatypeID,mem_space_id,dataspaceID,'H5P_DEFAULT',B)
                                        else                            % Selected hyperslabs are non-contiguous
                                            mem_space_id = H5S.create_simple(length(S.subs),[colsB rowsB],[]);
                                            H5S.select_hyperslab(dataspaceID,'H5S_SELECT_SET',[S.subs{2}(1)-1 0],[],[1 input_args.size(2)],[]);
                                            for  i = 2 : length(S.subs{2})
                                                H5S.select_hyperslab(dataspaceID,'H5S_SELECT_OR',[S.subs{2}(i)-1 0],[],[1 input_args.size(2)],[]);
                                            end
                                            [junk,Idx] = sort(S.subs{2});
                                            H5D.write(input_args.datasetID,indatatypeID,mem_space_id,dataspaceID,'H5P_DEFAULT',B(:,Idx));
                                        end
                                    else
                                        error('unequal subscript assigment')
                                    end
                                else
                                    error('invalid subscript')
                                end
                            case 2 % Handle (x:y,:)
                                if strcmp(S.subs{2},':')
                                    if isequal(dataspacedims(1),colsB) || isequal([rowsB colsB],[1 1]) % Have same colon dimension or Single value input
                                        if isequal([rowsB colsB],[1 1])  % Single value input
                                            B = B*ones(length(S.subs{1},dataspacedims(1)));
                                            [rowsB,colsB] = size(B);
                                        end

                                        if ~isequal(length(S.subs{1}),rowsB) % Check input subscript length is same as asignment input B
                                            error('unequal subscript assigment')
                                        end

                                        if  max(S.subs{1}) > dataspacedims(2) % Extend dataset dimensions if needed
                                            H5D.extend(input_args.datasetID,[dataspacedims(1) max(S.subs{1})]);

                                            dataspaceID = H5D.get_space(input_args.datasetID);

                                            [dataspacendims,dataspacedims] = H5S.get_simple_extent_dims(dataspaceID);
                                            input_args.size = dataspacedims;
                                            input_args.currentoutputdims = dataspacedims;
                                        end

                                        if iscontiguous(S.subs{1})      % Selected block is contiguous
                                            mem_space_id = H5S.create_simple(length(S.subs),[colsB rowsB],[]);
                                            H5S.select_hyperslab(dataspaceID,'H5S_SELECT_SET',[0 S.subs{1}(1)-1],[],[input_args.size(1) length(S.subs{1})],[]);
                                            H5D.write(input_args.datasetID,indatatypeID,mem_space_id,dataspaceID,'H5P_DEFAULT',B)
                                        else                            % Selected hyperslabs are non-contiguous
                                            mem_space_id = H5S.create_simple(length(S.subs),[colsB rowsB],[]);
                                            H5S.select_hyperslab(dataspaceID,'H5S_SELECT_SET',[0 S.subs{1}(1)-1],[],[input_args.size(1) 1],[]);
                                            for  i = 2 : length(S.subs{1})
                                                H5S.select_hyperslab(dataspaceID,'H5S_SELECT_OR',[0 S.subs{1}(i)-1],[],[input_args.size(1) 1],[]);
                                            end
                                            [junk,Idx] = sort(S.subs{1});
                                            H5D.write(input_args.datasetID,indatatypeID,mem_space_id,dataspaceID,'H5P_DEFAULT',B(Idx,:));
                                        end
                                    else
                                        error('unequal subscript assigment')
                                    end
                                else
                                    error('invalid subscript')
                                end
                            otherwise
                                error('Imposible to reach here!')
                        end
                    end
                else
                    error('Handle multiple subscripts, Code not writen yet!')
                end
            else
                error('incompatible number of dims')
            end
        elseif (isempty(input_args.datasetID) || isempty(input_args.size)) % Data file is initialised but has no dataset.
            switch class(B)
                case 'uint8'
                    datatypeID = H5T.copy('H5T_NATIVE_UCHAR');
                    datatype = 'uint8';
                case 'double'
                    datatypeID = H5T.copy('H5T_NATIVE_DOUBLE');
                    datatype = 'double';
                case 'single'
                    datatypeID = H5T.copy('H5T_NATIVE_FLOAT');
                    datatype = 'single';
                otherwise
                    error('unsuported data types')
            end

            if strcmp(S.subs{1},':') && strcmp(S.subs{2},':')
                VarDims = size(B);
                VarDims = fliplr(VarDims);
            elseif strcmp(S.subs{1},':')
                VarDims = size(B);
                VarDims = [max(S.subs{2}) VarDims(1)];
            elseif strcmp(S.subs{2},':')
                VarDims = size(B);
                VarDims = [VarDims(2) max(S.subs{1})];
            else
                VarDims = [max(S.subs{2}) max(S.subs{1})];
            end

            dataspaceID = H5S.create_simple(length(VarDims),VarDims,{'H5S_UNLIMITED' 'H5S_UNLIMITED'});
            plistID = H5P.create('H5P_DATASET_CREATE');
            H5P.set_chunk(plistID,input_args.chunk_size); % set chunk size in property list
            
            H5P.set_fill_value(plistID,datatypeID,zeros(1,1,datatype));
            H5P.set_fill_time(plistID,'H5D_FILL_TIME_IFSET');
            H5P.set_alloc_time(plistID,'H5D_ALLOC_TIME_INCR');
           
            datasetID = H5D.create(input_args.fileID,input_args.datasetName,datatypeID,dataspaceID,plistID);
            
            [dataspacendims,dataspacedims] = H5S.get_simple_extent_dims(dataspaceID);
            input_args.size = dataspacedims;
            input_args.datasetID = datasetID;
            input_args.currentselection = 'full';
            input_args.currentoutputdims = dataspacedims;
            input_args = subsasgn(input_args,S,B);
        end
    otherwise
        error('Unsuported subs reference type')
end

%             f_plist = H5F.get_access_plist(input_args.fileID);
%             [mdc_nelmts rdcc_nelmts rdcc_nbytes rdcc_w0] = H5P.get_cache(f_plist);
%             H5P.set_cache(f_plist,mdc_nelmts,rdcc_nelmts,H5T.get_size(datatypeID)*prod(chunk_size)*rdcc_nelmts,rdcc_w0);
