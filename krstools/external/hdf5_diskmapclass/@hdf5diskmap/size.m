function [ output_args ] = size(varargin)
%SIZE Summary of this function goes here
%   Detailed explanation goes here

switch nargin
    case 0
        error('no input')
    case 1
        if isa(varargin{1},'hdf5diskmap')
            tempobj = varargin{1};
            output_args = flipud(tempobj.size)';
        else
            error('incorect input type')
        end
    case 2
         if isa(varargin{1},'hdf5diskmap') && isscalar(varargin{2})
            tempobj = varargin{1};
            if varargin{2} > length(tempobj.size)
                error('incorect dimension selection')
            else
                output_args =flipud(tempobj.size);
                output_args = output_args(varargin{2});
            end
        else
            error('incorect input type')
        end
       
    otherwise
        error('incorect number of input arguments')
end