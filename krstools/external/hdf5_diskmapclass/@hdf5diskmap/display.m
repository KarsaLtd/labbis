function display( input_args )
%DISP Summary of this function goes here
%   Detailed explanation goes here
    if isempty(input_args.size)
        disp([class(input_args) ' object ' inputname(1) ' = empty']);
        return
    end

    disp([class(input_args) ' object ' inputname(1)]);
    disp(input_args)
end
