function [ output_args ] = subselect( input_args,varargin )
%SUBSELECT Summary of this function goes here
%   Detailed explanation goes here

if isempty(input_args.datasetID) || isempty(input_args.fileID) || isempty(input_args.filepath) || isempty(input_args.size)
    error('Object not complete')
else
    dataspaceID = H5D.get_space(input_args.datasetID);
    %[dataspacendims,dataspacedims,dataspacemaxdims] = H5S.get_simple_extent_dims(dataspaceID);
    if length(varargin) == length(input_args.size)
        if any(cellfun('isclass', varargin, 'char'))
            if all(cellfun('isclass', varargin, 'char')) % Handle (:,:) get all data
                if strcmp(varargin{1},':') && strcmp(varargin{2},':')
                    output_args = input_args;
                else
                    error('invalid subscript')
                end
            else
                switch find(cellfun('isclass', varargin, 'char')) % Handle (x:x,:) and (:,x:x)
                    case 1
                        if strcmp(varargin{1},':') % Handle (:,x:x)
                            %H5S.select_hyperslab(input_args.memspaceID,'H5S_SELECT_AND',[varargin{2}(1)-1 0],[],[length(varargin{2}) input_args.size(2)],[]);
                            mem_space_id = H5S.create_simple(length(varargin),[length(varargin{2}) input_args.size(2)],[]);
                            if ((sum(diff(varargin{2}))+1) == length(varargin{2})) && issorted(varargin{2}) % Selected block is contiguous
                                H5S.select_hyperslab(input_args.dataspaceID,'H5S_SELECT_AND',[varargin{2}(1)-1 0],[],[length(varargin{2}) input_args.size(2)],[]);
                            else
                                [X,Y] = meshgrid(varargin{2},1:input_args.size(2));
                                H5S.select_elements(input_args.dataspaceID,'H5S_SELECT_AND',[X(:) Y(:)]'-1);
                            end
                            [dataspacendims,dataspacedims,dataspacemaxdims] = H5S.get_simple_extent_dims(mem_space_id);
                            output_args = input_args;
                            output_args.memspaceID = mem_space_id;
                            output_args.size = dataspacedims;
                        else
                            error('invalid subscript')
                        end
                    case 2
                        if strcmp(varargin{2},':') % Handle (:,x:x)
                            mem_space_id = H5S.create_simple(length(varargin),[input_args.size(1) length(varargin{1})],[]);

                            if ((sum(diff(varargin{1}))+1) == length(varargin{1})) && issorted(varargin{1}) % Selected block is contiguous
                                H5S.select_hyperslab(dataspaceID,'H5S_SELECT_SET',[0 varargin{1}(1)-1],[],[input_args.size(1) length(varargin{1})],[]);
                            else
                                [X,Y] = meshgrid(1:input_args.size(1),varargin{1});
                                H5S.select_elements(dataspaceID,'H5S_SELECT_SET',[X(:) Y(:)]'-1);
                            end
                            output_args = H5D.read(input_args.datasetID,'H5ML_DEFAULT',mem_space_id,dataspaceID,'H5P_DEFAULT');
                        else
                            error('invalid subscript')
                        end
                    otherwise
                        error('Imposible to reach here!')
                end
            end
        else
            [X,Y] = meshgrid(varargin{2},varargin{1});
            %file_space_id = H5S.create_simple(2,[128 20],[]);
            mem_space_id = H5S.create_simple(length(varargin),[length(varargin{2}) length(varargin{1})],[]);
            %H5S.select_none(mem_space_id);
            H5S.select_elements(dataspaceID,'H5S_SELECT_SET',[X(:) Y(:)]'-1);
            %dataspaceID = H5D.get_space(input_args.datasetID);
            %[dataspacendims,dataspacedims,dataspacemaxdims] = H5S.get_simple_extent_dims(file_space_id);
            output_args = H5D.read(input_args.datasetID,'H5ML_DEFAULT',mem_space_id,dataspaceID,'H5P_DEFAULT');
        end
    else
        output_args = input_args;
        %error('incompatible number of dims')
    end
    %file_space_id = H5S.create_simple(2,[128 1],[]);
    %output_args = H5D.read(datasetID,'H5ML_DEFAULT',file_space_id,file_space_id,'H5P_DEFAULT');
    %output_args = input_args(varargin{:});
end
