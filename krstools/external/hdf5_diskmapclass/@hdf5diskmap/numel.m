function [ output_args ] = numel( input_args )
%NUMEL Summary of this function goes here
%   Detailed explanation goes here
    
    if isempty(input_args.size)
        output_args = 0;
        return
    end
    
    output_args = prod(input_args.size);
end
