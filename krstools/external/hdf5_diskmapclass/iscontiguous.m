function b = iscontiguous(a)
if all(size(a) > 1)
    error('not supported')
else
    if length(a) > 1
        if issorted(a)
            b = all(diff(a) == 1);
        else
            b = false;
        end
    elseif length(a) == 1
        b = true;
    else
        b = false;
    end
end

