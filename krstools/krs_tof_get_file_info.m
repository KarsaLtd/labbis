function [flsInfo,tm]=krs_tof_get_file_info(fls, verbose)
%
% get files sampling parameters
% flsInfo=tof_get_file_info(fls)
%
% fls - file structure (from dir)
% flsInfo - structure with parameters
% 
% flsInfo.NbrSamples - number of samples per spectrum
% flsInfo.TofPeriod - tof period
% flsInfo.NbrWaveforms - number of coadded extractions
% flsInfo.NbrSegments - number of segments set to measure
% flsInfo.NbrBlocks - number of Blocks set to measure
% flsInfo.NbrMemories - number of memories set to measure
% flsInfo.NbrBufs - number of bufs set to measure
% flsInfo.NbrWrites - number of files set to measure
% flsInfo.SampleInterval - sample interval
% flsInfo.SI - single ion
% flsInfo.StartDelay - start delay
% flsInfo.IonMode - polarity
% flsInfo.startTime - start time
% flsInfo.flsDuration -file duration 
% flsInfo.stopTime - stop time
% flsInfo.actualDuration - actual length of the file
% flsInfo.DC_card - digital converter card used
% flsInfo.hasIMS - true if instrument was IMS
% flsInfo.NdigoDataProcessingType - in case of Ndigo DC-card, which data
%        processing was used. 0 -raw, 1 - ADC, 2, - TDC, 3 - hybrid (both)
%        -1 - if DC card was not Ndigo
% 

% Heikki Junninen
% 2011

if nargin == 1
    verbose = false;
end

global tofPath

if nargin==0
    verbose = false;
    fls=dir([tofPath,'*.h5']);
end

nrFls=length(fls);
NbrSamp=NaN(nrFls,1);
IonMode=cell(nrFls,1);
SampleInterval=NbrSamp;
StartDelay=SampleInterval;
NbrWaveforms=SampleInterval;
NbrSegments=SampleInterval;
NbrBlocks=SampleInterval;
NbrMemories=SampleInterval;
NbrBufs=SampleInterval;
NbrWrites=SampleInterval;
si=SampleInterval;
NbrSamples=SampleInterval;
TofPeriod=SampleInterval;
startTim=SampleInterval;
flsDuration=SampleInterval;
stopTim=SampleInterval;
actualDuration=SampleInterval;
hasIMS=zeros(nrFls,1);
DC_card=cell(nrFls,1);
NdigoDataProcessingType=ones(nrFls,1)*-1;

saveType=0;

if verbose
    disp(sprintf('Getting file info: '));
    reprint;
    reprint('0 %');
end

for f=1:nrFls
    try
        path_fNam_AS=fullfile(fls(f).folder,fls(f).name);
        
        IonMode0 = hdf5read(path_fNam_AS,'/','IonMode');
        IonMode{f} = IonMode0.Data;
        NbrSamples(f)=single(hdf5read(path_fNam_AS,'/','NbrSamples'));
        SampleInterval(f) = hdf5read(path_fNam_AS,'/FullSpectra/','SampleInterval');
        si(f) = hdf5read(path_fNam_AS,'/FullSpectra/','Single Ion Signal');
        StartDelay(f)=single(hdf5read(path_fNam_AS,'/TimingData/','StartDelay'));
        NbrWaveforms(f)=single(hdf5read(path_fNam_AS,'/','NbrWaveforms'));
        NbrSegments(f)=single(hdf5read(path_fNam_AS,'/','NbrSegments'));
        NbrBlocks(f)=single(hdf5read(path_fNam_AS,'/','NbrBlocks'));
        NbrMemories(f)=single(hdf5read(path_fNam_AS,'/','NbrMemories'));
        NbrBufs(f)=single(hdf5read(path_fNam_AS,'/','NbrBufs'));
        NbrWrites(f)=single(hdf5read(path_fNam_AS,'/','NbrWrites'));
        TofPeriod(f)=single(hdf5read(path_fNam_AS,'/TimingData','TofPeriod'));
        
        a=h5info(path_fNam_AS,'/RawData');
        c=regexp(a.Groups.Name,'/','split');
        DC_card{f}=c{end};
        
        if strcmp(DC_card{f},'Ndigo5G')
            NdigoDataProcessingType(f)=single(hdf5read(path_fNam_AS,['/RawData/',DC_card{f}],'Data processing type'));
        end
        
        
        % AcqTimZero=double(hdf5read(path_fNam_AS,'/TimingData','AcquisitionTimeZero'));
        % startTim(f)=datenum(1601,1,1,0,0,AcqTimZero*100*1e-9); %this time is in UTC
        
        %to get local time we need parse the string.
        %some old tofDAQ verison had an error in this string!
        %Chould be ok with new versions (something like 2010 or 2011 ->)
        
        %daq_ver=round(double(hdf5read(path_fNam_AS,'/','TofDAQ Version'))*100)/100;
        
        
        st=krs_tof_parse_time_from_file_name(path_fNam_AS);
        if isempty(st)
            d=hdf5read(path_fNam_AS,'/','HDF5 File Creation Time');
            st=krs_tof_parse_date_string(d.Data);
        end
        if isempty(st)
            d=hdf5read(path_fNam_AS,'/','HDF5 File Creation Time');
            krs_tof_message(['Date format not supported ',d.Data]);
        else
            startTim(f)=st;
        end
        
        
        %calculate files duration and stop times
        %	flsDuration(f)=TofPeriod(f)*1e-9.*NbrSegments(f)...
        %	.*NbrBufs(f).*NbrWaveforms(f).*NbrWrites(f).*NbrMemories(f);
        %
        %stopTim(f)=startTim(f)+flsDuration(f)/(60*60*24);
        
        %check the actual duration of the file
        try
            %tm=h5read(path_fNam_AS,'/TimingData/BufTimes');
            dim=krs_tof_get_dataset_size(path_fNam_AS,'/TimingData/BufTimes');
            tm = h5read(path_fNam_AS,'/TimingData/BufTimes',[1 dim(1)],[dim(2) 1]);
            
        catch
            tm=hdf5read(path_fNam_AS,'/TimingData/BufTimes');
        end
        
        [mx,Imx]=max(tm(:));
        actualDuration(f)=mx+TofPeriod(f)*1e-9;
        %tm=tm(1:Imx);
        stopTim(f)=startTim(f)+actualDuration(f)/(60*60*24);
        if verbose
            perc = ceil(f / nrFls * 100);
            if mod(perc, 5) == 0
                reprint(sprintf('%d %%', perc));
            end
        end
        if NbrWaveforms(f) == 1 && NbrSegments(f) > 50
            hasIMS(f) = 1;
        end
    catch ER
        if verbose
            reprint(' ');
            drawnow
            disp(sprintf('%s', repmat(char(8), 1, 22)));
        end
        krs_tof_message(['bad file: ',path_fNam_AS])
        if verbose
            disp(sprintf('Getting file info: '));
            reprint;
        end
        %rethrow(ER)
    end
end

flsInfo.NbrSamples=NbrSamples;
flsInfo.TofPeriod=TofPeriod;
flsInfo.NbrWaveforms=NbrWaveforms;
flsInfo.NbrSegments=NbrSegments;
flsInfo.NbrBlocks=NbrBlocks;
flsInfo.NbrMemories=NbrMemories;
flsInfo.NbrBufs=NbrBufs;
flsInfo.NbrWrites=NbrWrites;
flsInfo.SampleInterval=SampleInterval;
flsInfo.SI=si;
flsInfo.StartDelay=StartDelay;
flsInfo.IonMode=IonMode;
flsInfo.startTime=startTim;
flsInfo.flsDuration=flsDuration;
flsInfo.stopTime=stopTim;
flsInfo.actualDuration=actualDuration;
flsInfo.DC_card=DC_card;
flsInfo.hasIMS=hasIMS;
flsInfo.NdigoDataProcessingType=NdigoDataProcessingType;

