function krs_tof_write_attributes(fNam,attributes,path,type)

% write attributes to HDF file
%
% fNam         - filename
% attributes   - structure/cell of attributes to be written, cell should be
%                in form of {name, value, name, value, ...}
% path         - where to attach the attributes
% type         - type of object to attach attributes to (e.g. dataset,
%                group), if left empty, the script does an assumption based
%                on the path

%% interpret arguments

setRoot = 0;

if strcmp(path, '/')
    setRoot = 1;
    parts = {' '};
    type = 'group';
else
    if strcmp('/', path(1))
        path = path(2:end);
    end
    
    if strcmp('/', path(end))
        path = path(1:end-1);
    end
    parts = regexp(path, '/', 'split');
end

if nargin == 3 && ~setRoot
    if length(parts) == 1
        type = 'group';
    else
        type = 'dataset';
    end
end

%% convert structure to cell form

if isstruct(attributes)
    oldattributes = attributes;
    clear attributes;
    structnames = fieldnames(oldattributes);
    attributes = cell(1,length(structnames)*2);
    for i = 1:length(structnames)
        attributes{2 * i - 1} = structnames{i};
        attributes{2 * i} = oldattributes.(structnames{i});
    end
end

try
    %% delete old attributes
    
    file = H5F.open(fNam,'H5F_ACC_RDWR','H5P_DEFAULT');
    % delete 3rd argument to test on 2009b version of MATLAB
    %group = H5G.open(file,parts{1},'H5P_DEFAULT');
    if ~setRoot
        group = H5G.open(file, parts{1});
        if length(parts) == 2
            dset = H5D.open(group, parts{2});
        end
    else
        group = file;
    end
    for i = 1:length(attributes)/2
        
        % delete attribute if exist (in cases where parameters are
        % updated)
        if length(parts) == 1
            try
                H5A.open(group,attributes{2 * i - 1},'H5P_DEFAULT');
                H5A.delete(group,attributes{2 * i - 1});
            catch ME
            end
        else
            try
                H5A.open(dset,attributes{2 * i - 1},'H5P_DEFAULT');
                H5A.delete(dset,attributes{2 * i - 1});
            catch ME
            end
        end
    end
    if ~setRoot
        if length(parts) == 2
            H5D.close(dset);
        end
        H5G.close(group);
    end
    H5F.close(file);
    
    %% write new attributes
    
    for i = 1:length(attributes)/2
        attr = attributes{2 * i};
        if isa(attr, 'function_handle')
            attr = func2str(attr);
        elseif isa(attr, 'logical')
            attr = double(attr);
        end
        attr_details.Name = attributes{2 * i - 1};
        attr_details.AttachedTo = path;
        attr_details.AttachType = type;
        hdf5write(fNam, attr_details, attr, 'WriteMode', 'append');
    end
catch ME
    ME.message
end