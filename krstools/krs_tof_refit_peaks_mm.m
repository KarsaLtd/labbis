function krs_tof_refit_peaks_mm(fNam, param, labapp)

% fit peaks
% peaklist can either be given or read from file
%
% fNam            - file to which the fitted peaks will be written; peaks are
%                   also read from this file if no custom peaklist is given
% param           - parameters for fitting
%
% param.R         - used to calculate peak widths
% param.peakList  - custom peaklist, cell containing exact masses or chemical
%                   formulas of the peaks to be fitted
% param.peakShape - path to peakShape-function file

% argument check

if nargin==3
    filenos = regexp(labapp.progbar.Message,'\d*','Match');
    filei = str2double(filenos{1});
    fileno = str2double(filenos{2});
end
cancel = false;

R=param.R;
isoAbund = [];
maxIso = 10;

ps=param.peakShape;
doPs = ~isempty(ps);

if isempty(param.peakList)
    pl = -1;
elseif ~iscell(param.peakList) && exist(param.peakList,'file')
    rpl = krs_tof_read_peakList(param.peakList);
    param.peakList = {rpl.molComp};
    numeric = cellfun(@isempty,param.peakList);
    pl = [rpl.mass]';
    if any(numeric)
        param.peakList(numeric) = num2cell(pl(numeric));
    end
    pIds = [rpl.pId]';
    
    % Find all unique elements in peak list
    %unique_element_list = regexp([rpl.elComp],'[A-Z][a-z]*','match'); % retrieve names of elements
    [~,unique_element_list,~] = krs_tof_exact_mass([rpl.elComp]);
    unique_element_list = unique_element_list{1};
    if ~isempty(unique_element_list)
        unique_element_list = sort(unique_element_list); % unique elements in cell form
        lenU = length(unique_element_list);
        unique_element_list = cellfun(@(x)[x ' '],unique_element_list,'UniformOutput',false); % add spaces
        unique_element_list = [unique_element_list{:}]; % concatenate
    else
        unique_element_list = ' ';
        lenU = 1;
    end
    elInd = [1 regexp(unique_element_list,' ')+1];
    elInd(end) = [];
    elListNr = zeros(1,length(unique_element_list));
    elListNr(elInd) = 1;
    elListNr = cumsum(elListNr); % array with numbers corresponding to elements above, e.g. 112233445566777
    
    isoAbund = zeros(length(pl),10,2);
    elMatrix = zeros(length(pl),lenU); % Rows = peaks in peak list, columns = elements in unique_element_list, values = nr of atoms of the element
    for i = 1:length(pl)
        if ~isempty(rpl(i).isoAbund)
            isoAbund(i,1:size(rpl(i).isoAbund,1),:) = rpl(i).isoAbund;
        end
        if ~isempty(rpl(i).elComp)
            elInd = [1 regexp(rpl(i).elComp,' ')+1 length(rpl(i).elComp)+2];
            for j = 1:rpl(i).nrEl
                curEl = rpl(i).elComp(elInd(j):elInd(j+1)-2);
                nonNumInds = find(~isstrprop(curEl,'digit'));
                lastNum = nonNumInds(end) + 1;
                elNr = elListNr(strfind(unique_element_list,[curEl(1:lastNum-1) ' ']));
                elMatrix(i,elNr) = str2double(curEl(lastNum:end));
            end
        end
    end

    % Take into account elements that have isotopes at lower masses than
    % the most abundant isotope. Requires that isoAbund exists.
    isoAbund2 = squeeze(isoAbund(:,:,1));
    isoAbund2(isoAbund2==0) = NaN;
    [~,minind] = krs_tof_nanmin(isoAbund2,[],2);
    isoInds = find(minind~=1)';
    for ii = isoInds
        [~, IX] = sort(isoAbund2(ii,:));
        isoAbund(ii,:,:) = isoAbund(ii,IX,:);  
        pl(ii) = isoAbund(ii,1,1);
    end 
    clear isoAbund2
else
    [pl,elNam,elNum] = krs_tof_exact_mass(param.peakList);
    pl = pl(:);
    lenPl = size(pl,1);
    pIds = 1:lenPl;
    
    isoAbund = zeros(1,maxIso,2);
    for i = 1:lenPl
        elemComp=[];
        if elNum{i}>0
            for e=1:length(elNum{i})
                elemComp=[elemComp,elNam{i}{e},num2str(elNum{i}(e)),' '];
            end
        else
            elemComp = pl(i);
        end
        if ~isnumeric(param.peakList{i})
            charge = param.peakList{i}(regexp(param.peakList{i},'-|+'));
            elemComp = [elemComp charge];
        end
        tIso = krs_tof_exact_isotope_masses(elemComp,maxIso);
        isoAbund(i,1:size(tIso,1),:) = tIso;
    end
    
    % Find all unique elements in peak list
    elNamCat = [elNam{:}];
    if ~isempty(elNamCat)
        unique_element_list = unique([elNam{:}]); % unique elements in cell form
        lenU = length(unique_element_list);
        unique_element_list = cellfun(@(x)[x ' '],unique_element_list,'UniformOutput',false); % add spaces
        unique_element_list = [unique_element_list{:}]; % concatenate
    else
        lenU = 1;
        unique_element_list = ' ';
    end
    elInd = [1 regexp(unique_element_list,' ')+1];
    elInd(end) = [];
    elListNr = zeros(1,length(unique_element_list));
    elListNr(elInd) = 1;
    elListNr = cumsum(elListNr); % array with numbers corresponding to elements above, e.g. 112233445566777

    elMatrix = zeros(lenPl,lenU); % Rows = peaks in peak list, columns = elements in unique_element_list, values = nr of atoms of the element
    for i = 1:lenPl    
        for j = 1:length(elNum{i})
            elNr = elListNr(strfind(unique_element_list,[elNam{i}{j} ' ']));
            elMatrix(i,elNr) = elNum{i}(j);
        end
    end
    
    if ~any(isoAbund(:))
        isoAbund = [];
    end
      
    % Take into account elements that have isotopes at lower masses than
    % the most abundant isotope. Requires that isoAbund exists.
    isoAbund2 = squeeze(isoAbund(:,:,1));
    isoAbund2(isoAbund2==0) = NaN;
    [~,minind] = krs_tof_nanmin(isoAbund2,2);
    isoInds = find(minind~=1)';
    for ii = isoInds
        [~, IX] = sort(isoAbund2(ii,:));
        isoAbund(ii,:,:) = isoAbund(ii,IX,:);  
        pl(ii) = isoAbund(ii,1,1);
    end 
    clear isoAbund2
end

% read data
datRaw = krs_get_signal(fNam);

[pars,eq] = krs_tof_get_mz_calib_pars(fNam);

convcoef = krs_tof_get_convcoef(fNam);
convcoef = convcoef(1);
%Iok=find(pars(1,:)~=-999)'; %select valid data points, -999 is entered when time point is not processed
nrSmpls=size(datRaw,1);
nrSpec=size(datRaw,2);
oor = 0;
if pl == -1
    try
        peakData = hdf5diskmap(fNam,'/Peaks/PeakData');
        %         peaklist = data(1:7,1:size(data,1));
    catch
        krs_tof_message('Error reading peaklist from file. Please run tof_find_peaks_mm.', 'err');
        return
    end
    isoInds = [];
end

% unlink PeakDataGauss-dataset

try
    file = H5F.open(fNam,'H5F_ACC_RDWR','H5P_DEFAULT');
    group = H5G.open(file,'/Peaks');
    try
        H5G.unlink(group,'PeakDataGauss');
    end
    try
        H5G.unlink(group,'PeakDataGaussIso');
    end
    try
        H5G.unlink(group,'PeakElements');
    end
    try
        H5G.unlink(group,'Rvalues');
    end
    H5G.close(group);
    H5F.close(file);
catch
    try
        H5G.close(group);
    end
    try
        H5F.close(file);
    end
end

% refit peaks
funcArgs = eq();

if doPs
    if ~isstruct(ps)
        psFileName = ps;
        load(ps);
        %replace the file name with actual peakshape and spline funtion
        sp=spline(peakShape.dat(:,1),peakShape.dat(:,2));
        peakShape.sp=sp;
        param.peakShape=peakShape;
    else
        psFileName = ' '; % file name not available if input is peak shape structure
        peakShape = ps;
        sp=spline(ps.dat(:,1),ps.dat(:,2));
        peakShape.sp = sp;
        param.peakShape = peakShape;
    end
    if ~isfield(param.peakShape, 'id')
        peakShape.id = krs_tof_ps_generate_id(peakShape.dat(:,2));
    end
    try
        calibPeakShapeId = hdf5read(fNam, '/MassCalib', 'peakShapeId');
        calibPeakShapeId = calibPeakShapeId.Data;
        if ~strcmp(calibPeakShapeId, {peakShape.id, '0'})
            krs_tof_message('Different peak shapes used for mass calibration and peak fitting.', 'warn');
        end
    end
else
    rxi = -30:0.01:30;
    ps = krs_tof_normpdf(rxi,0,1);
    peakShape.dat = [rxi' ps'];
    peakShape.par = [1 0];
    peakShape.id = '0';
    %doPs = 1;
end

isoWritten = 0;
ids = struct('fId',{[],[],[]},'dId',[],'dims_old',[]);
for s=1:nrSpec
    spec=datRaw(:,s)';
    if pl == -1
        Isel=peakData(1,1:size(peakData,1)) == s;
        if any(Isel)
            Pks = peakData(1:7,find(Isel))';
        else
            Pks=[];
        end
        pIds = 1:size(Pks,1);
    end
    if all(pl ~= -1) && oor == 1
        Pks = NaN(size(pl,1),7);
        Pks(:,2) = pl;
    end
    if oor
        Pks(Pks(:,2) > mhmax | Pks(:,2) < mhmin,:) = [];
    end
    if ~isempty(Pks)
        mzc.pars = pars;
        mzc.eq = eq;
        if ~isempty(R)
            mz=krs_tof_mass_axis(nrSmpls,pars,eq);
            if doPs

                [Pksg,Rn]=krs_tof_refit_gauss_peakShape(mz,spec,Pks,R,peakShape,param.peakList,pIds,mzc,isoAbund);
                %[Pksg,Rn]=tof_refit_kernel(mz,spec,Pks,param);
            else
                %[Pksg,Rn]=tof_refit_gauss(mz,spec,Pks,R,mzc,param.peakList,pIds,isoAbund);
                [Pksg,Rn]=krs_tof_refit_gauss(mz,spec,Pks,R,mzc,param.peakList,isoAbund);
            end
        else
            mz=krs_tof_mass_axis(nrSmpls,pars,eq);
            if doPs
                [Pksg,Rn]=krs_tof_refit_gauss_peakShape(mz,spec,Pks,[],peakShape,param.peakList,pIds,mzc,isoAbund);
            else
                %[Pksg,Rn]=tof_refit_gauss(mz,spec,Pks,[],mzc,param.peakList,pIds,isoAbund);                
                [Pksg,Rn]=krs_tof_refit_gauss(mz,spec,Pks,[],mzc,param.peakList,isoAbund);
            end
        end
        % Elements with isotopes at lower masses than max need to be
        % switched back between "isotopes" and the "normal" mass.

        %%%%%%%% TEMPORARYILY ONLY IF USING PEAKSHAPE
        if doPs
            for ii = isoInds
                ind = find(Pksg(:,8)==pIds(ii));
                [~,ind0] = krs_tof_nanmax(Pksg(ind,5));
                if ~isempty(ind0)
                    Pksg([ind(1) ind(ind0)],:) = Pksg([ind(ind0) ind(1)],:);
                end
            end
        end
        
        % write to file
        if ~isempty(Pksg)
            Pksg(:,1) = s; %first column equals spectrum number
            Pksg(:,5) = Pksg(:,5) * convcoef;
            ind = find(Pksg(:,2)==0);
            PksgIso = Pksg(ind+1:end,:);
            Pksg = Pksg(1:ind-1,:);
            ids(1) = krs_tof_write_HDF(fNam,'/Peaks/PeakDataGauss',Pksg,'memType','H5T_NATIVE_FLOAT','keepopen',true,'ids',ids(1));
            q = [s Rn];
            ids(3) = krs_tof_write_HDF(fNam,'/Peaks/Rvalues',q,'memType','H5T_NATIVE_FLOAT','keepopen',true,'ids',ids(3));
            if ~isempty(PksgIso)
                ids(2) = krs_tof_write_HDF(fNam,'/Peaks/PeakDataGaussIso',PksgIso,'memType','H5T_NATIVE_FLOAT','keepopen',true,'ids',ids(2));
                isoWritten = 1;
            end
            %t1 = t1 + size(Pksg,1);
            %offset = [t3,1];
            %t3 = t3 + 1;
        end
    end
    progBar(labapp, 'value', ((filei-1)/fileno)+(0.5/fileno)+(s/nrSpec*(0.5/fileno)));
    if labapp.progbar.CancelRequested
        cancel = true;
        break
    end
end
krs_tof_close_HDF(ids);

if ~cancel
    if pl == -1
        elMatrix = 0;
        unique_element_list = ' ';
    end

    krs_tof_write_HDF(fNam,'/Peaks/PeakElements',elMatrix,'memType','H5T_NATIVE_CHAR');
    if doPs
        krs_tof_ps_write(fNam, peakShape);
    end

    krs_tof_switch_chunk_size(fNam,'/Peaks/PeakDataGauss');
    if isoWritten
        krs_tof_switch_chunk_size(fNam,'/Peaks/PeakDataGaussIso');
    end

    attrout = {'peakShape', single(doPs), ...
        'peakShapeId', peakShape.id, ...
        '1: Spectrum number', '#', ...
        '2: Position', 'Th', ...
        '3: Height', 'mV', ...
        '4: Full width at half maximum', 'Th', ...
        '5: Area', 'ions/sec', ...
        '6: Sigma', 'Th', ...
        '7: Error', ' ', ...
        '8: PeakId', ' '};
    krs_tof_write_attributes(fNam, attrout, '/Peaks/PeakDataGauss', 'dataset');
    attroutEl = {'Elements', unique_element_list, ...
        'Rows', 'Listed according to PeakId (col 8 in PeakDataGauss)', ...
        'Columns', 'Nr of atoms (according to attr Elements)'};
    krs_tof_write_attributes(fNam, attroutEl, '/Peaks/PeakElements', 'dataset');
    if strcmp(class(R),'function_handle')
        Rout = func2str(R);
    else
        Rout = num2str(R);
    end
    krs_tof_write_attributes(fNam, {'Rfnc', Rout}, '/Peaks/Rvalues', 'dataset');
end