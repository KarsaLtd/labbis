function [data,a,b,timePointsUsed,fls,flsInfo,fTim]=krs_tof_load_raw_spectra(dateRange, varargin)
%
% Load raw spectra from HDF files
%
% args: dataSet, verbose
%
global tofPath

%%
% Default parameter values
args.dataSet = '/FullSpectra';
args.verbose = 0;
args.mask = [];

if nargin>1
    for i=1:2:(nargin-1)
        field = varargin{i};
        if isvarname(field)
            args.(field) = varargin{i+1};
        end
    end
end
%% Go through files in datadir
[~,~,~,flsNfo]=krs_tof_scan_files(dateRange);

a=[]; b=[]; timePointsUsed=[]; fTim=[];

nrFls = length(flsNfo.flsList);
for i=1:nrFls
    flsNfo.loadParam.dataSetFullSpectra{i} = args.dataSet;
end

if nrFls~=0
    fls = flsNfo.flsList;
    flsInfo = flsNfo.flsParam;

    %files that start earlier and last longer than the date range
    Iok = find(flsInfo.startTime<=dateRange(2) & ...
                flsInfo.stopTime>dateRange(1) & ...
                  flsInfo.startTime~=dateRange(2));
    
    fls = fls(Iok);
    flsTim = flsInfo.startTime(Iok);
    nrFls = length(Iok);
    flsInfo = structfun(@(x) (x(Iok)),flsInfo,'UniformOutput',false);
    
    file_tim = flsNfo.timVectors(Iok);
    file_timDims = flsNfo.timDims(Iok);
    
    if ~isempty(Iok)
        dataSetName = flsNfo.loadParam.dataSetFullSpectra{Iok};
    end   
    if args.verbose
        disp([' '])
        disp(['tof_load_raw_spectra: ',fls(1).name])
        disp(['tof_load_raw_spectra: ',datestr(dateRange(1)),' - ',datestr(dateRange(2))])
        disp(['tof_load_raw_spectra: number of files: ' num2str(nrFls)])
    end
    
    %% index files, load ionmode, nrSamples and sampleinterval
    data = 0;
    for f=1:nrFls
        path_fNam = fullfile(fls(f).folder, fls(f).name);
        %find which time points have to be loaded from HDF
        if isempty(args.mask)
            Itime_to_load = file_tim{f}>=dateRange(1) & file_tim{f}<dateRange(2);
        else
            Itime_to_load = file_tim{f}>=dateRange(1) & ...
                              file_tim{f}<dateRange(2) & ...
                                ismember(file_tim{f},args.mask(args.mask(:,2)==1,1));
        end
        timePointsUsed{f} = Itime_to_load; %save time points loaded for each timestep
        nbrTims = sum(Itime_to_load(:));
        fTim = file_tim{f}(Itime_to_load);
        
        if args.verbose
            disp(['tof_load_raw_spectra: ',datestr(fTim(1)),' - ',datestr(fTim(end))])
        end
        
        if all(nbrTims>0)
            %load tof
            if all(flsInfo.hasTofData)
                dataSetNam = [dataSetName,'/TofData/'];
            elseif all(flsInfo.hasEventList)
                dataSetNam = [dataSetName,'/EventList/'];
            end
            
            %check dataset name
            dataSetDim = krs_tof_get_dataset_size(path_fNam,dataSetNam);
            
            a(f) = hdf5read(path_fNam,[dataSetName,'/'],'MassCalibration a');
            b(f) = hdf5read(path_fNam,[dataSetName,'/'],'MassCalibration b');
            
            if a<1
                a(f) = hdf5read(path_fNam,[dataSetName,'/'],'MassCalibration p1');
                b(f) = hdf5read(path_fNam,[dataSetName,'/'],'MassCalibration p2');
            end
            
            %check if time_to_load is continues, if not load in parts
            Itim2load = find(Itime_to_load);
            Itim2load = Itim2load(:);
            %             Ichange=cumsum([1;diff(Itim2load)]~=1)+1;
            Ichange = cumsum([1;diff(Itim2load)]~=1)+1;
            if length(size(Itime_to_load))==2
                Itime2load_tmp(size(Itime_to_load,1),size(Itime_to_load,2)) = false;
            elseif length(size(Itime_to_load))==3
                Itime2load_tmp(size(Itime_to_load,1),size(Itime_to_load,2),size(Itime_to_load,3)) = false;
            end
            
            for ic=Ichange(1):Ichange(end)
                Isel = Ichange==ic;
                Itime2load_tmp(Itim2load(Isel)) = true;
                d = krs_tof_load_data_from_HDF_total_spectra(path_fNam, dataSetNam,dataSetDim,Itime2load_tmp,flsInfo.NbrSamples(f),file_timDims{f},flsInfo.hasIMS(f));
                Itime2load_tmp(Itim2load(Isel)) = 0; %fill with zeros, for next round
                data = data+sum(d,4);
            end
            if args.verbose
                disp(['tof_load_raw_spectra: number of spectras: ',num2str(sum(timePointsUsed{f}))])
            end
        else
            if args.verbose
                disp('no data')
            end
            data = 0;
            a(f) = NaN;
            b(f) = NaN;
        end
    end
else
    if args.verbose
        disp('tof_load_raw_spectra: no files')
    end
    data = [];
    a = [];
    b = [];
    
    timePointsUsed = [];
    fls = [];
    flsInfo = [];
    fTim = [];
end
[n,m] = size(data);
if n==m && n==1
    data = [];
end
data = data';