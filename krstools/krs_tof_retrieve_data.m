function datastruct = krs_tof_retrieve_data(fNam,specInd,varargin)

% datastruct = tof_retrieve_data(fNam,specInd,varargin)
% Reads data from a preprocessed file and outputs it in an easily plottable
% form.
%
% in:
%     fNam       - path of the preprocessed file
%     specInd    - number/time of spectrum to extract data from
%     varargin   - what to read, can be string(s) from the following list:
%       'spec'         - spectrum
%       'diff'         - difference spectrum
%       'mz'           - mass axis
%       'baseline'     - calculated (and removed) baseline
%       'mzcalib'      - masses used for calibration
%       'sir'          - stick integration regions
%       'nir'          - noise integration regions
%       'foundpeaks'   - found peaks
%       'sumfitfound'  - sum fit of found peaks
%       'fittedpeaks'  - fitted peaks
%       'sumfitfitted' - sum fit of fitted peaks
%       'residual'     - residual of fitted peaks
%       'transmission' - enables transmission correction for all output,
%                        also returns a ppform of the transmission
%       'locatedpeaks' - located peaks
%       'pmf'          - PMF factors
%
% out:
%     datastruct - structure containing the requested data, the field names
%                  are the same as the strings in the list above
%
% usage:
%     data = tof_retrieve_data(fNam,3,'mz','residual');

% Gustaf L?nn
% June 2012

args.internal = false;
args.spec = false;
args.diff = false;
args.mz = false;
args.baseline = false;
args.mzcalib = false;
args.sir = false;
args.nir = false;
args.foundpeaks = false;
args.sumfitfound = false;
args.fittedpeaks = false;
args.sumfitfitted = false;
args.residual = false;
args.transmission = false;
args.locatedpeaks = false;
args.sumfitlocated = false;
args.pmf = false;
datastruct = [];
dat = [];
convcoef = [];

for i = 1:length(varargin)
    args.(lower(varargin{i})) = true;
end

if args.mzcalib
    args.spec = true;
    args.mz = true;
end

if args.sumfitfound || args.sumfitfitted || args.residual || args.sumfitlocated
    args.mz = true;
end

if args.residual
    args.spec = true;
end

if ~args.internal
    numSpec = krs_tof_get_dataset_size(fNam,'/tofData/time',1);
    if specInd > numSpec
        tim = hdf5read(fNam,'/tofData/time');
        [~,specInd] = min(abs(tim(1,:) - specInd));
    end
end

if args.transmission
    try
        trans = hdf5read(fNam, '/Transmission/points')';
        %pf = hdf5read(fNam, '/Transmission', 'Pulsing frequency (Hz)');
        args.mz = true;
    catch
        args.transmission = false;
    end
end

if args.mz
    if isempty(dat)
        try
            numPoints = krs_tof_get_dataset_size(fNam,'/tofData/SumSpectrum',2);
        catch
            numPoints = krs_tof_get_dataset_size(fNam,'/tofData/signal',2);
        end
    else
        numPoints = size(dat,2);
    end
    [pars,eq] = krs_tof_get_mz_calib_pars(fNam);
    datastruct.mz = krs_tof_mass_axis(numPoints,pars(:, specInd),eq);
    if args.transmission
        trans = [0 1; trans; max(datastruct.mz) trans(end, 2)];
        datastruct.transmission = interp1(trans(:, 1), trans(:, 2), 'linear', 'pp');
    end
end

if args.spec
    dat = hdf5diskmap(fNam,'/tofData/signal');
    convcoef = krs_tof_get_convcoef(fNam);
    convcoef = convcoef(specInd);
    datastruct.spec = dat(:,specInd)' * convcoef;
    if args.transmission
        datastruct.spec = datastruct.spec ./ ppval(datastruct.mz, datastruct.transmission);
    end
end

if args.diff
    dat = hdf5diskmap(fNam,'/tofData/difference');
    convcoef = krs_tof_get_convcoef(fNam);
    convcoef = convcoef(specInd);
    datastruct.diff = dat(:,specInd)' * convcoef;
    if args.transmission
        datastruct.diff = datastruct.diff ./ ppval(datastruct.mz, datastruct.transmission);
    end
end


if args.baseline
    try
        if isempty(convcoef)
            convcoef = krs_tof_get_convcoef(fNam);
            convcoef = convcoef(specInd);
        end
        baseline = hdf5diskmap(fNam,'/tofData/baseline');
        datastruct.baseline = baseline(:,specInd)' * convcoef;
        if args.transmission
            datastruct.baseline = datastruct.baseline ./ ppval(datastruct.mz, datastruct.transmission);
        end
    catch
        datastruct.baseline = [];
    end
end

if args.mzcalib
    try
        masses = hdf5diskmap(fNam,'/MassCalib/masses');
        masses = masses(:,specInd);
        massesAll = single(hdf5read(fNam,'/MassCalib','masses'));
        funcArgs = eq();
        for i = 1:length(massesAll)
            closestInd = round(funcArgs.inverseEQ(massesAll(i),pars(:,specInd)));
            if any(massesAll(i) == masses)
                col = [1 0 0];
            else
                col = [0 0 1];
            end
            datastruct.mzcalib.data{i} = [massesAll([i i]) [0;datastruct.spec(closestInd)]];
            datastruct.mzcalib.color{i} = col;
        end
    catch
        datastruct.mzcalib = [];
    end
end

if args.sir
    try
        stcs1 = hdf5diskmap(fNam,'/Sticks/stick_edge1');
        stcs2 = hdf5diskmap(fNam,'/Sticks/stick_edge2');
        tof_stc_edges1 = stcs1(:,specInd);
        tof_stc_edges2 = stcs2(:,specInd);
        datastruct.sir = constructFromEdges(tof_stc_edges1,tof_stc_edges2);
    catch
        datastruct.sir = [];
    end
end

if args.nir
    try
        bl1 = hdf5diskmap(fNam,'/Sticks/baseline_edge1');
        bl2 = hdf5diskmap(fNam,'/Sticks/baseline_edge2');
        tof_bl_edges1 = bl1(:,specInd);
        tof_bl_edges2 = bl2(:,specInd);
        datastruct.nir = constructFromEdges(tof_bl_edges1,tof_bl_edges2);
    catch
        datastruct.nir = [];
    end
end

if args.foundpeaks || args.sumfitfound
    draw = 1;
    try
        if args.sumfitfound
            step = 0.0005;
            xax = floor(datastruct.mz(1)):step:ceil(datastruct.mz(end));
            fitsuml = length(xax);
            fitsum = zeros(1,fitsuml);
        end
        pea = hdf5read(fNam,'/Peaks/PeakData');
        Isel = find(pea(1,:) == specInd);
        ppos = pea(2,Isel);
        if isempty(convcoef)
            convcoef = krs_tof_get_convcoef(fNam);
            convcoef = convcoef(specInd);
        end
        phei = pea(3,Isel) * convcoef;
        if args.transmission
            phei = phei ./ ppval(ppos, datastruct.transmission);
        end
        psig = pea(6,Isel);
        rxi = -10:0.2:10;
        peakProfile = exp(-0.5*rxi.^2);
        peakProfileSpline = spline(rxi,peakProfile);
        if args.foundpeaks
            tmz = NaN(length(ppos)*3,1);
            tdat = tmz;
            for i = 1:length(ppos)
                if draw
                    tmz(3*(i-1)+1) = ppos(i);
                    tmz(3*(i-1)+2) = ppos(i);
                    tdat(3*(i-1)+1) = 0;
                    tdat(3*(i-1)+2) = phei(i);
                end
            end
            datastruct.foundpeaks.lines = [tmz tdat]';
            lrxi = length(rxi);
            tmz = NaN(length(ppos)*(lrxi+1),1);
            tdat = tmz;
        end
        for i=1:length(ppos)
            if ~isnan(psig(i))
                b = peakProfile * phei(i);
                rxi2 = rxi*psig(i) + ppos(i);
                if args.foundpeaks
                    tmz((i-1)*(lrxi)+1+(i-1):i*(lrxi)+(i-1)) = rxi2;
                    tdat((i-1)*(lrxi)+1+(i-1):i*(lrxi)+(i-1)) = b;
                end
                if args.sumfitfound
                    ind1 = floor((min(rxi2) - xax(1)) / step) + 1;
                    ind2 = ceil((max(rxi2) - xax(1)) / step) + 1;
                    if ~isnan(ind1) && ~isnan(ind2)
                        if ind2 > length(xax)
                            ind2 = length(xax);
                        end
                        inds = ind1:ind2;
                        partxax = xax(inds);
                        fit = ppval(peakProfileSpline, (partxax - ppos(i)) / psig(i));
                        
                        fit = fit * phei(i);
                        
                        fitsum(inds) = fitsum(inds) + fit;
                        
                        % make the fit go to zero at the edges of the peak
                        if ind1 > 1
                            fitsum(ind1 - 1) = fitsum(ind1 - 1) + eps;
                        end
                        if ind2 < fitsuml
                            fitsum(ind2 + 1) = fitsum(ind2 + 1) + eps;
                        end
                    end
                end
                
            end
        end
        if args.foundpeaks
            datastruct.foundpeaks.data = [tmz tdat]';
        end
        if args.sumfitfound
            fitsum(1) = eps;
            fitsum(end) = eps;
            dEl = fitsum <= 0;
            xax(dEl) = [];
            fitsum(dEl) = [];
            datastruct.sumfitfound = [xax;fitsum];
        end
    catch ME
        if args.foundpeaks
            datastruct.foundpeaks = [];
        end
        if args.sumfitfound
            datastruct.sumfitfound = [];
        end
    end
end

if args.locatedpeaks || args.sumfitlocated
    retrievePeaks('/Peaks/LocateAndFit', 'located');
end

if args.fittedpeaks || args.sumfitfitted || args.residual
    retrievePeaks('/Peaks/PeakDataGauss', 'fitted');
end

if args.pmf
    if krs_tof_exist_in_file(fNam, '/pmfData/factors')
        pmfFact = hdf5read(fNam, '/pmfData/factors');
        pmfMz = hdf5read(fNam, '/pmfData/masses');
        pmfNam = h5readatt(fNam, '/pmfData', 'factNames');
        pmfDat = hdf5read(fNam, '/pmfData/data');
        pmfWeights = hdf5read(fNam, '/pmfData/weights');
        pmfPred = pmfFact' * pmfWeights;
        pmfRes = pmfDat - pmfPred;
        if length(pmfNam) == 1 && all(pmfNam{1} == [32 0])
            pmfNam = [];
        else
            pmfNam = cellfun(@(x)x(x ~= 0), pmfNam, 'UniformOutput', false);
        end
    else
        pmfFact = [];
    end
    datastruct.pmf.fact = pmfFact;
    datastruct.pmf.mz = pmfMz;
    datastruct.pmf.nam = pmfNam;
    datastruct.pmf.dat = pmfDat;
    datastruct.pmf.res = pmfRes;
    datastruct.pmf.pred = pmfPred;
    datastruct.pmf.weight = pmfWeights;
end

    function retrievePeaks(dataset, field)
        draw = 1;
        fieldP = [field 'peaks'];
        fieldL = ['sumfit' field];
        try
            try
                doPeakShape = hdf5read(fNam,dataset,'peakShape');
            catch
                doPeakShape = false;
            end
            if args.(fieldL) || args.residual
                step = 0.0005;
                xax = floor(datastruct.mz(1)):step:ceil(datastruct.mz(end));
                fitsuml = length(xax);
                fitsum = zeros(fitsuml,1);
            end
            if doPeakShape
                peakShape = hdf5read(fNam,'/Peaks/PeakShape')';
                par1 = hdf5read(fNam,'/Peaks/PeakShape','par1');
                par2 = hdf5read(fNam,'/Peaks/PeakShape','par2');
                yi = peakShape(:,2)';
                rxi = peakShape(:,1)';
                if par1 ~= 0
                    a = krs_tof_normpdf(rxi,par2,par1).*yi;
                    a(isnan(a)) = 0;
                else
                    a = yi;
                end
                a = krs_H_scale(a);
                pp = spline(rxi,a);
            else
                rxi = -10:0.1:10;
                a = krs_H_scale(krs_tof_normpdf(rxi,0,1));
                pp = spline(rxi,a);
            end
            pea = hdf5read(fNam,dataset);
            try
                % if the dataset PeakDataGaussIso exists, we concatenate the
                % datasets and mark the border between them with a row of zeros
                peaIso = hdf5read(fNam,[dataset 'Iso']);
                pea = [pea [specInd;zeros(size(pea,1)-1,1)] peaIso];
            end
            Isel = find(pea(1,:) == specInd);
            ppos = pea(2,Isel);
            if isempty(convcoef)
                convcoef = krs_tof_get_convcoef(fNam);
                convcoef = convcoef(specInd);
            end
            phei = pea(3,Isel) * convcoef;
            if args.transmission
                phei = phei ./ ppval(ppos, datastruct.transmission);
            end
            psig = pea(6,Isel);
            if field(1) == 'l'
                plis = pea(8, Isel);
            end
            pare = pea(5,Isel);
            col = [0 0.7 0];
            colIso = [0.7 0.7 0];
            colout = col;
            indIsoBegin = find(ppos==0);
            ppos(indIsoBegin) = [];
            phei(indIsoBegin) = [];
            psig(indIsoBegin) = [];
            pare(indIsoBegin) = [];
            for i=1:length(ppos)
                b = a * phei(i);
                if i == indIsoBegin
                    colout = colIso;
                end
                rxi2 = rxi*psig(i) + ppos(i);
                if args.(fieldP)
                    structData = {'Position', ppos(i), 'Height', phei(i), 'Area', pare(i), 'FWHM', psig(i) * 2.35482};
                    if fieldP(1) == 'l'
                        structData = [structData 'Peaklist' plis(i) > 0];
                    end
                    datastruct.(fieldP).peakDat(i) = struct(structData{:});
                    datastruct.(fieldP).data{i} = [rxi2;b];
                end
                if args.(fieldL) || args.residual
                    ind1 = floor((min(rxi2) - xax(1)) / step) + 1;
                    ind2 = ceil((max(rxi2) - xax(1)) / step) + 1;
                    if ~isnan(ind1) && ~isnan(ind2)
                        if ind2 > length(xax)
                            ind2 = length(xax);
                        end
                        
                        partxax = xax(ind1:ind2);
                        fit = ppval(pp, (partxax - ppos(i)) / psig(i));
                        
                        fit = fit * phei(i);
                        
                        fitsum(ind1:ind2) = fitsum(ind1:ind2) + fit';
                        
                        % make the fit go to zero at the edges of the peak
                        if ind1 > 1
                            fitsum(ind1 - 1) = fitsum(ind1 - 1) + eps;
                        end
                        if ind2 < fitsuml
                            fitsum(ind2 + 1) = fitsum(ind2 + 1) + eps;
                        end
                    end
                end
            end
            if args.(fieldP)
                if isempty(indIsoBegin) || indIsoBegin == 0
                    maxInd = length(ppos);
                else
                    maxInd = indIsoBegin-1;
                end
                tmz = NaN(maxInd*3,1);
                tdat = tmz;
                for i = 1:maxInd
                    if draw
                        tmz(3*(i-1)+1) = ppos(i);
                        tmz(3*(i-1)+2) = ppos(i);
                        tdat(3*(i-1)+1) = 0;
                        tdat(3*(i-1)+2) = phei(i);
                    end
                end
                datastruct.(fieldP).lines = [tmz tdat]';
                if ~isempty(indIsoBegin) && indIsoBegin ~= 0
                    tmz = NaN((length(ppos)-indIsoBegin+1)*3,1);
                    tdat = tmz;
                    for i = indIsoBegin:length(ppos)
                        if draw
                            tmz(3*(i-indIsoBegin)+1) = ppos(i);
                            tmz(3*(i-indIsoBegin)+2) = ppos(i);
                            tdat(3*(i-indIsoBegin)+1) = 0;
                            tdat(3*(i-indIsoBegin)+2) = phei(i);
                        end
                    end
                    datastruct.(fieldP).linesIso = [tmz tdat]';
                else
                    datastruct.(fieldP).linesIso = [];
                end
            end
            if args.(fieldL)
                fitsum(1) = eps;
                fitsum(end) = eps;
                dEl = fitsum <= 0;
                xax(dEl) = [];
                fitsum(dEl) = [];
                datastruct.(fieldL) = [xax;fitsum'];
            end
            if args.residual
                fitsum(1) = eps;
                fitsum(end) = eps;
                fitsumip = interp1(xax,fitsum,datastruct.mz);
                resid = datastruct.spec - fitsumip;
                datastruct.residual = resid;
            end
        catch
            if args.(fieldP)
                datastruct.(fieldP) = {};
            end
            if args.residual
                datastruct.residual = [];
            end
            if args.(fieldL)
                datastruct.(fieldL) = [];
            end
        end
        if args.(fieldP) && (isempty(datastruct.(fieldP)) || isempty(datastruct.(fieldP).lines))
            datastruct.(fieldP) = {};
        end
    end

    function Isel = constructFromEdges(var1,var2)
        Isel = zeros(max(var2),1);
        ind = 1;
        for j = 1:length(var1)
            if ~isnan(var1(j))
                add = var1(j):var2(j);
                lengthAdd = length(add);
                Isel(ind:ind + lengthAdd - 1) = add;
                Isel(ind + lengthAdd) = 1;
                ind = ind + lengthAdd + 1;
            end
        end
        Isel(Isel == 0) = [];
    end

end