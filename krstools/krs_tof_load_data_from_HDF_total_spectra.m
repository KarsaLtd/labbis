function data=krs_tof_load_data_from_HDF_total_spectra(path_fNam, dataSetNam,dataSetDim,Itime_to_load,NbrSamples,file_timDims,hasIMS)
%
% Load tof raw spectra from HDF, split into pieces that will fit to memory
% calculate total spectra
%

%separate loading functionality for tofData and eventList

switch dataSetNam
    
    case {'/FullSpectra/TofData/', '/FullSpectra2/TofData/'}
        %     dims=tof_get_dataset_size(path_fNam,dataSetNam);
        dims=dataSetDim;
        nbrTims=sum(Itime_to_load(:));
        %nbrSamples=dims(4);
        nbrSegments=dims(3);
        nbrBufs=dims(2);
        nbrWrites=dims(1);
        
        if nbrTims>1
            memAvailab = krs_tof_get_free_mem / 4; % for data in single
        else
            %assume you can fit the one spectra to memeory. Speeds up the coping
            memAvailab=prod([nbrTims nbrBufs nbrSegments NbrSamples])*2+1;
        end
        
        if 0 %temp for testing
            %tic
            if nbrBufs==1 && nbrSegments==1
                Istrt=find(Itime_to_load, 1);
                
                memReq=prod([nbrTims nbrBufs nbrSegments NbrSamples])*2;
                if memReq<memAvailab
                    data = hdf5readslab(path_fNam, dataSetNam, [Istrt-1 0 0 0], [nbrTims 1 1 NbrSamples]);
                else
                    
                    nbrTimsMem=max(1, floor(3*memAvailab./(4*memReq./nbrTims))); %max time points that fits to memory
                    timBreaks=unique([Istrt:nbrTimsMem:Istrt+nbrTims,Istrt+nbrTims]);
                    for mi=1:length(timBreaks)-1
                        try
                            d = hdf5readslab(path_fNam, dataSetNam, [timBreaks(mi)-1 0 0 0], [timBreaks(mi+1)-timBreaks(mi) 1 1 NbrSamples]);
                            if mi==1
                                data(:,:,:,1)=sum(d,4);
                            else
                                data(:,:,:,1)=data(:,:,:,1)+sum(d,4);
                            end
                            clear d
                        catch
                            disp(path_fNam)
                            disp(lasterr)
                        end
                    end
                    
                end
                %if bufs are not even few extra spectras are loaded
                %remove spectras from incomplete bufs
                I=Itime_to_load(:,any(Itime_to_load,1));
                data(:,:,:,I(:)~=1)=[];
                
            elseif nbrSegments == 1
                nbrTims=sum(Itime_to_load,2);
                %     memReq=prod([max(nbrTims) file_timDims(1) nbrSegments NbrSamples]);
                
                memReq=prod([max(nbrTims(:)) nbrBufs nbrSegments NbrSamples])*2;
                
                %first and last buf where loading starts
                Istr_buf=find(any(Itime_to_load,1),1);
                Iend_buf=find(any(Itime_to_load,1),1,'last');
                I=Itime_to_load(:,any(Itime_to_load,1));
                if memReq<memAvailab/2
                    dat = hdf5readslab(path_fNam, dataSetNam, [Istr_buf-1 0 0 0], [Iend_buf-Istr_buf+1 file_timDims(1) 1 NbrSamples]);
                    dims=size(dat);
                    if length(dims)==3
                        dims(4)=1;
                    end
                    data=reshape(dat,dims(1),1,dims(2),dims(3)*dims(4));
                    clear dat
                    %if bufs are not even few extra spectras are loaded
                    %remove spectras from incomplete bufs
                    %I=Itime_to_load(:,any(Itime_to_load,1));
                    I=Itime_to_load(:,Istr_buf:Iend_buf);
                    data(:,:,:,I(:)~=1)=[];
                    
                    %=============
                    
                    %=============
                else
                    nbrTimsMem=max(1, floor(1*memAvailab./(2*memReq./max(nbrTims)))); %max time points that fits to 1/2 of available memory memory
                    %         timBreaks=unique([Istr_buf:nbrTimsMem:Istr_buf+max(nbrTims),Istrt+max(nbrTims)]);
                    timBreaks=unique([Istr_buf:nbrTimsMem:Iend_buf,Iend_buf+1]);
                    for mi=1:length(timBreaks)-1
                        d = hdf5readslab(path_fNam, dataSetNam, [timBreaks(mi)-1 0 0 0], [timBreaks(mi+1)-timBreaks(mi) file_timDims(1) 1 NbrSamples]);
                        
                        %if bufs are not even few extra spectras are loaded
                        %remove spectras from incomplete bufs
                        %             Itmp=I(:,(timBreaks(mi):((timBreaks(mi+1)-timBreaks(mi))+timBreaks(mi+1)-2))-(Istr_buf-1));
                        %Itmp=Itime_to_load(:,(timBreaks(mi):timBreaks(mi+1)-1)-(Istr_buf-1));
                        Itmp=Itime_to_load(:,(timBreaks(mi):timBreaks(mi+1)-1));
                        for ii=1:size(Itmp,2)
                            Iz=Itmp(:,ii)~=1;
                            if any(Iz)
                                d(:,:,Iz,ii)=0;
                            end
                        end
                        
                        if mi==1
                            dat(:,:,:,1)=sum(d,4);
                        else
                            dat(:,:,:,1)=dat(:,:,:,1)+sum(d,4);
                        end
                        %             dat_temp{mi}=sum(d,4);
                        %             size(d)
                        clear d
                    end
                    dims=size(dat);
                    if length(dims)==3
                        dims(4)=1;
                    end
                    data=reshape(dat,dims(1),1,dims(2),dims(3)*dims(4));
                    clear dat
                end %if
            else
                [Ix,Iy,Iz] = ind2sub(size(Itime_to_load),find(Itime_to_load == 1));
                memReq=prod([max(Iz) - min(Iz) + 1 max(Iy) max(Ix) NbrSamples])*2;
                if memReq < memAvailab || min(Iz) - max(Iz) == 0
                    data = hdf5readslab(path_fNam, dataSetNam, [min(Iz)-1 0 0 0], [max(Iz)-min(Iz)+1 max(Iy) max(Ix) NbrSamples]);
                    I = logical(~Itime_to_load(1:max(Ix),1:max(Iy),min(Iz):max(Iz)));
                    data(:,I) = 0;
                    data = reshape(data,dims(4),1,1,max(Ix)*max(Iy)*(max(Iz)-min(Iz)+1));
                else
                    nbrTimsMem = floor(memAvailab/(2*memReq./(max(Iz)-min(Iz)+1)));
                    if nbrTimsMem == 0
                        error('tof_load_data_from_HDF_total_spectra.m: Out of memory.');
                    end
                    timBreaks=[min(Iz):nbrTimsMem:max(Iz),max(Iz)+1];
                    for mi=1:length(timBreaks)-1
                        d = hdf5readslab(path_fNam, dataSetNam, [timBreaks(mi)-1 0 0 0], [timBreaks(mi+1)-timBreaks(mi) max(Iy) max(Ix) NbrSamples]);
                        Itmp=logical(~Itime_to_load(1:max(Ix),1:max(Iy),timBreaks(mi):timBreaks(mi+1)-1));
                        d(:,Itmp) = 0;
                        if mi==1
                            dat(:,:,:,1)=sum(d,4);
                        else
                            dat(:,:,:,1)=dat(:,:,:,1)+sum(d,4);
                        end
                        clear d;
                    end
                    data = reshape(dat,dims(4),1,1,dims(2)*dims(3));
                end
            end
            %t1=toc
            %==============
            %new
        else
            %tic
            [Iy,Ix,Iz] = ind2sub(size(Itime_to_load),find(Itime_to_load == 1));
            
            if nbrSegments==1
                
                [Ix,Iz,Iy] = ind2sub(size(Itime_to_load),find(Itime_to_load == 1));
                
                %             elseif nbrWrites==1
                %
                %
                %             [Iy,Ix,Iz] = ind2sub(size(Itime_to_load),find(Itime_to_load == 1));
                
            end
            
            try
                data = hdf5readslab(path_fNam, dataSetNam, [min(Iz)-1 min(Ix)-1 min(Iy)-1 0], [max(Iz)-min(Iz)+1 max(Ix)-min(Ix)+1 max(Iy)-min(Iy)+1 NbrSamples]);
            catch ER
                data=zeros(1,dims(4));
                
                krs_tof_message(ER.message)
            end
            %data = h5read(path_fNam, dataSetNam, fliplr([min(Iz)-1 min(Ix)-1 min(Iy)-1 0]+1), fliplr([max(Iz)-min(Iz)+1 max(Ix)-min(Ix)+1 max(Iy)-min(Iy)+1 NbrSamples]));
            
            if nbrSegments==1
                I = logical(~Itime_to_load(min(Ix):max(Ix),min(Iz):max(Iz)));
            else
                I = logical(~Itime_to_load(min(Iy):max(Iy),min(Ix):max(Ix),min(Iz):max(Iz)));
            end
            data(:,I) = 0;
            nrDim=length(size(data));
            nrR=(max(Ix)-min(Ix)+1)*(max(Iy)-min(Iy)+1)*(max(Iz)-min(Iz)+1);
            
            %             figure
            %             plot(squeeze(sum(data,1)))
            
            % estimate mobile phone interferance take 10% of the end of the
            % spectrum. NOT DONE IF MEASURED WITH EVENTLIST
            
            %MOBILE INTERFERENCE REMOVAL!!!
            mobInf=sum(data(round(NbrSamples-NbrSamples/10):NbrSamples,:,:,:));
            ImobInf=mobInf<-50;
             %[Ix,Iy,Iz,Ie]=ind2sub(size(ImobInf),ImobInf);
            %data(Ix,iy,Iz,Ie)=0;
            if nrDim==4
                data = reshape(data,NbrSamples,1,1,nrR);
                ImobInf = reshape(ImobInf,1,1,1,nrR);
                data(:,:,:,ImobInf)=0;
                if nrR>1
                    data=sum(data,4);
                end
            elseif nrDim==3
                data = reshape(data,NbrSamples,1,nrR);
                ImobInf = reshape(ImobInf,1,1,nrR);
                data(:,:,ImobInf)=0;
                if nrR>1
                    data=sum(data,3);
                end
            else
                data(:,ImobInf)=0;
                if nrR>1
                    data=sum(data,2);
                end
            end
            %t2=toc
            
        end
        
    case {'/FullSpectra/EventList/','/FullSpectra2/EventList/'}
        if hasIMS
            file_timDims=[dataSetDim(3),file_timDims];
            Itime_to_load=repmat(Itime_to_load,[1,1,dataSetDim(3)]);
            Itime_to_load=permute(Itime_to_load,[3,1,2]);
        end
        
        numDim = length(file_timDims);
        if numDim == 3
            [Ix,Iy,Iz] = ind2sub(size(Itime_to_load),find(Itime_to_load == 1));
            d = hdf5readslab(path_fNam, dataSetNam, [min(Iz)-1 0 0], [max(Iz)-min(Iz)+1 max(Iy) max(Ix)]);
            I = logical(~Itime_to_load(1:max(Ix),1:max(Iy),min(Iz):max(Iz)));
        elseif numDim == 2
            [Ix,Iy] = ind2sub(size(Itime_to_load),find(Itime_to_load == 1));
            d = hdf5readslab(path_fNam, dataSetNam, [0 0 0], [max(Iy) max(Ix) 1]);
            I = logical(~Itime_to_load(1:max(Ix), 1:max(Iy)));
        else
            krs_tof_message('Reading of raw data containing event list and one-dimensional TimingData not supported. This should be implemented.');
            error('Quitting.');
            %{
            CODE NOT TESTED ON REAL DATA! Something like this should work.
            Ix = find(Itime_to_load == 1);
            d = hdf5readslab(path_fNam, dataSetNam, [0 0 0], [max(Ix) 0 0]);
            I = logical(~Itime_to_load(1:max(Ix));
            %}
        end
        d(I) = [];
        
        nrSmp=double(hdf5read(path_fNam,'/','NbrSamples'));
        %read values from fullSpectra even if FullSpectra2 is requested. These values are the same
        SampleInterval=hdf5read(path_fNam,'/FullSpectra','SampleInterval');
        ClockPeriod=hdf5read(path_fNam,'/FullSpectra','ClockPeriod');
        
        data(1,nrSmp)=single(0);
        d = cat(1, d{:});
        if ~isempty(d)
            smplIndx = round(0.51+double(d)*ClockPeriod/SampleInterval);
            mult = histc(smplIndx', 1:NbrSamples);
            data = data + mult;
        end
    otherwise
        data=[];
        krs_tof_message(['Unknown datatype ',dataSetNam])
end