function [par1,ppm,yhat]=krs_tof_fit_tof_mz(mz,tof,init,opt)
%fit function for tof_vs_amu
%
% par1=tof_fit_tof_mz(mz,tof,init)
% mz = mass axis
% tof  = time of flight or sample number
% init = starting values
% amu=((tof-b)/a)^2
%

if nargin == 0
    par1.numOfInitValues = 2;
    par1.useOriginalParams = 1;
    par1.numOfPars = 2;
    par1.usedEQ = @(x,p)((x-p(2))./p(1)).^2;
    par1.inverseEQ = @(x,p)(p(1)*sqrt(x)+p(2));
    par1.accurateInverseEQ = par1.inverseEQ;
    par1.reprEQ = 'm = [(t - p2) / p1]^2';
    par1.texEQ = 'm = [(t - p_2) / p_1]^2';
    return;
end

xy=[tof(:),mz(:)];
% init=[600,-1300,0.49];

% limLow=[8300 -1600 0.480];
% limHgh=[900 -1000 0.50];
%optimize
%,'OutputFcn', @outfun

% [par1]=fmincon(@(par1) funfit(par1,xy),init,[],[],[],[],limLow,limHgh,[],opt);

[par1]=fminsearch(@(par1) funfit(par1,xy),init,opt);
% par1 = lsqnonlin(@(par1) funfit(par1,xy),init,[],[],opt);
% x=xy(:,1);
% y=xy(:,2);
% A = [ones(length(x),1) x.^2];
% A = [ones(length(x),1) x.^p1];
% p=A\y;


% bar(mzs_ok,predTOF_ok-tof_hat)
if nargout > 1
    yhat=((xy(:,1)-par1(2))/par1(1)).^2;
    L=length(yhat);
    f=sqrt(sum((xy(:,2)-yhat).^2)/2);
    
    ppm=mean(abs(xy(:,2)-yhat)./xy(:,2))*1000000;
end

function f=funfit(par,xy)
%par(1)= slope
%par(2)= intercept
%par(3)= power
yhat=((xy(:,1)-par(2))/par(1)).^2;
f = norm(xy(:,2)-yhat);


function stop = outfun(x, optimValues, state)
stop = false;
figure(201)
hold on;
plot(optimValues.funccount,optimValues.fval,'.')
drawnow