function krs_tof_switch_chunk_size(fNam,dataset,memType,chunkSize)

% Changes the chunk size of a dataset. If the argument chunkSize is empty,
% the chunk size will be the size of the whole dataset.
%
% fNam - file name
% dataset - dataset name
% memType - memory type for data, single if empty
% chunkSize - custom chunk size

if nargin == 4
    chunkSize = fliplr(chunkSize);
elseif nargin == 3
    chunkSize = [];
elseif nargin == 2
    memType = 'H5T_NATIVE_FLOAT';
    chunkSize = [];
end

datraw = hdf5diskmap(fNam,dataset);
s1 = size(datraw,1);
s2 = size(datraw,2);
if numel(datraw) > 20000 && s2 > 1
    memAvailable = krs_tof_get_free_mem;
    if memAvailable < s2 * s1 * 20
        disp('tof_switch_chunk_size: Not enough memory. Skipping.');
        return
    end
    data = zeros(s2,size(datraw,1));
    
    chunk = 20000;
    for j = 1:ceil(s1 / chunk);
        if (j-1)*chunk + chunk > s1
            data(1:s2,(j-1)*chunk+1:s1) = datraw(1:s2,(j-1)*chunk+1:s1);
        else
            data(1:s2,(j-1)*chunk+1:(j-1)*chunk+chunk) = datraw(1:s2,(j-1)*chunk+1:(j-1)*chunk+chunk);
        end
    end
    data = data';
else
    data = datraw(:,:);
    if s2>1
        data = data';
    end
end

if isempty(chunkSize)
    chunkSize = [s1 s2];
end

file = H5F.open(fNam,'H5F_ACC_RDWR','H5P_DEFAULT');
H5G.unlink(file,dataset);
H5F.close(file);

krs_tof_write_HDF(fNam,dataset,data,'mode','edit','offset',[1 1],'memType',memType,'chunk',chunkSize);